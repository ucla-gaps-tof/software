BINDIR = /usr/bin
HOMEDIR = /home/gaps
all:
	(cd drs4_ctrl; make all)
	(cd i2c_ctrl; make all)

install:
	(cd drs4_ctrl; make install)
	(cd i2c_ctrl; make install)
	 
clean:
	(cd drs4_ctrl; make clean)
	(cd i2c_ctrl; make clean)

tof-gfp-computer:
	(mkdir -p $(HOMEDIR)/bin)
	(cd daq_comm; make tof-gfp-computer)
	(cd net; cp server.sh $(HOMEDIR)/bin)

tof-gfp-rb:
	(mkdir -p $(HOMEDIR)/bin)
	(cd i2c_ctrl; make all)
	(cd i2c_ctrl; make install)
	(cd shell; cp init_script.sh $(HOMEDIR)/bin)
	(cd shell; cp run_GFP.sh $(HOMEDIR)/bin)
	(cd fpga_fw; cp prog_fpga.sh $(HOMEDIR)/bin)
	(cd drs4_ctrl; make all)
	(cd drs4_ctrl; make install)
	(cd daq_comm; make tof-gfp-rb)

event-counter:
	(mkdir -p $(HOMEDIR)/bin)
	(cd evt_ctr; make all)
	(cd evt_ctr; make install)
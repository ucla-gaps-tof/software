/**
 *
 */

#include <iostream>
#include <sstream>
#include <cstddef>
#include "libpb.h"

#include "EventProvider.h"
#include "serialization.h"

using namespace TOF;

int main() {

  // set up some example data
  std::vector<std::string> blobfiles;
  std::string blob_basename = "../resources/example-data/d20220308_230107_";
  for (int k=1;k<6;k++) {
    blobfiles.push_back(blob_basename + std::to_string(k) + ".dat");
  }
  EventProvider source = EventProvider(blobfiles);
  bool* has_calib = source.BoardHasCalibration();
  std::vector<std::vector<TOF::Calibrations_t>> cals 
    = source.GetCalibrations();

  bool hasNext;
  Event_t* event;
  int inevent[MAX_BRDS]; 

  // FIXME - just for the sake of the example, 
  // get nevents events only
  int nevents = 50;
  int sleep_time = 1;
  // set up zmq sockets
  zsock_t* sock = SetupZMQSocket("tcp://127.0.0.1:5555");
  uint32_t evtctr=0;
  std::cout << "[INFO} : Sending packets " << std::flush;
  REventPacket* r_event = new REventPacket();
  uint32_t trigger_cells[NCHN];
  for (int k=0;k<nevents;k++)
    {
        event = source.GetNextEvent(hasNext, inevent);
        FillREventPacket(event, cals, inevent,
                         has_calib, r_event, trigger_cells);
        SendREventPacket(r_event, sock);
        std::cout << "." <<std::flush;
        sleep(sleep_time);
    }
  delete r_event;
  std::cout << std::endl;
  zsock_destroy(&sock);

  return EXIT_SUCCESS;
}

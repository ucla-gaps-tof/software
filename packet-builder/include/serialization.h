#ifndef SERIALIZATION_H_INCLUDED
#define SERIALIZATION_H_INCLUDED

/**
 * Serializer/Deserializers
 *
 *
 *
 */

#include <iostream>
#include <sstream>
#include <bitset>
#include <vector>
#include <numeric>
#include <assert.h>

/***********************************************/

unsigned short decode_ushort(std::vector<unsigned char>& bytestream,
                             unsigned int start_pos=0);

/***********************************************/

void encode_ushort(unsigned short value, std::vector<unsigned char>& bytestream, unsigned int start_pos=0);

/***********************************************/

uint32_t decode_uint32(std::vector<unsigned char>& bytestream,
                       unsigned int start_pos=0);

/***********************************************/

void encode_uint32(uint32_t value, std::vector<unsigned char>& bytestream, unsigned int start_pos=0);

/***********************************************/

uint64_t decode_uint64(std::vector<unsigned char>& bytestream,
                       unsigned int start_pos=0);

/***********************************************/

void encode_uint64(uint64_t value, std::vector<unsigned char>& bytestream, unsigned int start_pos=0);

//
//
//template <typename T>
//T decode2(std::vector<unsigned char> buffer)
//{
//  unsigned int constexpr nbytes = sizeof(T);
//  unsigned int constexpr nbits  = 8*nbytes;
//  assert (buffer.size() == nbytes);
//  std::bitset<nbits> input;
//  for (unsigned int k=0; k<nbits; k++)
//    {input[k].set(1);}
//
//
//  for (unsigned int k=0;k<nbytes; k++) 
//    {
//      input |= (buffer[k] & 0xFF) << 8*k;
//    }
//  T result;
//
//}
//
//template <typename T>
//void decode(T& decoded, unsigned char* const buffer[])
//{
// std::vector<unsigned char> internal_buf(sizeof(T));
// for (size_t k=0;k<sizeof(T);k++)
//   {
//      internal_buf[k] = (*buffer[k] & 0xFF) << k*8;
//   }
// decoded = (T)(std::reduce(internal_buf.begin(), internal_buf.end(), [](unsigned char a, unsigned char b){return a | b;}));
// //std::vector<unsigned char> buffer(2);
// //unsigned short eventID = 17171; 
// //std::cout << std::bitset<16>(eventID) << std::endl;
// //buffer[1] = (eventID & 0xFF);
// //buffer[0] = (eventID >>8) & 0xFF;
// //std::cout << std::bitset<16>(eventID>>8) << std::endl;
// //std::cout << std::bitset<8>(buffer[1]) << std::endl;
// //std::cout << std::bitset<8>(buffer[0]) << std::endl;
// //unsigned short eventID_d = 0;
// ////eventID_d
// //std::cout << "--------------------" << std::endl; 
// //std::cout << std::bitset<16>(((buffer[0] & 0xFF) << 8) | buffer[1] ) << std::endl;
// //std::cout << std::bitset<16>(buffer[0]) << std::endl;
// //std::cout << std::bitset<16>(((buffer[0] & 0xFF) >> 8) & buffer[1]) << std::endl;
// //eventID_d = (unsigned short)(((buffer[0] & 0xFF) << 8) | buffer[1]);
// //std::cout << "TEST : "  <<eventID_d << std::endl;
//
//}
//
//template <typename Word>
//std::string write_word2(Word value )
//{
//std::stringstream ss;
//
//for (unsigned size = sizeof( Word ); size; --size, value >>= 8)
//    ss.put( static_cast <char> (value & 0xFF) );
////  outs.put( static_cast <char> (value & 0xFF) );
////return outs;
//std::string foo = ss.str();
//return foo;
//}
//
//template <typename Word>
//std::ostream& write_word( std::ostream& outs, Word value )
//{
//for (unsigned size = sizeof( Word ); size; --size, value >>= 8)
//  outs.put( static_cast <char> (value & 0xFF) );
//return outs;
//}
//
///********************************************/
//template <typename Word>
//Word read_word2(std::string input)
//{
//std::bitset<8> empty2byte{0x00};
//std::cout <<input << std::endl;
//
//Word value;
//
////value = value & empty2byte;
//for (unsigned int k=0;k< sizeof(Word); k++)
//  value = value & 0x00 << (8* k);
//
//std::stringstream ss(input);
//for (unsigned size = 0, value = 0; size < sizeof( Word ); ++size)
//  value |= ss.get() << (8 * size);
////return ins;
//return value;
//}
//
//template <typename Word>
//std::istream& read_word( std::istream& ins, Word& value )
//{
//for (unsigned size = 0, value = 0; size < sizeof( Word ); ++size)
//  value |= ins.get() << (8 * size);
//return ins;
//}
//
#endif

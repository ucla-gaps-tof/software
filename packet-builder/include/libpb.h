#ifndef LIBPB_H_INCLUDED
#define LIBPB_H_INCLUDED

#include <czmq.h>
#include <string>
#include <vector>

#include "REventPacket.h"
#include "EnvDPacket.h"
#include "WPacket.h"

#include "tof_dataclasses.h"

/**
 * \brief setup zmq socket
 *
 *
 */
zsock_t* SetupZMQSocket(std::string address);


void FillRPaddlePacket(short* adc_values,
                       uint32_t channel_id,
                       RPaddlePacket* r_paddle);

/**
 *
 *
 */
void FillREventPacket(TOF::Event_t*,
                      std::vector<std::vector<TOF::Calibrations_t>> cal,
                      int inevent[],
                      bool has_calibration[],
                      REventPacket*,
                      uint32_t trigger_cells[]);

/**
 *
 *
 *
 */
void SendREventPacket(REventPacket*, zsock_t* sock);

/**
 *
 *
 *
 */
void ReceiveREventPacket(REventPacket*, zsock_t* sock);


#endif

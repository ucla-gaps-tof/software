#ifndef WAVEFORMPROCESSING_H_INCLUDED
#define WAVEFORMPROCESSING_H_INCLUDED

// impedance 50 ohm
#define IMPEDANCE 50

namespace TOF {

/**
 *
 *
 */
float position_along_paddle(float t1, float t2, float length);

/**
 *
 *
 */
uint16_t peak_position(double wf[], float &val);

/**
 *
 *
 */
double calculate_charge(double wf[], double dx);




}

#endif

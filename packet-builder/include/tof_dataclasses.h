#ifndef DATACLASSES_H_INCLUDED
#define DATACLASSES_H_INCLUDED

#include <cstdio>

//-------------------------------------------------------------------------
// We use a #define statement, but NCHN must be 8 due to a number of
// hard-coded features below. It does make coding the for-loops easier
#define MAX_BRDS 6       // How many Readout Boards
#define NCHN 8           // How many ADC channels per board
#define NWORDS 1024      // How many Words per channel

// root compatibility
typedef double Double_t;

namespace TOF {

enum PADDLEEND {
    A = 10,
    B = 20
};

struct Header_t
{
  char           event_header[4];
  unsigned int   serial_number;
  unsigned short year;
  unsigned short month;
  unsigned short day;
  unsigned short hour;
  unsigned short minute;
  unsigned short second;
  unsigned short millisecond;
  unsigned short trigger_cell;

  float time[NCHN][1024];
};

//---------------------------------------------------------------------------
//  Waveform_t struct holds data for 1 channels
//---------------------------------------------------------------------------
struct Waveform_t
{
  char           chn_header[4];
  unsigned short chn[1024];
};

/******************************************/

struct Event_t
{
  unsigned short head; // Head of event marker
  unsigned short status;
  unsigned short len;
  unsigned short roi;
  unsigned long long dna;
  unsigned short fw_hash;
  unsigned short id;
  unsigned short ch_mask;
  unsigned long event_ctr;
  unsigned short dtap0;
  unsigned short dtap1;
  unsigned long long timestamp;
  unsigned short ch_head[NCHN];
  short ch_adc[NCHN][NWORDS];
  unsigned long ch_trail[NCHN];
  unsigned short stop_cell;
  unsigned long crc32;
  unsigned short tail; // End of event marker
};  

/******************************************/

struct Times_t
{
  int nbrds;
  unsigned long long time[MAX_BRDS][50]; // First 50 event times
  unsigned long evt_ctr[MAX_BRDS][50];   // First 50 event ctrs
  int first_evt[MAX_BRDS];               // First common event
  int first_evt_ID[MAX_BRDS];            // ID of first common event
  bool common;
};

/******************************************/

struct Calibrations_t
{
  Double_t vofs[NWORDS]; // voltage offset
  Double_t vdip[NWORDS]; // voltage "dip" (time-dependent correction)
  Double_t vinc[NWORDS]; // voltage increment (mV/ADC unit)
  Double_t tbin[NWORDS]; // cell width (ns)
};


/******************************************/

unsigned long long Decode64(unsigned long long tb[]);

/******************************************/

unsigned long long Decode48(unsigned long long tb[]);

/******************************************/

unsigned long Decode32(unsigned long tb[]);

/******************************************/

int ReadEvent(FILE *fp, struct Event_t *evt); 

/******************************************/

void VoltageCalibration(short traceIn[], Double_t traceOut[],
                        unsigned int tCell, struct Calibrations_t cal);

/******************************************/

void TimingCalibration(Double_t times[],
                       unsigned int tCell, struct Calibrations_t cal);

/******************************************/

void RemoveSpikes(Double_t wf[NCHN][1024], unsigned int tCell, int spikes[]);

/******************************************/

void BoardsInEvent(int status[], int evt_ID[], int inevent[], int nbrds);

/******************************************/

void FillFirstEvents(FILE *fp, int brd, struct Times_t *times ); 

/******************************************/

void FindFirstEvents(struct Times_t *times);

} //end namespace TOF
#endif


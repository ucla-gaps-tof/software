#ifndef EVENTPROVIDER_H_INCLUDED
#define EVENTPROVIDER_H_INCLUDED

#include "tof_dataclasses.h"

/**
 *
 *
 *
 */
class EventProvider{
  public:
    EventProvider(std::vector<std::string> blobfilenames);
    ~EventProvider();
    TOF::Event_t* GetNextEvent(bool &hasNextEvent, int inevent[]);

    bool* BoardHasCalibration();
    std::vector<std::vector<TOF::Calibrations_t>> GetCalibrations();
    bool EventIdSanityCheck(TOF::Event_t event[],
                            int inevent[],
                            int nbrds,
                            unsigned int&  eventid,
                            long& min_event,
                            long& max_event);



  private:

    void LoadCalibrations();
    struct TOF::Calibrations_t calibrations_[MAX_BRDS][NCHN];
    bool board_has_cal_[MAX_BRDS] = { 0 }; // init all to False

    std::vector<std::string> blob_filenames_;
    std::vector<FILE*> blob_files_;
    TOF::Times_t times_;
    TOF::Event_t event_[MAX_BRDS];
    // we need this since we have to 
    // "hold" one event during the 
    // readout loop, so that we can 
    // return it. 
    // There might be a smarter way
    TOF::Event_t event_buffer_[MAX_BRDS]; 
    int status_[MAX_BRDS];
};

#endif

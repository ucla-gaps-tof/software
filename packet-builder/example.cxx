/**
 *
 *
 */

#include <iostream>
#include <sstream>
#include <cstddef>
#include "libpb.h"

#include "EventProvider.h"
#include "serialization.h"

using namespace TOF;

int main() {
  // set up some example data
  std::vector<std::string> blobfiles;
  std::string blob_basename = "../resources/example-data/d20220308_230107_";
  for (int k=1;k<6;k++) {
    blobfiles.push_back(blob_basename + std::to_string(k) + ".dat");
  }
  EventProvider source = EventProvider(blobfiles);
  std::vector<std::vector<TOF::Calibrations_t>> cals 
    = source.GetCalibrations();



  bool hasNext;
  Event_t* event;
  int inevent[MAX_BRDS]; 


  size_t event_size;

  // FIXME - just for the sake of the example, 
  // get nevents events only
  int nevents = 10;
  for (int k=0;k<nevents;k++)
    {
        event = source.GetNextEvent(hasNext, inevent);
        std::cout << "---------------------------" << std::endl;
        for (size_t k=0;k<5;k++)
            {std::cout << "-- " << event[k].event_ctr << " " << event[k].timestamp << std::endl;}
    }

  // set up zmq sockets
  zsock_t* sock = SetupZMQSocket("tcp://127.0.0.1:5555");
  zsock_t* sock_sub = zsock_new(ZMQ_SUB);
  int rc = zsock_connect(sock_sub, "tcp://127.0.0.1:5555");
  zsock_set_subscribe (sock_sub, "");

  // create some packet as example
  REventPacket* r_event = new REventPacket();
  uint32_t trigger_cells[MAX_BRDS];
  bool* has_calib = source.BoardHasCalibration();
  FillREventPacket(event, cals, inevent,
                   has_calib, r_event, trigger_cells);
  //std::cout << *r_event << std::endl;


  std::vector<unsigned char> eventbuffer = r_event->serialize();
  REventPacket* receiver = new REventPacket;
  receiver->reset();
  receiver->deserialize(eventbuffer,0);
  //std::cout << *receiver << std::endl;

  // size checks
  std::cout << "size " << eventbuffer.size() << std::endl;
  std::cout << "claimed size " << r_event->calculate_length() << std::endl;
  std::cout << "object size " << sizeof(*r_event) << std::endl;
  unsigned char* eventbuffer_a = eventbuffer.data();
  std::cout << eventbuffer_a << std::endl;
  std::cout << "ZMQ test" << std::endl;
  std::cout << "-------------------------------" << std::endl;
  SendREventPacket(r_event, sock);
  sleep(1);
  ReceiveREventPacket(receiver, sock_sub);
  std::cout << "----RECEIVED----" << std::endl;
  std::cout << *receiver << std::endl;

  zsock_destroy(&sock);
  zsock_destroy(&sock_sub);

  delete[] event;

  bool test_decoders = false;
  if (test_decoders) {
    std::vector<unsigned char> buffer(2);
    unsigned short eventID = 17171; 
    
    std::vector<unsigned char> bytestream(4);
    uint32_t longtest = 4242424242; 
    std::vector<unsigned char> bytestream_l(8);
    uint64_t longlongtest = 424242424242; 

    encode_ushort(eventID, buffer,0);
    std::cout << " DE/ENC " <<decode_ushort(buffer,0) << std::endl;
    encode_uint32(longtest, bytestream,0);
    encode_uint64(longlongtest, bytestream_l, 0);
    for (auto k : buffer) {std::cout << std::bitset<8>(k) << std::endl;}
    std::cout << " DE/ENC " <<decode_uint32(bytestream,0) << std::endl;
    std::cout << " DE/ENC " <<decode_uint64(bytestream_l,0) << std::endl;
  }
  return 0;

}


## packet-builder 

### Packeging of tof data events

#### REventPacket.h
_reduced tof event format_

* provides serialization/deserialization
* variable size
* has vector of RPaddlePackets

#### RPaddlePacket.h
_reduced tof paddle data format_

* part of REventPacket
* fixed size
* serializat/deserialization

#### libpb.h
_assemble REventPackets and send them to the flight computer_

#### EventProvider.h
_read tof events from file_

* this can go away, it is just used for testing/debugging

### ISSUES


### How to build

Building with cmake





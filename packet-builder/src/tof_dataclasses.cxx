// memset
#include <string.h>
#include <math.h> //fabs
#include <climits>
#include <iostream>
#include "tof_dataclasses.h"

unsigned long long TOF::Decode64(unsigned long long tb[]) {
  unsigned long long val = (tb[1]<<56 | tb[0]<<48 | tb[3]<<40 | tb[2]<<32 |
                        tb[5]<<24 | tb[4]<<16 | tb[7]<<8 | tb[6]);
  return (val);
}

/******************************************/

unsigned long long TOF::Decode48(unsigned long long tb[]) {
  unsigned long long val = (tb[1]<<40 | tb[0]<<32 |
                        tb[3]<<24 | tb[2]<<16 | tb[5]<<8 | tb[4]);
  return (val);
}

/******************************************/

unsigned long TOF::Decode32(unsigned long tb[]) {
  unsigned long val = (tb[1]<<24 | tb[0]<<16 | tb[3]<<8 | tb[2]);
  return (val);
}

/******************************************/

int TOF::ReadEvent(FILE *fp, struct Event_t *evt) {

  unsigned short head = 0xaaaa; // Head of event marker
  unsigned short tail = 0x5555; // End of event marker
  
  // Some temporary words for checking data
  unsigned short temp_byte_top, temp_byte_bottom;
  unsigned short temp_short;
  unsigned long long tb[8]; // To read out the longer words (>4bytes)
  unsigned long sb[4]; // To read out the 4-bytes words)

  bool Print = true;
  
  // reads out 2 byte words one at a time from the stream, assigns
  // to temp_byte_top

  do { // Find the start of the next event
    fread(&temp_byte_top, 2, 1, fp);
    //printf("head = %x, %x--",temp_byte_top,head);printbinary(temp_byte_top,16);
    if (feof(fp)) 
      return (0);
  } while (temp_byte_top != head);
    
  //printf("Decoding event\n");
  evt->head = temp_byte_top;
  // Read in the status bytes
  fread(&evt->status, 2, 1, fp);
  // Read the packet length
  fread(&evt->len, 2, 1, fp);
  // Read the roi
  fread(&evt->roi, 2, 1, fp);
  // Read the dna, make sure we are only getting the proper 8 bits. 
  for (int i=0; i<8; i++) {fread(&tb[i], 1, 1, fp);tb[i] = tb[i] & 0xFF;}
  evt->dna = TOF::Decode64(tb);
  // Read the fw_hash
  fread(&evt->fw_hash, 2, 1, fp);
  // Read the id
  fread(&evt->id, 2, 1, fp);
  // Read the ch_mask
  fread(&evt->ch_mask, 2, 1, fp);
  // Read the event_ctr, make sure we are only getting the proper 8 bits. 
  for (int i=0; i<4; i++) {fread(&sb[i], 1, 1, fp); sb[i] = sb[i] & 0xFF;}
  evt->event_ctr = TOF::Decode32(sb);
  // Read the dtap0
  fread(&evt->dtap0, 2, 1, fp);
  // Read the dtap1
  fread(&evt->dtap1, 2, 1, fp);
  // Read the timestamp, make sure we are only getting the proper 8 bits. 
  for (int i=0; i<6; i++) {fread(&tb[i], 1, 1, fp);tb[i] = tb[i] & 0xFF;}
  evt->timestamp = TOF::Decode48(tb);
  
  // NOW WE READ IN THE ADC DATA
  for (int i=0; i<NCHN; i++) {
    // Read the channel header
    fread(&evt->ch_head[i], 2, 1, fp);
    // Read the channel data
    for (int j=0; j<NWORDS; j++) {
      fread(&temp_short, 2, 1, fp);
      evt->ch_adc[i][j] = temp_short & 0x3FFF; // Only 14-bit ADCs
    }
    // Read the channel trailer
    for (int k=0; k<4; k++) {fread(&sb[k], 1, 1, fp);sb[k] = sb[k] & 0xFF;}
    evt->ch_trail[i] = TOF::Decode32(sb);
  }          
  // Read the stop_cell
  fread(&evt->stop_cell, 2, 1, fp);
  // Read the crc32
  for (int i=0; i<4; i++) {fread(&sb[i], 1, 1, fp);sb[i] = sb[i] & 0xFF;}
  evt->crc32 = TOF::Decode32(sb);
  
  // read end bytes into temp_byte_bottom
  fread(&temp_byte_bottom, 2, 1, fp);
  if (Print) {
    //printf("status: %d--", evt->status); printbinary(evt->status,16);
    //printf("Packet Length = %d--", evt->len); printbinary(evt->len, 16); 
    //printf("ROI = %d--", evt->roi); printbinary(evt->roi, 16); 
    //printf("DNA = %llu--", evt->dna); printbinary(evt->dna, 64); 
    //printf("FW_hash = %d--",evt->fw_hash); printbinary(evt->fw_hash,16); 
    //printf("ID = %d--", evt->id); printbinary(evt->id, 16); 
    //printf("CH_MASK = %d--",evt->ch_mask); printbinary(evt->ch_mask,16); 
    //printf("EVT_CTR = %ld ",evt->event_ctr); 
    //printf("DTAP0 = %d--", evt->dtap0); printbinary(evt->dtap0, 16); 
    //printf("DTAP1 = %d--", evt->dtap1); printbinary(evt->dtap1, 16); 
    //printf("TIMESTAMP = %llu--", evt->timestamp);
    //printf("%llu\n", evt->timestamp);
    //printbinary(evt->timestamp, 48); 
    for (int i=0; i<NCHN; i++) {
      //printf("ch_head[%i] = %d--", i, evt->ch_head[i]);
      //printbinary(evt->ch_head[i], 16); 
      for (int j=0; j<NWORDS; j++) 
        if (j%600 == 0) {
          //printf("Ch %d: ADC %d = %d\n",i,j,evt->ch_adc[i][j]);
          //printf("ch_trail[%i] = %ld--", i, evt->ch_trail[i]);
          //printbinary(evt->ch_trail[i], 32); 
        }
    }
    //printf("STOP_CELL = %d--", evt->stop_cell);
    //printbinary(evt->stop_cell, 16); 
    //printf("CRC32 = %ld--", evt->crc32); printbinary(evt->crc32, 32); 
  }
  if (temp_byte_bottom==tail) { // Verify the event ended properly
    evt->tail = temp_byte_bottom;
    return (1);
  } else {
    return (0);
  }
}

//---------------------------------------------------------------------------
// VoltageCalibration :: translate ADC units into voltage measurement
//---------------------------------------------------------------------------
void TOF::VoltageCalibration(short traceIn[], Double_t traceOut[],
                    unsigned int tCell, struct Calibrations_t cal)
{
  for (unsigned short i = 0; i < 1024; i++) {
    traceOut[i] = (Double_t) traceIn[i];
    traceOut[i] -= cal.vofs[(i+tCell)%1024];
    traceOut[i] -= cal.vdip[i];
    traceOut[i] *= cal.vinc[(i+tCell)%1024];
  }
}

//---------------------------------------------------------------------------
// TimingCalibration :: determine calibrated readout time for each cell
//---------------------------------------------------------------------------
void TOF::TimingCalibration(Double_t times[],
                    unsigned int tCell, struct Calibrations_t cal)
{
  times[0] = 0.0;
  for (unsigned short i = 1; i < 1024; i++) {
    times[i] = times[i-1] + cal.tbin[(i-1+tCell)%1024];
  }
}

//---------------------------------------------------------------------------
// RemoveSpikes :: modified spike removal routine from drs-5.0.6/src/Osci.cpp
//---------------------------------------------------------------------------
void TOF::RemoveSpikes(Double_t wf[NCHN][1024], unsigned int tCell, int spikes[])
{
  int i, j, k, l;
  double x, y;
  int sp[8][10];
  int rsp[10];
  int n_sp[8];
  int n_rsp;
  int nNeighbor, nSymmetric;
  int nChn = NCHN;
  Double_t filter, dfilter;

  memset(sp, 0, sizeof(sp));
  memset(rsp, 0, sizeof(rsp));
  memset(n_sp, 0, sizeof(n_sp));
  n_rsp = 0;

  /* set rsp to -1 */
  for (i = 0; i < 10; i++)
  {
    rsp[i] = -1;
  }
  /* find spikes with special high-pass filters */
  for (j = 0; j < 1024; j++)
  {
    for (i = 0; i < nChn; i++)
    {
      filter = -wf[i][j] + wf[i][(j + 1) % 1024] + wf[i][(j + 2) % 1024] - wf[i][(j + 3) % 1024];
      dfilter = filter + 2 * wf[i][(j + 3) % 1024] + wf[i][(j + 4) % 1024] - wf[i][(j + 5) % 1024];
      if (filter > 20 && filter < 100)
      {
        if (n_sp[i] < 10)   // record maximum of 10 spikes
        {
          sp[i][n_sp[i]] = (j + 1) % 1024;
          n_sp[i]++;
        }
        else                // too many spikes -> something wrong
        {
          return;
        }
        // filter condition avoids mistaking pulse for spike sometimes
      }
      else if (dfilter > 40 && dfilter < 100 && filter > 10)
      {
        if (n_sp[i] < 9)   // record maximum of 10 spikes
        {
          sp[i][n_sp[i]] = (j + 1) % 1024;
          sp[i][n_sp[i] + 1] = (j + 3) % 1024;
          n_sp[i] += 2;
        }
        else                // too many spikes -> something wrong
        {
          return;
        }
      }
    }
  }

  /* find spikes at cell #0 and #1023
  for (i = 0; i < nChn; i++) {
    if (wf[i][0] + wf[i][1] - 2*wf[i][2] > 20) {
      if (n_sp[i] < 10) {
        sp[i][n_sp[i]] = 0;
        n_sp[i]++;
      }
    }
    if (-2*wf[i][1021] + wf[i][1022] + wf[i][1023] > 20) {
      if (n_sp[i] < 10) {
        sp[i][n_sp[i]] = 1022;
        n_sp[i]++;
      }
    }
  }
  */

  /* go through all spikes and look for neighbors */
  for (i = 0; i < nChn; i++)
  {
    for (j = 0; j < n_sp[i]; j++)
    {
      nSymmetric = 0;
      nNeighbor = 0;
      /* check if this spike has a symmetric partner in any channel */
      for (k = 0; k < nChn; k++)
      {
        for (l = 0; l < n_sp[k]; l++)
          if ((sp[i][j] + sp[k][l] - 2 * tCell) % 1024 == 1022)
          {
            nSymmetric++;
            break;
          }
      }
      /* check if this spike has same spike is in any other channels */
      for (k = 0; k < nChn; k++)
        if (i != k)
        {
          for (l = 0; l < n_sp[k]; l++)
            if (sp[i][j] == sp[k][l])
            {
              nNeighbor++;
              break;
            }
        }
      /* if at least two matching spikes, treat this as a real spike */
      if (nNeighbor >= 2)
      {
        for (k = 0; k < n_rsp; k++)
          if (rsp[k] == sp[i][j]) // ignore repeats
            break;
        if (n_rsp < 10 && k == n_rsp)
        {
          rsp[n_rsp] = sp[i][j];
          n_rsp++;
        }
      }
    }
  }

  /* recognize spikes if at least one channel has it */
  for (k = 0; k < n_rsp; k++)
  {
    spikes[k] = rsp[k];
    for (i = 0; i < nChn; i++)
    {
      if (k < n_rsp && fabs(rsp[k] - rsp[k + 1] % 1024) == 2)
      {
        /* remove double spike */
        j = rsp[k] > rsp[k + 1] ? rsp[k + 1] : rsp[k];
        x = wf[i][(j - 1) % 1024];
        y = wf[i][(j + 4) % 1024];
        if (fabs(x - y) < 15)
        {
          wf[i][j % 1024] = x + 1 * (y - x) / 5;
          wf[i][(j + 1) % 1024] = x + 2 * (y - x) / 5;
          wf[i][(j + 2) % 1024] = x + 3 * (y - x) / 5;
          wf[i][(j + 3) % 1024] = x + 4 * (y - x) / 5;
        }
        else
        {
          wf[i][j % 1024] -= 14.8f;
          wf[i][(j + 1) % 1024] -= 14.8f;
          wf[i][(j + 2) % 1024] -= 14.8f;
          wf[i][(j + 3) % 1024] -= 14.8f;
        }
      }
      else
      {
        /* remove single spike */
        x = wf[i][(rsp[k] - 1) % 1024];
        y = wf[i][(rsp[k] + 2) % 1024];
        if (fabs(x - y) < 15)
        {
          wf[i][rsp[k]] = x + 1 * (y - x) / 3;
          wf[i][(rsp[k] + 1) % 1024] = x + 2 * (y - x) / 3;
        }
        else
        {
          wf[i][rsp[k]] -= 14.8f;
          wf[i][(rsp[k] + 1) % 1024] -= 14.8f;
        }
      }
    }
    if (k < n_rsp && fabs(rsp[k] - rsp[k + 1] % 1024) == 2)
      k++; // skip second half of double spike
  }
}

/******************************************/


void TOF::BoardsInEvent(int status[], int evt_ID[], int inevent[], int nbrds) {
  int low_evt = INT_MAX;
  int low_brd=-1;

  // First, find the lowest time for all boards
  for (int i=0; i<nbrds; i++) {
    if (status[i]==1) {
      if (evt_ID[i] < low_evt) {
        low_evt = evt_ID[i];
        low_brd = i;
      }
    }
  }

  // Now find all boards with a matching time.
  for (int i=0; i<nbrds; i++) {
    if (status[i]==1) {
      if ( evt_ID[i] == evt_ID[low_brd] )
        inevent[i] = 1;
      else
        inevent[i] = 0;
    }
  }
}

void TOF::FillFirstEvents(FILE *fp, int brd, struct Times_t *times ) { 
  struct TOF::Event_t temp;

  temp.event_ctr = 1000;
  for (int i=0; i<50; i++) {
    int stat = TOF::ReadEvent(fp, &temp); // Get timestamp of first event
    times->time[brd][i]   = temp.timestamp;
    times->evt_ctr[brd][i] = temp.event_ctr;
  }
}

/******************************************/

void TOF::FindFirstEvents(struct TOF::Times_t *times) {

  unsigned long long curr[MAX_BRDS];
  int ctr = 1;

  times->common = false; // No common start event flag

  // The code in this for-loop uses time differences to find common events
  /*for (int i=1; i<50; i++) { // Loop through the times of the first board
    curr[0] = times->time[0][i] - times->time[0][i-1];
    for (int j=1; j<times->nbrds; j++) { // Loop through the other boards
      for (int k=1; k<50; k++) { // and each event for the board
        curr[j] = times->time[j][k] - times->time[j][k-1];
        if ( abs((int)(curr[0]-curr[j])) < 50 ) {
          ctr++; // found time that matches.
          //printf("ctr = %d: i=%d, j=%d, k=%d\n", ctr, i, j, k);
          times->first_evt[j] = k-1; 
          k=50;
        }
      }
      }*/

  // The code in this for-loop uses the evt_ctr to find common events
  for (int i=1; i<50; i++) { // Loop through the times of the first board
    for (int j=1; j<times->nbrds; j++) { // Loop through the other boards
      for (int k=1; k<50; k++) { // and each event for the board
  if ( times->evt_ctr[0][i] == times->evt_ctr[j][k] ) {
          ctr++; // found time that matches.
          //printf("ctr = %d: i=%d, j=%d, k=%d\n", ctr, i, j, k);
          times->first_evt[j] = k;
          k=50;
        }
      }
    }

    if (ctr == times->nbrds) { // Found our common event
      times->common = true;
      //times->first_evt[0] = i-1; // If using times
      times->first_evt[0] = i;   // If using evt_ctr
      // Stuff to print/debug algorithm.
      /*int t0 = times->first_evt[0];
      int t1 = times->first_evt[1];
      int t2 = times->first_evt[2];
      printf("First: %d  %d  %d\n", times->first_evt[0],
             times->first_evt[1], times->first_evt[2]);
      printf("Tdiff: %llu  %llu  %llu\n",
             times->time[0][t0+1] - times->time[0][t0],
             times->time[1][t1+1] - times->time[1][t1],
             times->time[2][t2+1] - times->time[2][t2]); */
      for(int m=0;m<times->nbrds;m++) printf("%d(%d) ",m,times->first_evt[m]);
      printf("\n");
      return;
    } else {
      ctr = 1; // Start with the next time of the first board
    }
  }
  // Did not find a common event. Reset all first_evts to -1 and return
  for (int j=0; j<times->nbrds; j++)
    times->first_evt[j] = -1;
  return;
}




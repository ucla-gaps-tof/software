#include <iostream>
#include <vector>
#include <climits>
#include <fstream>
#include "EventProvider.h"




EventProvider::EventProvider(std::vector<std::string> blobfilenames)
{
  blob_filenames_ = blobfilenames;
  std::cout << "[INFO] - got input blobfiles..." << std::endl;
  for (auto fname : blob_filenames_) std::cout << "-- " << fname << std::endl;
  // initailzie fields
  for (size_t k=0;k<MAX_BRDS;k++) status_[k] = 0;

  for (auto k : blob_filenames_)
    {
      std::cout << "[DEBUG] - Opening file " << k << std::endl;
      blob_files_.push_back(fopen(k.c_str(), "rb"));
    }
  uint16_t nboards = blob_files_.size();
  // files have to be closed and reopened
  for (int k=0;k<nboards;k++) {
    TOF::FillFirstEvents(blob_files_[k], k, &times_);
    fclose(blob_files_[k]);
  }
  for (size_t k=0;k<blob_filenames_.size(); k++)
    {
      std::cout << "[DEBUG] - Re-pening file " << blob_filenames_[k] << std::endl;
      blob_files_[k] = fopen(blob_filenames_[k].c_str(), "rb");
    }
  times_.nbrds = nboards;
  TOF::FindFirstEvents(&times_);
  //std::cout<< "oooooooooooooooooooo" << std::endl;
  //for (size_t k=0;k<nboards;k++)
  //  {
  //    std::cout << times_.first_evt[k] << std::endl;
  //    std::cout << times_.first_evt_ID[k] << std::endl;
  //    std::cout << times_.evt_ctr[k][0] << std::endl;
  //    std::cout << times_.time[k][0] << std::endl;
  //    std::cout << times_.common << std::endl;
  //std::cout<< "--------" << std::endl;

  //  }
  //std::cout<< "oooooooooooooooooooo" << std::endl;

  int orig_time[MAX_BRDS];

  for (size_t k=0;k<MAX_BRDS;k++) orig_time[k] = 0;
  for (int k=0;k<nboards;k++) {
    int e_ctr = 0;
    do { // Read to the first common event from each file
      status_[k] = TOF::ReadEvent(blob_files_[k], &event_[k]);
      //std::cout << "do-while " << event_[k].event_ctr << " " << e_ctr << " " << times_.first_evt[k] << std::endl;
      e_ctr++;
    } while (e_ctr <= times_.first_evt[k]);
    //} while (e_ctr <= firstEvt[k]);
    orig_time[k] = event_[k].timestamp;          // Record the start time
    times_.first_evt_ID[k] = event_[k].event_ctr; // Record the evt_ID
  }
  LoadCalibrations();

  //std::cout << "[INFO] : Throw first " << e_ctr << " events away" << std::endl;
}

EventProvider::~EventProvider()
{
  for (auto &k : blob_files_)
    {fclose(k);}
}

/*******************************************************/

TOF::Event_t* EventProvider::GetNextEvent(bool &hasNextEvent, int inevent[]){

  //struct TOF::Event_t* event_next = (TOF::Event_t*) malloc(MAX_BRDS*sizeof(TOF::Event_t));

  // remember, in the constructor we were already reading the
  // very first event, so we have one in the buffer already

  //int inevent[MAX_BRDS];
  int curr_event[MAX_BRDS];
  for (size_t k=0;k<MAX_BRDS;k++) inevent[k]    = 0;
  for (size_t k=0;k<MAX_BRDS;k++) curr_event[k] = 0;
   
  //std::cout << "[DEBUG] "<<blob_files_.size() << std::endl; 
  // copy the event in the buffer, so we can return it
  for (size_t k=0;k<times_.nbrds;k++)
    { event_buffer_[k] = event_[k];}
  for (unsigned int k=0; k<times_.nbrds ; k++)
    {
      curr_event[k] = event_[k].event_ctr - times_.first_evt_ID[k];
      //std::cout << "[DEBUG] evt ctr  " << times_.first_evt_ID[k] << std::endl;
      //std::cout << "[DEBUG] first ev " << event_[k].event_ctr << std::endl;
      //std::cout << "[DEBUG] curr evt " << curr_event[k] << std::endl;
    }
  TOF::BoardsInEvent(status_, curr_event, inevent, times_.nbrds);
  //std::cout << "---------------------------" << std::endl;
  for (unsigned int k=0; k<blob_files_.size() ; k++)
    {
      //std::cout << "[DEBUG] inevent " << inevent[k] << std::endl;
      
      // at the very last, read the next event if there is one more      
      //std::cout<<"[DEBUG] Reading events.."<< std::endl;
      if (inevent[k]==1 && status_[k] != -1 ) { // status=-1 -> EOF
          status_[k] = TOF::ReadEvent(blob_files_[k], &event_[k]);
      }
      //std::cout<<"[DEBUG] .. done"<< std::endl;
    }  
  uint32_t event_num;
  long min_event_id = INT_MAX;
  long max_event_id = -INT_MAX;
  //std::cout << "..done.." << std::endl;
  bool success = EventIdSanityCheck(event_buffer_, inevent , times_.nbrds, event_num, min_event_id, max_event_id);
  if (!success)
    {std::cerr << "[ERROR] - different event ids " << min_event_id << " " << max_event_id << std::endl;}
  //std:: cout << "[DEBUG] - read events, reports status  < ";
  //for (auto s : status_)
  //  {std::cout << " " << s;}
  //std::cout << " >" << std::endl;
  //std:: cout << "[DEBUG] - boards participating in event  < ";
  //for (auto s : inevent)
  //  {std::cout << " " << s;}
  //std::cout << " >" << std::endl;
  return event_buffer_;
}

bool EventProvider::EventIdSanityCheck(TOF::Event_t event[],
                                       int inevent[],
                                       int nbrds,
                                       unsigned int&  eventid,
                                       long& min_event,
                                       long& max_event)
{
  std::vector<long> allBoardEventId;
  //std::cout << "nboard "  << nbrds << std::endl;
  //std::cout << eventid << std::endl;
  //std::cout << min_event << std::endl;
  //std::cout << max_event << std::endl;
  for (int k=0; k<nbrds; k++) 
    {    
      //std::cout<< inevent[k] << std::endl;
      //std::cout << event[k].event_ctr << std::endl;
      //if (event[k].event_ctr == 0) continue; // 0 is typically for boards which are not present in the blob
      if (inevent[k] == 0) continue;// this board did not participate in the event id
      if (event[k].event_ctr < min_event) min_event = event[k].event_ctr;
      if (event[k].event_ctr > max_event) max_event = event[k].event_ctr;
      allBoardEventId.push_back(event[k].event_ctr);
    }    
    if ((allBoardEventId).size() == 0) 
        {}//std::cout << "vector empty" << std::endl;}
  if ( std::equal(allBoardEventId.begin() + 1, allBoardEventId.end(), allBoardEventId.begin()) )
     {    
      eventid = allBoardEventId[0];
      return true;
     }    
  else 
     {    
      std::cerr << "-----------------" << std::endl;
      for (auto k : allBoardEventId) { std::cerr << " -- " << k;}
      std::cerr << std::endl <<  "[ERROR] Event id not the same accross the boards!" << std::endl;
      eventid = 0; 
      return false;
     }    
}

void EventProvider::LoadCalibrations()
{

   // find relevant boards
  // TODO: change this to use event[k].id, eventually
  unsigned int boardnums[MAX_BRDS]; // maps event board number to RB#
  unsigned long long boarddnas[MAX_BRDS] = { 
    77380906573213780, // 1
    9609908518406236,  // 2
    78985381379328092, // 3
    24942185850882132, // 4
    25003068095826012, // 5
    32831594097035348  // 6
  };  
  for (int k=0; k<times_.nbrds; k++)
    for (int i=0;i<MAX_BRDS;i++)
      if (event_[k].dna == boarddnas[i])
        boardnums[k] = i;

  for (int k=0; k<times_.nbrds; k++) {
    std::string calfilename = "../resources/calibrations/rb" + std::to_string(boardnums[k]+1) + "_cal.txt";
    std::fstream calfile(calfilename.c_str(), std::ios_base::in);
    if (calfile.fail()) {
      std::cerr << "[ERROR] Can't open " << calfilename << " - not calibrating" << std::endl;
      continue;
    }
    for (int i=0; i<NCHN; i++) {
      for (int j=0; j<NWORDS; j++)
        calfile >> calibrations_[k][i].vofs[j];
      for (int j=0; j<NWORDS; j++)
        calfile >> calibrations_[k][i].vdip[j];
      for (int j=0; j<NWORDS; j++)
        calfile >> calibrations_[k][i].vinc[j];
      for (int j=0; j<NWORDS; j++)
        calfile >> calibrations_[k][i].tbin[j];
    }
    board_has_cal_[k] = true;
  }

}

bool* EventProvider::BoardHasCalibration()
{
  return board_has_cal_;
}

std::vector<std::vector<TOF::Calibrations_t>> EventProvider::GetCalibrations()
{
  std::vector<std::vector<TOF::Calibrations_t>> cals(MAX_BRDS);
  for (size_t i=0;i<MAX_BRDS;i++)
    {
      cals[i]  = std::vector<TOF::Calibrations_t>(NCHN);
      for (size_t j=0;j<NCHN;j++)
        {
            cals[i][j] = calibrations_[i][j];
        }
    }
  return cals;
}


#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <pybind11/functional.h>
#include <pybind11/chrono.h>

#include "REventPacket.h"
#include "RPaddlePacket.h"

int static_helper(RPaddlePacket& pp){
    return RPaddlePacket::calculate_length();
}

namespace py = pybind11;
PYBIND11_MODULE(_pyPacketBuilder, m) {
    m.doc() = "Pybindings for Tof packet builder";
    py::class_<REventPacket>(m, "REventPacket")
        .def(py::init())
        .def("serialize",         &REventPacket::serialize)
        .def("deserialize",       &REventPacket::deserialize)
        .def("calculate_length",  &REventPacket::calculate_length)
        .def("is_broken",         &REventPacket::is_broken)
        .def("reset",             &REventPacket::reset)
        .def("add_paddle_packet", &REventPacket::add_paddle_packet)
        .def("__repr__",          [](const REventPacket &ev) {
                                  return "<REventPacket : " + ev.to_string() + "'>";
                                  }) 

    ;
    py::class_<RPaddlePacket>(m, "RPaddlePacket")
        .def(py::init())
        .def("serialize",         &RPaddlePacket::serialize)
        .def("deserialize",       &RPaddlePacket::deserialize)
        .def("calculate_length",  &static_helper)
        .def("reset",             &RPaddlePacket::reset)
        .def("get_paddle_id",     &RPaddlePacket::get_paddle_id) 
        .def("get_time_a",        &RPaddlePacket::get_time_a) 
        .def("get_time_b",        &RPaddlePacket::get_time_b) 
        .def("get_peak_a",        &RPaddlePacket::get_peak_a) 
        .def("get_peak_b",        &RPaddlePacket::get_peak_b) 
        .def("get_charge_a",      &RPaddlePacket::get_charge_a) 
        .def("get_charge_b",      &RPaddlePacket::get_charge_b) 
        .def("get_charge_min_i",  &RPaddlePacket::get_charge_min_i) 
        .def("get_x_pos",         &RPaddlePacket::get_x_pos) 
        .def("get_t_avg",         &RPaddlePacket::get_t_avg) 

        // atributes
        .def_readwrite("paddle_id" ,    &RPaddlePacket::paddle_id)
        .def_readwrite("time_a" ,       &RPaddlePacket::time_a)
        .def_readwrite("time_b" ,       &RPaddlePacket::time_b)
        .def_readwrite("charge_a" ,     &RPaddlePacket::charge_a)
        .def_readwrite("charge_b" ,     &RPaddlePacket::charge_b)
        .def_readwrite("charge_min_i" , &RPaddlePacket::charge_min_i)
        .def_readwrite("x_pos" ,        &RPaddlePacket::x_pos)
        .def_readwrite("t_avg" ,        &RPaddlePacket::t_average)

        .def("__repr__",          [](const RPaddlePacket &pp) {
                                  return "<RPaddlePacket : " + pp.to_string() + "'>";
                                  }) 

    ;

}

#include <iostream>

#include "REventPacket.h"
#include "EnvDPacket.h"
#include "WPacket.h"
#include "TOFChannelMapping.h"
#include "WaveformProcessing.h"
#include "EventProvider.h"
#include "libpb.h"

/***********************************************/


zsock_t* SetupZMQSocket(std::string address)
{
  zsock_t* sock = zsock_new(ZMQ_PUB);
  int rc = zsock_bind(sock, address.c_str());
  // taken from example, meaning unknown
  assert (rc == 5555);
  return sock;

}

/***********************************************/

void FillRPaddlePacket(short* adc_values,
                       uint32_t channel_id,
                       RPaddlePacket* r_paddle)
{

}

/***********************************************/

void FillREventPacket(TOF::Event_t* event,
                      std::vector<std::vector<TOF::Calibrations_t>> cal,
                      int inevent[],
                      bool has_calibration[],
                      REventPacket* packet,
                      uint32_t trigger_cells[])
{
   // FIXEM
   size_t nboards = 0;
   for (size_t k=0;k<MAX_BRDS;k++)
    { nboards += inevent[k];}
   std::cout << "[DEBUG] We see " << nboards << " readout boards." << std::endl; 
   packet->reset();
   // we need to be sure here, that all the MAX_NBOARDS
   // in event have the same id -> This is done by
   // EventProvider::EventIdSanityCheck
   packet->event_ctr     = event[0].event_ctr;
   packet->utc_timestamp = event[0].timestamp;
   // currently, we create a paddleinfo for every hit channel?

   size_t npaddles = (NCHN*nboards)/2;
   std::vector<RPaddlePacket*> paddle_packets(npaddles);
   
   // keep track of which paddles ids have been seen
   // FIXME - this only works if paddle ids start with 0
   std::vector<bool> paddle_seen(npaddles,false);
   // each paddle packet has 2 channels 
   uint16_t paddle_id    = 0;
   uint8_t  paddle_end   = 0;  
   // the readout board number the paddle channel is
   // attached to
   uint8_t  brd          = 0;

   bool is_A_channel   = 0;
   float peak_val      = 0;
   uint16_t ppos       = 0;
   uint32_t charge     = 0;
   uint16_t nhit_outer = 0;

   // WARNING FIXME
   float paddle_length = 163;

   double t[MAX_BRDS * NCHN][NWORDS];
   double chn[MAX_BRDS * NCHN][NWORDS] {0.0}; // fill with 0s
   // we go through all channel sequentially and 
   // look up to which readout board and paddle
   // they are referring to.
   std::cout << "[TRACE] expecting " << nboards*NCHN << " channels" << std::endl;

   // do the calibration
   for (brd=0;brd<MAX_BRDS;brd++) 
   {
     if (has_calibration[brd]) {
       for (int i=0; i<NCHN; i++) {
         TOF::VoltageCalibration(event[brd].ch_adc[i],chn[brd*NCHN+i],trigger_cells[brd],cal[brd][i]);
         TOF::TimingCalibration(t[brd*NCHN+i],trigger_cells[brd],cal[brd][i]);
       }
     }
   } 


   for (size_t global_channel=0; global_channel<nboards*NCHN; global_channel++)
    {
      std::cout << "[TRACE] getting data for ch " << global_channel << std::endl;
      // identify where the channel is connected to
      brd          = TOF::CHANNELRBMAP.at(global_channel);
      paddle_end   = TOF::CHANNELABMAP.at(global_channel);
      paddle_id    = TOF::CHANNELMAP.at(global_channel);
      is_A_channel = (paddle_end == 0);

      if (!paddle_seen[paddle_id])
        {
          std::cout << "[TRACE] adding paddle packet for paddle id " << paddle_id << std::endl; 
          paddle_packets[paddle_id] = new RPaddlePacket();
          paddle_seen[paddle_id] = true;
        }

      if (TOF::TOFSUBSYSTEMMAP.at(paddle_id) == 1) nhit_outer +=1;
      // check if calibration is available for the board
      // the channel is connected to 
      if (has_calibration[brd]) 
        {
           std::cout << "[TRACE] found calibration data for board " << brd << std::endl;
           trigger_cells[brd] = event[brd].stop_cell;
           std::cout << "[TRACE] performing calibration!" << std::endl;
           for (int i=0; i<NCHN; i++)
             {
               TOF::VoltageCalibration(event[brd].ch_adc[i],chn[brd*NCHN+i],trigger_cells[brd],cal[brd][i]);
               TOF::TimingCalibration(t[brd*NCHN+i],trigger_cells[brd],cal[brd][i]);
             }  
           std::cout << "[TRACE] done!" << std::endl;
        }
      else
        {std::cout << "[WARNING] calibration data for board " << brd << " missing!" << std::endl;}

      double dt = t[1] - t[0];
      std::cout << " foo" ;
      ppos = TOF::peak_position(chn[global_channel], peak_val); 
      charge
           = TOF::calculate_charge(chn[global_channel], dt);
      std::cout << "[DEBUG] wf : " << ppos << " " << charge << " " << peak_val << " " << paddle_id <<  std::endl;

      paddle_packets[paddle_id]->paddle_id = (uint8_t)paddle_id;
      if (is_A_channel)
          { 
            paddle_packets[paddle_id]->set_peak_a(peak_val); 
            paddle_packets[paddle_id]->set_time_a(ppos);
            paddle_packets[paddle_id]->set_charge_a(charge);
          } 
      else
          {
             paddle_packets[paddle_id]->set_peak_b(peak_val); 
             paddle_packets[paddle_id]->set_time_b(ppos);
             paddle_packets[paddle_id]->set_charge_b(charge);
          } 
      std::cout << " foo" ;
    }
      std::cout << " foo" ;
   
   uint16_t nhit_inner = paddle_packets.size() - nhit_outer;
   packet->nhit_outer_tof = nhit_outer;
   packet->nhit_inner_tof = nhit_inner; 
   for (auto const &p : paddle_packets)
     {  
        packet->add_paddle_packet(*p);
     }
}

/***********************************************/

void SendREventPacket(REventPacket* event, zsock_t* sock)
{
  std::vector<unsigned char> eventbuffer = event->serialize();
  unsigned char* eventbuffer_a = eventbuffer.data();
  zframe_t *frame = zframe_new(eventbuffer_a,eventbuffer.size());
  zframe_send(&frame,sock,0);
}

/***********************************************/

void ReceiveREventPacket(REventPacket* event, zsock_t* sock)
{
  zframe_t* frame_r = zframe_recv(sock);
  size_t b_size = zframe_size(frame_r);
  std::cout << "[INVO] Received buffer of size " << b_size;
  unsigned char* buffer_r = zframe_data(frame_r);
  event->reset();
  std::vector<unsigned char> buffer_v(buffer_r, buffer_r+b_size);
  event->deserialize(buffer_v, 0); 
}



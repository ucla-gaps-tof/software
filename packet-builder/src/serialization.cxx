#include <serialization.h>

unsigned short decode_ushort(std::vector<unsigned char>& bytestream,
                             unsigned int start_pos)
{
  unsigned short value= (unsigned short)(((bytestream[start_pos+0] & 0xFF) << 8) | bytestream[start_pos+1]);
  return value;
}

/***********************************************/

void encode_ushort(unsigned short value,
                   std::vector<unsigned char>& bytestream,
                   unsigned int start_pos)
{
  //std::vector<unsigned char> buffer(2);
  bytestream[start_pos +0] = (value >> 8) & 0xFF;
  bytestream[start_pos + 1] = value & 0xFF;
}

/***********************************************/

uint32_t decode_uint32(std::vector<unsigned char>& bytestream,
                       unsigned int start_pos)
{
  uint32_t value = (uint32_t)(
         ((bytestream[start_pos+0] & 0xFF) << 24)
      |  ((bytestream[start_pos+1] & 0xFF) << 16)
      |  ((bytestream[start_pos+2] & 0xFF) << 8)
      |  (bytestream[start_pos+3]));
  return value;
}

/***********************************************/

void encode_uint32(uint32_t value, 
                   std::vector<unsigned char>& bytestream, 
                   unsigned int start_pos)
{
  bytestream[start_pos + 0] = (value >> 24) & 0xFF;
  bytestream[start_pos + 1] = (value >> 16) & 0xFF;
  bytestream[start_pos + 2] = (value >> 8) & 0xFF;
  bytestream[start_pos + 3] = value & 0xFF;
}

/***********************************************/

uint64_t decode_uint64(std::vector<unsigned char>& bytestream,
                       unsigned int start_pos)
{
  uint64_t buffer64 = 0x0000000000000000;

  //unsigned long long value = (unsigned long long)(
  uint64_t buffer =  
         ((bytestream[start_pos+0] & 0xFF | buffer64) << 56)
      |  ((bytestream[start_pos+1] & 0xFF | buffer64) << 48)
      |  ((bytestream[start_pos+2] & 0xFF | buffer64) << 40)
      |  ((bytestream[start_pos+3] & 0xFF | buffer64) << 32)
      |  ((bytestream[start_pos+4] & 0xFF | buffer64) << 24)
      |  ((bytestream[start_pos+5] & 0xFF | buffer64) << 16)
      |  ((bytestream[start_pos+6] & 0xFF | buffer64) << 8)
      |  (bytestream[start_pos+7]);

  //return value;
  return buffer;
}

/***********************************************/

void encode_uint64(uint64_t value, 
                   std::vector<unsigned char>& bytestream, 
                   unsigned int start_pos)
{
  bytestream[start_pos + 0] = (value >> 56) & 0xFF;
  bytestream[start_pos + 1] = (value >> 48) & 0xFF;
  bytestream[start_pos + 2] = (value >> 40) & 0xFF;
  bytestream[start_pos + 3] = (value >> 32) & 0xFF;
  bytestream[start_pos + 4] = (value >> 24) & 0xFF;
  bytestream[start_pos + 5] = (value >> 16) & 0xFF;
  bytestream[start_pos + 6] = (value >> 8) & 0xFF;
  bytestream[start_pos + 7] = value & 0xFF;
}



#include <cstddef>
#include <stdint.h>
#include "WaveformProcessing.h"
#include <climits>
#include "tof_dataclasses.h"


/***************************************************/

float TOF::position_along_paddle(float t1, float t2, float length)
{
  float c_paddle = 1;
  float d1 = (length - (t2 - t1)/c_paddle)/2;
  return d1;
}

/***************************************************/
// FIXME - eventually this needs to be replaced
// with the calibrated values
//short ch_adc[NCHN][NWORDS];

uint16_t TOF::peak_position(double wf[], float &val)
{
  float peak = -1e6;
  uint16_t pos  = 0;
  for (size_t k=0;k<NWORDS;k++)
    {
     val = wf[k];
     if (val > peak)
        {
           pos = k;
           peak = val;
        }
    }
  val = peak;
  return pos;
}

double TOF::calculate_charge(double wf[], double dx){
    double sum=0;
    for (size_t k=0; k<NWORDS;k++)
        { sum += (dx*wf[k]);}
    return sum/IMPEDANCE;

}



#! /usr/bin/env python

import zmq
import time
import sys
import random

import os.path

sys.path.append(os.path.abspath("../../build/"))

import _pyPacketBuilder as pb
paddle = pb.RPaddlePacket()

host = "127.0.0.1"
port = "5555"

# Creates a socket instance
context = zmq.Context()
socket = context.socket(zmq.PUB)

# Binds the socket to a predefined port on localhost
socket.bind("tcp://{}:{}".format(host, port))


while True:
    paddle_id = random.choice([20,30,130,4])
    print (f'paddle id = {paddle_id}')
    paddle.paddle_id = paddle_id
    payload = bytearray(paddle.serialize())
    #payload = ''.join([str(k) for k in paddle.serialize()])
    # Sends a string message
    #socket.send_string("hello")
    socket.send(payload)
    time.sleep(1)

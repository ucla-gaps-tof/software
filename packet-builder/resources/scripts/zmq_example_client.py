#! /usr/bin/env python
import zmq
import time
import sys
import os.path

#host = "127.0.0.1"
host = "10.97.108.31"
#port = "5555"
port = "10999"

sys.path.append(os.path.abspath("../../build/"))

import _pyPacketBuilder as pb
event = pb.REventPacket()



# Creates a socket instance
context = zmq.Context()
socket = context.socket(zmq.SUB)

# Connects to a bound socket
socket.connect("tcp://{}:{}".format(host, port))

# Subscribes to all topics
socket.subscribe("")

while True:
    print ("listening...")
    # Receives a string format message
    payload = (socket.recv())
    #print (payload)

    event.deserialize([k for k in payload],0)
    print (event)
    time.sleep(0.1)

#include <assert.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "si5395.h"
#include <i2c/smbus.h>
#include <string.h>
#include "filelock.h"

#include "ad5675_struct.h"
#include "ad5675.h"
#include "cy8c9560A.h"
#include "tof_iic_init.h"
#define I2C_SLAVE 0x0703
#define iic_device_file "/dev/i2c-0"

int fd;
int temperature_buffer;

/*
  RF Switch Control:
    To control switch, change value for RF_SWITCH
    0: RFC = OFF  (No Connection)
    1: RFC -> RF1 (Timing calibration input: 250 mVpp 25 MHz sinusoid)
    2: RFC -> RF2 (SMA RF input)
*/
int RF_SWITCH = 1;

/*
    DAC Channels:
      0x0: Vout -
      0x1: Vout +
      0X2: ROFS
      0X3: THS4509 Common Voltage
      0X4: DRS BIAS

    Notes:
        Must be run after successful initialization. Does not configure Si5395.
*/

ad5675_config ad5675 = {
  .master_fd = 0x00,
  .iic_mux_address = 0x00,
  .iic_mux_channel = 0x00,
  .ad5675_address = 0x00,
};

// This struct does not require reconfiguring Si5395_RST
// When this program is called the board should be initialized and Si5395 running
// We do not want to reset that part since the PL is relying on the 33 MHz clock.
cy8c9560A_config cy8c9560A= { 
  .master_fd = 0x00,
  .iic_mux_address = 0x00,
  .iic_mux_channel = 0x00,
  .cy8c9560A_address = 0x00,
  .gp0 = 0x00,
  .gp1 = 0x00,
  .gp2 = 0x00,
  .gp3 = 0x00,
  .gp4 = 0x00,
  .gp5 = 0x00,
  .gp6 = 0x00,
  .gp7 = 0x00
};

int main(int argc, char **argv){

  // Get execution time
  time_t start_t = time(NULL);
  struct tm *tm_start = localtime(&start_t);
  char start_t_str[80];
  assert(strftime(start_t_str, sizeof(start_t_str), "%c", tm_start));

  // Log info the ../bin/set_mode_tcal_log.txt
  FILE *tcal_log = fopen("../bin/set_mode_tcal_log.txt", "a");
  if (tcal_log == NULL)
  {
    printf("[ERROR] (set_mode_tcal.c, main): Could not open file\n");
  }

  fprintf(tcal_log, "\n===================================\n");
  fprintf(tcal_log, "Begin new set_mode_tcal.c entry\n");
  fprintf(tcal_log, "%s\n", start_t_str);

  // Do not step on other process
  int lock_rv;
  lock_rv = tryGetLock(LOCK_FILE);
  if(lock_rv < 0){
    fprintf(stderr, "Collision with other process, abort\n");
    abort();
  }

  // DAC timing calibration defaults
  // For this mode the input range should be -0.5 V to 0.5 V
  // IN_OFS
  int inofsp = 25600; // 0.8V
  int inofsn = 25600; // 0.8V
  // DRS ROFS, 1.6V Max
  int drsrofs = 49792; // 1.556V
  // THS4509 common mode voltage: V_CM
  int vcm = 32000; // 1V
  // DRS Bias
  int drsbias = 22400; // 0.7V

  printf("Configuring peripherals: DRS4 timing calibration\n");
  fprintf(tcal_log, "Configuring peripherals: DRS4 timing calibration\n");
  fd = open(iic_device_file , O_RDWR);

  // config GPIO expander with struct defined earlier
  cy8c9560A_ReadoutConfig(fd,&cy8c9560A,&iicMux_u63);

  printf("GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));
  fprintf(tcal_log, "GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));

  // Set RF sw to tcal input
  hmc849(&cy8c9560A,0,RF_SWITCH);
  hmc849(&cy8c9560A,1,RF_SWITCH);
  hmc849(&cy8c9560A,2,RF_SWITCH);
  hmc849(&cy8c9560A,3,RF_SWITCH);
  hmc849(&cy8c9560A,4,RF_SWITCH);
  hmc849(&cy8c9560A,5,RF_SWITCH);
  hmc849(&cy8c9560A,6,RF_SWITCH);
  hmc849(&cy8c9560A,7,RF_SWITCH);
  hmc849(&cy8c9560A,8,RF_SWITCH);

  // Enable timing calibration circuit buffer outputs
  tca_clk_0to5_en(&cy8c9560A,1);
  tca_clk_6to8_en(&cy8c9560A,1);

  // Setup DAC
  config_ad5674(fd,&ad5675, &iicMux_u63);

  // From cy8c9560A DAC initialized in reset (active low) set reset pin high
  ad5675_reset(&cy8c9560A,1);

  // Enable Si5395 outputs
  si5395_oe_rst(&cy8c9560A,0,1);

  //  DAC settings
  //  Next few lines will configure the DAC outputs for DRS4/analog front end
  //  The AD5675 is a 16-bit DAC with range 0 to 2.048 V
  //  Decimal step size is: 0.00003125 V / integer
  float dac_lsb = 0.00003125;
  float tmp_float;

  printf("\n\nAD5675 Set points\n");
  fprintf(tcal_log, "\n\nAD5675 Set points\n");
  //  DRS4 analog input offset/bias: IN+_OFS, IN-_OFS
  write_to_update(&ad5675,0x00, inofsp); //0.8V

  tmp_float = dac_lsb * inofsp;
  printf("IN+_OFS: %d = %.2f\n", inofsp, tmp_float);
  fprintf(tcal_log, "IN+_OFS: %d = %.2f\n", inofsp, tmp_float);

  write_to_update(&ad5675,0x01, inofsn); //0.8V 

  tmp_float = dac_lsb * inofsn;
  printf("IN-_OFS: %d = %.2f\n", inofsn, tmp_float);
  fprintf(tcal_log, "IN-_OFS: %d = %.2f\n", inofsn, tmp_float);

  //  DRS ROFS 1V, 1.6V max
  write_to_update(&ad5675,0x02, drsrofs);  //1.556 v

  tmp_float = dac_lsb * drsrofs;
  printf("ROFS: %d = %.2f\n", drsrofs, tmp_float);
  fprintf(tcal_log, "ROFS: %d = %.2f\n", drsrofs, tmp_float);

  //  THS4509 common mode voltage: V_CM
  //  For +3.5 V and -1.5 V split supply, half range is 1 V
  write_to_update(&ad5675,0x03, vcm);

  tmp_float = dac_lsb * vcm;
  printf("V_CM: %d = %.2f\n", vcm, tmp_float);
  fprintf(tcal_log, "V_CM: %d = %.2f\n", vcm, tmp_float);

  //  DRS BIAS 0.7V	
  write_to_update(&ad5675,0x04, drsbias);

  tmp_float = dac_lsb * drsbias;
  printf("DRS_BIAS: %d = %.2f\n", drsbias, tmp_float);
  fprintf(tcal_log, "DRS_BIAS: %d = %.2f\n", drsbias, tmp_float);

  // Get finish time
  time_t end_t = time(NULL);
  struct tm *tm_end = localtime(&end_t);
  char end_t_str[80];
  assert(strftime(end_t_str, sizeof(end_t_str), "%c", tm_end));
  fprintf(tcal_log, "%s\n", end_t_str);
  fprintf(tcal_log, "End set_mode_tcal.c entry\n");
  fprintf(tcal_log, "\n===================================\n");

  // Cleanup
  releaseLock(lock_rv, LOCK_FILE);
  close(fd);
  fclose(tcal_log);

  return 0;

}
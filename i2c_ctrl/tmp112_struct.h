#ifndef TMP112_STRUCT_H
#define TMP112_STRUCT_H
/*
Configuration Register: 
MSB: OS , R1, R0,F1,F0,POL,TM,SD
LSB: CR1, CR0,AL,EM,0, 0,  0, 0

Shutdown Mode(SD): Shuts down IC to 0.5uA.
                   1: Enable
                   0: Disable
Thermostat Mode(TM): 
                   1: Interrupt Mode
                   0: Comparator Mode
Polarity(POL): Sets polarity of Alert pin output.
                   1: Active high
                   0: Active low
Fault Queue(F1/F0): Fault condition exists when measured data exceeds Thigh and Tlow. 
                   00: 1 fault
                   01: 2 faults
                   10: 4 faults
                   11: 6 faults
Converter Resolution(R1/R0): Converter Resolution Read-only buts 
One-shot(OS): In shutdown mode, writing a 1 to this bit begins a single temperature conversion. 
Extended Mode(EM): 
                   1: Extended Mode: Allows measurements above 128 Celsius
                   0: Normal Mode:   Uses 12 bit format
Alert(AL): The AL bit is a read-only function.
            This function is dependent on POL 
                   1: Temperature is below Tlow
                   0: Temperature is above Tlow  
Continuous-Conversion Mode(CR1/CR0):
                   00: 0.25Hz
                   01: 1 Hz
                   10: 4 Hz(default)
                   11: 8 Hz
*/
 typedef struct  {
    char master_fd;
    char iic_mux_address;
    char iic_mux_channel;
    char tmp112_address;
    ////////////////////
    char Shutdown_Mode;
    char Thermostat_Mode;
    char Polarity;
    char Fault_Queue;
    char One_shot;
    char Extended_Mode;
    char Continuous_Conversion_Mode;
    uint16_t tmp112_tHigh;
    int16_t tmp112_tLow;    
}tmp112_config;


#endif
 
#include <assert.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "si5395.h"
#include <i2c/smbus.h>
#include <string.h>

#include "ad5675_struct.h"
#include "ad5675.h"
#include "cy8c9560A.h"
#include "tof_iic_init.h"
#include "filelock.h"
#define I2C_SLAVE 0x0703
#define iic_device_file "/dev/i2c-0"

int fd;
int temperature_buffer;
int RF_SWITCH = 2;

/*
  set_mode_rfin:
    This executable puts the board into DAQ mode.
    - Switches configured for RF input.
    - Si5395 is programmed.
    - User may enter DAC settings to adjust input range. Else, defaults used.

  RF Switch Control:
    To control switch, change value for RF_SWITCH
    0: RFC = OFF  (No Connection)
    1: RFC -> RF1 (Timing calibration input: 250 mVpp 25 MHz sinusoid)
    2: RFC -> RF2 (SMA RF input)

  DAC Channels:
    0x0: Vout -
    0x1: Vout +
    0X2: ROFS
    0X3: THS4509 Common Voltage
    0X4: DRS BIAS
*/

ad5675_config ad5675 = {
  .master_fd = 0x00,
  .iic_mux_address = 0x00,
  .iic_mux_channel = 0x00,
  .ad5675_address = 0x00,
};


cy8c9560A_config cy8c9560A= { 
  .master_fd = 0x00,
  .iic_mux_address = 0x00,
  .iic_mux_channel = 0x00,
  .cy8c9560A_address = 0x00,
  .gp0 = 0x00,
  .gp1 = 0x00,
  .gp2 = 0x00,
  .gp3 = 0x00,
  .gp4 = 0x00,
  .gp5 = 0x00,
  .gp6 = 0x00,
  .gp7 = 0x00
};

/*
 Write into Silicon Laboratories' Si5395 Clock Synthesizer registers.

*/

void configure_si5395(int fd, iic_addresses *data, FILE *xfd){
  int index = 0;
  int page = 0;
  int reg = 0;
  time_t timer;
  char *timer_string;
  struct timeval start,stop;
  // Vars to hold values for some status regs

  int internal_stat = 0;
  // 1'b0 = SYSINCAL: 1 if the device is calibrating
  // 1'b1 = LOSXAXB: 1 is there is no signal at the XAXB pins
  // 1'b3 = XAXB_ERR: 1 if there is a problem locking to the XAXB input signal
  // 1'b5 = SMBUS_TIMEOUT: 1 if there is an SMBus timeout error

  int holdover_lol_stat = 0;
  // 1'b1 = LOL: 1 if the DSPLL is out of lock
  // 1'b5 = HOLD: 1 if the DSPLL is in holdover (or free run)

  timer = time(NULL);
  //Select iic mux channel for clock synthesizer
  ioctl(fd,I2C_SLAVE,data->pca9548abs_iic_mux);
  i2c_smbus_write_byte(fd, 0x02);
  ioctl(fd,I2C_SLAVE,data->si5395_clock_synth);
  //iterate through 2d array
  printf("Configuring Si5395...\n");
  fprintf(xfd, "Configuring Si5395...\n");
  printf("Writing %i registers\n", SI5395_REVA_REG_CONFIG_NUM_REGS);
  fprintf(xfd, "Writing %i registers\n", SI5395_REVA_REG_CONFIG_NUM_REGS);
  while( index < SI5395_REVA_REG_CONFIG_NUM_REGS ){
    page = ((si5395_reva_registers + index)->address)>>8;
    reg = ((si5395_reva_registers + index)->address)&0x00FF;
    i2c_smbus_write_byte_data(fd,0x01,page);
    i2c_smbus_write_byte_data(fd,reg,(si5395_reva_registers + index)->value);

   index++;
    if(index == 2){
      printf("Implement a delay for 300ms\n\r");
      timer_string = ctime(&timer);
      printf("%s\n\r",timer_string);
      gettimeofday(&start,NULL);
      usleep(300000);//sleep for 300ms
      timer_string = ctime(&timer);
      printf("%s\n\r",timer_string);
      gettimeofday(&stop,NULL);
     }
   }

   printf("Done..\n\r");
   printf("took %lu us\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

  // Check status regs
  printf("Checking Si5395 status...\n");
  fprintf(xfd, "Checking Si5395 status...\n");
  printf("-------------------------\n");
  fprintf(xfd,"-------------------------\n");
  internal_stat = i2c_smbus_read_byte_data(fd, 0x000C);
  printf("Internal status bits: %i\n", internal_stat);
  fprintf(xfd,"Internal status bits: %i\n", internal_stat);
  holdover_lol_stat = i2c_smbus_read_byte_data(fd, 0x000E);
  printf("Holdover and LOL status: %i\n", holdover_lol_stat);
  fprintf(xfd,"Holdover and LOL status: %i\n", holdover_lol_stat);
  printf("-------------------------\n");
  fprintf(xfd,"-------------------------\n");
 }

int main(int argc, char **argv){

  // Get execution time
  time_t start_t = time(NULL);
  struct tm *tm_start = localtime(&start_t);
  char start_t_str[80];
  assert(strftime(start_t_str, sizeof(start_t_str), "%c", tm_start));

  // Log info the ../bin/set_mode_rfin_log.txt
  FILE *rfin_log = fopen("../bin/set_mode_rfin_log.txt", "a");
  if (rfin_log == NULL)
  {
    printf("[ERROR] (set_mode_rfin.c, main): Could not open file\n");
  }

  fprintf(rfin_log, "\n===================================\n");
  fprintf(rfin_log, "Begin new set_mode_rfin.c entry\n");
  fprintf(rfin_log, "%s\n", start_t_str);

  // Do not step on other process
  int lock_rv;
  lock_rv = tryGetLock(LOCK_FILE);
  if(lock_rv < 0){
    fprintf(stderr, "Collision with other process, abort\n");
    fprintf(rfin_log, "Collision with other process, abort\n");
    abort();
  }

  // DAC defaults
  // IN_OFS
  int inofsp = 25600; // 0.8V
  int inofsn = 25600; // 0.8V
  // DRS ROFS, 1.6V Max
  int drsrofs = 49792; // 1.556V
  // THS4509 common mode voltage: V_CM
  int vcm = 32000; // 1V
  // DRS Bias
  int drsbias = 22400; // 0.7V
  // Max range of DAC
  int maxadc = 65536;

  // arg parse vars
  int c;
  opterr = 0;
  int index;
  char *tmpptr;
  int tmplen;

  while ((c = getopt (argc, argv, "p:n:r:c:b:")) != -1)
  switch (c)
  {
    case 'p':
      // Input sanitation
      inofsp = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(inofsp > maxadc || inofsp < 0 || tmplen > 0){
        fprintf (stderr, "Invalid inofsp input\n");
        fprintf (rfin_log, "Invalid inofsp input\n");
        return 1;
      }
      break;
    case 'n':
      inofsn = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(inofsn > maxadc || inofsn < 0 || tmplen > 0){
        fprintf (stderr, "Invalid inofsn input\n");
        fprintf (rfin_log, "Invalid inofsn input\n");
        return 1;
      }
      break;
    case 'r':
      drsrofs = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(drsrofs > maxadc || drsrofs < 0 || tmplen > 0){
        fprintf (stderr, "Invalid drsrofs input\n");
        fprintf (rfin_log, "Invalid drsrofs input\n");
        return 1;
      }
      break;
    case 'c':
      vcm = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(vcm > maxadc || vcm < 0 || tmplen > 0){
        fprintf (stderr, "Invalid vcm input\n");
        fprintf (rfin_log, "Invalid vcm input\n");
        return 1;
      }
      break;
    case 'b':
      drsbias = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(drsbias > maxadc || drsbias < 0 || tmplen > 0){
        fprintf (stderr, "Invalid drsbias input\n");
        fprintf (rfin_log, "Invalid drsbias input\n");
        return 1;
      }
      break;        
    case '?':
      if (optopt == 'p' || optopt == 'n' || optopt == 'r' || optopt == 'c' || optopt == 'b')
        fprintf (stderr, "Option -%c requires an argument.\n", optopt);
      else if (isprint (optopt))
        fprintf (stderr, "Unknown option `-%c'.\n", optopt);
      else
        fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
      return 1;
    default:
      abort ();
  }

  // Debug
  printf ("inofsp = %d, inofsn = %d, drsrofs = %d, vcm = %d , drsbias = %d\n",
          inofsp, inofsn, drsrofs, vcm, drsbias);

  // Write params to log
  fprintf(rfin_log, "inofsp = %d\ninofsn = %d\ndrsrofs = %d\nvcm = %d\ndrsbias = %d\n",
          inofsp, inofsn, drsrofs, vcm, drsbias);

  for (index = optind; index < argc; index++)
  printf ("Non-option argument %s\n", argv[index]);

  printf("Configuring peripherals: DRS4 amplitude calibration\n");
  fprintf(rfin_log, "Configuring peripherals: DRS4 amplitude calibration\n");
  fd = open(iic_device_file , O_RDWR);

  cy8c9560A_ReadoutConfig(fd,&cy8c9560A,&iicMux_u63);

  config_ad5674(fd,&ad5675, &iicMux_u63);

  printf("GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));
  fprintf(rfin_log, "GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));

  hmc849(&cy8c9560A,0,RF_SWITCH);
  hmc849(&cy8c9560A,1,RF_SWITCH);
  hmc849(&cy8c9560A,2,RF_SWITCH);
  hmc849(&cy8c9560A,3,RF_SWITCH);
  hmc849(&cy8c9560A,4,RF_SWITCH);
  hmc849(&cy8c9560A,5,RF_SWITCH);
  hmc849(&cy8c9560A,6,RF_SWITCH);
  hmc849(&cy8c9560A,7,RF_SWITCH);
  hmc849(&cy8c9560A,8,RF_SWITCH);

  // Enable Si5395 outputs
  si5395_oe_rst(&cy8c9560A,0,1);

  // Configure Si5395 for DAQ
  configure_si5395(fd, &iicMux_u63, rfin_log);
  // Allow enough time for DSPLL to lock
  printf("\n\nWait for Si5395 to acquire lock\n");
  fprintf(rfin_log, "\n\nWait for Si5395 to acquire lock\n");
  sleep(50);

  ad5675_reset(&cy8c9560A,1);

  //  DAC settings
  //  Next few lines will configure the DAC outputs for DRS4/analog front end
  //  The AD5675 is a 16-bit DAC with range 0 to 2.048 V
  //  Decimal step size is: 0.00003125 V / integer

  float dac_lsb = 0.00003125;
  float tmp_float;

  printf("\n\nAD5675 Set points\n");
  fprintf(rfin_log, "\n\nAD5675 Set points\n");
  //  DRS4 analog input offset/bias: IN+_OFS, IN-_OFS
  write_to_update(&ad5675,0x00, inofsp); //0.8V

  tmp_float = dac_lsb * inofsp;
  printf("IN+_OFS: %d = %.2f\n", inofsp, tmp_float);
  fprintf(rfin_log, "IN+_OFS: %d = %.2f\n", inofsp, tmp_float);

  write_to_update(&ad5675,0x01, inofsn); //0.8V 

  tmp_float = dac_lsb * inofsn;
  printf("IN-_OFS: %d = %.2f\n", inofsn, tmp_float);
  fprintf(rfin_log, "IN-_OFS: %d = %.2f\n", inofsn, tmp_float);

  //  DRS ROFS 1V, 1.6V max
  write_to_update(&ad5675,0x02, drsrofs);  //1.556 v

  tmp_float = dac_lsb * drsrofs;
  printf("ROFS: %d = %.2f\n", drsrofs, tmp_float);
  fprintf(rfin_log, "ROFS: %d = %.2f\n", drsrofs, tmp_float);

  //  THS4509 common mode voltage: V_CM
  //  For +3.5 V and -1.5 V split supply, half range is 1 V
  write_to_update(&ad5675,0x03, vcm);

  tmp_float = dac_lsb * vcm;
  printf("V_CM: %d = %.2f\n", vcm, tmp_float);
  fprintf(rfin_log, "V_CM: %d = %.2f\n", vcm, tmp_float);

  //  DRS BIAS 0.7V	
  write_to_update(&ad5675,0x04, drsbias);

  tmp_float = dac_lsb * drsbias;
  printf("DRS_BIAS: %d = %.2f\n", drsbias, tmp_float);
  fprintf(rfin_log, "DRS_BIAS: %d = %.2f\n", drsbias, tmp_float);

  // Get finish time
  time_t end_t = time(NULL);
  struct tm *tm_end = localtime(&end_t);
  char end_t_str[80];
  assert(strftime(end_t_str, sizeof(end_t_str), "%c", tm_end));
  fprintf(rfin_log, "%s\n", end_t_str);
  fprintf(rfin_log, "End set_mode_rfin.c entry\n");
  fprintf(rfin_log, "\n===================================\n");

  // Cleanup
  releaseLock(lock_rv, LOCK_FILE);
  close(fd);
  fclose(rfin_log);
  return 0;

}

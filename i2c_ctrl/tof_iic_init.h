#ifndef TOF_IIC_INIT_H
#define TOF_IIC_INIT_H

iic_addresses iicMux_u63 = {
	.pca9548abs_iic_mux            = 0x77,
	.si5395_clock_synth_tempSense  = 0x4B, //Channel 0
	.si5395_clock_synth            = 0x68, //Channel 1
	.ad5675bcpz_dac                = 0x0C, //Channel 2
	.ad9245_adc_dvdd_currentSense  = 0x42, //Channel 3
	.ad9245_adc_tempSense          = 0x4A, //Channel 4
	.ad9245_adc_avdd_currentSense  = 0x4F, //Channel 5
	.drs4_avdd_currentSense        = 0x40, //Channel 6
	.cy8c9560A_gpio_eeprom         = 0x50, //Channel 7
	.cy8c9560A_gpio_multi_prom     = 0x20  //Channel 7
};

iic_addresses iicMux_u64 = { 
	.pca9548abs_iic_mux            = 0x75,
	.bme280_humidity_sensor        = 0x76, //Channel 0
	.lis3mdltr_mag_sensor          = 0x1E, //Channel 1
	.drs4_tempSense                = 0x48, //Channel 2
	.dsr4_dvdd_currentSense        = 0x4E, //Channel 3
	.vrail_3v3_currentSense        = 0x44, //Channel 4
	.marszx2_currentSense          = 0x41, //Channel 5
	.vrail_neg1_currentSense       = 0x45, //Channel 6
 	.vrail_4v_currentSense         = 0x43  //Channel 7
};

 

#endif

# Introduction

[[_TOC_]]

This code is used to configure the UCLA DRS4 board for data acquisition. It enables and programs various I2C peripherals needed for DAQ mode, including:
* IO expander (cy8c9560A)
* DAC (ad5675)
* clock synthesizer (si5395)
* RF switch (hmc849)
* amplifiers (ltc6269, ths4509)
* temperature sensors (tmp112) for DRS4, si5395, and ADC

# Prerequisites/dependencies
## Hardware
* V2.3+ UCLA DRS4 board
* External 1 MHz LVDS clock, connected to J1

## Software
* Debian 10+
* libi2c-dev
* g++
* make

# Building
Compile the executable using `make all`. Make and install with `make install`.

# Usage

## rbsetup
```bash
rbsetup [-i mode] [-p IN+] [-n IN-] [-r ROFS]
```
* -i mode: sets the input mode to 0 (no inputs), 1 (sine wave), or 2 (SMA/external). Default is 2
* -p/n IN+/-: sets the positive/negative end of the differential input IN+/-, used for voltage calibration. Defaults are 25600. ⚠️ Actual voltages differ from input values, and can be measured with TP40/TP41
* -r ROFS: sets the read offset. Lowest voltage = 1.05 - (ROFS/32000). Default is 35200 (giving range of -50mV to +950mV).


# Expected output
If things are successful you will see something like this
```
Setting up readout board
RF switch 2: SMA inputs
IN+: 25600
IN-: 25600
ROFS: 35200
GPIO Expander DEVICE ID: 0x60
Configuring Si5395
Writing 595 registers
Done!
Checking Si5395 status...
-------------------------
Internal status bits: 0
Holdover and LOL status: 0
-------------------------
DRS Temperature 472
Si5395 Temperature 3711
ADC Temperature 3711
```

# Defaults
| Signal/Device | Value/State  | DAC Code     |
| :---          | :---         | :---         |
| RF Switch     | SMA Input    |              |
| DRS IN+_OFS   | 0.8 V        | 25600        |
| DRS IN-_OFS   | 0.8 V        | 25600        |
| DRS ROFS      | 1.1 V        | 35200        |
| V_CM          | 1.0 V        | 3200         |
| DRS BIAS      | 0.7 V        | 22400        |
| DRS REF_CLK   | 1.000000 MHz |              |

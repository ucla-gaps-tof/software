/*
Authors: Jamie Ryan (jryan@astro.ucla.edu)
rbsetup.c (c) UCLA 2022
Desc: Set up readout boards
Created:  2021-10-31
Modified: 2022-03-02

Usage: ./rbsetup [-i N] 
	-i: Input mode
	-p: IN+
	-n: IN-
	-r: Read offset/ROFS
	

Input Mode Control:
	0: RFC = OFF  (No Connection)
	1: RFC -> RF1 (Timing calibration input: 250 mVpp 25 MHz sinusoid)
	2: RFC -> RF2 (SMA RF input)
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#include "si5395.h"
#include "ad5675_struct.h"
#include "ad5675.h"
#include "tmp112.h"
#include "tmp112_init.h"
#include "cy8c9560A.h"
#include "cy8c9560A_init.h"
#include "tof_iic_init.h"
#define I2C_SLAVE 0x0703
#define iic_device_file "/dev/i2c-0"

void configure_si5395(int fd, iic_addresses *data);

ad5675_config ad5675 = {
	.master_fd = 0x00,
	.iic_mux_address = 0x00,
	.iic_mux_channel = 0x00,
	.ad5675_address = 0x00,
};
	
int main(int argc, char **argv)
{
	printf("Setting up readout board\n");
	int mode = 2;
	uint16_t in_pos = 25600;
	uint16_t in_neg = 25600;
	uint16_t offset = 35200; // 35200=-50+950mV, 38400=-150mV+850mV
	
	// parse options (cf. man7.org/linux/man-pages/man3/getopt.3.html)
	int flag;
	char *end; // stores pointer to end
	while ((flag = getopt(argc, argv, "i:p:n:r:")) != -1) {
		switch (flag) {
			case 'i': // Input mode
				mode = (int) strtol(optarg,&end,10);
				if (mode < 0 || mode > 2) {
					printf("Invalid input mode\n");
					return 1;
				}
				break;
			case 'p': // IN+
				in_pos = (uint16_t) strtol(optarg,&end,10);
				if (in_pos < 0 || in_pos > 48000) {
					printf("Invalid IN+ value\n");
					return 1;
				}
				break;
			case 'n': // IN-
				in_neg = (uint16_t) strtol(optarg,&end,10);
				if (in_neg < 0 || in_neg > 48000) {
					printf("Invalid IN- value\n");
					return 1;
				}
				break;
			case 'r': // ROFS
				offset = (uint16_t) strtol(optarg,&end,10);
				if (offset < 0 || offset > 51200) {
					printf("Invalid ROFS value\n");
					return 1;
				}
				break;
			default:
				printf("Error parsing options\n");
				abort();
		}
	}
	if (mode == 0)
		printf("RF switch 0: No inputs\n");
	else if (mode == 1)
		printf("RF switch 1: Sine wave inputs\n");
	else if (mode == 2)
		printf("RF switch 2: SMA inputs\n");
	printf("IN+: %d\n",in_pos);
	printf("IN-: %d\n",in_neg);
	printf("ROFS: %d\n",offset);
	
	int fd = open(iic_device_file, O_RDWR);

 	cy8c9560A_ReadoutConfig(fd, &cy8c9560A, &iicMux_u63);

	config_ad5674(fd,&ad5675, &iicMux_u63);

	tmp112_init(fd,&tmp112_DRS4,  &iicMux_u64, 0x02);
	tmp112_init(fd,&tmp112_si5395,&iicMux_u63, 0x00);
	tmp112_init(fd,&tmp112_ADC,   &iicMux_u63, 0x04); 
	
	printf("GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));

	hmc849(&cy8c9560A, 0, mode);
	hmc849(&cy8c9560A, 1, mode);
	hmc849(&cy8c9560A, 2, mode);
	hmc849(&cy8c9560A, 3, mode);
	hmc849(&cy8c9560A, 4, mode);
	hmc849(&cy8c9560A, 5, mode);
	hmc849(&cy8c9560A, 6, mode);
	hmc849(&cy8c9560A, 7, mode);
	// channel 9 should 0 for voltage cal, 2 otherwise
	if (mode == 0)
		hmc849(&cy8c9560A, 8, 0);
	else
		hmc849(&cy8c9560A, 8, 2);

	si5395_oe_rst(&cy8c9560A, 0, 1);

	// Enable timing calibration outputs
	if (mode == 1) {
		tca_clk_0to5_en(&cy8c9560A, 1);
		tca_clk_6to8_en(&cy8c9560A, 1);
	}

	configure_si5395(fd, &iicMux_u63); // configure SI5395 (clock synthesizer)
	ad5675_reset(&cy8c9560A, 1);
 	ltc6269_shutdown(&cy8c9560A, 1);

	/*	DAC settings
		Next few lines will configure the DAC outputs for DRS4/analog front end
		The AD5675 is a 16-bit DAC with range 0 to 2.048 V
		Decimal step size is: 0.00003125 V / integer
		
		DAC Channels:
			0x0: Vout -
			0x1: Vout +
			0X2: ROFS
			0X3: THS4509 Common Voltage
			0X4: DRS BIAS
	*/
	// DRS4 analog input offset/bias: IN+_OFS, IN-_OFS
	write_to_update(&ad5675,0x00, in_neg);
	write_to_update(&ad5675,0x01, in_pos);
	
	// DRS ROFS 1V, 1.6V max
	write_to_update(&ad5675,0x02, offset);

	// THS4509 common mode voltage: V_CM
	// For +3.5 V and -1.5 V split supply, half range is 1 V
	write_to_update(&ad5675,0x03, 32000);
	
	// DRS BIAS 0.7V	
	write_to_update(&ad5675,0x04, 22400);

	/* Retrieve Temperature sensors */
	int temperature_buffer;
	temperature_buffer = tmp112_getTemperature(&tmp112_DRS4);
	// temperature_buffer = tmp112_getTemperature(&tmp112_DRS4)*0.0625;
	printf("DRS Temperature %d\n\r",temperature_buffer);
	// printf("DRS Temperature %d°C\n\r",temperature_buffer);
	temperature_buffer = tmp112_getTemperature(&tmp112_si5395);
	// temperature_buffer = tmp112_getTemperature(&tmp112_si5395)*0.0625;
	printf("Si5395 Temperature %d\n\r",temperature_buffer);
	// printf("Si5395 Temperature %d°C\n\r",temperature_buffer);
	temperature_buffer = tmp112_getTemperature(&tmp112_ADC);
	// temperature_buffer = tmp112_getTemperature(&tmp112_ADC)*0.0625;
	printf("ADC Temperature %d\n\r",temperature_buffer);
	// printf("ADC Temperature %d°C\n\r",temperature_buffer);

	sleep(2);
}

/*
Write into Silicon Laboratories' Si5395 Clock Synthesizer registers.
*/
void configure_si5395(int fd, iic_addresses *data)
{
	int index = 0;
	int page = 0;
	int reg = 0;
	// Vars to hold values for some status regs

	int internal_stat = 0;
	// 1'b0 = SYSINCAL: 1 if the device is calibrating
	// 1'b1 = LOSXAXB: 1 is there is no signal at the XAXB pins
	// 1'b3 = XAXB_ERR: 1 if there is a problem locking to the XAXB input signal
	// 1'b5 = SMBUS_TIMEOUT: 1 if there is an SMBus timeout error

	int holdover_lol_stat = 0;
	// 1'b1 = LOL: 1 if the DSPLL is out of lock
	// 1'b5 = HOLD: 1 if the DSPLL is in holdover (or free run)

	//Select iic mux channel for clock synthesizer
	ioctl(fd,I2C_SLAVE,data->pca9548abs_iic_mux);
	i2c_smbus_write_byte(fd, 0x02);
	ioctl(fd,I2C_SLAVE,data->si5395_clock_synth);
	//iterate through 2d array
	printf("Configuring Si5395\n\r");
	printf("Writing %i registers\n\r", SI5395_REVA_REG_CONFIG_NUM_REGS);
	while (index < SI5395_REVA_REG_CONFIG_NUM_REGS) {
		page = ((si5395_reva_registers + index)->address)>>8;
		reg = ((si5395_reva_registers + index)->address)&0x00FF;
		i2c_smbus_write_byte_data(fd,0x01,page);
		i2c_smbus_write_byte_data(fd,reg,(si5395_reva_registers+index)->value);
		index++;
		if (index == 2)
			usleep(300000); // sleep for 300ms
	}

	printf("Done!\n\r");
	// Check status regs
	printf("Checking Si5395 status...\n");
	printf("-------------------------\n");
	internal_stat = i2c_smbus_read_byte_data(fd, 0x000C);
	printf("Internal status bits: %i\n", internal_stat);
	holdover_lol_stat = i2c_smbus_read_byte_data(fd, 0x000E);
	printf("Holdover and LOL status: %i\n", holdover_lol_stat);
	printf("-------------------------\n");
 }

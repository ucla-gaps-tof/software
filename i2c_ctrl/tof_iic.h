#ifndef TOF_IIC_H
#define TOF_IIC_H

#include <stdint.h>
 
typedef struct{
	//Readout Board V.2 iic devices
	char pca9548abs_iic_mux;
	char bme280_humidity_sensor;
	char lis3mdltr_mag_sensor;
	char drs4_tempSense;
	char dsr4_dvdd_currentSense;
	char vrail_3v3_currentSense;
	char marszx2_currentSense;
	char vrail_neg1_currentSense;
	char vrail_4v_currentSense;
	
    	char si5395_clock_synth_tempSense;
	char si5395_clock_synth;
	char ad5675bcpz_dac;
	char ad9245_adc_dvdd_currentSense;
	char ad9245_adc_tempSense;
	char ad9245_adc_avdd_currentSense;
    	char drs4_avdd_currentSense;
 	char cy8c9560A_gpio_eeprom;
	char cy8c9560A_gpio_multi_prom; 
	
	///////////////////////////////////
	//Power Board iic devices
	///////////////////////////////////
	
    ///////////////////////////////////
	//Local Trigger iic devices
	///////////////////////////////////
	
}iic_addresses;


 
 
 /*lis3mdltr_config  lis3mdltr0 = {
	.master_fd = 0x00,
	.iic_mux_address = 0x00,
	.iic_mux_channel = 0x00,
	.lis3mdltr_address = 0x00
};*/
 
 
 #endif

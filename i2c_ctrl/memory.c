/*
Block Ram executable file.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>


 
#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

 
#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
  __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)
	  
off_t target;
int fd;


uint64_t i = 0;
 
void write_reg(uint32_t address,uint32_t data){ 
void *map_base, *virt_addr;


    if((fd = open("/dev/mem",  O_RDWR |O_SYNC)) == -1) FATAL;
  //  printf("/dev/mem opened.\n"); 
   // fflush(stdout);
    map_base= mmap(NULL,MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, address & ~MAP_MASK);
	
    if (map_base == MAP_FAILED) {
        perror("Can't map memory");
        //return -1;
    }
	
	virt_addr = map_base + (address & MAP_MASK) ;



//	printf("Virtual address: 0x%x.\n", virt_addr); 
//        printf("Address: 0x%x.\n", address);

	*((uint32_t *) virt_addr) = data;

	if(munmap(map_base, MAP_SIZE) == -1) FATAL;

	 close(fd);
 	
}

uint32_t  read_reg(uint32_t address){ 
void *map_base, *virt_addr;
uint32_t data = 0;


    if((fd = open("/dev/mem",  O_RDWR |O_SYNC)) == -1) FATAL;
  //  printf("/dev/mem read opened.\n"); 
    //fflush(stdout);
    map_base= mmap(NULL,MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, address & ~MAP_MASK);
	//printf("data:  0x%x.\n", map_base); 
    if (map_base == MAP_FAILED) {
        perror("Can't map memory");
        //return -1;
    }
	
	virt_addr = map_base + (address & MAP_MASK) ;
//	printf("Virtual address:  0x%x.\n", virt_addr); 
//        printf("Address 0x%x\n\r",address);
	data = *((unsigned long *)virt_addr);
//	printf("DATA: 0x%x\n\r", data);
	//return *((uint32_t *) virt_addr);
//        fflush(stdout);
	if(munmap(map_base, MAP_SIZE) == -1) FATAL;

	 close(fd);
	 
	 return data;
 	
}


//DRS4 Temperature Sensor
 tmp112_config tmp112_DRS4= { 
    .master_fd = 0x00, //master_fd
    .iic_mux_address = 0x00, //iic_mux_address
    .iic_mux_channel = 0x00, //iic_mux_channel
    .tmp112_address = 0x00, //tmp112_address
    ////////////////////
    .Shutdown_Mode = 0x00, //shutdown disabled     
    .Thermostat_Mode =0x00, //1 fault
    .Polarity = 0x00, //Active low alert
    .Fault_Queue = 0x00, //Comparator Mode
    .One_shot = 0x00, //one shot disabled
    .Extended_Mode = 0x00, //normal mode
    .Continuous_Conversion_Mode = 0x03, //8Hz conversion 
    .tmp112_tHigh = 40,   //High Temperature Threshold
    .tmp112_tLow  = 25     //Low Temperature Threshold
};
 
//Si5395 Temperature Sensor
 tmp112_config tmp112_si5395= { 
    .master_fd = 0x00, //master_fd
    .iic_mux_address = 0x00, //iic_mux_address
    .iic_mux_channel = 0x00, //iic_mux_channel
    .tmp112_address = 0x00, //tmp112_address
    ////////////////////
    .Shutdown_Mode = 0x00, //shutdown disabled     
    .Thermostat_Mode =0x00, //1 fault
    .Polarity = 0x00, //Active low alert
    .Fault_Queue = 0x00, //Comparator Mode
    .One_shot = 0x00, //one shot disabled
    .Extended_Mode = 0x00, //normal mode
    .Continuous_Conversion_Mode = 0x03, //8Hz conversion 
    .tmp112_tHigh = 40,   //High Temperature Threshold
    .tmp112_tLow = 25     //Low Temperature Threshold
};
 
//ADC Temperature Sensor
 tmp112_config tmp112_ADC= { 
    .master_fd = 0x00, //master_fd
    .iic_mux_address = 0x00, //iic_mux_address
    .iic_mux_channel = 0x00, //iic_mux_channel
    .tmp112_address = 0x00, //tmp112_address
    ////////////////////
    .Shutdown_Mode = 0x00, //shutdown disabled     
    .Thermostat_Mode =0x00, //1 fault
    .Polarity = 0x00, //Active low alert
    .Fault_Queue = 0x00, //Comparator Mode
    .One_shot = 0x00, //one shot disabled
    .Extended_Mode = 0x00, //normal mode
    .Continuous_Conversion_Mode = 0x03, //8Hz conversion 
    .tmp112_tHigh = 40,   //High Temperature Threshold
    .tmp112_tLow = 25     //Low Temperature Threshold
};
 
 

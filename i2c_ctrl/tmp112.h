#include "tmp112_struct.h"
#include "tof_iic.h"

void tmp112_init(int fd,tmp112_config *data,iic_addresses *base, char mux_channel);

uint16_t tmp112_getTemperature(tmp112_config *data);
void convertTemp(tmp112_config *data);
void tmp112_configuration(tmp112_config *data);
void tmp112_setLowTemp(tmp112_config *data);
void tmp112_setHighTemp(tmp112_config *data);


 
 

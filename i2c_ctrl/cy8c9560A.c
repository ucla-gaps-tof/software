#include <linux/i2c-dev.h>
#include <stdio.h>
#include <sys/ioctl.h>  
#include <unistd.h>
#include <stdint.h>
#include <i2c/smbus.h>

#include "cy8c9560A_struct.h"
#include "cy8c9560A.h"
#include "tof_iic.h"
 

/*
Ports used for readout board. 
cy8c9560A
//////////////////////////////////////////
//Output
//////////////////////////////////////////
GP7[7:0]
GP7[7]: TCA_CLK_6TO8_EN
GP7[6]: TCA_CLK_0TO5_EN
GP7[5]: HMC849_EN0
GP7[4]: HMC849_VCTL0
GP7[3]: HMC849_EN1
GP7[2]: HMC849_VCTL1
GP7[1]: HMC849_EN2
GP7[0]: HMC849_VCTL2

GP2[1:0]
GP2[1]: HMC849_EN3
GP2[0]: HMC849_VCTL3

GP5[1:0]
GP5[0]: HMC849_VCTL4
GP5[1]: HMC849_EN4
 
GP5[5:4]
GP5[4]: HMC849_VCTL5
GP5[5]: HMC849_EN5

GP4[7:1]
GP4[7]: HMC849_VCTL6
GP4[6]: HMC849_EN6
GP4[5]: HMC849_VCTL7
GP4[4]: HMC849_EN7
GP4[3]: HMC849_VCTL8
GP4[2]: HMC849_EN8
GP4[1]: SHDN

GP3[4,2:0]
GP3[4]: VCAL_RST
GP3[2]: Si5395_FDEC
GP3[1]: ~Si5395_OE
GP3[0]: ~Si5395_RST

GP0[7,5:4]
GP0[7]: Si5395_FINC
GP0[5]: ~LT_DAC_CLR
//////////////////////////////////////////
//Input
//////////////////////////////////////////
GP0[4]: ~LT_DAC_IRQ
 
*/

/*
TO-DO: Add comments to functions.
*/

 
void cy8c9560A_ReadoutConfig(int fd, cy8c9560A_config* data,iic_addresses *base)
{
	/*
	cy8c9560A_gpio_eeprom         = 0x50,//Channel 7
	.cy8c9560A_gpio_multi_prom     = 0x20 //Channel 7
	*/
	data->master_fd = fd;
	data->iic_mux_address   = base->pca9548abs_iic_mux,
	data->iic_mux_channel   = 0x80,
	data->cy8c9560A_address = base->cy8c9560A_gpio_multi_prom;

	//Port 7 
	cy8c9560A_portSelect(data, 0x07);      //Select port 7
	cy8c9560A_interruptMask(data,0x00);    //No interrupts enabled
	cy8c9560A_setPinDirection(data, 0x00); //Port 7 set to output
	cy8c9560A_strongDrive(data,1);

	//Port 5 
	cy8c9560A_portSelect(data, 0x05);      //Select port 5
	cy8c9560A_interruptMask(data,0x00);    //No interrupts enabled
	cy8c9560A_setPinDirection(data, 0x00); //Port 5 set to output
	cy8c9560A_strongDrive(data,1);

	//Port 4 
	cy8c9560A_portSelect(data, 0x04);      //Select port 4
	cy8c9560A_interruptMask(data,0x00);    //Interrupt enabled for pin 4
	cy8c9560A_setPinDirection(data, 0x00); //Port 4 pin 4 set to input, others to output      
	cy8c9560A_strongDrive(data,1);

	//Port 3
	cy8c9560A_portSelect(data, 0x03);      //Select port 3
	cy8c9560A_interruptMask(data,0x00);    //Interrupt enabled for pin 3
	cy8c9560A_setPinDirection(data, 0x00); //Port 3 set to output 
	cy8c9560A_strongDrive(data,1);

	//Port 0
	cy8c9560A_portSelect(data, 0x00);      //Select port 0
	cy8c9560A_interruptMask(data,0x10);    //Interrupt enabled for pin 0
	cy8c9560A_setPinDirection(data, 0x10); //Port 0 set to output 
	cy8c9560A_strongDrive(data,1);


	//Initialized ports to disable RF Switch, TCA Clock, and Clock Synth
	data->gp0 = 0x30;
	data->gp2 = 0x03;
	data->gp3 = 0x03;
	data->gp4 = 0xFE;
	data->gp5 = 0x33;
	data->gp7 = 0x3F;

	//Set output ports
	cy8c9560A_setOutputPort(data,0x07, data->gp7); //
	cy8c9560A_setOutputPort(data,0x05, data->gp5);
	cy8c9560A_setOutputPort(data,0x04, data->gp4);
	cy8c9560A_setOutputPort(data,0x03, data->gp3);
	cy8c9560A_setOutputPort(data,0x00, data->gp0);

	//Local Trigger Interrupt
	//cy8c9560A_getInputPort(&cy8c9560A, 0x00);
}



///////////////////////////////////////////////////////////////////////////////////////////
//AD5675 16 bit DAC
///////////////////////////////////////////////////////////////////////////////////////////

void ad5675_reset(cy8c9560A_config *data,char reset){
   data->gp3 = (data->gp3&~0x00000010) | reset<<4;
   cy8c9560A_setOutputPort(data, 0x03, data->gp3);
}

///////////////////////////////////////////////////////////////////////////////////////////
//LTC6269 OP-Amp
///////////////////////////////////////////////////////////////////////////////////////////

void ltc6269_shutdown(cy8c9560A_config *data,char shutdown){
   data->gp4 = (data->gp4&~0x00000002) | shutdown<<1;
   cy8c9560A_setOutputPort(data, 0x04, data->gp4);
}


///////////////////////////////////////////////////////////////////////////////////////////
//Si5395 (Clock Synthesizer) GPIO Expander Interface
///////////////////////////////////////////////////////////////////////////////////////////
/*  Needs to be modified */
void si5395_fdec_finc(cy8c9560A_config *data,char fdec,char finc){
    if((fdec&0x01) == 1){
      data->gp3 = (data->gp3&~0x04) | (fdec&0x01)<<2;
    }
    else if((finc&0x01) == 1){
      data->gp0 = (data->gp0&~0x80) | (fdec&0x01)<<7;   
    }
    else{
      data->gp3 = (data->gp3&~0x02);
      data->gp0 = (data->gp0&~0x80);
    }
     cy8c9560A_setOutputPort(data, 0x00, data->gp0);
     cy8c9560A_setOutputPort(data, 0x03, data->gp3);
}

/*
si5395: Output Enable and Reset are both active low inputs. Drive a logic 0 to enable the feature. 
*/
void si5395_oe_rst(cy8c9560A_config *data,char oe,char rst){
    data->gp3 = (data->gp3&~0x02) | oe<<1;
    data->gp3 = (data->gp3&~0x01) | rst;
    cy8c9560A_setOutputPort(data, 0x03, data->gp3);
}

///////////////////////////////////////////////////////////////////////////////////////////
//Si53362(TCA Clock) GPIO Expander Interface
///////////////////////////////////////////////////////////////////////////////////////////

void tca_clk_6to8_en(cy8c9560A_config *data,char enable){
     if(enable){
	   data->gp7 |= (data->gp7 | 0x80) | (enable&0x01)<<8;
     } else {
        data->gp7 &= (data->gp7&~0x80) | (0x00)<<8;
     }
        cy8c9560A_setOutputPort(data, 0x07, data->gp7);
}

void tca_clk_0to5_en(cy8c9560A_config *data,char enable){
     if(enable){
	   data->gp7 |= (data->gp7 | 0x40) | (enable&0x01)<<7;
     } else {
       data->gp7 &= (data->gp7&~0x40)  | (0x00)<<7;
     }     
       cy8c9560A_setOutputPort(data, 0x07, data->gp7);
}



///////////////////////////////////////////////////////////////////////////////////////////
//HMC849(RF Switch) GPIO Expander Interface
///////////////////////////////////////////////////////////////////////////////////////////

/*
  HMC849 Truth Table:
 |_VCTL__|__EN__|  |_RFC -> RF1_|_RFC -> RF2_|
    0    |   0           OFF    |     ON
    1    |   0           ON           OFF
    0    |   1           OFF          OFF
    1    |   1           OFF          OFF
    
    hmcChannel: 0 - 8
    mode: 0: RFC = OFF  (No Connection)
          1: RFC -> RF1 (RF Input )
          2: RFC -> RF2 (TCA Calibration input)

*/
void hmc849(cy8c9560A_config* data, char hmcChannel, int mode){
    switch(hmcChannel){
        case 0: 
           /*GP7[5] = EN
             GP7[4] = VCTL*/ 
           if(mode == 0){
             data->gp7 = (data->gp7 & ~0x30) | 0x20;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           }else if(mode == 1){
             data->gp7 &= (data->gp7 & ~0x30) | 0x10;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           } else {
             data->gp7 &= (data->gp7 & ~0x30) | 0x00;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           }
        case 1:
           /*GP7[3] = EN
             GP7[2] = VCTL*/             

           if(mode == 0){
             data->gp7 |= (data->gp7 | 0x0C) & 0x0C;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           }else if(mode == 1){
             data->gp7 &= (data->gp7 & ~0x0C) | 0x04;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           } else {
             data->gp7 &= (data->gp7 & ~0x0C) | 0x00;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           }           
        case 2:
           /*GP7[1] = EN
             GP7[0] = VCTL*/

           if(mode == 0){
             data->gp7 |= (data->gp7 | 0x03) & 0x03;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           }else if(mode == 1){
             data->gp7 &= (data->gp7 & ~0x03) | 0x01;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           } else {
             data->gp7 &= (data->gp7 & ~0x03) | 0x00;
             cy8c9560A_setOutputPort(data, 0x07, data->gp7);
             break;
           }
        case 3:         
           /*GP2[1] = EN
             GP2[0] = VCTL*/             
           if(mode == 0){
             data->gp2 |= (data->gp2 | 0x03) & 0x03;
             cy8c9560A_setOutputPort(data, 0x02, data->gp2);
             break;
           }else if(mode == 1){
             data->gp2 &= ((data->gp2 & ~0x03) | 0x01);
             cy8c9560A_setOutputPort(data, 0x02, data->gp2);
             break;
           } else {
             data->gp2 &= (data->gp2 & ~0x03) | 0x00;
             cy8c9560A_setOutputPort(data, 0x02, data->gp2);
             break;
           }
         case 4:
           /*GP5[1] = VCTL
             GP5[0] = EN*/           

           if(mode == 0){
             data->gp5 |= (data->gp5 | 0x03) & 0x03;
             cy8c9560A_setOutputPort(data, 0x05, data->gp5);
             break;
           }else if(mode == 1){
             data->gp5 &= (data->gp5 & ~0x03) | 0x02;
             cy8c9560A_setOutputPort(data, 0x05, data->gp5);
             break;
           } else {
             data->gp5 &= (data->gp5 & ~0x03) | 0x00;
             cy8c9560A_setOutputPort(data, 0x05, data->gp5);
             break;
           }
         case 5:
           /*GP5[5] = EN
             GP5[4] = VCTL*/           

           if(mode == 0){
             data->gp5 |= (data->gp5 | 0x30) & 0x30;
             cy8c9560A_setOutputPort(data, 0x05, data->gp5);
             break;
           }else if(mode == 1){
             data->gp5 &= (data->gp5 & ~0x30) | 0x10;
             cy8c9560A_setOutputPort(data, 0x05, data->gp5);
             break;
           } else {
             data->gp5 &= (data->gp5 & ~0x30) | 0x00;
             cy8c9560A_setOutputPort(data, 0x05, data->gp5);
             break;
           }
         case 6:
           /*GP4[7] = VCTL
             GP4[6] = EN*/

           if(mode == 0){
             data->gp4 |= (data->gp4 | 0xC0) & 0xC0;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           }else if(mode == 1){
             data->gp4 &= (data->gp4 & ~0xC0) | 0x80;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           } else {
             data->gp4 &= (data->gp4 & ~0xC0) | 0x00;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           }
         case 7:
           /*GP4[5] = VCTL
             GP4[4] = EN*/

           if(mode == 0){
             data->gp4 |= (data->gp4 | 0x30) &  0x30;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           }else if(mode == 1){
             data->gp4 &= ((data->gp4 & ~0x30) | 0x20);
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           } else {
             data->gp4 &= (data->gp4 & ~0x30) | 0x00;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           }
         case 8:
           /*GP4[3] = VCTL
             GP4[2] = EN*/

           if(mode == 0){
             data->gp4 |= (data->gp4 | 0x0C) & 0x0C;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           }else if(mode == 1){
             data->gp4 &= (data->gp4 & ~0x0C) | 0x08;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           } else {
             data->gp4 &= (data->gp4 & ~0x0C) | 0x00;
             cy8c9560A_setOutputPort(data, 0x04, data->gp4);
             break;
           }
     }
}


///////////////////////////////////////////////////////////////////////////////////////////
//GPIO Expander Functions
///////////////////////////////////////////////////////////////////////////////////////////

/*
cy8c9560A_getDeviceId():
   This function returns the Device ID. By default during startup, the device attempts
   to load the user default block. If corrupted, the factory defaults are loaded and low nibble 
   is set to indicate factory defaults.
  
  2Eh:
      Device Family[7:4] 2 = CY8C9520A, 4 = CY8C9540A, 6 = CY8C9560A
      Reserved [3:1]
      FD/~UD[0]          1 = Factory Default , 0 = User defaults
*/
char cy8c9560A_getDeviceId(cy8c9560A_config *data){
    char cy8c9560A_getID;
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address); 
    cy8c9560A_getID = i2c_smbus_read_byte_data(data->master_fd, 0x2E);
    return cy8c9560A_getID;
 }
 
 /*
cy8c9560A_portSelect():
   This function configures the GPort. It is used to select the port to configure registers
   19h - 23h. Write a value of 0 - 7 to select a port. 

  18h:
      port select[7:0] 7 = port7, ........ ,0 = port 0
 */
 void cy8c9560A_portSelect(cy8c9560A_config *data, char value){  
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);
    i2c_smbus_write_byte_data(data->master_fd,0x18,value);
 }
 
 /*
cy8c9560A_setPinDirection():
   This function tri-states the direction of the pins of a port. 
  1Ch:
      pin direction[7:0]    1: Assigns pin as an input.
                            0: Assigns pin as an output.
  Example: 
     port select[7:0] == 0x00 //port 0 is selected
     pin direction[7:0] == 0xFE //pins GP0[7:1] 
     
     pins GP0[7:1] are configured as inputs. 
     pin GP0[0] is configured as an output. 
     
*/ 
 void cy8c9560A_setPinDirection(cy8c9560A_config *data, char value){
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);
    i2c_smbus_write_word_data(data->master_fd,0x1C,value);
 }
 
/*
cy8c9560A_getInputPort(): 
  00h - 07h:
    This function returns a level driven on input ports. Direction of port is set 
    with Pin Direction register 1Ch.    
*/
char cy8c9560A_getInputPort(cy8c9560A_config *data, char port){
    char cy8c9560A_getInput;
     if(port >= 0x00 && port < 0x08){
       ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
       i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
       ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address); 
       cy8c9560A_getInput = i2c_smbus_read_byte_data(data->master_fd, 0x00 + port);
     }
    return cy8c9560A_getInput;
 }

/*
cy8c9560A_setOutputPort(): 
   This function is used to write data to GPIO ports.
  08h:  
    output port[7:0]    1: Assigns pin a logic 1 on pin.
                        0: Assigns pin a logic low on pin.
*/ 
void cy8c9560A_setOutputPort(cy8c9560A_config *data, char port, char value){
     if(port >= 0x00 && port < 0x08){
      ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
      i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
      ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address); 
      i2c_smbus_write_byte_data(data->master_fd,(0x08 + port),value);
    }
 }

/*
cy8c9560A_interruptMask(): 
   This function is used to enable or disable the INT line when GPIO input levels are changed. 
  19h:  
    interrupt mask[7:0]    1: Disables interrupt.
                           0: Enables interrupt.
*/ 
void cy8c9560A_interruptMask(cy8c9560A_config *data, char value){ 
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);  
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address); 
    i2c_smbus_write_word_data(data->master_fd,0x19,value);
 }

/*
cy8c9560A_resistivePullUp(): 
   This function is used to configure pin(s) with pull up feature. 
  1Dh:  
    resistive pull up[7:0]    1: Enables resistive high, strong low feature on pin.
                              0: Disables resistive high, strong low feature on pin.
*/ 
void cy8c9560A_resistivePullUp(cy8c9560A_config *data, char value){ 
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel); 
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address); 
    i2c_smbus_write_word_data(data->master_fd,0x1D,value);
 }
 
/*
cy8c9560A_resistivePullDown(): 
   This function is used to configure pin(s) with pull down feature. 
  1Eh:  
    resistive pull down[7:0]    1: Enables resistive low, strong high feature on pin.
                                0: Disables resistive low, strong high feature on pin.
*/  
void cy8c9560A_resistivePullDown(cy8c9560A_config *data, char value){ 
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);
    i2c_smbus_write_word_data(data->master_fd,0x1E,value);
 } 
 
/*
cy8c9560A_openDrainHigh(): 
   This function is used to configure pin(s) with open drain high feature. 
  1Fh:  
    open drain high[7:0]    1: Enables slow strong high, high z low feature on pin.
                            0: Disables slow strong high, high z low feature on pin.
*/ 
void cy8c9560A_openDrainHigh(cy8c9560A_config *data, char value){
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel); 
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);   
    i2c_smbus_write_word_data(data->master_fd,0x1F,value);
 } 
 
/*
cy8c9560A_openDrainLow(): 
   This function is used to configure pin(s) with open drain low feature. 
  20h:  
    open drain low[7:0]    1: Enables slow strong low, high z high feature on pin.
                           0: Disables slow strong low, high z high feature on pin.
*/  
void cy8c9560A_openDrainLow(cy8c9560A_config *data, char value){  
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel); 
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);       
    i2c_smbus_write_word_data(data->master_fd,0x20,value);
}

/*
cy8c9560A_strongDrive(): 
   This function is used to configure pin(s) with strong drive feature. 
  21h:  
    strong drive[7:0]    1: Enables  strong high, strong low, fast output feature on pin.
                         0: Disables strong high, strong low, fast output feature on pin.
*/ 
void cy8c9560A_strongDrive(cy8c9560A_config *data, char value){
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);   
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);   
    i2c_smbus_write_word_data(data->master_fd,0x21,value);
 } 
 
 /*
cy8c9560A_slowStrongDrive(): 
   This function is used to configure pin(s) with slow strong drive feature. 
  22h:  
    slow strong drive[7:0]    1: Enables  strong high, strong low, slow output feature on pin.
                              0: Disables disables  strong high, strong low, slow output feature on pin.
*/ 
 void cy8c9560A_slowStrongDrive(cy8c9560A_config *data, char value){
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);   
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);   
    i2c_smbus_write_word_data(data->master_fd,0x22,value);
 } 
 
  /*
cy8c9560A_slowStrongDrive(): 
   This function is used to configure pin(s) with high impedance feature. 
  22h:  
    high impedance[7:0]    1: Enables  high impedance feature on pin.
                           0: Disables high impedance feature on pin.
*/ 
 void cy8c9560A_highImpedance(cy8c9560A_config *data, char value){
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel); 
    ioctl(data->master_fd,I2C_SLAVE,data->cy8c9560A_address);       
    i2c_smbus_write_word_data(data->master_fd,0x23,value);
 }  

 

 

 

 

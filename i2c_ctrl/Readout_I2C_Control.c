#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include "si5395.h"
#include <i2c/smbus.h>

#include "ad5675_struct.h"
#include "ad5675.h"
#include "tmp112.h"
#include "tmp112_init.h"
#include "cy8c9560A.h"
#include "cy8c9560A_init.h"
#include "tof_iic_init.h"
//#include "lis3mdltr.h"
//#include "lis3mdltr_init.h"
#define I2C_SLAVE 0x0703
#define iic_device_file "/dev/i2c-0"

#define RF_SWITCH 0


int fd;
int temperature_buffer;

/*
        Readout I2C Control Software:

        RF Switch Control:
         To control switch, change value for RF_SWITCH
          0: RFC = OFF  (No Connection)
          1: RFC -> RF1 (Timing calibration input: 250 mVpp 25 MHz sinusoid)
          2: RFC -> RF2 (SMA RF input)

        DAC Channels:
          0x0: Vout -
          0x1: Vout +
          0X2: ROFS
          0X3: THS4509 Common Voltage
          0X4: DRS BIAS
*/

void configure_si5395(int fd, iic_addresses *data);

  ad5675_config ad5675 = {
    .master_fd = 0x00,
    .iic_mux_address = 0x00,
    .iic_mux_channel = 0x00,
    .ad5675_address = 0x00,
};
       
     
int main(){
	printf("Updated software\n\r");
	fd = open(iic_device_file , O_RDWR);

 	cy8c9560A_ReadoutConfig(fd,&cy8c9560A,&iicMux_u63);

	config_ad5674(fd,&ad5675, &iicMux_u63);

	tmp112_init(fd,&tmp112_DRS4,  &iicMux_u64, 0x02);
//	tmp112_init(fd,&tmp112_si5395,&iicMux_u63, 0x00);
//	tmp112_init(fd,&tmp112_ADC,   &iicMux_u63, 0x04); 
	
	printf("GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));
//	 si5395_oe_rst(&cy8c9560A,0,1);

	 hmc849(&cy8c9560A,0,RF_SWITCH);
	 hmc849(&cy8c9560A,1,RF_SWITCH);
	 hmc849(&cy8c9560A,2,RF_SWITCH);
	 hmc849(&cy8c9560A,3,RF_SWITCH);
	 hmc849(&cy8c9560A,4,RF_SWITCH);
	 hmc849(&cy8c9560A,5,RF_SWITCH);
	 hmc849(&cy8c9560A,6,RF_SWITCH);
	 hmc849(&cy8c9560A,7,RF_SWITCH);
	 hmc849(&cy8c9560A,8,RF_SWITCH);

	 si5395_oe_rst(&cy8c9560A,0,1);

//  Uncomment lines below to enabled timing calibration outputs. Default: OFF
//	 tca_clk_0to5_en(&cy8c9560A,1);
//	 tca_clk_6to8_en(&cy8c9560A,1);

	configure_si5395(fd, &iicMux_u63);

	ad5675_reset(&cy8c9560A,1);
 	ltc6269_shutdown(&cy8c9560A,1);

//	DAC settings
//	Next few lines will configure the DAC outputs for DRS4/analog front end
//	The AD5675 is a 16-bit DAC with range 0 to 2.048 V
//	Decimal step size is: 0.00003125 V / integer

//	DRS4 analog input offset/bias: IN+_OFS, IN-_OFS
	write_to_update(&ad5675,0x00, 25600); //0.8V
	write_to_update(&ad5675,0x01, 25600); //0.8V 
	 
//	DRS ROFS 1V, 1.6V max
	 write_to_update(&ad5675,0x02, 49792);  //1.556

//	THS4509 common mode voltage: V_CM
//	For +3.5 V and -1.5 V split supply, half range is 1 V
	write_to_update(&ad5675,0x03, 32000);
	 
//	DRS BIAS 0.7V	
	write_to_update(&ad5675,0x04, 22400);

	/* Retrieve Temperature sensors */
	temperature_buffer = tmp112_getTemperature(&tmp112_DRS4);
	printf("DRS Temperature %d\n\r",temperature_buffer);
	temperature_buffer = tmp112_getTemperature(&tmp112_si5395);
	printf("Si5395 Temperature %d\n\r",temperature_buffer);
	temperature_buffer = tmp112_getTemperature(&tmp112_ADC);
	printf("ADC Temperature %d\n\r",temperature_buffer);

	sleep(2);

}

/*
 Write into Silicon Laboratories' Si5395 Clock Synthesizer registers.

*/

void configure_si5395(int fd, iic_addresses *data){
  int index = 0;
  int page = 0;
  int reg = 0;
  time_t timer;
  char *timer_string;
  struct timeval start,stop;
  // Vars to hold values for some status regs

  int internal_stat = 0;
  // 1'b0 = SYSINCAL: 1 if the device is calibrating
  // 1'b1 = LOSXAXB: 1 is there is no signal at the XAXB pins
  // 1'b3 = XAXB_ERR: 1 if there is a problem locking to the XAXB input signal
  // 1'b5 = SMBUS_TIMEOUT: 1 if there is an SMBus timeout error

  int holdover_lol_stat = 0;
  // 1'b1 = LOL: 1 if the DSPLL is out of lock
  // 1'b5 = HOLD: 1 if the DSPLL is in holdover (or free run)

  timer = time(NULL);
  double timer_val;
  //Select iic mux channel for clock synthesizer
  ioctl(fd,I2C_SLAVE,data->pca9548abs_iic_mux);
  i2c_smbus_write_byte(fd, 0x02);
  ioctl(fd,I2C_SLAVE,data->si5395_clock_synth);
  //iterate through 2d array
  printf("Configuring Si5395...\n\r");
  printf("Writing %i registers\n\r", SI5395_REVA_REG_CONFIG_NUM_REGS);
  while( index < SI5395_REVA_REG_CONFIG_NUM_REGS ){
    page = ((si5395_reva_registers + index)->address)>>8;
    reg = ((si5395_reva_registers + index)->address)&0x00FF;
    i2c_smbus_write_byte_data(fd,0x01,page);
    i2c_smbus_write_byte_data(fd,reg,(si5395_reva_registers + index)->value);
   // printf("Page: 0x%x  ",page);
   // printf("reg: 0x%x  ",reg);
   // printf("| 0x%x\n\r", (si5395_reva_registers + index)->value);

   index++;
    if(index == 2){
      printf("Implement a delay for 300ms\n\r");
      timer_string = ctime(&timer);
      printf("%s\n\r",timer_string);
      gettimeofday(&start,NULL);
      usleep(300000);//sleep for 300ms
      timer_string = ctime(&timer);
      printf("%s\n\r",timer_string);
      gettimeofday(&stop,NULL);
     }
   }

   printf("Done..\n\r");
   printf("took %lu us\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

  // Check status regs
  printf("Checking Si5395 status...\n");
  printf("-------------------------\n");
  internal_stat = i2c_smbus_read_byte_data(fd, 0x000C);
  printf("Internal status bits: %i\n", internal_stat);
  holdover_lol_stat = i2c_smbus_read_byte_data(fd, 0x000E);
  printf("Holdover and LOL status: %i\n", holdover_lol_stat);
  printf("-------------------------\n");
 }

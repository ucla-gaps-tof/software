#ifndef CY8C9560A_STRUCT_H
#define CY8C9560A_STRUCT_H

typedef struct{
	char master_fd;
	char iic_mux_address;
	char iic_mux_channel;
	char cy8c9560A_address;
	char gp0;
	char gp1;
	char gp2;
	char gp3;
	char gp4;
	char gp5;
	char gp6;
	char gp7;
 
}cy8c9560A_config;


#endif
 
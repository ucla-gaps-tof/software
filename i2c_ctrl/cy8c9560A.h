#include "cy8c9560A_struct.h"
#include "tof_iic.h"

void cy8c9560A_ReadoutConfig(int fd,cy8c9560A_config *data,iic_addresses *base);



///////////////////////////////////////////////////////////////////////////////////////////
//LTC6269 OP-Amp
//////////////////////////////////////////////////////////////////////////////////////////
void ltc6269_shutdown(cy8c9560A_config *data,char shutdown);



///////////////////////////////////////////////////////////////////////////////////////////
//AD5675 16 bit DAC
///////////////////////////////////////////////////////////////////////////////////////////

void ad5675_reset(cy8c9560A_config *data,char reset);


void si5395_fdec_finc(cy8c9560A_config *data,char fdec,char finc);
/*
si5395: Output Enable and Reset are both active low inputs. Drive a logic 0 to enable the feature. 
*/
void si5395_oe_rst(cy8c9560A_config *data,char oe,char rst);

///////////////////////////////////////////////////////////////////////////////////////////
//Si53362(TCA Clock) GPIO Expander Interface
///////////////////////////////////////////////////////////////////////////////////////////

void tca_clk_6to8_en(cy8c9560A_config *data,char enable);

void tca_clk_0to5_en(cy8c9560A_config *data,char enable);



///////////////////////////////////////////////////////////////////////////////////////////
//HMC849(RF Switch) GPIO Expander Interface
///////////////////////////////////////////////////////////////////////////////////////////

/*
  HMC849 Truth Table:
 |_VCTL__|__EN__|  |_RFC -> RF1_|_RFC -> RF2_|
    0    |   0           OFF    |     ON
    1    |   0           ON           OFF
    0    |   1           OFF          OFF
    1    |   1           OFF          OFF
    
    hmcChannel: 0 - 8
    mode: 0: RFC = OFF  (No Connection)
          1: RFC -> RF1 (RF Input )
          2: RFC -> RF2 (TCA Calibration input)

*/
void hmc849(cy8c9560A_config *data, char hmcChannel, int mode); 

///////////////////////////////////////////////////////////////////////////////////////////
//GPIO Expander Functions
///////////////////////////////////////////////////////////////////////////////////////////

/*
cy8c9560A_getDeviceId():
   This function returns the Device ID. By default during startup, the device attempts
   to load the user default block. If corrupted, the factory defaults are loaded and low nibble 
   is set to indicate factory defaults.
  
  2Eh:
      Device Family[7:4] 2 = CY8C9520A, 4 = CY8C9540A, 6 = CY8C9560A
      Reserved [3:1]
      FD/~UD[0]          1 = Factory Default , 0 = User defaults
*/
char cy8c9560A_getDeviceId(cy8c9560A_config *data);
 
 /*
cy8c9560A_portSelect():
   This function configures the GPort. It is used to select the port to configure registers
   19h - 23h. Write a value of 0 - 7 to select a port. 

  18h:
      port select[7:0] 7 = port7, ........ ,0 = port 0
 */
void cy8c9560A_portSelect(cy8c9560A_config *data, char value);
 /*
cy8c9560A_setPinDirection():
   This function tri-states the direction of the pins of a port. 
  1Ch:
      pin direction[7:0]    1: Assigns pin as an input.
                            0: Assigns pin as an output.
  Example: 
     port select[7:0] == 0x00 //port 0 is selected
     pin direction[7:0] == 0xFE //pins GP0[7:1] 
     
     pins GP0[7:1] are configured as inputs. 
     pin GP0[0] is configured as an output. 
     
*/ 
void cy8c9560A_setPinDirection(cy8c9560A_config *data, char value);
 
/*
cy8c9560A_getInputPort(): 
  00h - 07h:
    This function returns a level driven on input ports. Direction of port is set 
    with Pin Direction register 1Ch.    
*/
char cy8c9560A_getInputPort(cy8c9560A_config *data, char port);
/*
cy8c9560A_setOutputPort(): 
   This function is used to write data to GPIO ports.
  08h:  
    output port[7:0]    1: Assigns pin a logic 1 on pin.
                        0: Assigns pin a logic low on pin.
*/ 
void cy8c9560A_setOutputPort(cy8c9560A_config *data, char port, char value);

/*
cy8c9560A_interruptMask(): 
   This function is used to enable or disable the INT line when GPIO input levels are changed. 
  19h:  
    interrupt mask[7:0]    1: Disables interrupt.
                           0: Enables interrupt.
*/ 
void cy8c9560A_interruptMask(cy8c9560A_config *data, char value);
/*
cy8c9560A_resistivePullUp(): 
   This function is used to configure pin(s) with pull up feature. 
  1Dh:  
    resistive pull up[7:0]    1: Enables resistive high, strong low feature on pin.
                              0: Disables resistive high, strong low feature on pin.
*/ 
void cy8c9560A_resistivePullUp(cy8c9560A_config *data, char value);
 
/*
cy8c9560A_resistivePullDown(): 
   This function is used to configure pin(s) with pull down feature. 
  1Eh:  
    resistive pull down[7:0]    1: Enables resistive low, strong high feature on pin.
                                0: Disables resistive low, strong high feature on pin.
*/  
void cy8c9560A_resistivePullDown(cy8c9560A_config *data, char value);
 
/*
cy8c9560A_openDrainHigh(): 
   This function is used to configure pin(s) with open drain high feature. 
  1Fh:  
    open drain high[7:0]    1: Enables slow strong high, high z low feature on pin.
                            0: Disables slow strong high, high z low feature on pin.
*/ 
void cy8c9560A_openDrainHigh(cy8c9560A_config *data, char value); 
 
/*
cy8c9560A_openDrainLow(): 
   This function is used to configure pin(s) with open drain low feature. 
  20h:  
    open drain low[7:0]    1: Enables slow strong low, high z high feature on pin.
                           0: Disables slow strong low, high z high feature on pin.
*/  
void cy8c9560A_openDrainLow(cy8c9560A_config *data, char value); 

/*
cy8c9560A_strongDrive(): 
   This function is used to configure pin(s) with strong drive feature. 
  21h:  
    strong drive[7:0]    1: Enables  strong high, strong low, fast output feature on pin.
                         0: Disables strong high, strong low, fast output feature on pin.
*/ 
void cy8c9560A_strongDrive(cy8c9560A_config *data, char value);
 
 /*
cy8c9560A_slowStrongDrive(): 
   This function is used to configure pin(s) with slow strong drive feature. 
  22h:  
    slow strong drive[7:0]    1: Enables  strong high, strong low, slow output feature on pin.
                              0: Disables disables  strong high, strong low, slow output feature on pin.
*/ 
void cy8c9560A_slowStrongDrive(cy8c9560A_config *data, char value); 
  
  /*
cy8c9560A_slowStrongDrive(): 
   This function is used to configure pin(s) with high impedance feature. 
  22h:  
    high impedance[7:0]    1: Enables  high impedance feature on pin.
                           0: Disables high impedance feature on pin.
*/ 
void cy8c9560A_highImpedance(cy8c9560A_config *data, char value);

#include <linux/i2c-dev.h>
#include <stdio.h>
#include <sys/ioctl.h>  
#include <unistd.h>
#include <stdint.h>
#include "ad5675_struct.h"
#include "ad5675.h"
#include "tof_iic.h"
#include <i2c/smbus.h>
 
/*
AD5675



*/

 
/*
Register 
0000 :  No operation
0001 : Write to input register
0010 : update dac register n with contents of input register
0011 : Write to and update DAC Channel n.
0100 : Power down/power up the DAC
0101 : Hardware LDAC mask register.
0110 : Software reset.
0111 : Gain setup register.
1000 : RSVD
1001 : Set up the readback register.
1010 : Update all channels of the input register 
1011 : Update all channels of the DAC register and input register 
       with the input data. 
1100 : RSVD
1101 : RSVD
1110 : RSVD
1111 : RSVD 
*/




/* ____________________________________________________________
  |__COMMAND__|__DAC ADDRESS__|__DAC DATA MS__|__DAC DATA LSB__| 
*/
void config_ad5674(int fd, ad5675_config *data, iic_addresses *base){
	
	data-> master_fd = fd;
	data -> iic_mux_address = base-> pca9548abs_iic_mux;
	data-> iic_mux_channel = 0x04; //channel 2 
	data-> ad5675_address = base -> ad5675bcpz_dac;
	 
}




void write_to_update(ad5675_config *data, int dac_channel, uint16_t value ){
    uint16_t buffer;
    //buffer = ( value&0xFF00 >> 8) + (value&0x00FF << 8);

    buffer = (value&0xFF00)>>8;
    buffer = buffer | (value&0x00FF)<<8;


    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);   
    ioctl(data->master_fd,I2C_SLAVE,data->ad5675_address);   
    i2c_smbus_write_word_data(data->master_fd,0x30+dac_channel,buffer);
}

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>  
#include <unistd.h>
#include <stdint.h>
#include "tof_iic.h"
#include "tmp112_struct.h"
#include "tmp112.h"
#include <stdio.h>
#include <math.h>
#include <i2c/smbus.h>

/////////////////////////////////////////////////////////////////
//Temperature Sensors
/////////////////////////////////////////////////////////////////

/*
TMP112 Registers: 
		  temperature_register (read)          : 0x00
		  configuaration_register (read/write) : 0x01		  
		  tlow_register (read/write)           : 0x02
		  thigh_register (read/write)          : 0x03
*/

void tmp112_init(int fd ,tmp112_config *data,iic_addresses *base, char mux_channel){
     mux_channel = 0x01 << mux_channel; 
	if(base->pca9548abs_iic_mux == 0x77){
      switch(mux_channel){
        case 0x01:
         data-> tmp112_address = base->si5395_clock_synth_tempSense;
         break;
        case 0x10:
        data-> tmp112_address = base->ad9245_adc_tempSense;
        break;
      }
    }else{
        data-> tmp112_address = base->drs4_tempSense;       
    }
	data->master_fd = fd;
    data->iic_mux_address = base->pca9548abs_iic_mux;
    data->iic_mux_channel = mux_channel;
 
    //Data is converted for temp112 registers
    tmp112_configuration(data);
    convertTemp(data);
    tmp112_setHighTemp(data);
    tmp112_setLowTemp(data);
}


/*
Configuration Register: 
MSB: OS , R1, R0,F1,F0,POL,TM,SD
LSB: CR1, CR0,AL,EM,0, 0,  0, 0

Shutdown Mode(SD): Shuts down IC to 0.5uA.
                   1: Enable
                   0: Disable
Thermostat Mode(TM): 
                   1: Interrupt Mode
                   0: Comparator Mode
Polarity(POL): Sets polarity of Alert pin output.
                   1: Active high
                   0: Active low
Fault Queue(F1/F0): Fault condition exists when measured data exceeds Thigh and Tlow. 
                   00: 1 fault
                   01: 2 faults
                   10: 4 faults
                   11: 6 faults
Converter Resolution(R1/R0): Converter Resolution Read-only buts 
One-shot(OS): In shutdown mode, writing a 1 to this bit begins a single temperature conversion. 

Extended Mode(EM): 
                   1: Extended Mode: Allows measurements above 128 Celsius
                   0: Normal Mode:   Uses 12 bit format
Alert(AL): The AL bit is a read-only function.
            This function is dependent on POL 
                   1: Temperature is below Tlow
                   0: Temperature is above Tlow
Continuous-Conversion Mode(CR1/CR0):
                                    00: 0.25Hz
                                    01: 1 Hz
                                    10: 4 Hz(default)
                                    11: 8 Hz
*/
void tmp112_configuration(tmp112_config *data){
    uint16_t buff;
    
    buff = ((data->Continuous_Conversion_Mode<<6)|(data->Extended_Mode<<4)|(0<<3)|(0<<2)|(0<<1)|(0<<0))<<8;
    buff =  buff | ((data->One_shot<<7)|(data->Fault_Queue<<3)|(data->Polarity<<2)|
              (data->Thermostat_Mode<<1)|(data->Shutdown_Mode<<0))&0x00FF;
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->tmp112_address); 
    i2c_smbus_write_word_data(data->master_fd,0x01,buff);
}

/*
convertTemp(tmp112_config *data):
  This function converts the temperature value from decimal to Celsius value.
  .0625 celcius per bit 
*/
void convertTemp(tmp112_config *data){
    data->tmp112_tHigh = data->tmp112_tHigh/(0.0625);
    data->tmp112_tLow = ((~data->tmp112_tLow+1)/(0.0625)); 
    data->tmp112_tLow = (data->tmp112_tLow)&0xFFF;
}

/*
  tmp112_getTemperature(tmp112_config *data):
  This function returns the temperature with 0.0635 Celsius resolution per bit.
*/
uint16_t tmp112_getTemperature(tmp112_config *data){
    uint16_t temperature_data;
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->tmp112_address); 
    temperature_data = i2c_smbus_read_word_data(data->master_fd, 0x00);
    temperature_data = (temperature_data&0xFF00)>>8 | (temperature_data&0x00FF)<<8;
    return temperature_data >> 4;
}

/*
  tmp112_setLowTemp(tmp112_config *data):
  This function sets the lower threshold value to trigger alert pin.
*/
void tmp112_setLowTemp(tmp112_config *data){
    uint16_t buff;
    buff = (data->tmp112_tLow&0xFF00)>>8;
    buff = buff | (data->tmp112_tLow&0x00FF)<<8;
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->tmp112_address); 
    i2c_smbus_write_word_data(data->master_fd,0x02,buff);
 }
 
/*
  tmp112_setHighTemp(tmp112_config *data):
  This function sets the high threshold value to trigger alert pin.
*/
void tmp112_setHighTemp(tmp112_config *data){
    uint16_t buff;
    buff = (data->tmp112_tHigh&0xFF00)>>8;
    buff = buff | (data->tmp112_tHigh&0x00FF)<<8;
    ioctl(data->master_fd,I2C_SLAVE,data->iic_mux_address);
    i2c_smbus_write_byte(data->master_fd, data->iic_mux_channel);
    ioctl(data->master_fd,I2C_SLAVE,data->tmp112_address); 
    i2c_smbus_write_word_data(data->master_fd,0x03,buff);
 }
  


#ifndef AD5675_H
#define AD5675_H

#include "ad5675_struct.h"
#include "tof_iic.h"
#include <stdint.h>

void config_ad5674(int fd, ad5675_config *data, iic_addresses *base);
void write_to_update(ad5675_config *data, int dac_channel, uint16_t value );

#endif

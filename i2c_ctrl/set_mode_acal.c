#include <assert.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "si5395.h"
#include <i2c/smbus.h>
#include <string.h>

#include "ad5675_struct.h"
#include "ad5675.h"
#include "cy8c9560A.h"
#include "tof_iic_init.h"
#include "filelock.h"
#define I2C_SLAVE 0x0703
#define iic_device_file "/dev/i2c-0"

int fd;
int temperature_buffer;
int RF_SWITCH = 0;

/*
  set_mode_acal

  RF Switch Control:
    To control switch, change value for RF_SWITCH
    0: RFC = OFF  (No Connection)
    1: RFC -> RF1 (Timing calibration input: 250 mVpp 25 MHz sinusoid)
    2: RFC -> RF2 (SMA RF input)

  DAC Channels:
    0x0: Vout -
    0x1: Vout +
    0X2: ROFS
    0X3: THS4509 Common Voltage
    0X4: DRS BIAS

  Notes:
      Must be run after successful initialization. Does not configure Si5395. Only reconfigures DAC outputs for IN+-_OFS.
*/

ad5675_config ad5675 = {
  .master_fd = 0x00,
  .iic_mux_address = 0x00,
  .iic_mux_channel = 0x00,
  .ad5675_address = 0x00,
};

// This struct does not require reconfiguring Si5395_RST
// When this program is called the board should be initialized and Si5395 running
// We do not want to reset that part since the PL is relying on the 33 MHz clock.
cy8c9560A_config cy8c9560A= { 
  .master_fd = 0x00,
  .iic_mux_address = 0x00,
  .iic_mux_channel = 0x00,
  .cy8c9560A_address = 0x00,
  .gp0 = 0x00,
  .gp1 = 0x00,
  .gp2 = 0x00,
  .gp3 = 0x00,
  .gp4 = 0x00,
  .gp5 = 0x00,
  .gp6 = 0x00,
  .gp7 = 0x00
};

int main(int argc, char **argv){

  // Get execution time
  time_t start_t = time(NULL);
  struct tm *tm_start = localtime(&start_t);
  char start_t_str[80];
  assert(strftime(start_t_str, sizeof(start_t_str), "%c", tm_start));

  // Log info the ../bin/set_mode_acal_log.txt
  FILE *acal_log = fopen("../bin/set_mode_acal_log.txt", "a");
  if (acal_log == NULL)
  {
    printf("[ERROR] (set_mode_acal.c, main): Could not open file\n");
  }

  fprintf(acal_log, "\n===================================\n");
  fprintf(acal_log, "Begin new set_mode_acal.c entry\n");
  fprintf(acal_log, "%s\n", start_t_str);

  // Do not step on other process
  int lock_rv;
  lock_rv = tryGetLock(LOCK_FILE);
  if(lock_rv < 0){
    fprintf(stderr, "Collision with other process, abort\n");
    fprintf(acal_log, "Collision with other process, abort\n");
    abort();
  }

  // DAC defaults
  // IN_OFS
  int inofsp = 25600; // 0.8V
  int inofsn = 25600; // 0.8V
  // DRS ROFS, 1.6V Max
  int drsrofs = 49792; // 1.556V
  // THS4509 common mode voltage: V_CM
  int vcm = 32000; // 1V
  // DRS Bias
  int drsbias = 22400; // 0.7V
  // Max range of DAC
  int maxadc = 65536;

  // arg parse vars
  int c;
  opterr = 0;
  int index;
  char *tmpptr;
  int tmplen;

  while ((c = getopt (argc, argv, "p:n:r:c:b:")) != -1)
  switch (c)
  {
    case 'p':
      // Input sanitation
      inofsp = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(inofsp > maxadc || inofsp < 0 || tmplen > 0){
        fprintf (stderr, "Invalid inofsp input\n");
        fprintf (acal_log, "Invalid inofsp input\n");
        return 1;
      }
      break;
    case 'n':
      inofsn = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(inofsn > maxadc || inofsn < 0 || tmplen > 0){
        fprintf (stderr, "Invalid inofsn input\n");
        fprintf (acal_log, "Invalid inofsn input\n");
        return 1;
      }
      break;
    case 'r':
      drsrofs = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(drsrofs > maxadc || drsrofs < 0 || tmplen > 0){
        fprintf (stderr, "Invalid drsrofs input\n");
        fprintf (acal_log, "Invalid drsrofs input\n");
        return 1;
      }
      break;
    case 'c':
      vcm = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(vcm > maxadc || vcm < 0 || tmplen > 0){
        fprintf (stderr, "Invalid vcm input\n");
        fprintf (acal_log, "Invalid vcm input\n");
        return 1;
      }
      break;
    case 'b':
      drsbias = strtol(optarg, &tmpptr, 10);
      tmplen = strlen(tmpptr);
      if(drsbias > maxadc || drsbias < 0 || tmplen > 0){
        fprintf (stderr, "Invalid drsbias input\n");
        fprintf (acal_log, "Invalid drsbias input\n");
        return 1;
      }
      break;        
    case '?':
      if (optopt == 'p' || optopt == 'n' || optopt == 'r' || optopt == 'c' || optopt == 'b')
        fprintf (stderr, "Option -%c requires an argument.\n", optopt);
      else if (isprint (optopt))
        fprintf (stderr, "Unknown option `-%c'.\n", optopt);
      else
        fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
      return 1;
    default:
      abort ();
  }

  // Debug
  printf ("inofsp = %d, inofsn = %d, drsrofs = %d, vcm = %d , drsbias = %d\n",
          inofsp, inofsn, drsrofs, vcm, drsbias);

  // Write params to log
  fprintf(acal_log, "inofsp = %d\ninofsn = %d\ndrsrofs = %d\nvcm = %d\ndrsbias = %d\n",
          inofsp, inofsn, drsrofs, vcm, drsbias);

  for (index = optind; index < argc; index++)
  printf ("Non-option argument %s\n", argv[index]);

  printf("Configuring peripherals: DRS4 amplitude calibration\n");
  fprintf(acal_log, "Configuring peripherals: DRS4 amplitude calibration\n");
  fd = open(iic_device_file , O_RDWR);

  cy8c9560A_ReadoutConfig(fd,&cy8c9560A,&iicMux_u63);

  config_ad5674(fd,&ad5675, &iicMux_u63);

  printf("GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));
  fprintf(acal_log, "GPIO Expander DEVICE ID: 0x%x\n\r",cy8c9560A_getDeviceId(&cy8c9560A));

  hmc849(&cy8c9560A,0,RF_SWITCH);
  hmc849(&cy8c9560A,1,RF_SWITCH);
  hmc849(&cy8c9560A,2,RF_SWITCH);
  hmc849(&cy8c9560A,3,RF_SWITCH);
  hmc849(&cy8c9560A,4,RF_SWITCH);
  hmc849(&cy8c9560A,5,RF_SWITCH);
  hmc849(&cy8c9560A,6,RF_SWITCH);
  hmc849(&cy8c9560A,7,RF_SWITCH);
  hmc849(&cy8c9560A,8,RF_SWITCH);

  ad5675_reset(&cy8c9560A,1);

  // Enable Si5395 outputs
  si5395_oe_rst(&cy8c9560A,0,1);

  //  DAC settings
  //  Next few lines will configure the DAC outputs for DRS4/analog front end
  //  The AD5675 is a 16-bit DAC with range 0 to 2.048 V
  //  Decimal step size is: 0.00003125 V / integer

  float dac_lsb = 0.00003125;
  float tmp_float;

  printf("\n\nAD5675 Set points\n");
  fprintf(acal_log, "\n\nAD5675 Set points\n");
  //  DRS4 analog input offset/bias: IN+_OFS, IN-_OFS
  write_to_update(&ad5675,0x00, inofsp); //0.8V

  tmp_float = dac_lsb * inofsp;
  printf("IN+_OFS: %d = %.2f\n", inofsp, tmp_float);
  fprintf(acal_log, "IN+_OFS: %d = %.2f\n", inofsp, tmp_float);

  write_to_update(&ad5675,0x01, inofsn); //0.8V 

  tmp_float = dac_lsb * inofsn;
  printf("IN-_OFS: %d = %.2f\n", inofsn, tmp_float);
  fprintf(acal_log, "IN-_OFS: %d = %.2f\n", inofsn, tmp_float);

  //  DRS ROFS 1V, 1.6V max
  write_to_update(&ad5675,0x02, drsrofs);  //1.556 v

  tmp_float = dac_lsb * drsrofs;
  printf("ROFS: %d = %.2f\n", drsrofs, tmp_float);
  fprintf(acal_log, "ROFS: %d = %.2f\n", drsrofs, tmp_float);

  //  THS4509 common mode voltage: V_CM
  //  For +3.5 V and -1.5 V split supply, half range is 1 V
  write_to_update(&ad5675,0x03, vcm);

  tmp_float = dac_lsb * vcm;
  printf("V_CM: %d = %.2f\n", vcm, tmp_float);
  fprintf(acal_log, "V_CM: %d = %.2f\n", vcm, tmp_float);

  //  DRS BIAS 0.7V	
  write_to_update(&ad5675,0x04, drsbias);

  tmp_float = dac_lsb * drsbias;
  printf("DRS_BIAS: %d = %.2f\n", drsbias, tmp_float);
  fprintf(acal_log, "DRS_BIAS: %d = %.2f\n", drsbias, tmp_float);

  // Get finish time
  time_t end_t = time(NULL);
  struct tm *tm_end = localtime(&end_t);
  char end_t_str[80];
  assert(strftime(end_t_str, sizeof(end_t_str), "%c", tm_end));
  fprintf(acal_log, "%s\n", end_t_str);
  fprintf(acal_log, "End set_mode_acal.c entry\n");
  fprintf(acal_log, "\n===================================\n");

  // Cleanup
  releaseLock(lock_rv, LOCK_FILE);
  close(fd);
  fclose(acal_log);

  return 0;

}
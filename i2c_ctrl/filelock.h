#ifndef FILELOCK_H
#define FILELOCK_H
 
#define LOCK_FILE ".i2c_lock"

int tryGetLock( char const *lockName );
void releaseLock( int fd, char const *lockName );
 
#endif

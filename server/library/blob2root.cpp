/* blob2root -- Convert output binary from readout boards into ROOT format
 Copyright (C) ????-2019 University of California

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Original authors: Jeffrey Zweerink, ???
 Current maintainers: Jeffrey Zweerink, James Ryan, Sean Quinn
 E-mail: zweerink@astro.ucla.edu, jryan@astro.ucla.edu, spq@ucla.edu

 Usage examples: see README */
//---------------------------------------------------------------------------
// System Headers
//---------------------------------------------------------------------------
#include <fstream>
#include <ctime>
#include <iostream>
#include <cmath>
#include <filesystem>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <TString.h>
#include <experimental/filesystem>
#include "blob2root.h"
//#include "blobroutines.h"

//---------------------------------------------------------------------------
// Unpack :: Unpacks raw drs data into ROOT trees with variables:
// tX       :: time bins
// chnX     :: voltage measurement
//---------------------------------------------------------------------------

// I have modified this program to just output the data (with no real
// processing. The only values of interest at this time are the event
// number, time and data arrays. All the code to interpolate the data
// and find pulses/charges/etc is still included but not used. JAZ

int main(int argc, char *argv[])
{
  if (argc < 2) {
    printUsage(argv[0]);
    //    return 0;
  }

  gROOT->SetStyle("Plain");
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPalette(1);

  char blobfile[MAX_BRDS][500];
  char rootfile[500];
  // Copy the arguments into the blobfile array. 
  int nboards = argc-1; // Keep track of number of boards
  for (int i=0;i<nboards;i++) 
    strcpy(blobfile[i], argv[i+1]);

  // How many boards are in the datastream
  printf("nboards = %d\n", nboards);
  if (nboards > MAX_BRDS) {
    printf("Number of boards in datastream (%d) exceeds MAX_BRDS (%d)\n.",
           nboards, (int)MAX_BRDS);
    return (1);
  }
  
  Header_t header[MAX_BRDS];
  Waveform_t waveform[MAX_BRDS][NCHN];

  Double_t t[MAX_BRDS * NCHN][1024];
  Double_t chn[MAX_BRDS * NCHN][1024] {0.0}; // fill with 0s

  unsigned int eventNum;
  unsigned long long clockCyc[MAX_BRDS];
  unsigned int triggerCells[MAX_BRDS];

  // Various strings associated with filenames. 'blobfile' contains DRS4 data
  string fnamepath = fs::path(blobfile[0]).parent_path(); // The path
  string fnamestem = fs::path(blobfile[0]).stem();        // The basename
  string fnameext  = fs::path(blobfile[0]).extension();   // The extension
  string rfilestr = fnamestem + ".root";               // Output Root file 

  // TODO: decode timestamp in blob header
  long int EventDate;
  double   EventUTC;
  int year=2021, mon=10, day=29, hr=20, min=15, sec=30, msec=500;
  
  Int_t n = 0; // Number of different events looked at.
  int spikebranch[10];

  // Open the DRS data files for processing
  FILE *df_p[MAX_BRDS];

  for (int i=0; i<nboards; i++) {
    if ((df_p[i] = fopen(blobfile[i], "rb")) == NULL) {
      cout << "Problems opening file: " << blobfile << "\n";
      return 1;
    }
  }

  // tree
  TTree *rec = new TTree("rec", "rec");
  //open tfile
  TFile *outfile = new TFile(rfilestr.c_str(), "RECREATE");

  //event branches
  rec->Branch("eventNum", &eventNum, "eventNum/I");
  //rec->Branch("clockCyc", &clockCyc, "clockCyc/I");
  //rec->Branch("EventDate", &EventDate, "EventDate/L");
  rec->Branch("EventUTC", &EventUTC, "EventUTC/D");
  //rec->Branch("year", &year, "year/s");
  //rec->Branch("mon",  &mon,  "mon/s");
  //rec->Branch("day",  &day,  "day/s");
  //rec->Branch("hr",   &hr,   "hr/s");
  //rec->Branch("min",  &min,  "min/s");
  //rec->Branch("sec",  &sec,  "sec/s");
  //rec->Branch("msec", &msec, "msec/s");
  rec->Branch("triggerCells", &triggerCells,
              Form("triggerCells[%d]/I", NCHN*nboards));
  rec->Branch("spikes", &spikebranch, "spikebranch[10]/I");

  // Make branches for all channels
  char t_ch[10], t_br[20];
  char c_ch[10], c_br[20];
  char cc_ch[10], cc_br[20];
  for (int i = 0; i < nboards; i++) {
    for (int j = 0; j < NCHN; j++) {
      sprintf(t_ch, "t%d", i * NCHN + j);
      sprintf(c_ch, "chn%d", i * NCHN + j);
      sprintf(t_br, "%s[%d]/D", t_ch, NWORDS);
      sprintf(c_br, "%s[%d]/D", c_ch, NWORDS);

      rec->Branch(t_ch,   &t[i * NCHN + j], t_br);
      rec->Branch(c_ch, &chn[i * NCHN + j], c_br);

      // fill with nominal times, in case there's no time calibration later
      for (int k=0; k<NWORDS; k++) 
        t[i * NCHN + j][k] = (float)(k)/2.0;
    }

    sprintf(cc_ch, "clCyc%d", i);
    sprintf(cc_br, "%s/l", cc_ch);

    rec->Branch(cc_ch, &clockCyc[i], cc_br);
  }

  struct BlobEvt_t event[MAX_BRDS];
  struct Times_t times;
  unsigned long long prev_time[MAX_BRDS] = {0,0,0,0,0,0};
  unsigned long long orig_time[MAX_BRDS] = {0,0,0,0,0,0};
  unsigned long long curr_time[MAX_BRDS] = {0,0,0,0,0,0};
  unsigned long long first_time[MAX_BRDS][50];
  int status[MAX_BRDS] = {0,0,0,0,0,0};
  int inevent[MAX_BRDS] = {0,0,0,0,0,0};
  int firstEvt[MAX_BRDS] = {-1,-1,-1,-1,-1,-1};
  int curr_evt[MAX_BRDS] = {0,0,0,0,0,0};
  bool print = false; // Print out specific quantities when reading events
  
  // Read the first XX event timestamps for each board
  for (int k=0;k<nboards;k++) {
    FillFirstEvents(df_p[k], k, &times);
    // Now close and reopen file
    fclose(df_p[k]);
    df_p[k] = fopen(blobfile[k], "rb");
  }
  times.nbrds = nboards;
  FindFirstEvents(&times);  
  // Also, let's find the UTC of the reference event so that we can
  // calculate the UTC for each event based on the 16.5MHz clock value
  FindUTCReference(&times);
  
  for (int k=0;k<nboards;k++) {
    int e_ctr=0;
    do { // Read to the first common event from each file
      status[k] = ReadEvent(df_p[k], &event[k], print);
      e_ctr++;
    } while (e_ctr <= times.first_evt[k]);
    //} while (e_ctr <= firstEvt[k]);
    orig_time[k] = event[k].timestamp;          // Record the start time
    times.first_evt_ID[k] = event[k].event_ctr; // Record the evt_ID
  }
  //printf("%d\n", times.first_evt_ID[0]); 

   // find relevant boards
  // TODO: change this to use event[k].id, eventually
  unsigned int boardnums[MAX_BRDS]; // maps event board number to RB#
  unsigned long long boarddnas[MAX_BRDS] = {
    77380906573213780, // 1
    9609908518406236,  // 2
    78985381379328092, // 3
    24942185850882132, // 4
    25003068095826012, // 5
    32831594097035348  // 6
  };
  for (int k=0; k<nboards; k++)
    for (int i=0;i<MAX_BRDS;i++)
      if (event[k].dna == boarddnas[i])
        boardnums[k] = i;
  // read in calibrations
  struct Calibrations_t cal[MAX_BRDS][NCHN];
  bool board_has_cal[MAX_BRDS] = { 0 }; // init all to False
  for (int k=0; k<nboards; k++) {
    string calfilename = "rb" + to_string(boardnums[k]+1) + "_cal.txt";
    fstream calfile(calfilename.c_str(), ios_base::in);
    if (calfile.fail()) {
      printf("Can't open %s - not calibrating", calfilename.c_str());
      continue;
    }
    for (int i=0; i<NCHN; i++) {
      for (int j=0; j<NWORDS; j++)
        calfile >> cal[k][i].vofs[j];
      for (int j=0; j<NWORDS; j++)
        calfile >> cal[k][i].vdip[j];
      for (int j=0; j<NWORDS; j++)
        calfile >> cal[k][i].vinc[j];
      for (int j=0; j<NWORDS; j++)
        calfile >> cal[k][i].tbin[j];
    }
    board_has_cal[k] = true;
  }


  // loop over all events in all data files
  n = 0;

  while( HaveEvents(status, nboards) ) { // At least some boards have events
    //int old_buff_status = 0; //used to create root file with only new buffer evnts when we were having issues
    for (int k=0;k<nboards;k++) {
      //printf("%12llu ", event[k].timestamp-prev_time[k]);
      if (status[k]==1) {
        prev_time[k] = event[k].timestamp;
        curr_time[k] = event[k].timestamp - orig_time[k];
        curr_evt[k] = event[k].event_ctr - times.first_evt_ID[k];
      }
      // if (curr_evt[k] < 0) old_buff_status++; //for old/new buffer issue
    }
    //if (old_buff_status != 0) { //START for old/new buffer issue
    //  int anthrcntr = 0;
    //  for (int k=0;k<nboards;k++){
    //      if (status[k] != -1 ) {
    //        status[k] = ReadEvent(df_p[k], &event[k], false);
    //	    //printf(" not counted");
    //	    if (status[k] == 0) {
    //          status[k] = 1;
    //	    }
    //        inevent[k] = 0;
    //	  }
    //      if (status[k] == -1) anthrcntr++;
    //  }
    //  if (anthrcntr == 0) continue; 
    //  }  //END for old/new buffer issue

    //printf("\n");
    // Which boards are in event
    BoardsInEvent(status, curr_evt, inevent, nboards); 
    //BoardsInEventTime(status, curr_time, inevent, nboards); 
    //printf("%llu %llu %llu\n",curr_time[0],curr_time[1],curr_time[2]);
    
    // Let's insert some arbitrary time for now. 
    /*std::tm tm;
    tm.tm_year  = year-1900;
    tm.tm_mon   = mon-1;
    tm.tm_mday  = day;
    tm.tm_hour  = hr;
    tm.tm_min   = min;
    tm.tm_sec   = sec;
    tm.tm_isdst = -1; // No daylight savings info
    EventDate = std::mktime(&tm); */
    // For now, we calculate the UTC based on the first readout board's data
    EventUTC = times.utc_ref +
      ( (double)event[0].timestamp - (double)times.time_ref[0] ) / 1.65e7;

    for (int k=0; k<nboards; k++) {
      if (inevent[k] !=1 ) {
	triggerCells[k] = -1;
	continue;
      }
      triggerCells[k] = event[k].stop_cell;
      if (board_has_cal[k]) {
        for (int i=0; i<NCHN; i++) {
	  VoltageCalibration(event[k].ch_adc[i], chn[k*NCHN+i],
			     triggerCells[k], cal[k][i]);
	  TimingCalibration(t[k*NCHN+i], triggerCells[k], cal[k][i]);
	}
      }
    }
    //  find spikes
    for (int i = 0; i < nboards; i++){
      RemoveSpikes(&chn[i*NCHN], triggerCells[i], spikebranch);
      clockCyc[i] = curr_time[i];
    }
  
    eventNum = event->event_ctr;

    //Fill Histograms
    rec->Fill();
    if (n % 500 == 0) printf("%i events processed \n", n);
    n++; // Increment our event number
    
    // Finally, for all boards that were in event, read the next event. 
    for (int k=0;k<nboards;k++) 
      if (inevent[k]==1 && status[k] != -1 ) { // status=-1 -> EOF
	status[k] = ReadEvent(df_p[k], &event[k], print);
	if (status[k] == -1 ) {
	  //printf("Found end of file; Evt %d; status = %d\n", n, status[k]);
	} else if (status[k] == 0) {
	  //printf("Found bad event %d; status = %d\n", n, status[k]);
	  status[k] = 1;
	}
	inevent[k] = 0;
      }
  }
  // Close our files
  for (int k=0;k<nboards;k++) 
    fclose(df_p[k]);
  
  // print number of events and write ROOT file
  printf("%i events processed\n", n - 1);
  rec->Write();
  
  //Close ROOTfile
  outfile->Close();

  printf("First ID: %d %d\n", times.first_evt_ID[0], times.first_evt_ID[1]); 
  //printf("Last ID:  %d %d\n", curr_evt[0], curr_evt[1]); 
  printf("Last ID:  %lu %d\n", event[0].event_ctr, curr_evt[1]); 
  printf("First Time: %llu %llu\n", times.time[0][1], times.time[1][1]); 
  printf("Last Time:  %llu %llu\n", event[0].timestamp, curr_time[1]); 
  //printf("Last Time:  %llu %llu\n", curr_time[0], curr_time[1]); 

  
  return 0;
}

int NextPressureEntry(FILE *fp, float *pvalue, long int *tvalue) {
  char timestr[50], prompt[20];
  float pval;

  if (fscanf(fp, "%s %s %f", timestr, prompt, &pval) == EOF){
    return -1;
    }
  
  //printf("Time = %s\t", timestr);
  char year[5], mon[3], day[3], hr[3], min[3], sec[3];
  strncpy(year, timestr, 4); year[4] = '\0';
  strncpy(mon,  timestr + 5, 2); mon[2] = '\0';
  strncpy(day,  timestr + 8, 2); day[2] = '\0';
  strncpy(hr,   timestr + 11, 2); hr[2] = '\0';
  strncpy(min,  timestr + 14, 2); min[2] = '\0';
  strncpy(sec,  timestr + 17, 2); sec[2] = '\0';
  
  std::tm tm;
  std::time_t PressDate;
  tm.tm_year = atoi(year) - 1900;
  tm.tm_mon  = atoi(mon) - 1;
  tm.tm_mday = atoi(day);
  tm.tm_hour = atoi(hr);
  tm.tm_min = atoi(min);
  tm.tm_sec = atof(sec);
  tm.tm_isdst = -1;  // No daylight savings info.
  PressDate = std::mktime(&tm);
  //printf("PressDate = %ld: %s %s %s %s %s %s\n", PressDate, year, mon, day, hr, min, sec); 
  *tvalue = PressDate;
  *pvalue = pval;

  return 0;
}

int NextSIPMEntry(FILE *fp, float svalue[], long int *tvalue) {
  char timestring[50], prompt[20];
  float sval[8];
  int year, mon, day, hr, min, sec;

  // Read in the timestamp info
  if (fscanf(fp, "%d %d %d %d %d %d", &year, &mon, &day, &hr, &min, &sec) == EOF){
    return -1;
  }
  
  char junk[1000];
  // Read in the temperature values
  for (int i=0; i<8; i++)
    fscanf(fp, "%f", &sval[i]);
  if (sval[7] < -40 || sval[7] > 10000 ) { // Something is wrong with this data
    //sval[7] = -1;
    sval[7] = (sval[7]-year)/10000.0;
    fscanf(fp, "%[^\n]", junk); // read to end-of-line
  }
  
  std::tm tm;
  std::time_t SIPMDate;
  tm.tm_year = year - 1900;
  tm.tm_mon  = mon - 1;
  tm.tm_mday = day;
  tm.tm_hour = hr;
  tm.tm_min  = min;
  tm.tm_sec  = sec;
  tm.tm_isdst = -1;  // No daylight savings info.
  SIPMDate = std::mktime(&tm);
  //printf("SIPMDate = %ld: %d %d %d %d %d %d %.2f\n", SIPMDate, year, mon, day, hr, min, sec, sval[7]); 
  *tvalue = SIPMDate;
  for (int i=0; i<8; i++)
    svalue[i] = sval[i];

  return 0;
}

int NextVoltEntry(FILE *fp, float vvalue[], long int *tvalue) {
  char timestring[50], prompt[20];
  float vval[8];
  int year, mon, day, hr, min, sec;

  // Read in the timestamp info
  if (fscanf(fp,"%d %d %d %d %d %d", &year, &mon, &day, &hr, &min, &sec) == EOF){
    return -1;
  }
  
  char junk[1000];
  // Read in the voltage values

  // Use next line for VBias_cmds.txt data
  //for (int i=0; i<8; i++) fscanf(fp, "%f", &vval[i]);

  // Use next 3 lines for vbias_bk.txt data
  float temperature, vbias1, vbias2, extra;
  fscanf(fp,"%f %f %f %f", &temperature, &vbias1, &vbias2, &extra);
  for (int i=0;i<8;i++) if (i<4) {vval[i]=-1;} else {vval[i]=vbias1;}
  
  if (vval[7] < -10 || vval[7] > 100 ) { // Something is wrong with this data
    //vval[7] = -1;
    vval[7] = -2.0;
    fscanf(fp, "%[^\n]", junk); // read to end-of-line
  }
  
  std::tm tm;
  std::time_t VoltDate;
  tm.tm_year = year - 1900;
  tm.tm_mon  = mon - 1;
  tm.tm_mday = day;
  tm.tm_hour = hr;
  tm.tm_min  = min;
  tm.tm_sec  = sec;
  tm.tm_isdst = -1;  // No daylight savings info.
  VoltDate = std::mktime(&tm);
  //printf("VoltDate = %ld: %d %d %d %d %d %d %.2f\n", SIPMDate, year, mon, day, hr, min, sec, sval[7]); 
  *tvalue = VoltDate;
  for (int i=0; i<8; i++)
    vvalue[i] = vval[i];

  return 0;
}

int NextThermalEntry(FILE *fp, float *thvalue, long int *tvalue) {
  char timestr[50], prompt[100], junk[500];
  float thval;

  if (fscanf(fp, "%s ", prompt)  == EOF){
    return -1;
    }

  while (strncmp(prompt,"LP",2) == 0 ) {
    fscanf(fp, "%[^\n]", junk); // read to end-of-line
    fscanf(fp, "%s ", prompt);
  }
    
  //printf("Time = %s\t", timestr);
  int yday, mon, day, hr, min, sec, j, prog, interval, status;
  float t_set, t_meas, t_i, t_f;
  fscanf(fp, "%d %d %d %d %d %d", &yday, &mon, &day, &hr, &min, &sec); 
  fscanf(fp, "%d %f %f %f %f %d %d %d", &j, &t_set, &t_meas, &t_i, &t_f,
         &prog, &interval, &status); 

  
  std::tm tm;
  std::time_t ThermalDate;
  tm.tm_year = atoi(prompt) - 1900;
  tm.tm_mon  = mon - 1;
  tm.tm_mday = day;
  tm.tm_hour = hr;
  tm.tm_min = min;
  tm.tm_sec = sec;
  tm.tm_isdst = -1;  // No daylight savings info.
  ThermalDate = std::mktime(&tm);
  //printf("ThermalDate = %ld: %s %d %d %d %d %d\n", ThermalDate, prompt, mon, day, hr, min, sec); 
  *tvalue = ThermalDate;
  *thvalue = t_meas;

  return 0;
}

void printUsage(char *prog_name) {
  printf("\nUsage:\n\n  %s DATAFILE\n\n", prog_name);
  exit (-1);
}

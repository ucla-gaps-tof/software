// Header file for blob2root

//-------------------------------------------------------------------------
// Root Headers
//-------------------------------------------------------------------------
#include <TROOT.h>
#include <TTree.h>
#include <TBranch.h>
#include <TFile.h>
#include <TStyle.h>
#include <blobroutines.h>

//-------------------------------------------------------------------------
// Constants                                                                
//-------------------------------------------------------------------------
// We use a #define statement, but NCHN must be 8 due to a number of
// hard-coded features below. It does make coding the for-loops easier
//#define MAX_BRDS 6       // How many Readout Boards
//#define NCHN 8           // How many ADC channels per board
//#define NWORDS 1024      // How many Words per channel

using namespace std;
//namespace fs = std::__fs::filesystem;
namespace fs = std::experimental::filesystem::v1::__cxx11;

//---------------------------------------------------------------------------
// Prototypes
//---------------------------------------------------------------------------

void printUsage(char *root_file);
int  NextPressureEntry(FILE *fp, float *pvalue, long int *tvalue);
int  NextSIPMEntry(FILE *fp, float svalue[], long int *tvalue);
int  NextVoltEntry(FILE *fp, float vvalue[], long int *tvalue);
int  NextThermalEntry(FILE *fp, float *thvalue, long int *tvalue);

//---------------------------------------------------------------------------
//  Header_t struct stores event headers
//---------------------------------------------------------------------------
struct Header_t
{
  char           event_header[4];
  unsigned int   serial_number;
  unsigned short year;
  unsigned short month;
  unsigned short day;
  unsigned short hour;
  unsigned short minute;
  unsigned short second;
  unsigned short millisecond;
  unsigned short trigger_cell;

  float time[NCHN][1024];
};

//---------------------------------------------------------------------------
//  Waveform_t struct holds data for 1 channels
//---------------------------------------------------------------------------
struct Waveform_t
{
  char           chn_header[4];
  unsigned short chn[1024];
};


#ifndef RBCOMMUNICATION
#define RBCOMMUNICATION

#include <iostream>
#include <vector>
#include <string.h>
#include <time.h>

#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include <string>
#include <cstdio>
#include <cerrno>
#include <zmq.h>
#include <unistd.h>

#include <TOFParam.hh>

typedef struct SOCKET_INFO {
  void *data_socket;
  char svr_socket[200];
  int svr_port;
  int size;
} sockinf_t;

using namespace std;

//!\brief Ethernet communication

class RBComm
{
public:

//!\brief Constructor
RBComm(string Eth, TOFParam * TempTOF, int TempClassId);

//!\brief Deconstructor
~RBComm();

//!\brief Start all threads
void ThreadAllStart();

//!\brief Stop all threads
void ThreadAllStop();

//!\brief Set print level
void SetPrintLevel(int PrintLevel);

//!\brief Initialize ZMQ connections
void InitializeZMQ();
  
//!\brief Process the blob from a specific RB
void ProcessBlob(char* buffer, int blob_size, int rb, bool Write);
  
//!\brief Process the blob from a specific RB
void EventToMemory(BlobEvt_t *evt, int rb);
  
//!\brief Receive data thread: check for new data; save; store in memory
void ThreadReceiveDatastream();

//!\brief Start receive data thread
void ThreadReceiveDatastreamStart();

//!\brief Stop receive data thread
void ThreadReceiveDatastreamStop();

private:
	
  TOFParam * MyTOFParam;
  string MyEth;
  
  // Stuff for ZMQ connections
  // For now, I am making the array one larger than needed since the
  // lowest RB is rb1. For flight, we will start the RBs with rb0.
  sockinf_t rb[MAX_BRDS+1]; 
  void *context;
  bool ZMQ_Initialized;
  
  int MyPrintLevel;
  
  bool MyThreadReceiveDatastreamOn;
  boost::shared_ptr<boost::thread> MyThreadReceiveDatastream;
  
  int MyClassId;
};

#endif

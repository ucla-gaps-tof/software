#ifndef RPADDLEPACKETBASE_H_INCLUDED
#define RPADDLEPACKETBASE_H_INCLUDED

/**
 * Version 1.0
 *  -> extended tail and head to 2 bytes
 */
struct RPaddlePacketBase {
  //uint32_t event_ctr;
  //unsigned char utc_timestamp[8];
  
  unsigned char paddle_id;
  unsigned short time_a;
  unsigned short time_b;
  unsigned short peak_a;
  unsigned short peak_b;
  unsigned short charge_a;
  unsigned short charge_b;
  unsigned short charge_min_i;
  unsigned short x_pos;
  unsigned short t_average;

  unsigned char ctr_etx;
};

#endif

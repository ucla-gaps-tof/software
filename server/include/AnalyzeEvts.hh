#ifndef ANALYZEEVTS
#define ANALYZEEVTS

#include <iostream>
#include <vector>
#include <string.h>
#include <time.h>

#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include <string>
#include <cstdio>
#include <cerrno>
#include <zmq.h>
#include <unistd.h>

#include <TOFParam.hh>

using namespace std;

//!\brief Ethernet communication

class AnalyzeEvts
{
public:

//!\brief Constructor
AnalyzeEvts(TOFParam * TempTOF, int TempClassId);

//!\brief Deconstructor
~AnalyzeEvts();

//!\brief Start all threads
void ThreadAllStart();

//!\brief Stop all threads
void ThreadAllStop();

//!\brief Set print level
void SetPrintLevel(int PrintLevel);

//!\brief Analyze Data thread. Check for full events; process to reduced data
void ThreadAnalyzeData();

//!\brief Start receive data thread
void ThreadAnalyzeDataStart();

//!\brief Stop receive data thread
void ThreadAnalyzeDataStop();

private:
	
TOFParam * MyTOFParam;

int MyPrintLevel;

bool MyThreadAnalyzeDataOn;
boost::shared_ptr<boost::thread> MyThreadAnalyzeData;

int MyClassId;
};

#endif

#ifndef DISK
#define DISK

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

#include "TOFParam.hh"

enum FILE_FLAG {
  FF_NORMAL, FF_DATAOPEN, FF_DATACLOSE, FF_NUM 
}; 

using namespace std;

//!\brief Save data to disk

class Disk
{
public:

//!\brief Constructor
Disk(TOFParam * TempTOF, int TempClassId);

//!\brief Deconstructor
~Disk();

//!\brief Open new output file, new file every #seconds
void OpenOutputFile(string Filename, int Seconds);

//!\brief Open new Data file
void OpenDataFile(int TmpRunno);

//!\brief Close Data file
void CloseDataFile(int TmpRunno);

//!\brief Check if time is up to open new output file
void CheckOutputFile(unsigned int i, int flag, int interval);

//!\brief Close all output files
void CloseAllOutputFiles();

//!\brief Write data to disk. Mutex checks if sending is already busy
void Write(vector<unsigned char> & Datastream);

//!\brief Write data to disk. Mutex checks if sending is already busy
void WriteEvent(vector<unsigned char> & Datastream);

//!\brief Write BRUN event
void WriteBRUN(int Time);

//!\brief Write ERUN event
void WriteERUN(int Time);

private:

int  RunNo;
int  RunFileCtr;

int  MonFileInterval;
int  DataFileInterval;

bool MyMutexWrite;

vector<ofstream*> MyFile;
vector<string> MyBaseFilename;
vector<vector<int> > MyFileSeconds;

TOFParam * MyTOFParam;
int MyClassId;
};

#endif

#ifndef SENDTOFLIGHT
#define SENDTOFLIGHT

#include <iostream>
#include <vector>
#include <string.h>
#include <time.h>

#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include <string>
#include <cstdio>
#include <cerrno>
#include <zmq.h>
#include <unistd.h>

#include <TOFParam.hh>

using namespace std;

//!\brief Ethernet communication

class SendToFlight
{
public:

//!\brief Constructor
SendToFlight(string Eth, TOFParam * TempTOF, int TempClassId);

//!\brief Deconstructor
~SendToFlight();

//!\brief Start all threads
void ThreadAllStart();

//!\brief Stop all threads
void ThreadAllStop();

//!\brief Set print level
void SetPrintLevel(int PrintLevel);

//!\brief Receive data thread: check for new data, send them to the corresponding destination and save
void ThreadSendToFlight();

//!\brief Start receive data thread
void ThreadSendToFlightStart();

//!\brief Stop receive data thread
void ThreadSendToFlightStop();

private:
	
TOFParam * MyTOFParam;
string MyEth;

int MyPrintLevel;

bool MyMutexSendDatastream;

bool MyThreadSendToFlightOn;
boost::shared_ptr<boost::thread> MyThreadSendToFlight;

int MyClassId;
};

#endif

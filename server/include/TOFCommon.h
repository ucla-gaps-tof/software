#ifndef TOFCOMMON
#define TOFCOMMON

// This include file defines anything that is needed by multiple threads. 

#include <RPaddlePacketBase.h>
#include <REventPacketBase.h>

#define MEM_EVENTS   10000
#define MAX_BRDS     6
#define NCHN         9
#define CHNTOT       MAX_BRDS * NCHN
// To calculate NPADDLE, remember that CH9 on each RB is not SiPM data
#define NPADDLE      (MAX_BRDS * (NCHN - 1))/2 
#define NWORDS       1024

enum TOFDATA_TYPE { TOFDATA_NONE,     TOFDATA_EVENT, TOFDATA_FULL,
		    TOFDATA_INTEREST, TOFDATA_ALL};

// Status of memory event
enum EVT_STAGE { EVT_NONE, EVT_BLOBPart, EVT_BLOBFull,
		 EVT_PASS1, EVT_REDUCED, EVT_SENT};

// These quantities are read in from a blob
struct BlobEvt_t
{
  unsigned short head; // Head of event marker
  unsigned short status;
  unsigned short len;
  unsigned short roi;
  unsigned long long dna;
  unsigned short fw_hash;
  unsigned short id;
  unsigned short ch_mask;
  unsigned long event_ctr;
  unsigned short dtap0;
  unsigned short dtap1;
  unsigned long long timestamp;
  unsigned short ch_head[NCHN];
  short ch_adc[NCHN][NWORDS];
  unsigned long ch_trail[NCHN];
  unsigned short stop_cell;
  unsigned long crc32;
  unsigned short tail; // End of event marker
};  

// These quantities are stored in memory for analysis
struct MemEvt_t
{
  int EvtStatus;                 // Where is processing is this event
  // Quantities directly (or quickly found) from blobs
  unsigned long EvtNumber;       // event_ctr from blobs
  bool HaveRefTime;              // Reference time found and available
  bool HaveEventTime;            // Event time found and filled
  unsigned long RefEvtCtr;       // Event_ctr for reference event
  unsigned long RefEvtSeconds;   // tof-gfp time(s) for reference event
  unsigned long RefEvtMicroSecs; // tof-gfp time(microsec) for reference event
  unsigned long long RefTimestamp; // timestampe for reference event
  unsigned long  EventTimeSec;   // Time of this event
  unsigned short EventTimeMSec;  // Time of this event
  short ADC[CHNTOT][NWORDS];       // ADC values from blob by actual channel
  unsigned long long timestamp[MAX_BRDS]; // Blob timestamp
  unsigned long long dna[MAX_BRDS];   // Blob dna
  unsigned short ch_mask[MAX_BRDS];   // Blob ch_mask
  unsigned short stop_cell[MAX_BRDS]; // Blob stop_cell
  //unsigned short roi[NBRDS];       // Blob roi

  // Quantities found from analyzing event data
  RPaddlePacketBase Paddle[NPADDLE];
  REventPacketBase  EvtData;
};

typedef struct EVENT_DATA {
  unsigned int   EventNum;

  unsigned int   EventTimeSec;   // Time of this event
  unsigned short EventTimeMSec;  // Time of this event

  unsigned long  ClockCounter;
  unsigned long  ClockSync;

  unsigned int tdc[CHNTOT];
  unsigned int adc[CHNTOT];
  unsigned int scaler[CHNTOT];

  unsigned short n_tdc;
  unsigned short n_adc;

  unsigned short tdc_overflow[CHNTOT];
  unsigned short adc_overflow[CHNTOT];
  
  unsigned int tdc_event_ctr;
  unsigned int adc_event_ctr;
} event_t;

typedef struct BRUN_DATA {
  unsigned int   RunNum;
  unsigned int   RunStartTime;
  unsigned int   FileOpenTime;
} brun_t;

typedef struct ERUN_DATA {
  unsigned int   RunNum;
  unsigned int   FileCloseTime;
} erun_t;

#endif

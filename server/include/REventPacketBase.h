#ifndef REVENTPACKET_BASE_H_INCLUDED
#define REVENTPACKET_BASE_H_INCLUDED

struct REventPacketBase {
  uint32_t event_ctr;
  uint64_t utc_timestamp;

  // reconstructed quantities
  uint16_t primary_beta;
  uint16_t primary_beta_unc;
  uint16_t primary_charge;
  uint16_t primary_charge_unc;
  uint16_t primary_outer_tof_x;
  uint16_t primary_outer_tof_y;
  uint16_t primary_outer_tof_z;
  uint16_t primary_inner_tof_x;
  uint16_t primary_inner_tof_y;
  uint16_t primary_inner_tof_z;

  unsigned char nhit_outer_tof;
  // no need to save this, can be 
  // rereated from paddle_info.size() - nhit_outer_tof
  unsigned char nhit_inner_tof;

  unsigned char trigger_info;
  unsigned char ctr_etx;

  // payload 
  //std::vector<RPaddlePacketBase*> paddle_info;
};

#endif


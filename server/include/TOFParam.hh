#ifndef TOFPARAM
#define TOFPARAM

#include <iostream>
#include <vector>
#include <list>
#include <string>

#include <TOFCommon.h>
#include <CommandDefs.hh>

using namespace std;

//!\brief TOFParam streams are stored

class TOFParam
{
public:

//!\brief Constructor
TOFParam(int TempClassId);

//!\brief Deconstructor
~TOFParam();


//!\brief When did we start the DAQ?
void  SetStartTime();
unsigned int   GetStartTimeSec()           { return StartTime[0]; }
unsigned int   GetStartTimeUSec()          { return StartTime[1]; }

//!\brief When was the most recent run started?
void  SetRunStartTime();
unsigned int   GetRunStartTime()        { return RunStartTime; }

unsigned short GetEventNumber(short type); 

void    InitializeRun(short type);

// Stuff relevant to new EventTrigs
  int     FindEvtIndex(int evt_ctr, int rb);
void    AddEventToBuffer(BlobEvt_t evt_dta);
MemEvt_t GetEventFromBuffer();
short   GetEventReadCtr()           { return EventReadCtr; }
short   GetEventWriteCtr()          { return EventWriteCtr; }
// These should be the only two methods that change the Read/Write Ctrs. 
bool    IncrementEventReadCtr();  
bool    IncrementEventWriteCtr();
bool    EventDataReady();

// For determining which type of data we should read from the buffer first.
unsigned short DataReady();
// Should we send the full event data or a compressed form?
bool           IsFullEvent();

// Ethernet communication stuff
void   SetTOFEthAddress(string address);
string GetTOFEthAddresss()   { return TOFEthAddress; }
void   SetFlightEthAddress(string address);
string GetFlightEthAddresss()   { return FlightEthAddress; }

//!\brief Set Daemon flag
void SetDaemonFlag() { NotDaemonMode = false; }
//!\brief In Daemon mode?
bool NotDaemon()    { return NotDaemonMode; } 


//!\brief Shut down the DAQ
void ShutDownDAQ() { KeepRunning = false; }
//!\brief Continue running DAQ?
bool GetRunningStatus() { return KeepRunning ; }

  // Need access to the shared memory space through this class
  MemEvt_t      EventBuffer[MEM_EVENTS];

private:

  unsigned int  StartTime[2]; // DAQ start: 0 -> seconds, 1 -> microseconds
  unsigned int  RunStartTime; // Run start: 

  int           Index;
  unsigned long EventNumber;
  unsigned long EventZero;           // event_ctr in event[0]
  unsigned long LastEvent[MAX_BRDS]; // Last blob event processed
  
  // Used to determine where to read and write data in the buffers
  unsigned short EventReadCtr;
  unsigned short EventWriteCtr;
  bool           EventCtrWrap;
  bool           IsEventData;

  unsigned int   FullEventTargetTime;
  
  // A flag used to suppress output to screen. 
  bool NotDaemonMode;
  // A flag used to shut down the DAQ -- Be CAREFUL!!
  bool KeepRunning;
  string TOFEthAddress;
  string FlightEthAddress;
  
  int MyClassId;
};

#endif

#include <AnalyzeEvts.hh>
#include "../library/blobroutines.h"

AnalyzeEvts::AnalyzeEvts(TOFParam *TempTOF, int TempClassId) {

  MyTOFParam = TempTOF;

  MyClassId = TempClassId;
  
  MyThreadAnalyzeDataOn = false;
  
  MyPrintLevel = 0;
}

//----------------------------------------------------------------------

AnalyzeEvts::~AnalyzeEvts() {
  ThreadAllStop();
}

//----------------------------------------------------------------------

void AnalyzeEvts::ThreadAllStart() {
  ThreadAnalyzeDataStart();
}

void AnalyzeEvts::ThreadAllStop() {
  ThreadAnalyzeDataStop();
}

//----------------------------------------------------------------------

void AnalyzeEvts::SetPrintLevel(int PrintLevel) {
  if(PrintLevel >= 0) MyPrintLevel = PrintLevel;
  else {
    if (MyTOFParam->NotDaemon()) cout<<"Error: AnalyzeData bad print level!"<<endl;
  }
}

//----------------------------------------------------------------------

void AnalyzeEvts::ThreadAnalyzeData() {
  // Code to find Reduced data for each event
}

//----------------------------------------------------------------------

void AnalyzeEvts::ThreadAnalyzeDataStart() {
  if(!MyThreadAnalyzeDataOn) {
    MyThreadAnalyzeDataOn = true;
    MyThreadAnalyzeData = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&AnalyzeEvts::ThreadAnalyzeData, this)));
    if (MyTOFParam->NotDaemon()) cout<<"AnalyzeData thread started!"<<endl;
  } else {
    if (MyTOFParam->NotDaemon()) cout<<"AnalyzeData thread already running!"<<endl;
  }
}

//----------------------------------------------------------------------

void AnalyzeEvts::ThreadAnalyzeDataStop() {
  
  if(MyThreadAnalyzeDataOn) {
    if(MyThreadAnalyzeData != 0) {
      MyThreadAnalyzeDataOn = false;		
      if (MyTOFParam->NotDaemon()) cout<<"AnalyzeData thread stopped!"<<endl;
    }
  } else {
    if (MyTOFParam->NotDaemon()) cout<<"AnalyzeData thread cannot be stopped because it was not running!"<<endl;
  }
}

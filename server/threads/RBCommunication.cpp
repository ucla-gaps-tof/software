#include <RBCommunication.hh>
#include "../library/blobroutines.h"

RBComm::RBComm(string Eth, TOFParam *TempTOF, int TempClassId) {

  MyEth = Eth;
  
  MyTOFParam = TempTOF;
  
  MyClassId = TempClassId;
  
  MyThreadReceiveDatastreamOn = false;
  
  MyPrintLevel = 0;
}

//----------------------------------------------------------------------

RBComm::~RBComm() {
  ThreadAllStop();
}

//----------------------------------------------------------------------

void RBComm::ThreadAllStart() {
  ThreadReceiveDatastreamStart();
}

void RBComm::ThreadAllStop() {
  ThreadReceiveDatastreamStop();
}

//----------------------------------------------------------------------

void RBComm::SetPrintLevel(int PrintLevel) {
  if(PrintLevel >= 0) MyPrintLevel = PrintLevel;
  else {
    if (MyTOFParam->NotDaemon()) cout<<"Error: RB comm bad print level!"<<endl;
  }
}

//----------------------------------------------------------------------

void RBComm::InitializeZMQ() {
  if (!ZMQ_Initialized) {

    context = zmq_ctx_new();
    
    for (int i=0; i<=6; i++) {
      rb[i].data_socket = zmq_socket(context, ZMQ_REP);
      rb[i].svr_port = 38830+i;
      sprintf(rb[i].svr_socket, "tcp://%s:%d", MyEth.c_str(), rb[i].svr_port);
      zmq_bind(rb[i].data_socket, rb[i].svr_socket);
      //printf("Socket %d = %s\n", rb[i].svr_port, rb[i].svr_socket);
    }
    ZMQ_Initialized = true;
  } else {
    cout<<"Error: ZMQ for RBs already initialized!"<<endl;
  }
}

//----------------------------------------------------------------------

void RBComm::ProcessBlob(char* buffer, int blob_size, int rb, bool Write) {

  // Open blob as a file
  FILE *mp = fmemopen(buffer, blob_size, "rb");
  struct BlobEvt_t event;
  int status = 1;
  do {
    status = ReadEvent(mp, &event, false);
    if (status == -1 ) {
      //printf("Found EOF; Evt %ld; status = %d\n",event.event_ctr, status); 
    } else if (status == 0) {
      //printf("Found bad event d; status = %d\n", status);    
      status = 1;
    } 
    // Event is good so put into memory
    //if (event.event_ctr%100==0){printf("Found evt %ld\n",event.event_ctr);}
    EventToMemory(&event, rb);
  } while (status != -1); 
  fclose(mp);
  //printf("\n");

  if (status != -1) {
    cout<<"Error: Unable to completely process blob!"<<endl;
  }
  
  if (Write) { 
    // For writing blob to disk
    char fname[500];
    FILE *fout;
    
    // Construct strings for data filename
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    sprintf(fname, "/home/gaps/gfp/rawdata/d%04d%02d%02d_%02d%02d%02d_%d.dat",
	    tm->tm_year+1900,
	    tm->tm_mon+1, tm->tm_mday, tm->tm_hour,
	    tm->tm_min, tm->tm_sec, rb);
    // Open output file and write blob to it. 
    fout = fopen(fname,"wb");
    fwrite(buffer, 1, blob_size , fout);
    fclose(fout);
    printf("Data saved to: %s\n", fname); 
  }
}

//----------------------------------------------------------------------

void RBComm::EventToMemory(BlobEvt_t *evt, int rb) {
  // First, we find the index in memory for this event
  int idx = MyTOFParam->FindEvtIndex(evt->event_ctr, rb);

  MyTOFParam->EventBuffer[idx].EvtNumber = evt->event_ctr;
  MyTOFParam->EventBuffer[idx].timestamp[rb] = evt->timestamp;
  MyTOFParam->EventBuffer[idx].dna[rb] = evt->dna;
  MyTOFParam->EventBuffer[idx].ch_mask[rb] = evt->ch_mask;
  MyTOFParam->EventBuffer[idx].stop_cell[rb] = evt->stop_cell;

  // Calculate the UTC from the reference event time for the first
  // blob that arrives. Eventually we will want a more robust way to
  // do this that either uses the same RB data all the time or
  // utilizes data from all RBs.
  
  if (MyTOFParam->EventBuffer[idx].HaveRefTime and
      !MyTOFParam->EventBuffer[idx].HaveEventTime) {

    float NewSecs;
    NewSecs =(float)(evt->timestamp-MyTOFParam->EventBuffer[idx].RefTimestamp);
    NewSecs /= 1.65e7;
    MyTOFParam->EventBuffer[idx].EventTimeSec =
      MyTOFParam->EventBuffer[idx].RefEvtSeconds + (int)NewSecs;

    float NewMSecs = NewSecs - (int)NewSecs +
      (float)MyTOFParam->EventBuffer[idx].RefEvtMicroSecs/1e6;
    if (NewMSecs > 1.0) { // Add another second
      MyTOFParam->EventBuffer[idx].EventTimeSec++;
      NewMSecs--;
    } 
    MyTOFParam->EventBuffer[idx].EventTimeMSec = (int)(NewMSecs*1e6);
  }

  // Fill the ADC values
  for (int i=0; i<NCHN; i++) { 
    for (int j=0; j<NWORDS; j++) { 
      MyTOFParam->EventBuffer[idx].ADC[i+rb*NCHN][j] = evt->ch_adc[i][j];
    }
  }
}

//----------------------------------------------------------------------

void RBComm::ThreadReceiveDatastream() {
  //sockinf_t rb[10];

  InitializeZMQ();
  
  char blob_info[20];
  char *buffer;
  size_t buff_size = strtoul("0x4000000", NULL, 0);
  buffer = (char*)malloc(buff_size);

  printf("Listening for connections ... \n");
  int blob_size;

  while (1) {
    for (int i=0; i<=6; i++) {
      while (1) {
	// Monitor the each socket to see if we have received data...
	rb[i].size = zmq_recv(rb[i].data_socket, blob_info, 20, ZMQ_DONTWAIT);
	
	if (rb[i].size != -1) {// ...read the second packet and process. 
	  // If we have data, the first packet gives the blob size...
	  blob_size = atoi(blob_info);
	  //printf("Size = %d -- %d\n", rb[i].size, blob_size);
	  // Must complete the first recv/send cycle
	  zmq_send(rb[i].data_socket, "[SVR]: Rcv'd blob_info", 100, 0);
	  // Now read the blob using the specified blob size. 
	  zmq_recv(rb[i].data_socket, buffer, blob_size, 0);
	  // Complete recv/send cycle.
	  zmq_send(rb[i].data_socket, "[SVR]: Received data", 100, 0);

	  ProcessBlob(buffer, blob_size, i, true);
	}
	else break;
      }
    }
    usleep(500000);
  }
  zmq_ctx_destroy(context);
}

//----------------------------------------------------------------------

void RBComm::ThreadReceiveDatastreamStart() {
  if(!MyThreadReceiveDatastreamOn) {
    MyThreadReceiveDatastreamOn = true;
    MyThreadReceiveDatastream = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&RBComm::ThreadReceiveDatastream, this)));
    
    if (MyTOFParam->NotDaemon()) cout<<"Receive RB data thread started!"<<endl;
  } else {
    if (MyTOFParam->NotDaemon())
      cout<<"Receive RB data thread already running!"<<endl;
  }
}

//----------------------------------------------------------------------

void RBComm::ThreadReceiveDatastreamStop() {

  if(MyThreadReceiveDatastreamOn) {
    if(MyThreadReceiveDatastream != 0) {
      MyThreadReceiveDatastreamOn = false;		
      if (MyTOFParam->NotDaemon()) cout<<"RB data thread stopped!"<<endl;
    }
  } else {
    if (MyTOFParam->NotDaemon()) cout<<"RB data thread cannot be stopped because it was not running!"<<endl;
  }
}

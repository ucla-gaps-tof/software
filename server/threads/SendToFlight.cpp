#include <SendToFlight.hh>
#include "../library/blobroutines.h"

SendToFlight::SendToFlight(string Eth, TOFParam *TempTOF, int TempClassId) {

  MyEth = Eth;
  MyTOFParam = TempTOF;

  MyClassId = TempClassId;
  
  MyThreadSendToFlightOn = false;
  MyMutexSendDatastream = false;
  
  MyPrintLevel = 0;
}

//----------------------------------------------------------------------

SendToFlight::~SendToFlight() {
  ThreadAllStop();
}

//----------------------------------------------------------------------

void SendToFlight::ThreadAllStart() {
  ThreadSendToFlightStart();
}

void SendToFlight::ThreadAllStop() {
  ThreadSendToFlightStop();
}

//----------------------------------------------------------------------

void SendToFlight::SetPrintLevel(int PrintLevel) {
  if(PrintLevel >= 0) MyPrintLevel = PrintLevel;
  else {
    if (MyTOFParam->NotDaemon()) cout<<"Error: SendToFlight bad print level!"<<endl;
  }
}

//----------------------------------------------------------------------

void SendToFlight::ThreadSendToFlight() {
  // Code to find Send data to flight computer
}

//----------------------------------------------------------------------

void SendToFlight::ThreadSendToFlightStart() {
  if(!MyThreadSendToFlightOn) {
    MyThreadSendToFlightOn = true;
    MyThreadSendToFlight = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SendToFlight::ThreadSendToFlight, this)));
    if (MyTOFParam->NotDaemon()) cout<<"SendToFlight thread started!"<<endl;
  } else {
    if (MyTOFParam->NotDaemon()) cout<<"SendToFlight thread already running!"<<endl;
  }
}

//----------------------------------------------------------------------

void SendToFlight::ThreadSendToFlightStop() {
  
  if(MyThreadSendToFlightOn) {
    if(MyThreadSendToFlight != 0) {
      MyThreadSendToFlightOn = false;		
      if (MyTOFParam->NotDaemon()) cout<<"SendToFlight thread stopped!"<<endl;
    }
  } else {
    if (MyTOFParam->NotDaemon()) cout<<"SendToFlight thread cannot be stopped because it was not running!"<<endl;
  }
}

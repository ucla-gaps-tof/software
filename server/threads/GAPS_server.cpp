#include <iostream>
#include <vector>
#include <string>

#include <TOFCommon.h>
#include <RBCommunication.hh>
#include <AnalyzeEvts.hh>
#include <SendToFlight.hh>
#include <Disk.hh>
#include <TOFParam.hh>

using namespace std;

int main(int argc, char * argv[])
{

  bool daemon_mode = false;
  // Handle command line arguments. The only one that is currently
  // acknowledged is to run in daemon mode.
  if ( argc > 1 ) {
    if ( strcmp(argv[1], "-d" ) == 0 ) {
      daemon_mode = true;
    }
  } 
  
  stringstream TempT, TempF;
  TempT<< 10 << "." << 0 << "." << 1 << "." << 10;    // gaps_tof for RB comm
  TempF<< 10 << "." << 97 << "." << 108 << "." << 31; // flight (at UCLA)
  string TOFEthAddr = TempT.str();
  string FlightEthAddr = TempF.str();
  
  // Initialize the Parameter Class that many other objects will use
  TOFParam * TOFParams = new TOFParam(0);
  TOFParams->SetTOFEthAddress(TOFEthAddr);
  TOFParams->SetFlightEthAddress(FlightEthAddr);
  if (daemon_mode) 
    TOFParams->SetDaemonFlag();
  
  //Initialize data 
  //Disk * DataDisk = new Disk(TOFParams, 1); // To record data to disk
  
  //Initialize Readout Board communication
  RBComm * TheRBComm = new RBComm(TOFEthAddr, TOFParams, 2);
  
  //Configure RB communication
  TheRBComm->SetPrintLevel(1);
  
  //Initialize the thread to analyze events
  AnalyzeEvts * Analysis = new AnalyzeEvts(TOFParams, 3); // Analyze Evts

  //Initialize Flight Computer communication: THIS IS WHERE ACHIM'S
  //CODE SHOULD GO
  SendToFlight * FlightComm = new SendToFlight(FlightEthAddr, TOFParams, 4);
  
  //Start threads
  TOFParams->SetStartTime();
  TheRBComm->ThreadAllStart();
  Analysis->ThreadAllStart();
  FlightComm->ThreadAllStart();
  
  do {
    sleep(1);
  } while ( TOFParams->GetRunningStatus() );
  
  //Stop threads
  
  TheRBComm->ThreadAllStop();
  Analysis->ThreadAllStop();
  FlightComm->ThreadAllStop();

  return 0;
}

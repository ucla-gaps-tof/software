#include <TOFParam.hh>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>



TOFParam::TOFParam(int TempClassId)
{
  KeepRunning = true; // If ever set to false, DAQ program will shutdown
  NotDaemonMode = true; // Don't suppress output to screen
  MyClassId = TempClassId;
  
  // Initialize the Data buffers
  InitializeRun(TOFDATA_EVENT);

  FullEventTargetTime = 0; // First event will be a full event
}

//----------------------------------------------------------------------

TOFParam::~TOFParam()
{
  ;
}

void TOFParam::InitializeRun(short type)
{
  // Initialize the Data buffer counters
  if ( (type == TOFDATA_EVENT) || (type == TOFDATA_ALL) ) {
    Index = -1;
    EventNumber  = 0;
    EventZero    = 0;
    EventReadCtr = 0;
    EventWriteCtr = 0;
    EventCtrWrap = false;
    IsEventData  = false;
    for (int i=0;i<MAX_BRDS;i++) LastEvent[i] = 0;
    for (int i=0;i<MEM_EVENTS;i++)  EventBuffer[i].EvtStatus = EVT_NONE;
  }
}


unsigned short TOFParam::GetEventNumber(short type) {
  if ( type == TOFDATA_EVENT ) 
    return ++EventNumber;
  else 
    return 0 ;
}

void TOFParam::SetStartTime()
{
  struct timeval tmp;
  gettimeofday(&tmp, NULL);
  StartTime[0] = (unsigned int)tmp.tv_sec;
  StartTime[1] = (unsigned int)tmp.tv_usec;
  if (NotDaemonMode) printf("Time = %d  %d\n", StartTime[0], StartTime[1]);
}

void TOFParam::SetRunStartTime()
{
  struct timeval tmp;
  gettimeofday(&tmp, NULL);
  RunStartTime = (unsigned int)tmp.tv_sec;
  if (NotDaemonMode) printf("Time = %d\n", RunStartTime);
}

int TOFParam::FindEvtIndex(int evt_ctr, int rb){

  if (Index < 0) {  // Only way this happens is on startup
    Index = 0;
    EventZero = evt_ctr;
  } else { // Otherwise, Index is calculated from the evt_ctr where Index = 0
    Index = evt_ctr - EventZero;
    if (Index < 0) Index += MEM_EVENTS; // Make sure we are positive
    while (Index >= MEM_EVENTS) { // Handle looping back to start of memory
      Index -= MEM_EVENTS;
      EventZero += MEM_EVENTS;
    }
  }
  return (Index);
}

//==================================================================
// NOW DEAL WITH EVENT DATA BUFFER
//==================================================================
void TOFParam::AddEventToBuffer(BlobEvt_t evt_dta) 
{// STILL CONFIGURED FOR PGAPS DATA. NEED TO UPDATE! JAZ
  /*
  // First, we need to transfer the information from the event
  // structure into the EventBuffer;
  int ctr = EventWriteCtr;
  // Write the time information...
  EventBuffer[ctr].EventTimeSec  = evt_dta.EventTimeSec;
  EventBuffer[ctr].EventTimeMSec = evt_dta.EventTimeMSec;
  // Write the clock information...
  EventBuffer[ctr].ClockCounter = evt_dta.ClockCounter;
  EventBuffer[ctr].ClockSync    = evt_dta.ClockSync;
  // ...then the channel data...
  for (int i=0; i<CHNTOT; i++) {
    EventBuffer[ctr].tdc[i]    = evt_dta.tdc[i];
    EventBuffer[ctr].adc[i]    = evt_dta.adc[i];
    EventBuffer[ctr].scaler[i] = evt_dta.scaler[i];
    EventBuffer[ctr].tdc_overflow[i] = evt_dta.tdc_overflow[i];
    EventBuffer[ctr].adc_overflow[i] = evt_dta.adc_overflow[i];
  }
  // ...and some event info.
  EventBuffer[ctr].tdc_event_ctr = evt_dta.tdc_event_ctr;
  EventBuffer[ctr].adc_event_ctr = evt_dta.adc_event_ctr;
  EventBuffer[ctr].n_tdc = evt_dta.n_tdc;
  EventBuffer[ctr].n_adc = evt_dta.n_adc;
  // Finally, we update the Write counter.  
  */
  // Increment the EventWriteCtr(), but don't overtake the EventReadCtr.
  bool success = false;
  struct timespec tmp_sleep;
  tmp_sleep.tv_sec  = 0;
  tmp_sleep.tv_nsec = 1000;

  int test=0;
  do {
    if (test++ && NotDaemonMode) printf("Trying to increment write ctr\n"); 
    success = IncrementEventWriteCtr();
    nanosleep(&tmp_sleep, NULL);
  } while (!success);
  IsEventData = true;
}

MemEvt_t TOFParam::GetEventFromBuffer() {
  // STILL CONFIGURED FOR PGAPS DATA. NEED TO UPDATE! JAZ
  MemEvt_t evt_dta;
  /*
  // First, we need to transfer the information from the EventBuffer
  // into the event structure
  int ctr = EventReadCtr;
  // Write the time information...
  evt_dta.EventTimeSec = EventBuffer[ctr].EventTimeSec;
  evt_dta.EventTimeMSec = EventBuffer[ctr].EventTimeMSec;
  // ... and the clock information...
  evt_dta.ClockCounter = EventBuffer[ctr].ClockCounter;
  evt_dta.ClockSync    = EventBuffer[ctr].ClockSync;
  // ...then the channel data...
  for (int i=0; i<CHNTOT; i++) {
    evt_dta.tdc[i]    = EventBuffer[ctr].tdc[i];
    evt_dta.adc[i]    = EventBuffer[ctr].adc[i];
    evt_dta.scaler[i] = EventBuffer[ctr].scaler[i];
    evt_dta.tdc_overflow[i] = EventBuffer[ctr].tdc_overflow[i];
    evt_dta.adc_overflow[i] = EventBuffer[ctr].adc_overflow[i];
  }
  // ...and some event info.
  evt_dta.tdc_event_ctr = EventBuffer[ctr].tdc_event_ctr;
  evt_dta.adc_event_ctr = EventBuffer[ctr].adc_event_ctr;
  evt_dta.n_tdc = EventBuffer[ctr].n_tdc;
  evt_dta.n_adc = EventBuffer[ctr].n_adc;
  // Finally, we update the Write counter.  
  */
  // Increment the EventReadCtr(), but don't catch the EventWriteCtr.
  bool success = false;
  struct timespec tmp_sleep;
  tmp_sleep.tv_sec  = 0;
  tmp_sleep.tv_nsec = 1000000;

  //int test=0;
  do {
    //if (test++ && NotDaemonMode) printf("Trying to increment read ctr\n"); 
    success = IncrementEventReadCtr();
    nanosleep(&tmp_sleep, NULL);
  } while (!success);

  return evt_dta;
}

bool TOFParam::IncrementEventReadCtr()
{
  short tmp = EventReadCtr;
  tmp ++;
  
  // Is the counter ready to loop?
  if ( tmp == MEM_EVENTS ) { 
    tmp = 0; 
    EventCtrWrap = 0; // Now both Read and Write ctrs are in the same loop.
  } 
  // We have to make sure that the ReadCtr has not "caught" the WriteCtr
  if ( (tmp <= EventWriteCtr) || EventCtrWrap ) {
    EventReadCtr = tmp; 
    return true;
  } else {
    return false;
  }
}

bool TOFParam::IncrementEventWriteCtr()
{
  short tmp = EventWriteCtr;
  tmp ++;

  // Is the counter ready to loop?
  if ( tmp == MEM_EVENTS ) { 
    tmp = 0;
    EventCtrWrap = 1; // Write Ctr now on a different loop than Read Ctr
  } 
  // We have to make sure that the WriteCtr has not run all the way
  // around the buffer and "caught-up" to the ReadCtr.
  if ( (EventCtrWrap && tmp < EventReadCtr) || !EventCtrWrap ) {
    EventWriteCtr = tmp; 
    return true;
  } else {
    return false;
  }
}

bool TOFParam::EventDataReady() 
{
  // Must ensure that ReadCtr is not equal to WriteCtr so that we
  // never try to read data that some other process may be writing.
  // Also, we don't want to try to access an event buffer if no events
  // have occurred. We will get a segfault.
  if (EventReadCtr != EventWriteCtr && IsEventData ) 
    return true;
  else 
    return false;
}

unsigned short TOFParam::DataReady() 
{
  // Must ensure that ReadCtr is not equal to WriteCtr (for either
  // Event, or Environ Data) so that we never try to read data
  // that some other process may be writing.  Also, we don't want to
  // try to access a buffer if no events have occurred or else we will
  // get a segfault.
  bool Event = false;
  
  // Do we have new Event Data?
  if (EventReadCtr != EventWriteCtr && IsEventData ) Event = true;
  
  if (Event) return TOFDATA_EVENT;
  else       return TOFDATA_NONE;
}

bool TOFParam::IsFullEvent()
{
  struct timeval tmp_time;
  gettimeofday(&tmp_time, NULL);
  unsigned int currentDuration = tmp_time.tv_sec - StartTime[0];
  if (currentDuration > FullEventTargetTime) {
    FullEventTargetTime = currentDuration + 3;
    return true;
  } else 
    return false;
}

void TOFParam::SetTOFEthAddress(string addr) {
  TOFEthAddress = addr;
}

void TOFParam::SetFlightEthAddress(string addr) {
  FlightEthAddress = addr;
}

//----------------------------------------------------------------------


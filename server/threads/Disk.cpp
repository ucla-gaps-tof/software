#include <Disk.hh>

Disk::Disk(TOFParam * TempTOF, int TempClassId) {
  MyClassId = TempClassId;	
  
  MyTOFParam = TempTOF;
  
  MyMutexWrite = false;
  
  // To keep track of the filenames we should use. 
  RunNo = -1;
  RunFileCtr = 0;

  MonFileInterval = 3600;
  DataFileInterval = 3600;

}

//----------------------------------------------------------------------

Disk::~Disk() {
  ;
}

//----------------------------------------------------------------------

void Disk::OpenOutputFile(string Filename, int Seconds) {

  MyBaseFilename.push_back(Filename);	

  stringstream Temp;
  int Time = time(NULL);
  Temp<<Time;
  string T = Temp.str();	
  
  Filename.append(T);
  Filename.append(".dat");
  
  ofstream * File = new ofstream(Filename.c_str(), ios::binary);	
  MyFile.push_back(File);
  
  vector<int> TempSeconds;
  TempSeconds.push_back(Time);
  TempSeconds.push_back(Time);
  TempSeconds.push_back(MonFileInterval);
  MyFileSeconds.push_back(TempSeconds);
}

//----------------------------------------------------------------------

void Disk::OpenDataFile(int TmpRunno) {
  
  if (TmpRunno < 1000) { // If not a valid runno, return
    if (MyTOFParam->NotDaemon()) printf("Not opening file: Invalid run number: %d \n", TmpRunno);
  } else { // With a valid runno, open the proper file.    
    RunNo = TmpRunno;
    RunFileCtr = 0;
    for(unsigned int i = 0; i < MyFile.size(); i++) {
      CheckOutputFile(i, FF_DATAOPEN, DataFileInterval);
    }   
  }
}

void Disk::CloseDataFile(int TmpRunno) {

  if (TmpRunno == RunNo) { // Am I closing the proper file number?
    // Now, close the file and open another "monitoring" file.
    for(unsigned int i = 0; i < MyFile.size(); i++) {
      CheckOutputFile(i, FF_DATACLOSE, MonFileInterval);
    }   
    RunNo = -1; // Set the file number as invalid
    RunFileCtr = 0; // Reset the file counter
  }
}

void Disk::CheckOutputFile(unsigned int i, int flag, int interval) {
  if (i < MyFile.size()) {
    int Time = time(NULL);
    
    MyFileSeconds.at(i).at(1) = Time;
    
    if ( (MyFileSeconds.at(i).at(1) - MyFileSeconds.at(i).at(0) >= MyFileSeconds.at(i).at(2)) or flag ) {
      
      // Write the end-of-run record (if appropriate) before closing
      if ( flag == FF_DATACLOSE ) WriteERUN(Time);

      MyFile.at(i)->close();	
      
      stringstream Temp;	
      Temp<<Time;
      string T = Temp.str();	
      string Filename = MyBaseFilename.at(i);
      
      if ( flag == FF_DATAOPEN || ((flag==FF_NORMAL) && (RunNo>=1000)) ) {
	// If called with dataOpen flag or with a valid run number,
	// open another file with the run number.
	char tmp_ext[500]; 
	sprintf(tmp_ext, "%4d_%02d.dat", RunNo, RunFileCtr++);
	Filename.append(tmp_ext);
      } else if ( flag == FF_DATACLOSE || 
		  ((flag==FF_NORMAL) && (RunNo<1000)) ) {
	// If called with dataClose flag or with an invalid run
	// number, open another file with the timestamp as the
	// descriptor
	Filename.append(T);
	Filename.append(".dat");
      } else {
	// Use the timestamp by default
	Filename.append(T);
	Filename.append(".dat");
      }	
      
      MyFile.at(i)->open(Filename.c_str());
      MyFileSeconds.at(i).at(2) = interval;
      MyFileSeconds.at(i).at(0) = Time;

      // Write the beginning-of-run record (if appropriate)
      if ( flag == FF_DATAOPEN ) WriteBRUN(Time);

    }
  } else {
    cout<<"Error: Wrong output file index!."<<endl;
  }
}

//----------------------------------------------------------------------

void Disk::CloseAllOutputFiles() {
  for(unsigned int i = 0; i < MyFile.size(); i++) MyFile.at(i)->close();
}

//----------------------------------------------------------------------

void Disk::Write(vector<unsigned char> & Datastream) {
  while(MyMutexWrite);
  MyMutexWrite = true;
  
  for(unsigned int i = 0; i < MyFile.size(); i++) {
    for(unsigned int j = 0; j < Datastream.size(); j++) 
      MyFile.at(i)->write((char *) & Datastream.at(j), sizeof(unsigned char));
    
    if ( RunNo >= 1000 ) 
      CheckOutputFile(i, FF_NORMAL, DataFileInterval);
    else 
      CheckOutputFile(i, FF_NORMAL, MonFileInterval);
  }  
  MyMutexWrite = false;
}

void Disk::WriteEvent(vector<unsigned char> & Datastream) {
  while(MyMutexWrite);
  MyMutexWrite = true;
  
  for(unsigned int i = 0; i < MyFile.size(); i++) {
    for(unsigned int j = 0; j < Datastream.size(); j++) 
      MyFile.at(i)->write((char *) & Datastream.at(j), sizeof(unsigned char));
  }
  
  MyMutexWrite = false;
}

void Disk::WriteERUN(int Time) {
  // First, we fill the ERUN record, then we write it to disk
  unsigned short vers  = 2; // 1 byte for type, 1 for version
  unsigned short leng  = 2; // need 2 bytes
  unsigned short run   = 2; // need 2 bytes
  unsigned short time  = 4; // 4 bytes for sec
  short length = vers + leng + run + time; 
  vector<unsigned char> TempStream(length);
  //Now fill the record stream 
  unsigned short ctr=0; 
  TempStream.at(ctr++) = DATA_ERUN;
  TempStream.at(ctr++) = DATA_VER1;
  // Next add the length of the data stream
  TempStream.at(ctr++) = (unsigned char)(length>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(length)&0xFF;
  // Next add the run number
  TempStream.at(ctr++) = (unsigned char)(RunNo>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(RunNo)&0xFF;
  // And the timestamp
  unsigned int tmp_time1 = (unsigned int)Time;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>24)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>16)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1)&0xFF;
  
  // Call the routine that writes the data to disk
  WriteEvent(TempStream);
}

void Disk::WriteBRUN(int Time) {
  // First, we fill the ERUN record, then we write it to disk
  unsigned short vers  = 2; // 1 byte for type, 1 for version
  unsigned short leng  = 2; // need 2 bytes
  unsigned short run   = 2; // need 2 bytes
  unsigned short time  = 2*4; // 4 bytes for sec
  short length = vers + leng + run + time; 
  vector<unsigned char> TempStream(length);
  //Now fill the record stream 
  unsigned short ctr=0; 
  TempStream.at(ctr++) = DATA_BRUN;
  TempStream.at(ctr++) = DATA_VER1;
  // Next add the length of the data stream
  TempStream.at(ctr++) = (unsigned char)(length>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(length)&0xFF;
  // Next add the run number
  TempStream.at(ctr++) = (unsigned char)(RunNo>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(RunNo)&0xFF;
  // And the timestamp of the run beginning...
  unsigned int tmp_time1 = (unsigned int)MyTOFParam->GetRunStartTime();
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>24)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>16)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1)&0xFF;
  // ...and of this file opening
  tmp_time1 = (unsigned int)Time;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>24)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>16)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1>>8)&0xFF;
  TempStream.at(ctr++) = (unsigned char)(tmp_time1)&0xFF;
  
  // Call the routine that writes the data to disk
  WriteEvent(TempStream);
}



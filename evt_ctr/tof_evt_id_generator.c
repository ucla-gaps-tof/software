/*
SPI code: https://www.raspberrypi.org/forums/viewtopic.php?t=211798

IRQ code: https://gitlab.cs.duke.edu/wjs/rpi-weather/-/blob/master/GPIO/test_code/timetocharge.c

Based on 
isr4pi.c
D. Thiebaut
based on isr.c from the WiringPi library, authored by Gordon Henderson
https://github.com/WiringPi/WiringPi/blob/master/examples/isr.c


 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <pigpio.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>


// Use physical in 11, which is pin 0 for wiringPi library

#define BUTTON_PIN 0


// the event counter 
volatile int eventCounter = 0;
unsigned char buf[4];
unsigned bytes = 4;
int speed = 1000000;

unsigned spi_fd;

// -------------------------------------------------------------------------
// myInterrupt:  called every time an event occurs
void myInterrupt(void) {
    eventCounter++;
    buf[0] = (eventCounter >> 24) & 0xFF;
    buf[1] = (eventCounter >> 16) & 0xFF;
    buf[2] = (eventCounter >> 8) & 0xFF;
    buf[3] = eventCounter & 0xFF;
    // sprintf(buf, "%d", eventCounter);
    spiWrite(spi_fd,(char *)(&buf), bytes);
}


// -------------------------------------------------------------------------
// main
int main(void) {
  // sets up the wiringPi library
  if (wiringPiSetup () < 0) {
      fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror (errno));
      return 1;
  }

  // SPI setup stuff
  if (gpioInitialise() < 0) return -1;
  // Open SPI file descriptor
  spi_fd = spiOpen(0, speed, 0);
  // Check ret value, halt if unsuccessful
  if (spi_fd < 0) return -1;

  // set Pin 17/0 generate an interrupt on high-to-low transitions
  // and attach myInterrupt() to the interrupt
  if ( wiringPiISR (BUTTON_PIN, INT_EDGE_RISING, &myInterrupt) < 0 ) {
      fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno));
      return 1;
  }
  int evtCounter_old = 0;
  int last_evt = 0;
  char time_buff[50];
  char log_file[50];
  char last_evt_log[50] = "/home/gaps/evt_ctr_log/last_event";
  struct timeval tv;
  time_t tnow;

  FILE *sp = fopen(last_evt_log, "r");
  fscanf(sp, "%d", &last_evt);
  // printf("%d\n", last_evt);
  fclose(sp);
  eventCounter = last_evt + 1;


  // printf("UCLA TOF Event Generator\n");
  // printf("Hastily written and pilfered from internet by Sean Quinn, spq@ucla.edu\n");
  // printf("Increments 32b counter when pulse asserted on wPi GPIO #0 (BCM 17, physical 11)\n");
  // printf("-------------------------------------------------------------------------------\n");
  // printf("-------------------------------------------------------------------------------\n");
  // print event counter on update, poll 500 us
  while ( 1 ) {
    if ( evtCounter_old != eventCounter){
      gettimeofday(&tv, NULL);
      tnow=tv.tv_sec;
      // strftime(time_buff, 30, "%m-%d-%Y %T.", localtime(&tnow));
      strftime(time_buff, 30, "%Y %m %d %H %M %S", localtime(&tnow));
      strftime(log_file, sizeof(log_file), "/home/gaps/evt_ctr_log/event_id_%m%d%Y.log", localtime(&tnow));
      FILE *fp = fopen(log_file, "a+");
      fprintf(fp, "%d %s%1d\n", eventCounter - 1, time_buff, tv.tv_usec );
      FILE *sp = fopen(last_evt_log, "w+");
      fprintf(sp, "%d\n", eventCounter);
      fclose(sp);
      printf( "%d %s%1d\n", eventCounter - 1, time_buff, tv.tv_usec );
      evtCounter_old = eventCounter;
      fclose(fp);
    }
    usleep( 500 ); // 500 us
  }

  // FILE *fw = fopen(last_evt_log, "w");
  // fprintf(fw, "%d\n", eventCounter - 1);
  // fclose(fw);
  // fclose(fr);

  spiClose(spi_fd);
  gpioTerminate();
  return 0;
}

1. Connect trigger iput to physical pin #11 (wiringPi pin #0) on Raspberry Pi.
2. Conncect SPI MOSI (physical pin #19) and SCLK (physical pin #23) pins on Raspberry Pi to readout board GPIO pin #29 (GPIO7_P) and pin #31 (GPIO7_N) respectively.
3. Connect GND (physical pin #20) on Raspberry Pi to readout board pin# 30 (TRB_GND)
3. `make all`
4. `./tof_evt_id_generator`

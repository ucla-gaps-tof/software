#!/bin/bash

## database directory is static now

logdir=$(python3 << EOI
import configparser
cfg = configparser.ConfigParser()
cfg.read("/home/gaps/software/config.ini")
print(cfg['net']['log_dir'])
EOI
)

savedir=$(python3 << EOI
import configparser
cfg = configparser.ConfigParser()
cfg.read("/home/gaps/software/config.ini")
print(cfg['net']['save_dir'])
EOI
)

swdir=$(python3 << EOI
import configparser
cfg = configparser.ConfigParser()
cfg.read("/home/gaps/software/config.ini")
print(cfg['net']['sw_dir'])
EOI
)

dbdir=$(python3 << EOI
import configparser
cfg = configparser.ConfigParser()
cfg.read("/home/gaps/software/config.ini")
print(cfg['net']['db_dir'])
EOI
)

RBS=(1 2 3 4 5 6)

srv_process=$(pgrep -af server.py | wc -l)

while true
do
    if [ $srv_process = 0 ]; then
        echo "There is no background server.py process is running"
        read -p "Do you want to start server.py? [y/n]" input
        case "$input" in
            [yY]) echo "Start server.py..."
            for board in ${RBS[@]}; do
                /usr/bin/python3 $swdir/net/server.py $board $savedir $dbdir & >> $logdir/tof-gfp-rb${board}_server.log
            done
            break ;;
            [nN]) echo "End this process."
            break ;;
            *) echo "Type y/Y or n/N."
        esac
    else
        echo "There are ${srv_process} of server.py proccess are running"
        read -p "Do you want to kill them? [y/n]" input
        case "$input" in
            [yY]) echo "Killing server.py processes..."
            pkill -f server.py
            break ;;
            [nN]) echo "End this process."
            break ;;
            *) echo "Type y/Y or n/N."
        esac
    fi
done

#!/usr/bin/env python3

import sys
import zmq
import datetime
import configparser

svr_ip = '10.0.1.10'
# svr_port = '38835'
svr_port = '3883{}'.format(sys.argv[1])
datadir = sys.argv[2]
dbdir = sys.argv[3]

run_config = configparser.ConfigParser()
run_config_path = dbdir + '/run_config.ini'

def create_instance(svr_ip, svr_port):

    context = zmq.Context()
    skt = context.socket(zmq.REP)
    skt.bind("tcp://%s:%s" %(svr_ip, svr_port))
    print("Listening for connections ...")

    while True:
        data = skt.recv()
        skt.send(b"[SVR]: Received data")
        print("[SVR %s] Received %i bytes from " %(svr_port, len(data)))
        create_time = datetime.datetime.now()
        datetime_str = create_time.strftime("%Y%m%d_%H%M%S")
        run_config.read(run_config_path)
        runnum = run_config['RBinfo']['runnum']
        
        print("Saving data... CHECKPOINT")

        file_str = "%s/%s/d%s_%s.dat" %(datadir, runnum, datetime_str, svr_port)
        with open(file_str, "wb") as f:
            f.write(data)
        print("Data saved to: %s" %file_str)

if __name__ == "__main__":
    create_instance(svr_ip, svr_port)

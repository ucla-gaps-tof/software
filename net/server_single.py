#!/usr/bin/env python3
"""
Author: Sean Quinn (spq@ucla.edu)
server.py (c) UCLA 2021
Desc: Receives RAM buffer data from DRS boards
Created:  2021-02-11T00:02:10.814Z
Modified: 2021-09-21
"""

import zmq
import datetime
import time

# Server parameters
svr_port = '38835'
svr_ip = '10.0.1.10'

# Create socket and listen
context = zmq.Context()
skt = context.socket(zmq.REP)
skt.bind("tcp://%s:%s" %(svr_ip, svr_port))

# Main loop
while True:
	print("Listening for connections ...")
	data = skt.recv()
	skt.send(b"[SVR]: Received data")
	print("[SVR %s] Received %i bytes from " %(svr_port, len(data)))
	create_time = datetime.datetime.now()
	time_str = create_time.strftime("%x_%H%M%S").replace("/","")
	print("Saving data...")
	file_str = "svr_%s_data_%s.dat" %(svr_port, time_str)
	with open(file_str, "wb") as f:
			f.write(data)
	print("Data saved to: %s" %file_str)
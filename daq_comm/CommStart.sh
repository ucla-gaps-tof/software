#!/bin/bash 

if [[ $1 =~ "10.1.0" ]]; then 
    dir=/home/gaps/tfp-test/zweerink
elif [[ $1 =~ "10.0.1" ]]; then 
    dir=/home/gaps/bin
else
    dir=/home/gaps/bin
fi

#ssh -n -f $1 "sh -c 'cd "$dir"; nohup ./GFPcomm.sh "$2" > /dev/null 2>&1 &'"

#echo $1 $2 $dir

ssh -l gaps -n -f $1 "sh -c 'nohup "$dir"/GFPcomm.sh "$2" > /dev/null 2>&1 &'"

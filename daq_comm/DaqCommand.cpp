#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <map>

#include "TOFCommand.hh"
#include "CommandDefs.hh"

#define MAX_LINE 256

//   *****************************************************

int main (int argc, char *argv[]) {
  
  int status = 0;

  char line[MAX_LINE];        /* Command line entered by the user. */
  char cmnd[MAX_LINE];        /* Command entered by the user. */
  char param[MAX_LINE];       /* Parameter to affect. */
  char setting1[MAX_LINE];     /* Setting for parameter. */
  char setting2[MAX_LINE];     /* Setting for parameter. */
  
  // trying to start GFPRecieve on all the boards at once
  //if ((argc > 1) && (strncasecmp(argv[1],"start",4) == 0)) { 
  int NPis = 6;
  int NRBs = 6;
  char syscmd[1024];
  
  /*std::map<int,const char*> Pimap;
  Pimap[0] = "10.1.0.1";
  Pimap[1] = "10.1.0.2";
  Pimap[2] = "10.1.0.3";
  Pimap[3] = "10.1.0.4";
  Pimap[4] = "10.1.0.5";
  Pimap[5] = "10.1.0.6";*/
  
  //std::map<int,const char*> RBmap;
  char RBmap[NRBs][50];
  char Pimap[NRBs][50];
  char VMEmap[50] = "192.168.37.33";

  for(int i=0;i<NRBs;i++) {
    sprintf(Pimap[i],"10.1.0.%d",i+1);
    sprintf(RBmap[i],"10.0.1.%d",i+1);
  }
  /*RBmap[0] = "10.0.1.1";
  RBmap[1] = "10.0.1.2";
  RBmap[2] = "10.0.1.3";
  RBmap[3] = "10.0.1.4";
  RBmap[4] = "10.0.1.5";
  RBmap[5] = "10.0.1.6"; */
  
  //std::map<int,const char*> VMEmap;
  //VMEmap[0] = "192.168.37.33";
  
  unsigned char sent_cmnd[4];
  unsigned char cmnd_length;
  
  TOFCommand * Cmnd = new TOFCommand();
  
  do {
    // Parse the line and get the command
    printf("Next Command:  ");
    fgets(line, MAX_LINE, stdin);
    strcpy( cmnd, "\0" );
    strcpy( param, "\0" );
    strcpy( setting1, "-1" );
    strcpy( setting2, "\0" );
    sscanf( line, "%s %s %s %s", cmnd, param, setting1, setting2 );
    //printf("%s\n%s  %s  %s", line, cmnd, param, setting1, setting2);
    
    // Start the GFPReceive programs...
    if (strncasecmp(cmnd,"comm",3) == 0) {
      char srcdir[500] = "/home/gaps/bin";
      // ...on the Pis...
      if (strncasecmp(param,"pis",3) == 0) {
	for (int j = 0; j < NPis; j++) {
	  if (strcasecmp(setting1,"-1") == 0) strcpy(setting1,"start"); 
	  sprintf(syscmd, "%s/CommStart.sh %s %s", srcdir, Pimap[j], setting1);
	  system(syscmd);
	}
      } else if (strncasecmp(param,"rbs",3) == 0) {
	for (int j = 0; j < NRBs; j++) {
	  if (strcasecmp(setting1,"-1") == 0) strcpy(setting1,"start"); 
	  sprintf(syscmd, "%s/CommStart.sh %s %s", srcdir, RBmap[j], setting1);
	  //if (j==0 || j==4) 
	  // if (j==0 || j==1 || j==2 || j==3 || j==4) 
	  system(syscmd);
	}
      } else if (strncasecmp(param,"vme",3) == 0) {
	if (strcasecmp(setting1,"-1") == 0) strcpy(setting1,"start"); 
	sprintf(syscmd, "%s/CommStart.sh %s %s", srcdir, VMEmap, setting1);
	system(syscmd);
      }
    } else {
    
      status = Cmnd->GenerateCommand(cmnd, param, setting1, setting2);
    
      if (status == 0) {
	Cmnd->SendCommand();
	Cmnd->GetCommand(sent_cmnd, &cmnd_length);
	for (int i=0; i<(int)cmnd_length; i++) printf("%02X",sent_cmnd[i]);
	printf("\n");
      }
    }
  } while (Cmnd->CommandWait());
}

 

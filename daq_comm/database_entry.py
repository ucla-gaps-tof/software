import configparser
import sys
from pathlib import Path
import sqlite3
from datetime import datetime

config0 = configparser.ConfigParser()
config0.read('/home/gaps/software/config.ini')
savedir = config0['net']['save_dir']
dbdir = config0['net']['db_dir']

filename = dbdir + '/run_config.ini'
dbfile = dbdir + '/GFPRuns.db'

def createDir(new):

    newpath_str = savedir + '/' + str(new)
    newpath = Path(newpath_str)
    newpath.mkdir()

def changeRunConfig(section,key,val):

    config = configparser.ConfigParser()
    config.read(filename)
    if (val == 'increment'):
        current = int(config[section][key])
        new = current+1
        config[section][key] = str(new)
        createDir(new)
    else:
        config[section][key] = val
    with open(filename,'w') as configfile:
        config.write(configfile)

def makeNewEntry():

    ## TABLE STRUCTURE:
    ## runnum real, date text, starttime text, runtype text, logiccode real,
    ## thr0 real, thr1 real, thr2 real, RB1status text, RB2status text, 
    ## RB3status text, RB4status text, RB5status test, RB6status text
    ## saved_where text

    con = sqlite3.connect(dbfile)
    cur = con.cursor()

    config = configparser.ConfigParser()
    config.read(filename)

    now = datetime.now()

    runnum = config['RBinfo']['runnum']
    date = now.strftime("%b-%d-%Y")
    starttime = now.strftime("%H:%M:%S")
    runtype = config['RBinfo']['runtype']
    logiccode = config['TriggerConfig']['logiccode']
    thr0 = config['TriggerConfig']['thr0']
    thr1 = config['TriggerConfig']['thr1']
    thr2 = config['TriggerConfig']['thr2']
    RB1 = config['RBinfo']['rb1']
    RB2 = config['RBinfo']['rb2']
    RB3 = config['RBinfo']['rb3']
    RB4 = config['RBinfo']['rb4']
    RB5 = config['RBinfo']['rb5']
    RB6 = config['RBinfo']['rb6']
    saved = savedir + '/' + runnum

    row = runnum + ",'" + date + "','" + starttime + "','" + runtype + "'," + logiccode + "," + thr0 + "," + thr1 + "," + thr2 + ",'" + RB1 + "','" + RB2 + "','" + RB3 + "','" + RB4 + "','" + RB5 + "','" + RB6 + "','" + saved + "'"

    command = "INSERT INTO runs VALUES (" + row + ")"

    #print(command)

    cur.execute(command)
    con.commit()
    con.close()

    print("\nNew entry created in GFPRuns.db\n")

def getInfo(key, val):

    con = sqlite3.connect(dbfile)
    cur = con.cursor()

    command = "SELECT * FROM runs WHERE " + key + "=?"
    cur.execute(command, (val,))

    rows = cur.fetchall()

    for row in rows:

        print("Run Number " + str(row[0]))
        print("raw data saved at: " + row[14])
        print("\tdate: " + row[1])
        print("\tstart time: " + row[2])
        print("\trun type: " + row[3])
        print("\tlogic code: " + str(row[4]))
        print("\tTHR0 setting: " + str(row[5]) + " mV")
        print("\tTHR1 setting: " + str(row[6]) + " mV")
        print("\tTHR2 setting: " + str(row[7]) + " mV")
        print("\tRB1 status: " + row[8])
        print("\tRB2 status: " + row[9])
        print("\tRB3 status: " + row[10])
        print("\tRB4 status: " + row[11])
        print("\tRB5 status: " + row[12])
        print("\tRB6 status: " + row[13])


if (sys.argv[1] == "new"):
    makeNewEntry()
elif (sys.argv[1] == "query"):
    getInfo(sys.argv[2],sys.argv[3])
else:
    changeRunConfig(sys.argv[1],sys.argv[2],sys.argv[3])

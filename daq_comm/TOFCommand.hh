#ifndef TOFCOMMAND
#define TOFCOMMAND

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "CommandDefs.hh"

using namespace std;

//!\brief TOFCommand streams are stored

class TOFCommand
{
public:

//!\brief Constructor
TOFCommand();

//!\brief Deconstructor
~TOFCommand();

  int  GenerateCommand(char *cmnd, char *param, char *set1, char *set2);
  bool CommandWait();
  int  GetCommand(unsigned char *sent, unsigned char *length);
  int  SendCommand();
  void PrintUsage(char *cmd);
  

private:

  int  CheckChannel(char *tmp_ch, int *ch);
  int  CheckProfile(char *tmp_pr, int *pr);
  int  CheckVoltage(char *tmp_hv, int ch, int *hv);
  int  CheckInterval(char *tmp_int, int *interval);
  
  unsigned char tmp_cmnd[4];
  unsigned char cmnd_length;


  bool commandWait;

};

#endif

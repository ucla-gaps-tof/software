#!/bin/bash 

flag=$1

if [ -z $flag ]; then
    noflag="true"
    exit
else
    if [ $flag == "start" ]; then
	start="true"
    elif [ $flag == "stop" ]; then
	stop="true"
    else
	badflag="true"
	exit
    fi
fi

# First, find hostname so we know if we can set OS specific variables
hostprog=`/usr/bin/which hostname`
host=`$hostprog`

# Generic for most systems. 
homedir=/home/gaps

if [[ $host =~ "tof-gfp-rb" ]]; then 
    # We are on the readout board and need sudo
    sudo=/usr/bin/sudo
fi
if [[ $host =~ "tof-gfp-pi" ]]; then 
    # We are on the pis board, progdir is different
    homedir=$homedir/tfp-test
fi

progdir=$homedir/bin
logdir=$homedir/logs

# Check if GFPReceive is already running. Needed to start or stop
pidof=`/usr/bin/which pidof`
GFP_status=`$pidof GFPReceive`

# If we have a "start" argument...
if [ ! -z $start ]; then 

    # Make sure directory for logfiles exists
    if [ ! -d "$logdir" ] ; then
	/bin/mkdir $logdir
    fi

    if [ -z $GFP_status ]; then
	# No process running, so start GFPReceive
	datestr=`/bin/date +%Y%m%d.%H%M%S`
	$sudo nohup $progdir/GFPReceive > $logdir/$datestr.txt 2>&1 &
    fi

# If we have a "stop" argument...
elif [ ! -z $stop ]; then 

    if [ ! -z $GFP_status ]; then
	# Process running, so send kill signal
	$sudo /bin/kill $GFP_status
    fi
fi 

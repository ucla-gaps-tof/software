#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <errno.h>
#include <netdb.h>
#include <sys/types.h>

#include "CommandDefs.hh"

//#include <vector>
#include <iostream>
#include <string>
#include <map>

int main (int argc, char *argv[])
{
  int i;
  
  signed short int receivefrom_port = 10067;
  //char this_eth[] = "10.0.1.1";
  //char this_eth[] = "10.97.108.11";
  char this_eth[50];
  char host[256];

  // This is a kludge so that we don't have to set 'this_eth' on each
  // readout board, but rather we can read the hostname.
  gethostname(host, sizeof(host)); //find the host name (tof-gfp-rb#)
  // Make ethernet address based on last character of 'host'

  int host_num = host[strlen(host)-1]-48;

  if ( strncmp(host, "tof-gfp-rb", 10) == 0 ) 
    sprintf(this_eth, "10.0.1.%d", host_num );
  if ( strncmp(host, "tof-gfp-pi", 10) == 0 )
    sprintf(this_eth, "10.1.0.%d", host_num );
  if ( strncmp(host, "gaps-vme", 8) == 0 )
    sprintf(this_eth, "192.168.37.33");
  //printf("Host Name(IP): %s (%s)\n", host, this_eth);

  struct sockaddr_in receivefrom_address;
  struct sockaddr_in this_address;
  socklen_t addrLen;
  
  char receivefrom_eth[INET_ADDRSTRLEN];
  
  // Open/Bind Data Socket
  //  Do this in a do/while loop because we need to call socket at 
  //  least once, and possibly more than once
  //  If we open the socket without a problem, we don't need to delay
  //  Otherwise we do, just to make sure we don't end up swamping the 
  //  system with socket calls
  bzero(&this_address, sizeof(this_address));
  this_address.sin_family = AF_INET;
  this_address.sin_port = htons(receivefrom_port);
  inet_pton(AF_INET,this_eth,&this_address.sin_addr.s_addr);
  
  signed short int dataSocket;
  do 
    {
      printf("*");
      dataSocket = socket(AF_INET, SOCK_DGRAM, 0);
      usleep(10);
    } 
  while(dataSocket == -1);
  
  // Variables related to type of run taken. 
  // Some useful default values 
  bool Time = false;
  bool Evts = true;  // Default is to take runs with given NEvents
  unsigned short NEvents = 10000;  // Default events in run 
  unsigned short Duration = 10;    // In minutes
  unsigned short MonInterval = 10; // In seconds                                
  unsigned short MonDuration = 1;  // In minutes                                 
  
  signed short int errorCode;
  do 
    {
      printf("#");
      errorCode = bind(dataSocket, (struct sockaddr *)&this_address, sizeof(this_address));
      usleep(10);
    } 
  while (errorCode == -1);
  printf("\n");
  
  addrLen = sizeof(this_address);
  
  int data_buffer_size = 2048;
  char data_buffer[data_buffer_size];
  while(1) {
    signed short int data_length = recvfrom(dataSocket, data_buffer, data_buffer_size, 0, (struct sockaddr *)&receivefrom_address, (socklen_t *)&addrLen);
    
    //format the network address into dotted decimal
    inet_ntop(AF_INET,&receivefrom_address.sin_addr.s_addr,receivefrom_eth,
	      sizeof(receivefrom_eth));
    
    //make sure each byte has only 1 byte of data (some data showed
    //strange FFFFFF at the end)
    
    unsigned short CheckedData[data_buffer_size];
    for(i = 0; i < data_length; i++)
    	CheckedData[i] = data_buffer[i] & 0xFF;	
    
    //unsigned short type = CheckedData[1] | (CheckedData[0] << 8);
    
    //for(int k=0;k<data_length;k++)printf("%02X",CheckedData[k]);printf("\n");
    
    char syscmd[1024];
    char pdir[256]="/home/gaps/bin/";
    unsigned short type = (CheckedData[0]);
    
    switch(type) {
      
      // Stuff only related to the readout boards
    case RUN_START:
      {
	printf("Received RunStart Command.\n");
	// run_GFP.sh passes arguments 'as-is' to GFP_daq
	if ( Evts ) 
	  sprintf(syscmd, "%s/run_GFP.sh -r 1 -t 1 -s 1 -n %d -m 1 ",
		  pdir, NEvents);
	else if ( Time ) 
	  sprintf(syscmd, "%s/run_GFP.sh -r 1 -t 1 -s 1 -d %d -m 1 ",
		  pdir, Duration);
	printf("%s\n", syscmd); 
	system(syscmd);
	break;
      }
      
    case RUN_STOP:
      {
	pid_t pid = 0;
	char line[500] = "";
	FILE *cmd = popen("pidof GFP_daq", "r");
	fgets(line, 500, cmd);
	pid = strtoul(line, NULL, 10);
	pclose(cmd);

	if (pid != 0) { // If PID exists, send sigint to it. 
	  sprintf(syscmd, "kill -2 %d", pid);
	  system(syscmd);
	}
	break;
      }
      
    case RUN_EVENTS:
      {
	NEvents = CheckedData[1] | (CheckedData[2] << 8);
	printf("Setting NEvents to %d.\n", NEvents);
	Evts = true; Time = false;
	break;
      }

    case RUN_DURATION:
      {
	Duration = CheckedData[1] | (CheckedData[2] << 8);
	printf("Setting Duration to %d minutes.\n", Duration);
	Evts = false; Time = true;
	break;
      }

    case RB_INIT:
      {
	sprintf(syscmd, "/home/gaps/bin/init_script.sh");
	printf("%s\n", syscmd); 
	system(syscmd);
	break;
      }

      // Stuff only related to the Pis (LTB and powerboard)
   case VB_ON:
      {
	sprintf(syscmd,"/home/gaps/tfp-test/sh_scripts/pb_set.sh");
        printf("VBias ON\n");
	system(syscmd);
        break;
      }

   case VB_OFF:
      {
	sprintf(syscmd,"/home/gaps/tfp-test/sh_scripts/pb_set0.sh");
        printf("VBias OFF\n");
	system(syscmd);
        break;
      }

   case VB_SET:
      {
	//sprintf(syscmd,"/home/gaps/tfp-test/sh_scripts/pb_set.sh");
        printf("Received SET command\n");
	//system(syscmd);
        break;
      }

    case LTB_START:
      {
	sprintf(syscmd,"/home/gaps/tfp-test/sh_scripts/gpio.sh");
	printf("Start LTB\n");
	system(syscmd);
        break;
      }

   case LTB_SET:
      {
	std::map<int,const char*> LTBmap;
	    LTBmap[1] = "10,1b";
	    LTBmap[2] = "10,12";
	    LTBmap[3] = "13,18";
	    LTBmap[4] = "1c,1e";
	    LTBmap[5] = "13,1a";
	    LTBmap[6] = "1c,1e";
	int thr = CheckedData[1];
        int volt = CheckedData[2];
        sprintf(syscmd, "/home/gaps/tfp-test/sh_scripts/set_ltb.sh %d %d %s",
		thr, volt, LTBmap[host_num]);
	printf("%s\n", syscmd);
	system(syscmd);
	break;
      }

      // Stuff only related to the VME crate
    case MON_START:
      {
       	char v_dir[500] = "/home/gaps/TOF_daq/vme_src";
        sprintf(syscmd,"cd %s; ./read_scalers %d %d", v_dir, MonDuration,
                MonInterval);
        printf("%s\n", syscmd);
        system(syscmd);                                                       
        break;
      }

    case MON_STOP:
      {
	//sprintf(syscmd,"/home/gaps/sh_scripts/gpio.sh");
	printf("Received Stop\n");
	//system(syscmd);
        break;
      }

   case MON_INTERVAL:
      {
        MonInterval = CheckedData[1] | (CheckedData[2] << 8);
       	printf("Setting Interval to %d seconds.\n", MonInterval);
        break;
      }

    case MON_DURATION:
      {
        MonDuration = CheckedData[1] | (CheckedData[2] << 8);
	printf("Setting Duration to %d minutes.\n", MonDuration);
        break;
      }

     case MON_LOGIC:
      {
        char logic_dir[100] = "/home/gaps/TOF_daq/logic_files";
	unsigned short logic = CheckedData[1];
        sprintf(syscmd,"cd %s; load_logic.bsh %d", logic_dir, logic);
        printf("%s\n", syscmd);
	//system(syscmd);                                                       
        break;
      }

    default:
      {
	printf("Unknown packet from: %s, length: %d,  %02X\n", this_eth,
	       data_length, type);
      }
    }
    
    
    /*    if (checked_data_buffer[0] == 0x01) {
	sprintf(syscmd,"/bin/ls /home/gaps");
	system(syscmd);
      } else if (checked_data_buffer[0] == 0x02) {
	sprintf(syscmd,"/bin/ls /home/gaps/zweerink");
	system(syscmd);
	system(syscmd);
	}*/
    }
  return(0);
}

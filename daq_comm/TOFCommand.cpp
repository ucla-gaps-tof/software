#include "TOFCommand.hh"

TOFCommand::TOFCommand()
{
  commandWait = true;
}

//----------------------------------------------------------------------

TOFCommand::~TOFCommand()
{
  ;
}

int TOFCommand::GenerateCommand(char *cmnd, char *param, char *set1, char *set2) {

  // Encode the command to send to the DAQ
  int bad_command = 0;
  // Initialize the command so we know how to pack it later
  for (int i=0; i<4; i++)
    tmp_cmnd[i] = 0xFF;
  cmnd_length = 1; // Default value. Only change if we add more to command
  // Initialize the system command to modify run config file
  char syscmd[1024];
  char syscmd2[1024];
  
  // Handle HELP command
  if (strncasecmp(cmnd,"help",3) == 0) {
    char help_cmnd[100];
    sprintf(help_cmnd, "run");
    PrintUsage(help_cmnd);
    sprintf(help_cmnd, "vbias");
    PrintUsage(help_cmnd);
    sprintf(help_cmnd, "ltb");
    PrintUsage(help_cmnd);
    sprintf(help_cmnd, "mon");
    PrintUsage(help_cmnd);
    sprintf(help_cmnd, "comm");
    PrintUsage(help_cmnd);
    sprintf(help_cmnd, "daq");
    //PrintUsage(help_cmnd);
    bad_command = 1;
    
    // Find proper run function 
    // Handle RUN related commands
  } else if (strncasecmp(cmnd,"run",3) == 0) {
    
    // Find proper run function 
    if (strncasecmp(param,"start",3) == 0) {
      // Handle the RUN start command
      tmp_cmnd[0] = RUN_START; 
      // increment run number and create new directory for this run's data with new run number
      sprintf(syscmd, "python3 database_entry.py RBinfo runnum increment");
      //system(syscmd);
      // create database entry for run
      sprintf(syscmd2, "python3 database_entry.py new");
      //system(syscmd2); 
 
    } else if (strncasecmp(param,"stop",3) == 0) {
      // Handle the RUN stop command
      tmp_cmnd[0] = RUN_STOP;
      
    } else if (strncasecmp(param,"pause",3) == 0) {
      // Handle the RUN pause command
      tmp_cmnd[0] = RUN_PAUSE;
      
    } else if (strncasecmp(param,"restart",3) == 0) {
      // Handle the RUN restart command
      tmp_cmnd[0] = RUN_RESTART;
      
    } else if (strncasecmp(param,"events",3) == 0) {
      // Handle the RUN events command
      tmp_cmnd[0] = RUN_EVENTS;
      int evts = atoi(set1);
      if ( (evts < 0) || (evts > MAX_RUN_EVT) ) {
	printf("\tUsage 'run events MIN' where MIN > 0 and MIN < %d\n", 
	       MAX_RUN_EVT);
	bad_command = 1;
      } else {
	tmp_cmnd[1] = (unsigned char)(evts)&0xFF;
	tmp_cmnd[2] = (unsigned char)(evts>>8)&0xFF;
	cmnd_length = 3;
	// adjust run config in database file
	sprintf(syscmd, "python3 database_entry.py RBinfo runtype %sevent", set1);
        system(syscmd);
      }
      
    } else if (strncasecmp(param,"duration",3) == 0) {
      // Handle the RUN length command
      tmp_cmnd[0] = RUN_DURATION;
      int dur = atoi(set1);
      if ( (dur < 0) || (dur > MAX_RUN_DUR) ) {
	printf("\tUsage 'run duration MIN' where MIN > 0 and MAX < %d\n", 
	       MAX_RUN_DUR);
	bad_command = 1;
      } else {
	tmp_cmnd[1] = (unsigned char)(dur)&0xFF;
	tmp_cmnd[2] = (unsigned char)(dur>>8)&0xFF;
	cmnd_length = 3;
	// adjust run config in database file
	sprintf(syscmd, "python3 database_entry.py RBinfo runtype %smin", set1);
        system(syscmd);
      }
      
    } else if (strncasecmp(param,"init",3) == 0) {
      // Handle the RUN init -- Initialize the readout board
      tmp_cmnd[0] = RB_INIT;
      printf("\tWait 15 seconds before executing another 'run' option\n");
      
    } else if (strncasecmp(param,"pedestal",3) == 0) {
      // Handle the RUN pedestal command
      tmp_cmnd[0] = RUN_PEDESTAL;
      int ped = atoi(set1);
      int min_ped = 60, max_ped = 255; 
      if ( (ped < min_ped) || (ped > max_ped) ) {
	printf("\tUsage 'run pedestal PED' where PED > %d and PED < %d\n", 
	       min_ped, max_ped);
	bad_command = 1;
      }
      tmp_cmnd[1] = (unsigned char)(ped)&0xFF;
      cmnd_length = 2;
      
    } else if (strncasecmp(param,"runno",3) == 0) {
      // Handle the RUN runno command
      tmp_cmnd[0] = RUN_RUNNO;
      int runno = atoi(set1);
      if ( (runno < 0) || (runno > MAX_RUN_NUM) ) {
	printf("\tUsage 'run runno NUM' where NUM > 0 and MIN < %d\n", 
	       MAX_RUN_NUM);
	bad_command = 1;
      } else {
	tmp_cmnd[1] = (unsigned char)(runno)&0xFF;
	tmp_cmnd[2] = (unsigned char)(runno>>8)&0xFF;
	cmnd_length = 3;
      }
      
    } else {
      // Error message for bad RUN command
      PrintUsage(cmnd);
      bad_command = 1;
    }	
    
    //  Handle Local Trigger Board commands
  } else if (strncasecmp(cmnd,"ltb",3) == 0) {

    if (strncasecmp(param,"start",3) == 0) {
      tmp_cmnd[0] = LTB_START;

    } else if (strncasecmp(param,"set",3) == 0) {
      int thr = atoi(set1);
      int volt = atoi(set2);
      if (thr == 0 || thr == 1 || thr == 2) {
        if (volt >= 20 && volt <= 2500) {
          tmp_cmnd[0] = LTB_SET;
	  tmp_cmnd[1] = (unsigned char)(thr)&0xFF;
	  tmp_cmnd[2] = (unsigned char)(volt)&0xFF;
	  cmnd_length = 3;
	  // adjust thr X param setting in database file
	  sprintf(syscmd, "python3 database_entry.py TriggerConfig thr%s %s", set1, set2);
          system(syscmd);
	}
	else {
          printf("invalid voltage value, use values between 20 - 2500 [mV]\n");
	  bad_command = 1;
	}
      } else {
	printf("invalid threshold, use 0, 1, or 2\n");
	bad_command = 1;
      }
    } else {
      // Error message for bad LTB command
      PrintUsage(cmnd);
      bad_command = 1;
    }
    
    // Handle VB related commands
  } else if (strncasecmp(cmnd,"vbias",3) == 0) {
    
    // Find function and assign values 
    if (strncasecmp(param,"on",2) == 0) {
      // Handle the VB on command
      tmp_cmnd[0] = VB_ON;
      
    } else if (strncasecmp(param,"off",2) == 0) {
      // Handle the VB off command
      tmp_cmnd[0] = VB_OFF;
      
    } else if (strncasecmp(param,"set",2) == 0) {
      // Handle the VB setting command
      tmp_cmnd[0] = VB_SET;
      /*      bad_command |= CheckChannel(set1, &ch);
      if (ch==MAX_VB_CHNLS) printf("\tSetting VB for all channels.\n");
      // We have a valid channel, so encode it into the command
      tmp_cmnd[1] = (unsigned char)(ch&0xFF);
      bad_command |= CheckVoltage(set2, ch, &volt);
      // We have a valid VBset value which we encode into the command
      tmp_cmnd[2] = (unsigned char)(volt)&0xFF;
      tmp_cmnd[3] = (unsigned char)(volt>>8)&0xFF;
      cmnd_length = 4;
      if (bad_command) {
	printf("\tUse 'vbias %s CHNL VB' where CHNL is 0,1...,%d or \"all\".\n",
	       param, MAX_VB_CHNLS-1);
	printf("\t\t\t VB is 0-900 or 0-4 for \"all\".\n");
	}	*/
      
    } else {
      // Error message for bad VB command
      PrintUsage(cmnd);
      bad_command = 1;
    }
    
  } else if (strncasecmp(cmnd,"mon",3) == 0) {
    
    // Find proper monitor function 
    if (strncasecmp(param,"start",3) == 0) {
      // Handle the MON stop command
      tmp_cmnd[0] = MON_START;
      
    } else if (strncasecmp(param,"stop",3) == 0) {
      // Handle the MON start command
      tmp_cmnd[0] = MON_STOP;
      
    } else if (strncasecmp(param,"interval",3) == 0) {
      // Handle the MON interval setting command
      int interval;
      tmp_cmnd[0] = MON_INTERVAL;
      bad_command |= CheckInterval(set1, &interval);
      if (bad_command) {
	printf("\tUse 'mon interval XX' where XX = 1...3600(s).\n");
      } else {
	tmp_cmnd[1] = (unsigned char)(interval&0xFF);
	tmp_cmnd[2] = (unsigned char)(interval>>8)&0xFF;
	cmnd_length = 3;
	//printf("\tSetting DaqMonInterval to %d.\n", interval);
      }
      
    } else if (strncasecmp(param,"duration",3) == 0) {
      // Handle the MON duration setting command
      int dur = atoi(set1);
      tmp_cmnd[0] = MON_DURATION;
      if ( (dur < 0) || (dur > MAX_RUN_DUR) ) {
	printf("\tUsage 'mon duration MIN' where MIN > 0 and MAX < %d\n", 
	       MAX_RUN_DUR);
	bad_command = 1;
      } else {
	tmp_cmnd[1] = (unsigned char)(dur&0xFF);
	tmp_cmnd[2] = (unsigned char)(dur>>8)&0xFF;
	cmnd_length = 3;
	//printf("\tSetting Mon Duration to %d.\n", dur);
      }
      
    } else if (strncasecmp(param,"logic",3) == 0) {
      // Handle the MON logic command
      tmp_cmnd[0] = MON_LOGIC;
      int logic = atoi(set1);
      if ( (logic < 1) || (logic > 15) ) {
	printf("\tUsage 'mon logic OPT' where: \n OPT = 1 => GFP 12x12\n OPT = 2 => GFP 12x4\n OPT = 3 => GFP middle 4x4\n OPT = 4 => GFP middle 4x2\n OPT = 5 => RB 8x8\n OPT = 6 through 9 => RB channels 1x1 for each of the middle 4 top+bottom paddle pairs\n OPT = 10 => RB 2+5 4x4\n OPT = 11 => RB 2+5 4x2\n OPT = 12 through 15 => RB 2+5 channels 1x1 for each of the end 4 top+bottom paddle pairs\n \nFor more info, see the Data Taking section of the GFP Setup at Bates Wiki page\n");
	bad_command = 1;
      } else {
	printf("Logic = %d\n", logic);
	tmp_cmnd[1] = (unsigned char)(logic)&0xFF;
	cmnd_length = 2;
	// adjusting database file happens from the vme computer
      }      
    } else { 
      PrintUsage(cmnd);
      bad_command = 1;
    }
    
  } else if (strncasecmp(cmnd,"daq",3) == 0) {
    
    // Find proper daq function 
    if (strncasecmp(param,"stop",3) == 0) {
      // Handle the DAQ stop command
      printf("THIS WILL STOP THE DAQ AND SEVER COMMUNICATIONS!!\n");
      printf("\tARE YOU SURE? (answer with 'Y') ");
      
      int response = 0 , tmp = 0;
      char string[10] = "Y";
      
      response = getc(stdin);
      tmp = response;
      while(tmp != 10) tmp = getc(stdin); // Read until newline character
      if ( response != (int)*string ) 
	bad_command = 1;
      else 
	tmp_cmnd[0] = DAQ_STOP;
      
    } else if (strncasecmp(param,"restart",3) == 0) {
      // Handle the DAQ restart command
      tmp_cmnd[0] = DAQ_RESTART;
      
    } else if (strncasecmp(param,"halt",3) == 0) {
      // Handle the DAQ halt command
      tmp_cmnd[0] = DAQ_HALT;
      
    } else if (strncasecmp(param,"reboot",3) == 0) {
      // Handle the DAQ reboot command
      tmp_cmnd[0] = DAQ_REBOOT;
      
    } else if (strncasecmp(param,"noop",3) == 0) {
      // Handle the DAQ noop command
      tmp_cmnd[0] = DAQ_NOOP;
      
    } else {
      // Error message for bad DAQ command
      PrintUsage(cmnd);
      bad_command = 1;
    }	
    
    // Find proper run function 
  } else if (strncasecmp(cmnd,"quit",3) == 0) {
    commandWait = false;
    bad_command = 1;
  } else {
    PrintUsage(cmnd);
    bad_command = 1;
  }
  return bad_command;
}

void TOFCommand::PrintUsage(char *cmd) {

  if ( strncmp(cmd, "run", 3) == 0 ) {
    printf("\trun    -- start, stop, events, duration and init\n");
  } else if (strncasecmp(cmd,"vbias",3) == 0) {
    printf("\tvbias  -- on, off and set\n");
  } else if (strncasecmp(cmd,"ltb",3) == 0) {
    printf("\tltb    -- set and start\n");
  } else if (strncasecmp(cmd,"mon",3) == 0) {
    printf("\tmon    -- start, stop, interval, duration and logic\n");
  } else if (strncasecmp(cmd,"comm",3) == 0) {
    printf("\tcomm   -- rbs, pis, vme\n");
  } else if (strncasecmp(cmd,"daq",3) == 0) {
    printf("\tdaq -- stop, restart, halt, reboot and noop\n");
  } else {
    printf("Invalid command: %s\n", cmd);
  }
}

bool TOFCommand::CommandWait() {
  return commandWait;
}

int TOFCommand::GetCommand(unsigned char *sent, unsigned char *length) {
  for(int i=0; i<cmnd_length; i++) 
    sent[i] = tmp_cmnd[i];
  *length = cmnd_length;
  return 1;
}

int TOFCommand::SendCommand() {

  signed char i;
  unsigned short int NRBS = 6;
  unsigned short int NPBS = 6;
  
  signed short int sendtorb_port = PORT_RECV;
  signed short int sendtopb_port = PORT_RECV;
  signed short int sendtovme_port = PORT_RECV;
  // PORT_RECV is the port the Daq is listening on, thus the port this
  // program should send to.
  //char sendto_eth[] = "128.97.102.130"; // gaps1
  char sendto_rbs[NRBS][25];
  for (int i=0;i<NRBS;i++) 
    sprintf(sendto_rbs[i], "10.0.1.%d", i+1);
  char sendto_pbs[NRBS][25];
  for (int i=0;i<NRBS+1;i++) 
    sprintf(sendto_pbs[i], "10.1.0.%d", i+1);
  /* The following works on the Ubuntu systems but not the VME crate
  char sendto_rbs[NRBS][25] = {"10.0.1.1", "10.0.1.2", "10.0.1.3",
			       "10.0.1.4", "10.0.1.5", "10.0.1.6"};  
  char sendto_pbs[NRBS][25] = {"10.1.0.1", "10.1.0.2", "10.1.0.3",
                               "10.1.0.4", "10.1.0.5", "10.1.0.6"};  */
  //char sendto_vme[25] = "10.97.108.21"; // VME at UCLA
  char sendto_vme[25] = "192.168.37.33"; // VME at Bates
  
  // Connections to Readout Boards
  struct sockaddr_in sendto_rbs_addr[NRBS];
  for (int j=0; j<NRBS; j++) {
    sendto_rbs_addr[j].sin_family = AF_INET;
    sendto_rbs_addr[j].sin_port = htons(sendtorb_port);
    sendto_rbs_addr[j].sin_addr.s_addr = inet_addr(sendto_rbs[j]);
  }
  
  // Connections to Power Boards (Pis)
  struct sockaddr_in sendto_pbs_addr[NPBS];
  for (int j=0; j<NRBS; j++) {
    sendto_pbs_addr[j].sin_family = AF_INET;
    sendto_pbs_addr[j].sin_port = htons(sendtopb_port);
    sendto_pbs_addr[j].sin_addr.s_addr = inet_addr(sendto_pbs[j]);
  }
  
  // Connections to VME crate
  struct sockaddr_in sendto_vme_addr;
  sendto_vme_addr.sin_family = AF_INET;
  sendto_vme_addr.sin_port = htons(sendtovme_port);
  sendto_vme_addr.sin_addr.s_addr = inet_addr(sendto_vme);
  
  // The for loop gives us 10 chances to try and establish the 
  // command socket...after which we give up trying to send the 
  // command and return a FALSE to the calling function
  for(i = 0; i < 10 ; i++)   
    {
      signed short int cmdSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
      
      if(cmdSocket == -1) usleep(1000);
      else   
	{
	  // We've connected, send the command to the DSP, close the socket,
	  // and then break out of the loop

	  //signed short int errorCode = 0;
	  if (tmp_cmnd[0] > 0x00 && tmp_cmnd[0] < 0x0F) {// send to RBs
	    for (int j=0;j<NRBS;j++) {
	      sendto(cmdSocket, tmp_cmnd, cmnd_length, 0,
		     (struct sockaddr *)&sendto_rbs_addr[j],
		     sizeof(sendto_rbs_addr[j]));
	      /*errorCode = sendto(cmdSocket, tmp_cmnd, cmnd_length, 0,
				 (struct sockaddr *)&sendto_rbs_addr[j],
				 sizeof(sendto_rbs_addr[j])); */
	      //printf("RB%d(%d)  ", j, errorCode);
	    }
	    //printf("\n");
	  } else if (tmp_cmnd[0] >= 0x20 && tmp_cmnd[0] < 0x2F) {//send to Pis
	    for (int j=0;j<NPBS;j++) {
	      sendto(cmdSocket, tmp_cmnd, cmnd_length, 0,
		     (struct sockaddr *)&sendto_pbs_addr[j],
		     sizeof(sendto_pbs_addr[j]));
	    }
	  } else if (tmp_cmnd[0] >= 0x30 && tmp_cmnd[0] < 0x3F) {// send to VME
	    sendto(cmdSocket, tmp_cmnd, cmnd_length, 0,
		   (struct sockaddr *)&sendto_vme_addr,
		   sizeof(sendto_vme_addr));
	  }
	  
	  //printf("ip: %s ", sendto_eth);
	  //printf("port: %d ", sendto_port);
	  //printf("time: %d ",t);
	  //for(int k = 0; k < cmnd_length; k++) printf("%02X",tmp_cmnd[k]);
	  //printf("\n");
	  
	  close(cmdSocket);
	  break;
	}
    }
  return(0);
}

int TOFCommand::CheckChannel(char *tmp_ch, int *ch){ 
  // Check for a valid channel setting
  int tmp_val = atoi(tmp_ch);
  if (strncasecmp(tmp_ch,"all",3) == 0) {  // Check for "all"
    tmp_val = MAX_VB_CHNLS;
  } else if ( (tmp_val < 0) || (tmp_val > MAX_VB_CHNLS-1) ) { // Must be 0-31
    return 1;
  } 
  *ch = tmp_val;
  return 0;
}

int TOFCommand::CheckProfile(char *tmp_pr, int *pr){ 
  // Check for a valid profile setting
  int prof = -1;
  int tmp = atoi(tmp_pr);
  if ( tmp == 0 )      prof = VB_FILE_DEF;
  else if ( tmp == 1 ) prof = VB_FILE_1;
  else if ( tmp == 2 ) prof = VB_FILE_2;
  else if ( tmp == 3 ) prof = VB_FILE_3;
  else if ( tmp == 4 ) prof = VB_FILE_4;
  
  if (tmp<0 || tmp>4) 
    return 1;
  else {
    *pr = prof;
    return 0;
  }
}

int TOFCommand::CheckInterval(char *tmp_int, int *interval){ 
  // Check for a valid interval
  int tmp_val = atoi(tmp_int);
  if (tmp_val > 0 && tmp_val < 3600) {
    *interval = tmp_val;
    return 0;
  } else {
    *interval = -1;
    return 1;
  }
}

int TOFCommand::CheckVoltage(char *tmp_vb, int ch, int *voltage){
  // Check for a valid voltage setting
  int tmp_val;
  if (ch == MAX_VB_CHNLS) {
    int file = atoi(tmp_vb);
    if ( file == 0 ) tmp_val = VB_FILE_DEF;
    else if ( file == 1 ) tmp_val = VB_FILE_1;
    else if ( file == 2 ) tmp_val = VB_FILE_2;
    else if ( file == 3 ) tmp_val = VB_FILE_3;
    else if ( file == 4 ) tmp_val = VB_FILE_4;
    else tmp_val = -1;
  } else {
    float vbset_f = atof(tmp_vb);
    if ( (vbset_f > 60) || (vbset_f < 0) ) return 1;
    tmp_val = (int)(vbset_f*10);
  }
  *voltage = tmp_val;
  return 0;
}
 

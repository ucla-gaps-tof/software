#ifndef COMMANDDEFS
#define COMMANDDEFS

// This file defines many of the signals for communicating from the
// flight computer to the DAQ computer. If you change anything, you
// need to propagate the changes to the other computer. JAZ 20110402

#define PORT_SEND    10068
#define PORT_RECV    10067

// Run Control commands
#define RUN_START     0x01
#define RUN_STOP      0x02
#define RUN_PAUSE     0x03
#define RUN_RESTART   0x04
#define RUN_EVENTS    0x05
#define RUN_DURATION  0x06
#define RUN_RUNNO     0x07
#define RB_INIT       0x08
#define RUN_PEDESTAL  0x09
#define MAX_RUN_DUR   1440
#define MAX_RUN_EVT  65536
#define MAX_RUN_NUM  65535

// Reserve 10-1F for data sent back to Flight.

// VBias Control commands
#define MAX_VB_CHNLS    32
#define VB_ON         0x20
#define VB_OFF        0x21
#define VB_SET        0x22
#define VB_WRITE      0x23
// Raspberry Pi Commands
#define LTB_START     0x25
#define LTB_SET       0x26

#define VB_FILE_DEF   0x2A
#define VB_FILE_1     0x2B
#define VB_FILE_2     0x2C
#define VB_FILE_3     0x2D
#define VB_FILE_4     0x2E

// DAQ Monitor Controls
#define MON_START     0x30
#define MON_STOP      0x31
#define MON_INTERVAL  0x32
#define MON_DURATION  0x33
#define MON_LOGIC     0x34

// DAQ DATA Streams
#define DATA_EVENT    0x10
#define DATA_HOUSE    0x11
#define DATA_HBEAT    0x12
#define DATA_COMMAND  0x13
#define DATA_EVENTF   0x14
#define DATA_BRUN     0x15
#define DATA_ERUN     0x16
#define DATA_VER1     0x91
#define DATA_VER2     0x92
#define DATA_VER3     0x93
#define DATA_VER4     0x94

// DAQ Computer Controls
#define DAQ_NOOP      0xF0
#define DAQ_RESTART   0xF1
#define DAQ_REBOOT    0xFA
#define DAQ_HALT      0xFB
#define DAQ_STOP      0xFE

#define BAD_COMMAND   0xF2

#endif //COMMANDDEFS

"""
Author: Sean Quinn (spq@ucla.edu)
get_rand_unused_port.py (c) UCLA 2021
Desc: writes unused port number to stdout
Created:  2021-08-06T22:46:10.274Z
Modified: 2021-08-06T22:53:00.833Z
"""
import socket

def main():
  skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  skt.bind(('', 0))
  addr, port = skt.getsockname()
  print(port)

if __name__ == '__main__':
  main()
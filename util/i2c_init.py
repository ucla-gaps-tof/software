"""
Author: Sean Quinn
i2c_init.py (c) UCLA 2020
Desc: Tools to map file handles to hardware devices
Created:  2020-11-12T20:04:36.648Z
Modified: 2020-11-24T20:28:44.213Z
"""

import sys
import os
import logging
import pathlib

# Seems like utf encoding not supported
logging.basicConfig(filename='i2c_init.log', level=logging.DEBUG)
logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Magic vars
ps_i2c_bus = "0"
mux_chn_num = 8
mux0 = "77" #Hex value of mux hardware address
mux1 = "75" #Hex value of mux hardware address
mux_list = [mux0, mux1]
mux0_chn_info = {"channel-0":"Si5395_Temp",
            "channel-1":"Si5395_ctl",
            "channel-2":"dac_ctl",
            "channel-3":"adcdig_pwr",
            "channel-4":"adc_temp",
            "channel-5":"adcana_pwr",
            "channel-6":"drs4ana_pwr",
            "channel-7":"gpio_ctl"}
mux1_chn_info = {"channel-0":"BME280_Env",
            "channel-1":"Bfield_sense",
            "channel-2":"drs4_Temp",
            "channel-3":"drs4dig_pwr",
            "channel-4":"3v3_pwr",
            "channel-5":"3v3zynq_pwr",
            "channel-6":"n1v_pwr",
            "channel-7":"p4v_pwr"}

# Ensure mux chips present on bus
def mux_detect():
    i2c_dev_list = os.listdir('/sys/bus/i2c/devices/')
    # Assume no devices on bus
    dev_count = 0
    for x in i2c_dev_list:
        if mux0 in x:
            logging.info("Found mux0. File handle: %s" %x)
            dev_count += 1
        if mux1 in x:
            logging.info("Found mux1. File handle: %s" %x)
            dev_count +=1
    if dev_count < len(mux_list):
        logging.critical("Did not find all expected mux devices. There is a serious, possible hardware, problem.")
        return -1
    logging.info("Correct number of mux chips present on bus.")
    return 0

# Provide user with mapping of hardware devices to Linux file descriptors
def file_handle_map():
    mux_detect_ret = mux_detect()
    if mux_detect_ret < 0:
        return -1
    # Generic channel string list
    chn_str = ["channel-%i" %i for i in range(mux_chn_num)]
    # User friendly dict for applications
    usr_friendly_dict = {}
    # Map mux0
    mux0_enum = []
    for i in range(mux_chn_num):
        i2c_dev = os.listdir("/sys/bus/i2c/devices/%s-00%s/channel-%i/i2c-dev/" %(ps_i2c_bus,mux0,i))
        fd = "/dev/" + i2c_dev[0]
        mux0_enum.append(fd)
        usr_friendly_tmp = {mux0_chn_info["channel-%i" %i] : fd}
        usr_friendly_dict.update(usr_friendly_tmp)
        logging.info("%s --> /dev/%s" %(mux0_chn_info["channel-%i" %i], fd))
    mux0_dict = dict(zip(chn_str,mux0_enum))
    # Map mux1
    mux1_enum = []
    for i in range(mux_chn_num):
        i2c_dev = os.listdir("/sys/bus/i2c/devices/%s-00%s/channel-%i/i2c-dev/" %(ps_i2c_bus,mux1,i))
        fd = "/dev/" + i2c_dev[0]
        mux1_enum.append(fd)
        usr_friendly_tmp = {mux1_chn_info["channel-%i" %i] : fd}
        usr_friendly_dict.update(usr_friendly_tmp)
        logging.info("%s --> /dev/%s" %(mux1_chn_info["channel-%i" %i], fd))
    mux1_dict = dict(zip(chn_str,mux1_enum))
    # Full, nested, unambiguous mapping dict
    full_dict = {mux0 : mux0_dict, mux1 : mux1_dict}
    return usr_friendly_dict, full_dict
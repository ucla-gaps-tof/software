import configparser

from src.pca9548a import PCA9548A
from src.ina226 import INA226
from src.max11645 import MAX11645

config = configparser.ConfigParser()
config.read('readout-board.ini', encoding='utf-8')

i2c_bus = int(config['COMMON']['i2c_bus'])
pca9548a_address_0 = int(config['COMMON']['pca9548a_address_0'], 16)
pca9548a_address_1 = int(config['COMMON']['pca9548a_address_1'], 16)

def read_ina226(channel, pca9548a_address, i2c_bus, ina226_address, ina226_rshunt, ina226_mec):
    PCA9548A(channel, pca9548a_address, i2c_bus).channel_selector()
    voltage, current, power = INA226(ina226_address, ina226_rshunt, ina226_mec, i2c_bus).meas_all()
    return voltage, current, power

def read_max11645(channel, pca9548a_address, i2c_bus, max11645_address, max11645_channel):
    PCA9548A(channel, pca9548a_address, i2c_bus).channel_selector()
    voltage = MAX11645(max11645_address, max11645_channel, i2c_bus).adc_voltage()
    return voltage

drs4_dvdd_ina226_channel = int(config['INA226']['drs4_dvdd_ina226_channel'])
drs4_dvdd_ina226_address = int(config['INA226']['drs4_dvdd_ina226_address'], 16)
drs4_dvdd_ina226_mec = float(config['INA226']['drs4_dvdd_ina226_mec'])
drs4_dvdd_ina226_rshunt = float(config['INA226']['drs4_dvdd_ina226_rshunt'])
drs4_dvdd_voltage, drs4_dvdd_current, drs4_dvdd_power = read_ina226(drs4_dvdd_ina226_channel, pca9548a_address_0, i2c_bus, drs4_dvdd_ina226_address, drs4_dvdd_ina226_rshunt, drs4_dvdd_ina226_mec)
print("Measurements for DRS4 DVDD Rail")
print("Bus Voltage:   {}V".format(round(drs4_dvdd_voltage, 3)))
print("Current:       {}A".format(round(drs4_dvdd_current, 3)))
print("Power:         {}W".format(round(drs4_dvdd_power, 3)))

p3v3_ina226_channel = int(config['INA226']['p3v3_ina226_channel'])
p3v3_ina226_address = int(config['INA226']['p3v3_ina226_address'], 16)
p3v3_ina226_mec = float(config['INA226']['p3v3_ina226_mec'])
p3v3_ina226_rshunt = float(config['INA226']['p3v3_ina226_rshunt'])
p3v3_voltage, p3v3_current, p3v3_power = read_ina226(p3v3_ina226_channel, pca9548a_address_0, i2c_bus, p3v3_ina226_address, p3v3_ina226_rshunt, p3v3_ina226_mec)
print("Measurements for 3V3 Rail")
print("Bus Voltage:   {}V".format(round(p3v3_voltage, 3)))
print("Current:       {}A".format(round(p3v3_current, 3)))
print("Power:         {}W".format(round(p3v3_power, 3)))

zynq_ina226_channel = int(config['INA226']['zynq_ina226_channel'])
zynq_ina226_address = int(config['INA226']['zynq_ina226_address'], 16)
zynq_ina226_mec = float(config['INA226']['zynq_ina226_mec'])
zynq_ina226_rshunt = float(config['INA226']['zynq_ina226_rshunt'])
zynq_voltage, zynq_current, zynq_power = read_ina226(zynq_ina226_channel, pca9548a_address_0, i2c_bus, zynq_ina226_address, zynq_ina226_rshunt, zynq_ina226_mec)
print("Measurements for ZYNQ Rail")
print("Bus Voltage:   {}V".format(round(zynq_voltage, 3)))
print("Current:       {}A".format(round(zynq_current, 3)))
print("Power:         {}W".format(round(zynq_power, 3)))

p3v5_ina226_channel = int(config['INA226']['p3v5_ina226_channel'])
p3v5_ina226_address = int(config['INA226']['p3v5_ina226_address'], 16)
p3v5_ina226_mec = float(config['INA226']['p3v5_ina226_mec'])
p3v5_ina226_rshunt = float(config['INA226']['p3v5_ina226_rshunt'])
p3v5_voltage, p3v5_current, p3v5_power = read_ina226(p3v5_ina226_channel, pca9548a_address_0, i2c_bus, p3v5_ina226_address, p3v5_ina226_rshunt, p3v5_ina226_mec)
print("Measurements for 3V5 Rail")
print("Bus Voltage:   {}V".format(round(p3v5_voltage, 3)))
print("Current:       {}A".format(round(p3v5_current, 3)))
print("Power:         {}W".format(round(p3v5_power, 3)))

adc_dvdd_ina226_channel = int(config['INA226']['adc_dvdd_ina226_channel'])
adc_dvdd_ina226_address = int(config['INA226']['adc_dvdd_ina226_address'], 16)
adc_dvdd_ina226_mec = float(config['INA226']['adc_dvdd_ina226_mec'])
adc_dvdd_ina226_rshunt = float(config['INA226']['adc_dvdd_ina226_rshunt'])
adc_dvdd_voltage, adc_dvdd_current, adc_dvdd_power = read_ina226(adc_dvdd_ina226_channel, pca9548a_address_1, i2c_bus, adc_dvdd_ina226_address, adc_dvdd_ina226_rshunt, adc_dvdd_ina226_mec)
print("Measurements for ADC DVDD Rail")
print("Bus Voltage:   {}V".format(round(adc_dvdd_voltage, 3)))
print("Current:       {}A".format(round(adc_dvdd_current, 3)))
print("Power:         {}W".format(round(adc_dvdd_power, 3)))

adc_avdd_ina226_channel = int(config['INA226']['adc_avdd_ina226_channel'])
adc_avdd_ina226_address = int(config['INA226']['adc_avdd_ina226_address'], 16)
adc_avdd_ina226_mec = float(config['INA226']['adc_avdd_ina226_mec'])
adc_avdd_ina226_rshunt = float(config['INA226']['adc_avdd_ina226_rshunt'])
adc_avdd_voltage, adc_avdd_current, adc_avdd_power = read_ina226(adc_avdd_ina226_channel, pca9548a_address_1, i2c_bus, adc_avdd_ina226_address, adc_avdd_ina226_rshunt, adc_avdd_ina226_mec)
print("Measurements for ADC AVDD Rail")
print("Bus Voltage:   {}V".format(round(adc_avdd_voltage, 3)))
print("Current:       {}A".format(round(adc_avdd_current, 3)))
print("Power:         {}W".format(round(adc_avdd_power, 3)))

drs4_avdd_ina226_channel = int(config['INA226']['drs4_avdd_ina226_channel'])
drs4_avdd_ina226_address = int(config['INA226']['drs4_avdd_ina226_address'], 16)
drs4_avdd_ina226_mec = float(config['INA226']['drs4_avdd_ina226_mec'])
drs4_avdd_ina226_rshunt = float(config['INA226']['drs4_avdd_ina226_rshunt'])
drs4_avdd_voltage, drs4_avdd_current, drs4_avdd_power = read_ina226(drs4_avdd_ina226_channel, pca9548a_address_1, i2c_bus, drs4_avdd_ina226_address, drs4_avdd_ina226_rshunt, drs4_avdd_ina226_mec)
print("Measurements for DRS4 AVDD Rail")
print("Bus Voltage:   {}V".format(round(drs4_avdd_voltage, 3)))
print("Current:       {}A".format(round(drs4_avdd_current, 3)))
print("Power:         {}W".format(round(drs4_avdd_power, 3)))

max11645_channel = int(config['MAX11645']['max11645_channel'])
max11645_address = int(config['MAX11645']['max11645_address'], 16)
n1v5_ina200_voltage_channel = int(config['INA200']['n1v5_ina200_voltage_channel'])
n1v5_voltage = -1 * read_max11645(max11645_channel, pca9548a_address_0, i2c_bus, max11645_address, n1v5_ina200_voltage_channel)
n1v5_ina200_current_channel = int(config['INA200']['n1v5_ina200_current_channel'])
n1v5_ina200_gain = int(config['INA200']['n1v5_ina200_gain'])
n1v5_ina200_rshunt = float(config['INA200']['n1v5_ina200_rshunt'])
n1v5_current = read_max11645(max11645_channel, pca9548a_address_0, i2c_bus, max11645_address, 0) / n1v5_ina200_gain / n1v5_ina200_rshunt
n1v5_power = abs(n1v5_voltage) * n1v5_current
print("Measurements -1V5 Rail")
print("Bus Voltage:   {}V".format(round(n1v5_voltage, 3)))
print("Current:       {}A".format(round(n1v5_current, 3)))
print("Power:         {}W".format(round(n1v5_power, 3)))

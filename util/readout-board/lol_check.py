import smbus
import configparser

from src.pca9548a import PCA9548A

config = configparser.ConfigParser()
config.read('readout-board.ini', encoding='utf-8')

i2c_bus = int(config['COMMON']['i2c_bus'])
pca9548a_address_1 = int(config['COMMON']['pca9548a_address_1'], 16)

si5395a_channel = int(config['SI5395A']['si5395a_channel'])
si5395a_address = int(config['SI5395A']['si5395a_address'], 16)

PCA9548A(si5395a_channel, pca9548a_address_1, i2c_bus).channel_selector()

page = 0x000E >> 8
reg = 0x000E & 0xFF
smbus.SMBus(i2c_bus).write_byte_data(si5395a_address, 0x01, page)
lol_status = smbus.SMBus(i2c_bus).read_byte_data(si5395a_address, reg)

if ((lol_status & 0x02) >> 1)== 1:
    print("LOL Status:  Unlocked")
else:
    print("LOL Status:  Locked!")

# print(bin(lol_status))
# print(lol_status)
# print(hex(lol_status))

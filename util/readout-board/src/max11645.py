import smbus

# Register
SETUP = 0x80
CONFIG = 0x00

# Setup Parameters
SETUP_Vdd_NC_OFF = 0x00         # Reference Voltage = Vdd, REF = Not Connected, Internal Reference State = Always Off
SETUP_ER_RI_OFF = 0x20          # Reference Voltage = External Reference, REF = Reference Input, Internal Reference State = Always Off
SETUP_IR_NC_OFF = 0x40          # Reference Voltage = Internal Reference, REF = Not Connected, Internal Reference State = Always Off
SETUP_IR_NC_ON = 0x50           # Reference Voltage = Internal Reference, REF = Not Connected, Internal Reference State = Always On
SETUP_IR_RO_OFF = 0x60          # Reference Voltage = Internal Reference, REF = Reference Output, Internal Reference State = Always Off
SETUP_IR_RO_ON = 0x70           # Reference Voltage = Internal Reference, REF = Reference Output, Internal Reference State = Always On
SETUP_INT_CLK = 0x00            # Internal Clock
SETUP_EXT_CLK = 0x08            # External Clock
SETUP_UNI = 0x00                # Unipolar
SETUP_BIP = 0x04                # Bipolar
SETUP_RST = 0x00                # Reset
SETUP_NA = 0x01                 # No Action

# Configuration Parameters
CONFIG_SCAN_0 = 0x00            # Scans up from AIN0 to the input selected by CS0.
CONFIG_SCAN_1 = 0x20            # Converts the input selected by CS0 eight times.
CONFIG_SCAN_2 = 0x40            # Reserved. Do not use.
CONFIG_SCAN_3 = 0x60            # Converts channel selected by CS0.
CONFIG_DIF = 0x00               # Differential
CONFIG_SGL = 0x01               # Single-ended

class MAX11645:

    def __init__(self, address, channel, bus=0):
        self._address = address
        self._channel = channel
        self._bus = smbus.SMBus(bus)
        self.setup()
        self.configure()

    def setup(self):
        setup_register = SETUP | SETUP_IR_RO_ON | SETUP_INT_CLK | SETUP_UNI | SETUP_RST
        self._bus.write_byte_data(self._address, 0, setup_register)

    def configure(self):
        self.config = CONFIG | CONFIG_SCAN_3 | self.channel_selector() | CONFIG_SGL
        self._bus.write_byte_data(self._address, 0, self.config)

    def channel_selector(self):
        if self._channel == 0:    self.select = 0x00
        elif self._channel == 1:  self.select = 0x02
        else:
            select = 0x00
        
        return self.select

    def read_adc(self):
        block_data = self._bus.read_i2c_block_data(self._address, self.config, 2)
        self.adc_value = (((block_data[0] & 0x0f) << 8) | (block_data[1] & 0xFF))
        return self.adc_value

    def adc_voltage(self):
        self.internal_reference = 2.048
        self.voltage = self.internal_reference*self.read_adc()/4096
        return self.voltage

if __name__ == '__main__':
    adc_bus = 0
    adc_address = 0x35
    adc_channels = [0, 1]
    for channel in adc_channels:
        voltage = MAX11645(adc_address, channel, adc_bus).adc_voltage()
        print("Channel {}".format(channel))
        print(voltage)
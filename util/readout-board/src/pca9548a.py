import smbus

class PCA9548A:
    def __init__(self, channel, address, bus=0):
        self._channel = channel
        self._address = address
        self._bus = smbus.SMBus(bus)

    def channel_selector(self):

        if self._channel == 0:    select = 0x01
        elif self._channel == 1:  select = 0x02
        elif self._channel == 2:  select = 0x04
        elif self._channel == 3:  select = 0x08
        elif self._channel == 4:  select = 0x10
        elif self._channel == 5:  select = 0x20
        elif self._channel == 6:  select = 0x40
        elif self._channel == 7:  select = 0x80
        else:
            select = 0x00

        self._bus.write_byte_data(self._address, 0x00, select)

if __name__ == '__main__':
    while True:
        pca9548a_bus = int(input("Type I2C Bus Number [default: 0]: ") or "0")
        if pca9548a_bus >= 0 and pca9548a_bus <= 6:
            break
        else:
            print("Select Appropriate I2C Bus Number!")

    while True:
        pca9548a_address = int(input("Select I2C Multiplexer Address [default: 0x75]: ") or "0x75", 16) # input must be hex
        if pca9548a_address == 0x75 or pca9548a_address == 0x77:
            break
        else:
            print("Select Appropriate I2C Address!")

    while True:
        pca9548a_channel = int(input("Select Channel from 0 to 7 [default: 0]: ") or "0")
        if pca9548a_channel >= 0 and pca9548a_channel < 8:
            break
        else:
            print("Channel must be between 0 to 7!")

    PCA9548A(pca9548a_channel, pca9548a_address, pca9548a_bus).channel_selector()
    print("Channel {} on the i2c multiplexer at {} is selected.".format(pca9548a_channel, hex(pca9548a_address)))
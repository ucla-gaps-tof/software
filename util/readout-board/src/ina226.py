import smbus

# Register
CONFIG = 0x00
SHUNT = 0x01
BUS = 0x02
POWER = 0x03
CURRENT = 0x04
CALIB = 0x05


# Configuration Parameters
CONFIG_RST = 0xC000

CONFIG_AVG_1 = 0x4000
CONFIG_AVG_4 = 0x4200
CONFIG_AVG_16 = 0x4400
CONFIG_AVG_64 = 0x4600
CONFIG_AVG_128 = 0x4800
CONFIG_AVG_256 = 0x4A00
CONFIG_AVG_512 = 0x4C00
CONFIG_AVG_1024 = 0x4E00

CONFIG_VBUSCT_140 = 0x4000
CONFIG_VBUSCT_204 = 0x4040
CONFIG_VBUSCT_332 = 0x4080
CONFIG_VBUSCT_588 = 0x40C0
CONFIG_VBUSCT_1100 = 0x4100
CONFIG_VBUSCT_2116 = 0x4140
CONFIG_VBUSCT_4156 = 0x4180
CONFIG_VBUSCT_8244 = 0x41C0

CONFIG_VSHCT_140 = 0x4000
CONFIG_VSHCT_204 = 0x4008
CONFIG_VSHCT_332 = 0x4010
CONFIG_VSHCT_588 = 0x4018
CONFIG_VSHCT_1100 = 0x4020
CONFIG_VSHCT_2116 = 0x4028
CONFIG_VSHCT_4156 = 0x4030
CONFIG_VSHCT_8244 = 0x4038

CONFIG_MODE_PDS = 0x4000            # Power-Down (or Shutdown)
CONFIG_MODE_SVT = 0x4001            # Shunt Voltage, Triggered
CONFIG_MODE_BVT = 0x4002            # Bus Voltage, Triggered
CONFIG_MODE_SBT = 0x4003            # Shunt and Bus, Triggered
CONFIG_MODE_PDS_2 = 0x4004          # Power-Down (or Shutdown) 2
CONFIG_MODE_SVC = 0x4005            # Shunt Voltage, Continuous
CONFIG_MODE_BVC = 0x4006            # Bus Voltage, Continuous
CONFIG_MODE_SBC = 0x4007            # Shunt and Bus, Continuous

def write_register_16bit(register):
    upper_byte = (register >> 8) & 0xff
    lower_byte = register & 0xff
    return [upper_byte, lower_byte]

def read_register_16bit(register):
    data = (register[0] << 8) | (register[1])
    return data

class INA226():

    def __init__(self, address, rshunt, max_expected_current, bus=0):
        self._address = address
        self._bus = smbus.SMBus(bus)
        self._rshunt = rshunt
        self._max_expected_current = max_expected_current
        self.configure()

    def configure(self):
        setup_configuration = CONFIG_AVG_16 | CONFIG_VBUSCT_1100 | CONFIG_VSHCT_1100 | CONFIG_MODE_SBC
        self._bus.write_i2c_block_data(self._address, CONFIG, write_register_16bit(setup_configuration))

    def meas_shunt_voltage(self):
        shunt_voltage = self._bus.read_i2c_block_data(self._address, SHUNT, 2)
        shunt_voltage = read_register_16bit(shunt_voltage)
        if shunt_voltage >= 0x8000:
            shunt_voltage -= 0x10000
        shunt_voltage = shunt_voltage * 0.0000025   # in Volt with 2.5uV LSB
        return shunt_voltage
    
    def meas_bus_voltage(self):
        bus_voltage = self._bus.read_i2c_block_data(self._address, BUS, 2)
        bus_voltage = read_register_16bit(bus_voltage)
        bus_voltage = bus_voltage * 0.00125     # in Volt with 1.25mV LSB
        return bus_voltage

    def meas_power(self):
        self.calibration()
        power = self._bus.read_i2c_block_data(self._address, POWER, 2)
        power = read_register_16bit(power)
        power = power * self.power_lsb
        return power
    
    def meas_current(self):
        self.calibration()
        current = self._bus.read_i2c_block_data(self._address, CURRENT, 2)
        current = read_register_16bit(current)
        if current >= 0x8000:
            current -= 0x10000
        current = current * self.current_lsb
        return current

    def calibration(self):
        self.current_lsb = self._max_expected_current / 2**15
        self.power_lsb = 25 * self.current_lsb
        cal = 0.00512 / (self.current_lsb * self._rshunt)
        self._bus.write_i2c_block_data(self._address, CALIB, write_register_16bit(int(cal)))

    def meas_all(self):
        bus_voltage = self.meas_bus_voltage()
        current = self.meas_current()
        power = self.meas_power()
        return bus_voltage, current, power


if __name__ == '__main__':
    ina226_bus = 0
    ina226_address = 0x44                   # +3.3V
    ina226_max_expected_current = 1.25       # +3.3V
    ina226_rshunt = 0.039
    shunt_voltage, bus_voltage, current, power = INA226(ina226_address, ina226_rshunt, ina226_max_expected_current, ina226_bus).meas_all()
    print("Shund Voltage: {}V".format(shunt_voltage))
    print("Bus Voltage:   {}V".format(bus_voltage))
    print("Current:       {}A".format(current))
    print("Power:         {}W".format(power))
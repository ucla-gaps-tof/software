import smbus

# Register
TEMP = 0x00                     # Temperature Register
CONFIG = 0x01                   # Configuration Register
T_LOW = 0x02                    # T Low Register
T_HIGI = 0x03                   # T High Register


# Configuration Register
CONFIG_CR_025 = 0x0000          # Continuos-Conversion Rate 0.25Hz
CONFIG_CR_1 = 0x0040            # Continuos-Conversion Rate 1Hz
CONFIG_CR_4 = 0x0080            # Continuos-Conversion Rate 4Hz, Default
CONFIG_CR_8 = 0x00C0            # Continuos-Conversion Rate 8Hz

CONFIG_NM = 0x0000              # Normal Mode (12 bit), Default
CONFIG_EM = 0x0010              # Extended Mode (13 bit)

CONFIG_OS = 0x8000              # One-Shot/Conversion Ready Mode

CONFIG_TM_CM = 0x0000           # Thermostat Mode, Comparator Mode
CONFIG_TM_IM = 0x0200           # Thermostat Mode, Interrupt Mode

CONFIG_SD_CC = 0x0000           # Continuous Conversion Mode
CONFIG_SD_SM = 0x0100           # Shutdown Mode

CONFIG_POL = 0x0400             # Polarity

CONFIG_F_1 = 0x0000             # Fault Queue, 1 Consecutive Fault
CONFIG_F_1 = 0x0800             # Fault Queue, 2 Consecutive Faults
CONFIG_F_1 = 0x1000             # Fault Queue, 4 Consecutive Faults
CONFIG_F_1 = 0x1800             # Fault Queue, 6 Consecutive Faults

# High Limit Register
T_HIGH_TEMP = 0x4B0             # 65°C for High Limit

# Low Limit Register
T_LOW_TEMP = 0x3C0              # 60°C for Low Limit

def write_register_16bit(register):
    upper_byte = (register >> 8) & 0xff
    lower_byte = register & 0xff
    return [upper_byte, lower_byte]

class TMP112:
    def __init__(self, address, bus=0):
        self._address = address
        self._bus = smbus.SMBus(bus)
        self.configure()

    def configure(self):
        setup_configuration = CONFIG_F_1 | CONFIG_TM_CM | CONFIG_SD_CC | CONFIG_CR_4
        self._bus.write_i2c_block_data(self._address, CONFIG, write_register_16bit(setup_configuration))

    def read_temp(self):
        block_data = self._bus.read_i2c_block_data(self._address, TEMP, 2)
        temp_adc = ((block_data[0] << 4) | (block_data[1] >> 4)) & 0xFFF
        if temp_adc >= 0x800:
            temp_adc -= 0x1000
        temperature = temp_adc * 0.0625     # LSB = 0.0625
        return temperature

    def set_high_limit(self):
        temperature = T_HIGH_TEMP >> 4
        self._bus.write_i2c_block_data(self._address, T_HIGI, temperature)

    def set_low_limit(self):
        temperature = T_LOW_TEMP >> 4
        self._bus.write_i2c_block_data(self._address, T_LOW, temperature)

if __name__ == '__main__':
    tmp112_bus = 0
    tmp112_address = 0x48
    temperature = TMP112(tmp112_address, tmp112_bus).read_temp()
    print(temperature)
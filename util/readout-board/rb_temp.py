import configparser

from src.pca9548a import PCA9548A
from src.tmp112 import TMP112

config = configparser.ConfigParser()
config.read('readout-board.ini', encoding='utf-8')

i2c_bus = int(config['COMMON']['i2c_bus'])
pca9548a_address_0 = int(config['COMMON']['pca9548a_address_0'], 16)
pca9548a_address_1 = int(config['COMMON']['pca9548a_address_1'], 16)

def read_tmp112(channel, pca9548a_address, i2c_bus, tmp112_address):
    PCA9548A(channel, pca9548a_address, i2c_bus).channel_selector()
    temperature = TMP112(tmp112_address, i2c_bus).read_temp()
    return temperature

drs4_tmp112_channel = int(config['TMP112']['drs4_tmp112_channel'])
drs4_tmp112_address = int(config['TMP112']['drs4_tmp112_address'], 16)
drs4_temp = read_tmp112(drs4_tmp112_channel, pca9548a_address_0, i2c_bus, drs4_tmp112_address)

clk_tmp112_channel = int(config['TMP112']['clk_tmp112_channel'])
clk_tmp112_address = int(config['TMP112']['clk_tmp112_address'], 16)
clk_temp = read_tmp112(clk_tmp112_channel, pca9548a_address_1, i2c_bus, clk_tmp112_address)

adc_tmp112_channel = int(config['TMP112']['adc_tmp112_channel'])
adc_tmp112_address = int(config['TMP112']['adc_tmp112_address'], 16)
adc_temp = read_tmp112(adc_tmp112_channel, pca9548a_address_1, i2c_bus, adc_tmp112_address)

print("DRS4 TEMP:   {}°C".format(drs4_temp))
print("CLK TEMP:    {}°C".format(clk_temp))
print("ADC TEMP:    {}°C".format(adc_temp))
Clock synthesis
----------------

This folder contains the files needed to program the Si5395 to enabled DAQ on the DRS board.

```
si5395/
  ├── prj             : Si Labs Clock Builder Pro binary files 
  └── header          : Exported C header file containing register data
```

## Output defaults

  | Si5395 Pin | Description | Frequency  | Format        | Notes      | 
  | :----      | :---------  | :--------- | :----         | :-------   |  
  |OUT0A       |   PL clock  | 33 MHz     | LVDS          |            |   
  |OUT0        |   PL clock  | 33 MHz     | LVDS          |            |   
  |OUT1        |   DRS ref   | 1 MHz      | LVDS          | 2.048 GS/s |   
  |OUT3        |   DRS CH8   | 50 MHz     | LVCMOS (2V5)  | In phase, 24 Ohm           |
  |OUT9A       |PLL feedback | 1 MHz      | LVDS          | Let clock builder software automanage            | 

## Required inputs

  | Si5395 Pin | Description | Frequency  | Format        | Notes      | 
  | :----      | :---------  | :--------- | :----         | :-------   |  
  |IN0         |   MAIN CLK  | 1 MHz      | LVDS          | Central TOF ref clock  |

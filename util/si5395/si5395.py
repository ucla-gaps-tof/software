import smbus
import time

import si5395_header

si5395_preamble = si5395_header.si5395_preamble
si5395_payload = si5395_header.si5395_payload

si5395_postamble = si5395_header.si5395_postamble

def config_si5395_manual_mux(bus_num):
    
    fd = smbus.SMBus(bus_num)

    mux_i2c_addr = 0x77

    mux_si5395_ch = 0x68

    # Setup mux
    fd.write_byte(mux_i2c_addr, 0x02)

    # Force hard reset
    print("Resetting Si5395")

    fd.write_byte_data(mux_si5395_ch, 0x01, 0x0)
    fd.write_byte_data(mux_si5395_ch, 0x1e, 0x01)
    
    time.sleep(5)
    print("Turning on Si5395")
    fd.write_byte_data(mux_si5395_ch, 0x1e, 0x0)
    time.sleep(5)


    print("Writing Si5395 preamble")

    for member in si5395_preamble:
        page = member[0] >> 8
        reg = member[0] & 0xff
        data = member[1]
        fd.write_byte_data(mux_si5395_ch, 0x01, page)
        fd.write_byte_data(mux_si5395_ch, reg, data)

    # Delay to wait for device calibration

    print("Waiting for device calibration")

    time.sleep(0.310)

    # Write payload
    print("Writing Si5395 configuration payload")

    for member in si5395_payload:
        page = member[0] >> 8
        reg = member[0] & 0xff
        data = member[1]
        fd.write_byte_data(mux_si5395_ch, 0x01, page)
        fd.write_byte_data(mux_si5395_ch, reg, data)

    # Finish with postamble
    print("Writing Si5395 postamble")
    for member in si5395_postamble:
        page = member[0] >> 8
        reg = member[0] & 0xff
        data = member[1]
        fd.write_byte_data(mux_si5395_ch, 0x01, page)
        fd.write_byte_data(mux_si5395_ch, reg, data)
    
    # Finished
    print("Done programming Si5395")
    # Cleanup
    fd.write_byte(mux_i2c_addr, 0x0)
    fd.close()

    return 0


# Do configuration

mars_i2c = 0

config_si5395_manual_mux(mars_i2c)
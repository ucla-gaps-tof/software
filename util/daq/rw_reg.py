import xml.etree.ElementTree as xml
import os, subprocess

DEBUG = True
ADDRESS_TABLE_TOP = 'registers.xml'

nodes = []

FPGA_BASE = 0x80000000
FPGA_SIZE = 0x0000FFFF

class Node:
    name = ''
    vhdlname = ''
    address = 0x0
    physical_address = 0x0
    permission = ''
    mask = 0x0
    isModule = False
    parent = None
    level = 0
    mode = None

    def __init__(self):
        self.children = []

    def addChild(self, child):
        self.children.append(child)

    def getVhdlName(self):
        return self.name.replace(TOP_NODE_NAME + '.', '').replace('.', '_')

    def output(self):
        print('Name:',self.name)
        print('Address:','{0:#010x}'.format(self.address))
        print('Permission:',self.permission)
        print('Mask:','{0:#010x}'.format(self.mask))
        print('Module:',self.isModule)
        print('Parent:',self.parent.name)

def test():
    print ("# of nodes in XML file: %d" % len(nodes))

    print("Getting node:")
    #print(displayReg(getNode("DRS.CHIP.CHANNEL_CONFIG")))
    #print(displayReg(getNode("DRS.FPGA.DNA.DNA_LSBS")))
    for node in getRegsContaining("DRS"):
        print(displayReg(node))


def main():
    parseXML()
    test()

def parseXML(filename = None, num_of_oh = None):
    if filename == None:
        filename = ADDRESS_TABLE_TOP
    #print('Parsing',filename,'...')
    tree = xml.parse(filename)
    root = tree.getroot()[0]
    substitutions = {}
    makeTree(root,'',0x0,nodes,None,substitutions,False,num_of_oh)

def makeTree(node,baseName,baseAddress,nodes,parentNode,substitutions,isGenerated,num_of_oh=None):

    if (isGenerated == None or isGenerated == False) and node.get('generate') is not None and node.get('generate') == 'true':
        if (node.get('generate_idx_var') == 'OH_IDX' and num_of_oh is not None):
            generateSize = num_of_oh
        else:
            generateSize = parseInt(node.get('generate_size'))

        generateAddressStep = parseInt(node.get('generate_address_step'))
        generateIdxVar = node.get('generate_idx_var')

        for i in range(0, generateSize):
            substitutions[generateIdxVar] = i
            makeTree(node, baseName, baseAddress + generateAddressStep * i, nodes, parentNode, substitutions, True)
        return

    newNode = Node()
    name = baseName

    if baseName != '':
        name += '.'

    name += node.get('id')
    name = substituteVars(name, substitutions)
    newNode.name = name
    address = baseAddress

    if node.get('address') is not None:
        address = baseAddress + parseInt(node.get('address'))

    newNode.address = address
    newNode.physical_address = (address<<2|FPGA_BASE)
    newNode.permission = node.get('permission')
    newNode.mask = parseInt(node.get('mask'))
    newNode.isModule = node.get('fw_is_module') is not None and node.get('fw_is_module') == 'true'

    if node.get('mode') is not None:
        newNode.mode = node.get('mode')
    if node.get('permission') is not None:
        nodes.append(newNode)
    if parentNode is not None:
        parentNode.addChild(newNode)
        newNode.parent = parentNode
        newNode.level = parentNode.level+1
    for child in node:
        makeTree(child,name,address,nodes,newNode,substitutions,False,num_of_oh)

def getAllChildren(node,kids=[]):
    if node.children==[]:
        kids.append(node)
        return kids

    for child in node.children:
        getAllChildren(child,kids)

def getNode(nodeName):
    return next((node for node in nodes if node.name == nodeName),None)

def getNodeFromAddress(nodeAddress):
    return next((node for node in nodes if node.physical_address == nodeAddress),None)

def getNodesContaining(nodeString):
    nodelist = [node for node in nodes if nodeString in node.name]
    if len(nodelist):
        return nodelist
    else:
        return None

#returns *readable* registers
def getRegsContaining(nodeString):
    nodelist = [node for node in nodes if nodeString in node.name
            and node.permission is not None and 'r' in node.permission]

    if len(nodelist):
        return nodelist

    return None

def mpeek(address):
    try:
        output = subprocess.check_output('devmem '+str(address), stderr=subprocess.STDOUT , shell=True)
        value = int(output,16)
    except subprocess.CalledProcessError as e:
        value = parseError(int(str(e)[-1:]))
    return value

def mpoke(address,value):
    try:
        output = subprocess.check_output('devmem '+str(address)+' 32 '+str(value), stderr=subprocess.STDOUT , shell=True)
    except subprocess.CalledProcessError as e:
        return parseError(int(str(e)[-1:]))
    return 0

def readReg(reg):
    address = reg.physical_address

    if 'r' not in reg.permission:
        return 0xDEADDEAD

    value=mpeek(address)

    # Apply Mask
    if reg.mask is not None:
        shift_amount=0
        for bit in reversed('{0:b}'.format(reg.mask)):
            if bit=='0':
                shift_amount+=1
            else:
                break
        final_value = (reg.mask & value) >> shift_amount
    else:
        final_value = value

    return final_value

def displayReg(reg):
    if reg.mask is not None:
        final_int =  readReg(reg)
        return ("0x%04X" % reg.address) + ' ' + reg.permission + \
                '\t' + tabPad(reg.name,7) + ("0x%08X" % final_int)
    else:
        return ("Not readable")

def writeReg(reg, value):
    try:
        address = reg.physical_address
    except:
        print('Reg',reg,'not a Node')
        return
    if 'w' not in reg.permission:
        return 'No write permission!'

    # Apply Mask if applicable
    if reg.mask is not None:
        shift_amount=0
        for bit in reversed('{0:b}'.format(reg.mask)):
            if bit=='0': shift_amount+=1
            else: break
        shifted_value = value << shift_amount
        if 'r' not in reg.permission:
            final_value = shifted_value
        else:
            initial_value = readReg(reg)
            final_value = (shifted_value & reg.mask) | (initial_value & ~reg.mask)
    else:
        final_value = value

    # mpoke
    try:
        output = mpoke (address, final_value)
        return final_value
    except subprocess.CalledProcessError as e:
        return parseError(int(str(e)[-1:]))

def isValid(address):
    try:
        mpeek(address)
    except subprocess.CalledProcessError as e:
        return False
    return True

def completeReg(string):
    possibleNodes = []
    completions = []
    currentLevel = len([c for c in string if c=='.'])

    possibleNodes = [node for node in nodes if node.name.startswith(string) and node.level == currentLevel]
    if len(possibleNodes)==1:
        if possibleNodes[0].children == []: return [possibleNodes[0].name]
        for n in possibleNodes[0].children:
            completions.append(n.name)
    else:
        for n in possibleNodes:
            completions.append(n.name)
    return completions


def parseError(e):
    if e==1:
        return "Failed to parse address"
    if e==2:
        return "Bus error"
    else:
        return "Unknown error: "+str(e)

def parseInt(s):
    if s is None:
        return None
    string = str(s)
    if string.startswith('0x'):
        return int(string, 16)
    elif string.startswith('0b'):
        return int(string, 2)
    else:
        return int(string)


def substituteVars(string, substitutions):
    if string is None:
        return string
    ret = string
    for varKey in substitutions.keys():
        ret = ret.replace('${' + varKey + '}', str(substitutions[varKey]))
    return ret

def tabPad(s,maxlen):
    return (s+"\t"*(int((8*maxlen-len(s)-1)/8)+1))

if __name__ == '__main__':
    main()

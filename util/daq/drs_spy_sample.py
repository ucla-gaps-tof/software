import drs_config
import ctypes
# Use libc for usleep
libc = ctypes.CDLL('libc.so.6')
import rw_reg
import mmap
import datetime

# Parse register stuff
rw_reg.parseXML()

# Setup DRS4
drs_config.configure_drs()
drs_config.start()
# Reset spy fifo
rw_reg.writeReg (rw_reg.getNode("DRS.SPY.RESET"), 1)

fifo_stat = int(rw_reg.readReg (rw_reg.getNode("DRS.SPY.FULL")))

num_packets = 0
# Keep injecting packets until spy fifo filled
while fifo_stat == 0:
    fifo_stat = int(rw_reg.readReg (rw_reg.getNode("DRS.SPY.FULL")))
    drs_config.debug_inject()
    # Add safety delay
    libc.usleep(100000)
    num_packets += 1

print("SPY FULL: %i" %fifo_stat)
print("Num packets written: %i" %num_packets)
# fifo_dat = rw_reg.readReg (rw_reg.getNode("DRS.SPY.DATA"))
# print(fifo_dat)

# # Try grabbing first ~five packets
# packet_str = ""
# for i in range(330000):
#     fifo_dat = rw_reg.readReg (rw_reg.getNode("DRS.SPY.DATA"))
#     # fifo_hex = "0x%0.4X" %fifo_dat
#     # packet_str = f"{packet_str}{fifo_hex}\n"

# with open("spy_test.txt", "w") as f:
#     f.write(packet_str)

# print(fifo_stat)
# fifo_dat = rw_reg.readReg (rw_reg.getNode("DRS.SPY.DATA"))
# print("0x%0.4X" %fifo_dat)
# # Begin sampling (debug testing)
# for i in range(num_events):
#     print("Calling debug_inject")
#     drs_config.debug_inject()
#     libc.usleep(1)
#     fifo_stat = int(rw_reg.readReg (rw_reg.getNode("DRS.SPY.FULL")))
#     packet_str = ""
#     while fifo_stat == 0:
#         fifo_dat = rw_reg.readReg (rw_reg.getNode("DRS.SPY.DATA"))
#         fifo_hex = "0x%0.4X" %fifo_dat
#         packet_str = f"{packet_str} {fifo_hex}"
#         fifo_stat = int(rw_reg.readReg (rw_reg.getNode("DRS.SPY.FULL")))
#     event_list.append(packet_str)
#     # Block until packet counter increments (use in normal mode)
#     # packet_incr_blocker()
#     # Trigger every 200 ms
#     libc.usleep(200000)

# Dump memory contents to file
# TODO: automatically read registers to determine memory region to dump?

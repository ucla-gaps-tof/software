import drs_config
import ctypes
# Use libc for usleep
libc = ctypes.CDLL('libc.so.6')
import rw_reg
import mmap
import datetime

def packet_incr_blocker():
    start_val = int(rw_reg.readReg (rw_reg.getNode("CNT_DMA_READOUTS_COMPLETED")))
    # Copy start_val to x to ensure same type
    x = start_val
    while x <= start_val:
        x = int(rw_reg.readReg (rw_reg.getNode("CNT_DMA_READOUTS_COMPLETED")))
        # Don't spin CPU doing this loop
        libc.usleep(1)
    # Sleep for a small amount to ensure DMA engine wrote data to RAM
    libc.usleep(5)
    return 0

# Number of waveforms/events to record
num_events = 10

# Setup DRS4
drs_config.configure_drs()
drs_config.start()

# Begin sampling (debug testing)
for i in range(num_events):
    drs_config.debug_inject()
    libc.usleep(1)
    drs_config.force_trigger()
    # Block until packet counter increments (use in normal mode)
    # packet_incr_blocker()
    # Trigger every 200 ms
    libc.usleep(200000)

# Dump memory contents to file
# TODO: automatically read registers to determine memory region to dump?

drs_data_offset = 
drs_data_size = 

with open("/dev/mem", "r+b") as f:
    drs_data = mmap.mmap(f.fileno(), drs_data_size, offset = drs_data_offset)
    the_time = datetime.datetime.now()
    time_str = datetime.datetime.isoformat(the_time, timespec = "seconds")
    with open("DRS4_DATA_debug_%i_samples_%s.dat" %(num_events, time_str), "w+b") as g:
        g.write(drs_data)

# More code to depacketize data here...
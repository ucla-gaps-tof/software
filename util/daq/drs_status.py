
import rw_reg

def print_reg (node_name):
    print ("  " + rw_reg.displayReg(rw_reg.getNode(node_name)))

def status():

    print ("Status:")
    #for node in nodes:
        #displayReg(node)
    print_reg("DRS.CHIP.DRS_PLL_LOCK")
    print_reg("DRS.CHIP.DTAP_HIGH_CNTS")
    print_reg("DRS.CHIP.DTAP_LOW_CNTS")

    print_reg("DRS.CHIP.DMODE")
    print_reg("DRS.CHIP.STANDBY_MODE")
    print_reg("DRS.CHIP.TRANSPARENT_MODE")
    print_reg("DRS.CHIP.DRS_PLL_LOCK")
    print_reg("DRS.CHIP.CHANNEL_CONFIG")
    print_reg("DRS.CHIP.DTAP_HIGH_CNTS")
    print_reg("DRS.CHIP.DTAP_LOW_CNTS")
    print_reg("DRS.READOUT.ROI_MODE")
    print_reg("DRS.READOUT.BUSY")
    print_reg("DRS.READOUT.ADC_LATENCY")
    print_reg("DRS.READOUT.SAMPLE_COUNT")
    print_reg("DRS.READOUT.READOUT_MASK")

    print ("")

def dna():
    lsbs = rw_reg.readReg(rw_reg.getNode("DRS.FPGA.DNA.DNA_LSBS"))
    msbs = rw_reg.readReg(rw_reg.getNode("DRS.FPGA.DNA.DNA_MSBS"))
    dna = msbs << 32 | lsbs
    if (dna==0x589CC92F904854):
        name = "Millenium Falcon"
    else:
        name = "Unknown DNA..."
    print ("  FPGA DNA = 0x%08X (%s)" % (dna, name))


def version():
    print("Version:")
    print_reg("DRS.HOG.GLOBAL_DATE")
    print_reg("DRS.HOG.GLOBAL_TIME")
    print_reg("DRS.HOG.GLOBAL_VER")
    print_reg("DRS.HOG.GLOBAL_SHA")

def uptime():
    print("Uptime:")
    lsbs = rw_reg.readReg(rw_reg.getNode("DRS.FPGA.TIMESTAMP.TIMESTAMP_LSBS"))
    msbs = rw_reg.readReg(rw_reg.getNode("DRS.FPGA.TIMESTAMP.TIMESTAMP_MSBS"))
    uptime =  ((msbs << 32) | lsbs) / 33000000.0
    hours = (uptime // 3600)
    minutes = (uptime // 60) % 60
    secs = (uptime % 60)
    print ("  Uptime = %dh %dm %ds" % (hours, minutes, secs))

if __name__ == '__main__':
    rw_reg.parseXML()
    uptime()
    dna()
    version()
    status()

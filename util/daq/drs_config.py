import rw_reg
import time

def configure_drs():
  print("Configure DRS4")

  rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.DRS_RESET"), 1)
  # Configure
  # Set DRS4 configure mode

  # set continuous mode
  rw_reg.writeReg (rw_reg.getNode("DRS.CHIP.DMODE"), 1) # 1 = continuous domino

  rw_reg.writeReg (rw_reg.getNode("DRS.CHIP.CHANNEL_CONFIG"), 1) # 1 = 8192 cells per ch

  # 7 clock latency
  rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.ADC_LATENCY"), 8)

  # Set sample count to 1024 samples */
  rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.SAMPLE_COUNT"), 1024)

#   rw_reg.writeReg (rw_reg.getNode("DRS.CHIP.TRANSPARENT_MODE"), 0) # 1 = transparent mode
  rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.ROI_MODE"), 1) # 1 = region of interest mode

  rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.READOUT_MASK"), 0x01) #

  # Configure
  #Set DRS4 normal mode
  time.sleep(2)
  rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.CONFIGURE"), 0x01) # Write 1 to configure the DRS. Should be done before data taking


def force_trigger ():
    rw_reg.writeReg (rw_reg.getNode("DRS.Trigger.FORCE_TRIGGER"), 1)

def debug_inject ():
    rw_reg.writeReg (rw_reg.getNode("DRS.DAQ.INJECT_DEBUG_PACKET"), 1)

def start():
    rw_reg.writeReg (rw_reg.getNode("DRS.READOUT.START"), 1)

def print_reg (node_name):
    print ("  " + rw_reg.displayReg(rw_reg.getNode(node_name)))

if __name__ == '__main__':
    rw_reg.parseXML()
    configure_drs()

import mmap
import datetime
import sys
import zmq

#drs_data_offset = 0x18000000
drs_data_offset = 0x04000000
#drs_data_offset = 0x0

drs_data_size = 0x8500000
#drs_data_size = 0xfffffff

context = zmq.Context()

# Remote host parameters
#SECURITY WARNING: DO NOT COMMIT WITH SENSITIVE DATA
remote_host_ip = '10.0.1.10'
remote_host_port = '38835'

# RAM buffer config
buff_start_addr = drs_data_offset
buff_size = drs_data_size

# New socket
skt = context.socket(zmq.REQ)
skt.connect("tcp://%s:%s" %(remote_host_ip, remote_host_port))

create_time = datetime.datetime.now()
time_str = create_time.strftime("%x_%H%M").replace("/","")

# debug
print("Net stuff done.")

with open("/dev/mem", "r+b") as f:
    drs_data = mmap.mmap(f.fileno(), drs_data_size, offset = drs_data_offset)
    print("mmap done")
    mv = memoryview(drs_data)
    print("memview done")
    skt.send(mv)
    msg = skt.recv()
    print(msg)

# Graceful exit
skt.close()


import drs_config
import ctypes
# Use libc for usleep
libc = ctypes.CDLL('libc.so.6')
import rw_reg
import mmap
import datetime

rw_reg.parseXML()

# Number of waveforms/events to record
num_events = 1

# Setup DRS4
drs_config.configure_drs()
drs_config.start()

# # Begin sampling (debug testing)
# for i in range(num_events):
#     drs_config.force_trigger()
#     # Block until packet counter increments (use in normal mode)
#     # packet_incr_blocker()
#     # Trigger every 200 ms
#     libc.usleep(200000)
#     print("Event # %i" %i)

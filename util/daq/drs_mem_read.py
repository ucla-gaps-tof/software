import mmap

drs_data_offset = 0x10000000
drs_data_size = 0x10000

with open("/dev/mem", "r+b") as f:
    drs_data = mmap.mmap(f.fileno(), drs_data_size, offset = drs_data_offset)
    # the_time = datetime.datetime.now()
    # time_str = datetime.datetime.isoformat(the_time, timespec = "seconds")
    with open("test_dma_xfer.txt", "w+b") as g:
        g.write(drs_data)

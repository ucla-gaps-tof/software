


def get_samps(x,y):
    """loop over event traces and make histogram of all samples

    Args:
        x (str): PACKETS folder
        y (str): PACKETS folder

    Returns:
        nparray: flattened 1D array with all samples
    """
    wv_cal=np.loadtxt(y,usecols=(1,3,5,7,9,11,13,15)) 
    wv_f=os.listdir(x) 
    wv_f=[i for i in wv_f if ".txt" in i and "payload" in i] 
    wv_np=np.array([]) 
    for i in wv_f: 
        data=np.loadtxt(f"{x}/{i}") 
        cellid=data[:,0].astype(int) 
        cal_wave=data[:,1:]-wv_cal[cellid] 
        wv_np=np.append(wv_np,cal_wave) 
    wv_np_flat=wv_np.flatten() 
    return wv_np_flat
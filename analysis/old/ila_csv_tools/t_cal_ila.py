"""
Author: Sean Quinn (spq@ucla.edu)
t_cal_ila.py (c) UCLA 2021
Desc: Get timing calibration from ILA data
Created:  2021-02-25T18:26:26.393Z
Modified: 2021-02-25T21:37:19.060Z
"""
import pandas as pd
pd.set_option('display.float_format', lambda x: "%.4f" %x)
# Pandas dependency for stats summary
import numpy as np
import matplotlib.pyplot as plt
import os

def check_end_points(x):
    # x should be a 1D array of length 2
    signs = np.signbit(x)
    sign_change = signs[0] & signs[1]
    # return:
    #   0 if no sign change found
    #   1 if rising edge
    #   2 if falling edge
    if sign_change:
        return 0
    elif x[1] > x[0]:
        return 1
    else:
        return 2

# Folder containing ILA data
tcal_folders = ["v2_3_ila_data_25MHz_200mVpp"]

# ADC Vref [V]
adc_vref = 1.
adc_step = 1. / 2**14

# Muxout center
# E.g. +- 500 mV range means ADC code 0 = -500 mV, 8192 = 0 mV, 16381 = 500 mV 
cal_zero = 8192

# Number of cells
n_samp = 1024
x = np.arange(1024)

# ILA analysis parameters
csv_row_skip = 43
csv_adc_col = 5

# SCA sampling rate truth, determined by measuring DTAP.
# f_dtap = (1 / 2048) * fsca
fsca = 5.12e9

# Number of cells
n_samp = 1024

# Plotting axis
x = np.arange(1024)

# Amplitue calibration data
# TODO - perhas accept sysarg pointing to this file
# TODO - print warnings that amplitude data required, halt if not found
cell_avg = np.loadtxt("v2_3_ila_data_0VDC_acal.txt")

# Main loop
for h,i in enumerate(tcal_folders):
    # Parse files in folder
    f_list = os.listdir(i)
    f_list = [x for x in f_list if ".csv" in x and "~" not in x]
    n_evt = len(f_list)
    # Array for calibrated periods
    np_arr = np.zeros((n_samp,n_evt))
    cal_arr =  np.zeros((n_samp,n_evt))
    # Array for zero crossings
    rising_edges = np.zeros((n_samp,n_evt))
    falling_edges = np.zeros((n_samp,n_evt))
    for k,j in enumerate(f_list):
        hex_str_data = np.loadtxt(i + "/" + j, delimiter=",",skiprows=csv_row_skip,dtype='S20',max_rows=n_samp,usecols=csv_adc_col)
        dec_data = np.array([int(x,16) for x in hex_str_data])
        np_arr[:,k] = (dec_data - cal_zero)
        cal_arr[:,k] = (dec_data - cal_zero) - cell_avg
        tmp_amp = cal_arr[:,k]
        # Find falling edge zero crossings
        zc_p = (tmp_amp[:-1] < 0.) & (tmp_amp[1:] > 0.)
        # Build array with indices of zero crossings. Index gives data point ABOVE zero (positive)
        zc_p = np.flatnonzero(zc_p) + 1
        # Find rising edge zero crossings
        zc_n = (tmp_amp[:-1] > 0.) & (tmp_amp[1:] < 0.)
        # Build array with indices of zero crossings. Index gives data point ABOVE zero (positive)
        zc_n = np.flatnonzero(zc_n) + 1
        # Check if zero crossing for first two cells
        cell12_zc = check_end_points(tmp_amp[:2])
        # Append to appropriate array
        if cell12_zc == 1:
            zc_p = np.append(zc_p, 1)
        if cell12_zc == 2:
            zc_n = np.append(zc_n, 1)
        # Check if zero crossing for last two cells
        celln_n_1_zc = check_end_points(tmp_amp[2:])
        # Append to appropriate array
        if celln_n_1_zc == 1:
            zc_p = np.append(zc_p, n_samp - 1)
        if celln_n_1_zc == 2:
            zc_n = np.append(zc_n, n_samp - 1)
        # Add zero crossing voltage differences to storage array
        rising_edges[zc_p,k] = cal_arr[zc_p,k] - cal_arr[zc_p-1,k]
        falling_edges[zc_n,k] = cal_arr[zc_n - 1,k] - cal_arr[zc_n,k]
    # Find average channel period
    falling_cell_avg = np.zeros(n_samp)
    rising_cell_avg = np.zeros(n_samp)
    # It's possible that some cells won't have a zero crossing.
    # We either have to: take more samples using 25 MHz crystal, or increase the crystal frequency
    # Also going to tweak the code to ensure random phases used
    for i in range(n_samp):
        len_fall = len(falling_edges[i,falling_edges[i,:]>0])
        len_rise = len(rising_edges[i,rising_edges[i,:]>0])
        if len_rise == 0 or len_fall == 0:
            print("cell %i: len_fall %i, len_rise %i" %(i,len_fall,len_rise))
        falling_cell_avg[i] = np.nan_to_num(falling_edges[i,falling_edges[i,:]>0].mean())
        rising_cell_avg[i] = np.nan_to_num(rising_edges[i,rising_edges[i,:]>0].mean())
    # Figure out time intervals
    fall_time_bins = np.zeros(n_samp)
    rise_time_bins = np.zeros(n_samp)
    clean_fall = np.zeros(n_samp)
    clean_rise = np.zeros(n_samp)
    clean_fall = falling_cell_avg
    clean_rise = rising_cell_avg
    # Cleaning
    # FIXME - Hard coded cuts: may want to parametrize these
    for i in range(n_samp):
        # More cleaning
        if falling_cell_avg[i] > 150 or falling_cell_avg[i] == 0.:
            clean_fall[i] = 70
        if rising_cell_avg[i] > 150 or rising_cell_avg[i] == 0.:
            clean_rise[i] = 70
    fall_norm = clean_fall.sum()
    rise_norm = clean_rise.sum()    
    for i in range(n_samp):
        fall_time_bins[i] = clean_fall[i] * 1024 / (fsca * fall_norm)
        rise_time_bins[i] = clean_rise[i] * 1024 / (fsca * rise_norm)
    # Diagnostics
    # Compute non-linearities
    true_period = 1. / fsca
    time_bins = np.zeros(n_samp)
    time_bins_avg = (fall_time_bins + rise_time_bins) / 2.
    fall_dnl = fall_time_bins - true_period
    rise_dnl = rise_time_bins - true_period
    fall_inl = np.cumsum(fall_dnl)
    rise_inl = np.cumsum(rise_dnl)
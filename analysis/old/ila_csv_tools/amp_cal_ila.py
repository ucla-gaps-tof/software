"""
Author: Sean Quinn (spq@ucla.edu)
amp_cal_ila.py (c) UCLA 2021
Desc: Get amplitude calibration from ILA data
Created:  2021-02-02T00:26:26.393Z
Modified: 2021-02-25T17:58:46.699Z
"""
import pandas as pd
# Pandas dependency for stats summary
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime

# TODO - Handle multiple channels

# Folders containing .csv ILA data from Vivado
acal_folders = ["v2_3_ila_data_0VDC",
                "v2_3_ila_data_100mVDC",
                "v2_3_ila_data_200mVDC",
                "v2_3_ila_data_300mVDC"]

# Calibration truth values (cross-checked with NIST traceable measurement)
dc_volts = np.array([ 0, 0.1, 0.2, 0.3])

# ADC Vref [V]
adc_vref = 1.
adc_step = 1. / 2**14

# Muxout center
# E.g. +- 500 mV range means ADC code 0 = -500 mV, 8192 = 0 mV, 16381 = 500 mV 
cal_zero = 8192
cal_const = dc_volts / adc_step

# Number of cells
n_samp = 1024
x = np.arange(1024)

# Arrays to hold cell calibrations per folder/data run
cell_avg = np.zeros((1024,len(acal_folders)))
cell_unc = np.zeros((1024,len(acal_folders)))

# ILA analysis parameters
csv_row_skip = 43
csv_adc_col = 5

# Main loop
for h,i in enumerate(acal_folders):
    # Folder parsing stuff
    f_list = os.listdir(i)
    f_list = [x for x in f_list if ".csv" in x and "~" not in x]
    n_evt = len(f_list)
    # Arrays for all waveforms
    np_arr = np.zeros((n_samp,n_evt))
    acal_arr = np.zeros((n_samp,n_evt))
    # Loop over csv files
    for k,j in enumerate(f_list):
        hex_str_data = np.loadtxt(i + "/" + j, delimiter=",",skiprows=csv_row_skip,dtype='S20',max_rows=n_samp,usecols=csv_adc_col)
        # Convert to decimal integer
        dec_data = np.array([int(x,16) for x in hex_str_data])
        # Residuals compared to truth set point
        np_arr[:,k] = (dec_data - cal_zero) - cal_const[h]
        # Raw data (centered)
        acal_arr[:,k] = dec_data - cal_zero
    # Compute average for cell for n_evt samples
    cell_avg[:,h] = np_arr.mean(axis=1)
    # Save cell calibration data
    create_time = datetime.datetime.now()
    time_str = create_time.strftime("%x_%H%M").replace("/","")
    np.savetxt(i + "_" + time_str +"_acal.txt", acal_arr.mean(axis=1), fmt='%i')
    # Save cell variance data
    cell_unc[:,h] = np_arr.std(axis=1)
    np.savetxt(i + "_" + time_str +"_acal_unc.txt", acal_arr.std(axis=1), fmt='%i')
    # Some diagnostic plots. These will pop up during execution
    plt.subplot(2,2,h+1)
    plt.errorbar(x, cell_avg[:,h], yerr=cell_unc[:,h], marker='.', label = "DAC Inp.: %.1f VDC" %dc_volts[h], ls="None")
    plt.legend()
    # Assumes channel 0
    plt.xlabel("CH0 DRS4 Cell ID", fontsize=12)
    plt.ylabel("Cell offset mean and std. [ADC Code]", fontsize=12)
plt.show()

# Show histogram of raw cell 0 values for given DAC set point
plt.hist(np_arr[0,:]*adc_step,histtype='step',label='%.1f VDC DAC input' %dc_volts[h])
plt.xlabel("CH 0 DRS4 Cell Offset [V]");plt.ylabel("Entries")
plt.legend()
tmp_df = pd.DataFrame(np_arr[0,:]*adc_step)
tmp_str = tmp_df.describe().to_string()
plt.figtext(0.7,0.55,tmp_str,size='x-small')
plt.show()

# Diagnostics for cell uncertainties for all runs
all_unc = cell_unc.flatten()
# Convert to mV
all_unc_df = pd.DataFrame(all_unc*adc_step*1000)

# Plot histogram for all uncertainties in mV
plt.hist(all_unc*adc_step*1000.,histtype='step',range=(0,1.5),bins=100)
plt.figtext(0.65,0.65,str(all_unc_df.describe()),fontsize=12,
bbox=dict(facecolor='none', edgecolor='black', boxstyle='round,pad=1'))
plt.xlabel("All DRS4 cell variances [mV]")
plt.ylabel("Entries")
plt.show()

# Show post calibration example for a random event (whatever index 45 corresponds to in this case)
# Residuals in this plot correspond to any systematic noise still left in the electronics
ydata = np_arr[:,45]
# Post calibration only
fig, ax = plt.subplots()
ax.set_xlabel("CH0 DRS4 Cell ID")
ax2=ax.twinx()
ax.step(np.arange(1024),ydata-cell_avg[:,-1],)
ax.set_ylabel("Amplitude [ADC Code]", ha='center')
ax2.step(np.arange(1024),(ydata-cell_avg[:,-1])*adc_step, label='Calibrated')
ax2.set_ylabel("Amplitude [V]", ha='center', rotation=-90., labelpad=25)
plt.legend()
plt.show()

# Post calibration example like above but compares raw data to calibrated data, along with calibration truth
fig, ax = plt.subplots()
ax.set_xlabel("CH0 DRS4 Cell ID")
ax2=ax.twinx()
ydata = np_arr[:,100] + cal_const[-1]
ax2.axhline(0.3, label='DAC set point', ls='dashed')
ax.step(np.arange(1024),ydata)
ax.step(np.arange(1024),ydata-cell_avg[:,-1],)
ax.set_ylabel("Amplitude [ADC Code]", ha='center')
ax2.step(np.arange(1024),(ydata-cell_avg[:,-1])*adc_step, label='Calibrated')
ax2.step(np.arange(1024),ydata*adc_step, label = 'Uncalibrated')
ax2.set_ylabel("Amplitude [V]", ha='center', rotation=-90., labelpad=25)
plt.legend()
plt.show()

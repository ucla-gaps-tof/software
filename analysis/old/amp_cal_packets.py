"""
Author: Sean Quinn (spq@ucla.edu)
amp_cal_packets.py (c) UCLA 2021
Desc: Compute statistics of calibration runs
Created:  2021-02-03T02:01:11.886Z
Modified: 2021-08-17T04:49:49.863Z
"""

import sys
import os
import numpy as np
import natsort
import argparse
import pprint
import matplotlib.pyplot as plt
from pathlib import Path
from color_print import cprint_err, cprint_info, cprint_ok, cprint_warn

# Usage
# python3 amp_cal_packets.py input_folders drs4_len num_chans
#   input_folders: folders containing packet payload files


def gen_output_header(nc):
    # nc: number of channels, int
    out_str = "cell_id "
    for i in range(nc):
        out_str += f"ch{i}_avg ch{i}_stdev "
    return out_str


def main():

    # Parse user input args
    # TODO allow "full readout" mode data (should be easy enough to accommodate)
    parser = argparse.ArgumentParser(description='Calculate calibration offsets from unpacked data. Must be ROI mode')
    parser.add_argument('folders', metavar='f', type=str, nargs='+',
                        help='Folders to parse. Can handle wildcard PACKETS_*')
    parser.add_argument('-n', type=int, default=1024,
                        help='-n specifies DRS4 sample depth. default=1024')
    parser.add_argument('-c', type=int, default=9,
                        help='-c specifies number of channels in files. default=9')
    parser.add_argument('-d', type=int, default=8192,
                        help='-d specifies dac truth for run. default=8192')

    args = parser.parse_args()  # Class/namespace holding args
    args_list = args.folders  # Get folders list
    n_samp = args.n
    n_chan = args.c
    dac_set_truth = args.d
    data_folders = []  # New list for validated folders only

    cal_cell_id = np.arange(n_samp)

    # Input sanitizing

    # Check dirs exists and not empty
    for dir in args_list:
        if os.path.exists(dir) and not os.path.isfile(dir):
            if len(os.listdir(dir)) > 0:
                data_folders.append(dir)
            else:
                cprint_warn(f"Empty directory: {dir}, skipping.")
        else:
            cprint_warn(f"Directory: {dir} does not exist, or is a file, skipping.")

    # Check number of samples makes sense: (0,1024]
    if n_samp < 0 or n_samp > 1024:
        cprint_err(f"n_samp = {n_samp} is out of bounds (0,1024]")
        sys.exit()

    # Check number of channels makes sense
    if n_chan < 0 or n_chan > 18:
        cprint_err(f"n_chan = {n_chan} is out of bounds (0,18]")
        sys.exit()

    # Check dac input code makes sense
    # The DAC calibration source is 16-bits, with a 2.048 V reference (AD5675BCPZ)
    if dac_set_truth < 0 or dac_set_truth > 2**16 + 1:
        cprint_err(f"dac_set_truth = {dac_set_truth} is out of bounds (0,65536]")

    # Update user about status
    cprint_info(f"Using the following folder list for calibration run..")
    pprint.pprint(data_folders)
    # TODO Would be nice to use { n_samp = } in newer versions of Python
    cprint_info(f"Using n_samp = {n_samp}")
    cprint_info(f"Using n_chan = {n_chan}")
    cprint_info(f"Using dac_set_truth = {dac_set_truth}")

    # Main loop
    for dir in data_folders:
        dirlist = os.listdir(dir) # List of all files in folder
        dirlist = [x for x in dirlist if '.txt' in x and '_payload' in x]  # Filter only payload files
        dirlist = natsort.natsorted(dirlist)  # Do not use system default sorting
        n_files = len(dirlist)
        # Pre-allocate numpy array
        all_data_nparray = np.zeros((n_samp, n_chan, n_files))  # n_chan + 1 to account for cell ID
        for i, pkt_file in enumerate(dirlist):
            tmp_nparray = np.loadtxt(dir + "/" + pkt_file)
            cell_id = tmp_nparray[:, 0].astype(int)
            all_data_nparray[cell_id, :, i] = tmp_nparray[:, 1:]  # First column is cell ids, skip
        # Save np array containing entire folder's data
        # This uses standard cell sequence (0, 1, 2, ..., 1023)
        np.save(dir + "/" + "all_data_{:d}samp_{:d}ch_{:d}evt.npy".format(n_samp, n_chan, n_files), all_data_nparray)
        # Stats for folder
        all_offsets = all_data_nparray - dac_set_truth
        all_chan_avg_offset = all_offsets.mean(axis=2)
        all_chan_std_offset = all_offsets.std(axis=2)
        out_nparray = np.zeros((n_samp, 1 + 2 * n_chan))  # 1 for cell id, 2 * n_chan since avg and stdev given for each channel
        out_nparray[:, 0] = cal_cell_id
        out_nparray_index = [i for i in range(1, 1 + 2 * n_chan, 2)]
        for i in range(n_chan):
            out_i = out_nparray_index[i]
            out_nparray[:, out_i] = all_chan_avg_offset[:, i]
            out_nparray[:, out_i + 1] = all_chan_std_offset[:, i]
            out_header = gen_output_header(n_chan)
            np.savetxt(dir + "/" + "amp_cal_stats.txt", out_nparray, fmt="%i", header=out_header)
        # Make some diagnostic plots
        # TODO add verbose option to enable histograms for every cell of every channel
        plot_dir = dir.replace("PACKETS", "PLOTS")
        Path(plot_dir).mkdir(parents=True, exist_ok=True)
        # TODO Make this flexible for up to 18 channels (e.g. needed when 2nd DRS4 installed)
        fig, axs = plt.subplots(3, 3, sharex=True, sharey=True, figsize=(16, 10))
        ar1024 = np.arange(1024)
        for i, ax in enumerate(axs.flat):
            pi = out_nparray_index[i]
            ax.errorbar(ar1024, all_chan_avg_offset[:, i], yerr=all_chan_std_offset[:, i], marker='.', ms=2, elinewidth=0.75, ls='None')
            ax.set_title(f"CH{i}")
        # Since the default grid is 3x3, if data 
            if i == n_chan - 1:
                break
        plt.setp(axs[:, 0], ylabel='ADC Code')
        plt.setp(axs[-1, :], xlabel='DRS4 Cell')
        fig.suptitle(f"{dir} Amplitude Calibration Offsets, dac_truth={dac_set_truth}")
        plt.savefig(plot_dir + "/" + "channel_avg_offsets.pdf")


if __name__ == '__main__':
    main()

"""
Author: Sean Quinn (spq@ucla.edu)
blob_to_packets.py (c) UCLA 2021
Desc: Go from memory dump blob to individual packets
Created:  2021-02-02T00:26:26.393Z
Modified: 2021-08-20T17:39:25.468Z
"""
import sys
import os
import argparse
import struct
from color_print import cprint_err, cprint_info, cprint_ok, cprint_warn
import logging
from format_time_rfc3339 import formatTime_RFC3339
import pickle
import time

# Usage
# python3 blob_to_packets.py [-z] input_file(s)
# input_file: decompressed binary file(s) of the DRS4 readout board RAM buffer.
#   Should contain a vertical stack of data packets.
#   If packets written using old firmware version
#   that zero pads, include -z flag. Default assumes
#   non-zero pad packet.

# TODO convert to module that can be imported by other scripts
#      so the blob packet list can remain in memory


def main() -> None:
    """Extract packets from binary blob."""
    # Magic vars (should not change, fixed in firmware)
    # TODO Add option to skip folders if exists. Default: overwrites.
    #      Useful if you have a populated directory then add a few new files.
    # TODO Keep running count of all packets written (diagnostics)
    head = b'\xaa\xaa'
    tail = b'\x55\x55'
    # Minimum full packet size:
    # Metadata: 42 bytes
    # 1 channel payload: 2048 bytes
    # Total: 2090
    min_size_bytes = 2090
    len_index = 4  # Offset (in number of bytes) from start of packet
    z = False
    skip_files = False
    save_pickle = False
    # helpful str value for tail
    tail_str = hex(int.from_bytes(tail, 'little'))

    # Check for -z flag
    desc_str = "Save packet binaries from RAM buffer dump"
    parser = argparse.ArgumentParser(description=desc_str)
    parser.add_argument('-z', default=argparse.SUPPRESS,
                        action='store_const', const=True,
                        help='use flag -z if firmware version before e595a443')
    parser.add_argument('-s', '--skip', action='store_const', const=True,
                        default=False,
                        help='skips existing files that have been unpacked',
                        dest="skip_flag")
    # Option to save a list of packets to single pickle dump
    #  instead of writing individual files
    parser.add_argument('-p', '--pickle', action='store_const', const=True,
                        default=False,
                        help='saves packet list to single pickle file',
                        dest="save_pickle")    
    parser.add_argument('file', metavar='f', type=str, nargs='+',
                            help='file(s) to parse')

    args = parser.parse_args()

    # Overwrite default if user sets -z flag
    if hasattr(args, 'z'):
        z = args.z

    # Overwrite default if -s flag set
    if hasattr(args, 'skip_flag'):
        skip_files = args.skip_flag

    # If zero padding, use larger min_size
    if z is True:
        min_size_bytes *= 2

    # Overwrite default if -p flag set
    if hasattr(args, "save_pickle"):
        save_pickle = args.save_pickle

    # Assume this call always works, since parser
    # will throw exception if file args not given.. let's hope.
    file_list = args.file

    # Remove .dat file from list if
    # PACKETS folder for that file already exists (previously unpacked)
    if skip_files:
        all_files_list = os.listdir('.')
        existing_pkt_dirs = [x for x in all_files_list if "PACKETS" in x]
        # Delete .dat file from file_list pkt dir exists
        for x in file_list:
            for y in existing_pkt_dirs:
                if x.replace(".dat", "") in y:
                    file_list.remove(x)
                    cprint_info(f"Skipping file {x}")
        # Check that file_list still has elements
        # Handle case where user skips but only entered one file
        if len(file_list) < 1:
            cprint_err("No files to process (skip with one file?)")
            log.error("No files to process (skip with one file?)")
            sys.exit()

    # Main loop over files to unpack

    for k, input_file in enumerate(file_list):
        # Check for compression based on extension
        if ".7z" in input_file or ".zip" in input_file:
            cprint_err("Program cannot parse compressed files!")
            log.error("Program cannot parse compressed files!")
            cprint_info("Try decompressing file(s) before using this script.")
            sys.exit()

        # Create output folder
        try:
            # Handle extension
            output_folder_base = input_file.split('.')[0]
        except ValueError:
            # Use file name if no extension
            output_folder_base = input_file

        output_folder = "PACKETS_" + output_folder_base

        # Create directory
        check_dir = os.path.isdir(output_folder)
        if not check_dir:
            os.mkdir(output_folder)
        # List to hold packets
        packets = []

        # Packet start index
        s = 0
        # Packet counter
        pn = 0
        # Bad packet counter
        bpn = 0

        with open(input_file, "rb") as f:
            cprint_info(f"Opening file {input_file}")
            log.info(f"Opening file {input_file}")
            # Blob file read into byte array
            data = f.read()
            cprint_info(f"Done reading file. Collecting packets.")
            log.info(f"Done reading file. Collecting packets.")
            # Loop over half-words (16 bytes) looking for packet boundaries
            # But also check packet length to avoid writing a fragment.
            start_time = time.time()
            for i, j in enumerate(data, 2):
                if data[i-2:i] == head:
                    s = i-2
                    # At start of packet, get fw pre-calculated length
                    len_bytes = data[s+len_index:s+len_index+2]
                    pkt_len = struct.unpack('<H', len_bytes)[0]*2
                    # above is times 2 because value given in 2 byte words
                    # Check for tail at expected offset.
                    # If found, add valid packet to list
                    tmp_bytes = data[s:s+pkt_len]
                    if tmp_bytes[-2:] == tail:
                        packets.append(tmp_bytes)
                        pn += 1
                    else:
                        s_hex = hex(s)
                        mal_tail_bytes = tmp_bytes[-2:]
                        if len(mal_tail_bytes) == 2:
                            mal_tail_int = struct.unpack('<H', mal_tail_bytes)[0]
                        else:
                            cprint_err(f"Could not unpack tail bytes")
                            mal_tail_int = 9999
                        mal_tail_hex = hex(mal_tail_int)
                        mal_pkt_info = (f"Malformed packet in {input_file},"
                                        f" byte_offset = {s_hex},"
                                        f" pkt_len = {pkt_len},"
                                        f" byte_array_len = {len(tmp_bytes)},"
                                        f" expected {tail_str} got {mal_tail_hex}")
                        cprint_err(mal_pkt_info)
                        log.error(mal_pkt_info)
                        bpn += 1
        finish_time = time.time()
        # NOTE this uses the size of the last byte 
        pkt_byte_size = len(tmp_bytes)
        num_packets = len(packets)
        time_elapsed = finish_time - start_time
        pkts_per_sec = num_packets / time_elapsed
        MB_per_sec = (pkts_per_sec * pkt_byte_size) / 10**6
        cprint_info(f"Found {len(packets)} valid packets")
        log.info(f"Found {len(packets)} valid packets")
        if bpn > 0:
            cprint_err(f"Encountered {bpn} malformed packets")
            log.info(f"Encountered {bpn} malformed packets")
        perf_str = ("Perf: {:.1f} packets/s"
                    " {:.3f} MB/s".format(pkts_per_sec, MB_per_sec))
        cprint_info(perf_str)

        cprint_info("Writing valid packets, please wait")
        log.info("Writing valid packets, please wait")
        # Second loop to write packets. Can do this in first loop technically,
        #  but may want to use list (memory access) in future
        if save_pickle:
            # List can be recovered/unpickled using pickle.load()
            pkl_fstr = f"{output_folder}.pkl"
            with open(pkl_fstr, "wb") as f:
                pickle.dump(packets, f)
            cprint_info(f"Wrote {len(packets)} files to {pkl_fstr}")
            log.info(f"Wrote {len(packets)} files to {pkl_fstr}")
        else:
            for i, pkt in enumerate(packets):
                with open(f"{output_folder}/packet_{i}.bin", "wb") as f:
                    f.write(pkt)
            cprint_info(f"Wrote {i+1} files to {output_folder}/")
            log.info(f"Wrote {i+1} files to {output_folder}/")


if __name__ == '__main__':
    # Logging
    # log file is simply name of script but with .log extension
    log_file_name_str = "blob_to_packets.log"
    # Configure log message
    # Sort of a cludge, but this does not use stdout printing
    # we already have nice colorized printing
    # TODO use a custom class with logging to make
    #      nice color print
    logging.Formatter.formatTime = formatTime_RFC3339
    log_fmt = '%(asctime)s : %(levelname)s : %(module)s : %(message)s'
    log = logging.getLogger()
    log_fhandler = logging.FileHandler(filename=log_file_name_str, mode="a")
    log_fhandler.setLevel(logging.DEBUG)
    log_fhandler.setFormatter(logging.Formatter(log_fmt))
    log.addHandler(log_fhandler)

    main()

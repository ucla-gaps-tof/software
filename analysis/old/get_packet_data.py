"""
Author: Sean Quinn (spq@ucla.edu)
get_packet_data.py (c) UCLA 2021
Desc: Packet parser syncrhonized with latest firmware
Created:  2021-03-29T23:37:11.388Z
Modified: 2021-08-18T02:06:09.009Z
"""

import os
import struct
import numpy as np
import argparse
import libscrc
import natsort
from color_print import cprint_err, cprint_info, cprint_ok, cprint_warn
import logging
from format_time_rfc3339 import formatTime_RFC3339
import pickle
import time
from typing import Union

# Usage:
# python3 get_packet_data.py PACKET_FOLDER [-z] [-u] [-n]
#   Should be run *after* blob_to_packets.py
#   Needs to run on individual packet files,
#   not the entire RAM buffer stack.

# Output:
# Clear text files for each packet with metadata and payload
# numpy binary array of all packets processed

# Dependencies:
#  libscrc
#  natsort
#  numpy

# TODO fn type hints

def get_adc_data(x):
    # x should be int type (2 byte unsigned)
    # FIXME replace the bin business with formatted strings
    bin_str = bin(x).replace('0b', '').rjust(16, "0")[2:]  # First 14 bits are ADC data, last two bits parity
    out_int = int(bin_str, 2)
    return out_int


def get_adc_sample_parity(x):
    # x should be int type (2 byte unsigned)
    # FIXME replace the bin business with formatted strings
    bin_str = bin(x).replace('0b', '').rjust(16, "0")[:2]  # First 14 bits are ADC data, last two bits parity
    out_int = int(bin_str, 2)
    return out_int


def xor_reduce(x):
    # x should be str type binary number
    # Parity calculator that mirrors calculation done in firmware
    bin_str = x
    result = 0
    for i in bin_str:
        result = (result ^ int(i))
    return str(result)


def calc_adc_parity(x):
    # x should be int type (2 byte unsigned)
    # FIXME replace the bin business with formatted strings
    bin_str = bin(x).replace('0b', '').rjust(16, "0")
    half1 = bin_str[7:14]
    half2 = bin_str[:7]
    result = xor_reduce(half1) + xor_reduce(half2)
    return result


def unpack_32(x):
    # x is type "bytes"
    x_bytearray = bytearray(x)
    # Perform word swap
    x_bytearray[:]=x_bytearray[1],x_bytearray[0],x_bytearray[3],x_bytearray[2]
    x_new_bytes = bytes(x_bytearray)
    x_int = int.from_bytes(x_new_bytes, 'big')
    return x_int


def unpack_48(x):
    # x is type "bytes"
    x_bytearray = bytearray(x)
    # Perform word swap
    x_bytearray[:]=x_bytearray[1],x_bytearray[0],x_bytearray[3],x_bytearray[2],x_bytearray[5],x_bytearray[4]
    x_new_bytes = bytes(x_bytearray)
    x_int = int.from_bytes(x_new_bytes, 'big')
    return x_int


def unpack_64(x):
    # x is type "bytes"
    x_bytearray = bytearray(x)
    # Perform word swap
    x_bytearray[:]=x_bytearray[1],x_bytearray[0],x_bytearray[3],x_bytearray[2],x_bytearray[5],x_bytearray[4],x_bytearray[7],x_bytearray[6]
    x_new_bytes = bytes(x_bytearray)
    x_int = int.from_bytes(x_new_bytes, 'big')
    return x_int


def outstr_to_dict(x):
    # x input is a string
    x = x.replace(" ", "")  # Remove spaces
    x_list = x.split("\n")[:-1]  # Do not include dangling empty element
    new_dict = { y.split("=")[0] : y.split("=")[1] for y in x_list}
    # NOTE: all values are strings
    return new_dict


def get_status(bytes_in: bytes) -> str:
    """Parses DRS status info from status field

    Args:
        bytes_in (bytes): 2 byte status field

    Returns:
        str: formatted output str
    """
    # [0] = sync_err  [1] = drs was busy (lost trigger)  [3:2] = reserved  [15:4]= 12 bit temperature
    tmp_int = struct.unpack("<H", bytes_in)[0]
    tmp_int_binstr = "{:016b}".format(tmp_int)
    sync_str = tmp_int_binstr[-1]
    drs_busy_str = tmp_int_binstr[-2]
    zynq_temp_int = int(tmp_int_binstr[:12], 2)
    tmp_str = (f"status = {tmp_int_binstr}\n"
               f"sync_err = {sync_str}\n"
               f"lost_trig = {drs_busy_str}\n"
               f"zynq_temp_adc = {zynq_temp_int}\n")
    return tmp_str


def get_generic_word_and_int(bytes_in: bytes, fld_str: str) -> Union[str, int]:
    """Parse 1 word len field

    Args:
        bytes_in (bytes): 2 byte packet total length
        fld_str (str): field keyword str

    Returns:
        Union[str, int]: [formatted len str, len int]
    """
    tmp_int = struct.unpack("<H", bytes_in)[0]
    tmp_str = "{0:s} = {1:d}\n".format(fld_str, tmp_int)
    return tmp_str, tmp_int

# RFE Allow caller to specify formatting of string representation


def get_generic_word(bytes_in: bytes, fld_str: str) -> str:
    """Accepts generic 2 byte word and returns formatted str

    Args:
        bytes_in (bytes): 2 byte generic field
        fld_str (str): field str keyword

    Returns:
        str: formatted output str
    """
    tmp_int = struct.unpack("<H", bytes_in)[0]
    tmp_str = "{0:s} = {1:d}\n".format(fld_str, tmp_int)
    return tmp_str


def get_dna(bytes_in: bytes) -> str:
    """Parse 4 word long dna field

    Args:
        bytes_in (bytes): 8 byte dna field

    Returns:
        str: formatted output str
    """
    tmp_int = unpack_64(bytes_in)
    tmp_str = "dna = {:#x}\n".format(tmp_int)
    return tmp_str


def get_fw_hash(bytes_in: bytes) -> str:
    """Parse 1 word fw_hash field

    Args:
        bytes_in (bytes): 2 byte fw_hash field

    Returns:
        str: formatted id str
    """
    tmp_int = struct.unpack("<H", bytes_in)[0]
    tmp_str = "fw_hash = {:#x}\n".format(tmp_int)
    return tmp_str


def get_board_id(bytes_in: bytes) -> str:
    """Parse 1 word id field

    Args:
        bytes_in (bytes): 2 byte board id field

    Returns:
        str: formatted id str
    """
    tmp_str = "id = {:#06x}\n".format(struct.unpack("<H", bytes_in)[0])
    return tmp_str


def get_ch_mask(bytes_in: bytes) -> Union[str, int]:
    """Parse 1 word ch_mask field

    Args:
        bytes_in (bytes): 2 byte ch_mask field

    Returns:
        Union[str, int]: formatted ch_mask str, number of channels read out
    """
    tmp_int = struct.unpack("<H", bytes_in)[0]
    tmp_str = "ch_mask = {:010b}\n".format(tmp_int)
    # !!! This must be done carefully
    # num_chans must be found from first 9 bits ONLY
    # 10th bit is related to AUTO_9TH_CHANNEL
    num_chans_str = "{:016b}\n".format(tmp_int)[7:]
    num_chans = num_chans_str.count('1')
    return tmp_str, num_chans


def get_event_cnt(bytes_in: bytes) -> str:
    """Parse 2 word event_cnt field

    Args:
        bytes_in (bytes): 4 byte event_cnt field

    Returns:
        str: formatted event_cnt str
    """
    tmp_int = unpack_32(bytes_in)
    tmp_str = "event_cnt = {}\n".format(tmp_int)
    return tmp_str


def get_timestamp(bytes_in: bytes) -> str:
    """Parse 3 word timestamp field

    Args:
        bytes_in (bytes): 6 byte timestamp field

    Returns:
        str: formatted timestamp str
    """
    tmp_int = unpack_48(bytes_in)
    tmp_str = "timestamp = {:d}\n".format(tmp_int)
    return tmp_str


def get_packet_data(x, zflag, pkt_name):
    # Return metadata as string for data file header
    # Makes this file much more maintainable.
    # Currently if a length value changes, would be necessary to
    # change index magic numbers used in this function
    try:
        header_index = x.index(head)
    except IndexError:
        cprint_err("Could not find packet header!")
        # Handle this gracefully (do not halt progress)
        # Prepend packet file name with some info, e.g. "bad", "malformed"
        return -1, -1
    out_str = f"header_byte_index = {header_index}\n"
    # Remove zero pad words if necessary
    if zflag is True:
        x_new = b''
        for i in range(header_index, len(x), word_size):
            x_new += x[i:i+word_size][:word_size-zero_pad_len]
    else:
        x_new = x
    # In the following code, x_new is basically the byte array of the packet
    # Packet metadata starts 2 bytes after header data found
    byte_index = header_index + dat_fmt["head"]
    # Always have the 9th channel, overwrite with packet value later
    num_chans = 1
    # Process data fields in place, do not loop over the dict
    # !!! this REQUIRES validated packets. WILL fail if there is misalignment
    # get status
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["status"]]
    byte_index = byte_index + dat_fmt["status"]  # Update index
    tmp_str = get_status(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get len
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["len"]]
    byte_index = byte_index + dat_fmt["len"]  # Update index
    tmp_str, pkt_len = get_generic_word_and_int(tmp_byte, "len")
    out_str = "".join([out_str, tmp_str])
    # get roi
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["roi"]]
    byte_index = byte_index + dat_fmt["roi"]  # Update index
    tmp_str, roi_int = get_generic_word_and_int(tmp_byte, "roi")
    out_str = "".join([out_str, tmp_str])
    # get dna
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["dna"]]
    byte_index = byte_index + dat_fmt["dna"]  # Update index
    tmp_str = get_dna(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get fw_hash
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["fw_hash"]]
    byte_index = byte_index + dat_fmt["fw_hash"]  # Update index
    tmp_str = get_fw_hash(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get id
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["id"]]
    byte_index = byte_index + dat_fmt["id"]  # Update index
    tmp_str = get_board_id(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get ch_mask
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["ch_mask"]]
    byte_index = byte_index + dat_fmt["ch_mask"]  # Update index
    tmp_str, num_chans = get_ch_mask(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get event_cnt
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["event_cnt"]]
    byte_index = byte_index + dat_fmt["event_cnt"]  # Update index
    tmp_str = get_event_cnt(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get dtap0
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["dtap0"]]
    byte_index = byte_index + dat_fmt["dtap0"]  # Update index
    tmp_str = get_generic_word(tmp_byte, "dtap0")
    out_str = "".join([out_str, tmp_str])
    # get dtap1
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["dtap1"]]
    byte_index = byte_index + dat_fmt["dtap1"]  # Update index
    tmp_str = get_generic_word(tmp_byte, "dtap1")
    out_str = "".join([out_str, tmp_str])
    # get timestamp
    tmp_byte = x_new[byte_index:byte_index + dat_fmt["timestamp"]]
    byte_index = byte_index + dat_fmt["timestamp"]  # Update index
    tmp_str = get_timestamp(tmp_byte)
    out_str = "".join([out_str, tmp_str])
    # get payload
    # Packet health check: fw length match actual byte array?
    fw_len = pkt_len
    actual_bytes_len = len(x_new)
    if actual_bytes_len < fw_len:
        err_msg = ("Packet size issue! Firmware says"
                   " {:d} but got {:d} byte array"
                   "".format(fw_len, actual_bytes_len))
        cprint_err(err_msg)
        log.error(err_msg)
        # Malformed packet: possible bad transfer or glitch
        return -1, -1
    # Get trailer metadata
    tail_index = 2 * pkt_len - 2  # Avoid using index
    crc32_len = dat_fmt["crc32"]
    crc32_bytes = x_new[tail_index - crc32_len:tail_index]
    crc32_int = unpack_32(crc32_bytes)
    # CRC32 comparison for entire packet
    crc32_calc = libscrc.crc32(x_new[header_index:tail_index - crc32_len])
    # Print if mismatch
    if crc32_calc != crc32_int:
        cprint_warn(f"Possibly corrupted packet: {pkt_name}")
        log.warning(f"Possibly corrupted packet: {pkt_name}")
        cprint_warn("FW CRC: {0:#010x}".format(crc32_int))
        log.warning("FW CRC: {0:#010x}".format(crc32_int))
        cprint_warn("SW CRC: {0:#010x}".format(crc32_calc))
        log.warning("SW CRC: {0:#010x}".format(crc32_calc))
    stopcell_len = dat_fmt["stop_cell"]
    stopcell_offset = stopcell_len + crc32_len
    stopcell_bytes = x_new[tail_index - stopcell_offset:tail_index - crc32_len]
    stopcell_int = struct.unpack("<H", stopcell_bytes)[0]
    last_metadata = ("stop_cell = {:d}\n"
                     "fw_pkt_crc32 = {:#010x}\n"
                     "sw_pkt_crc32 = {:#010x}"
                     "\n".format(stopcell_int, crc32_int, crc32_calc))
    out_str = "".join([out_str, last_metadata])
    npdata = np.zeros((1024, num_chans + 1))
    fw_adc_crc_str = "fw_adc_crc32"
    sw_adc_crc_str = "sw_adc_crc32"
    ch_id_str = "ch ID"
    # Assign cell ID to each sample
    paylod_len = roi_int + 1
    cell_id = np.roll(np.arange(paylod_len), -stopcell_int)
    npdata[:, 0] = cell_id
    failed_adc_crc_count = 0
    adc_len = dat_fmt["adc_dat"]
    for i in range(1, 1 + num_chans):
        ch_id_int = struct.unpack("<H", x_new[byte_index:byte_index + adc_len])[0]
        byte_index = byte_index + adc_len  # Update byte index
        ch_id_str = "".join([ch_id_str, " {:d}".format(ch_id_int)])
        # Skip first word since that is ch_id
        block_len = 2 * paylod_len
        ch_adc_payload = x_new[byte_index:byte_index + block_len]
        byte_index = byte_index + block_len  # Update byte index
        payload = struct.unpack(f"<{paylod_len}H", ch_adc_payload)
        payload_actual = list(map(get_adc_data, payload))
        payload_parity = list(map(get_adc_sample_parity, payload))
        payload_calc_parity = list(map(calc_adc_parity, payload))
        # TODO Write parities for each cell out
        # TODO Write metadata ints to numpy array as well
        npdata[:, i] = np.array(payload_actual)
        payload_crc32_start = byte_index + adc_len + block_len
        payload_crc32_len = dat_fmt["payload_crc32"]
        payload_crc32 = x_new[byte_index:byte_index + payload_crc32_len]
        byte_index = byte_index + payload_crc32_len  # update byte index
        crc32_int = unpack_32(payload_crc32)
        # CRC comparison for ADC data vectors per channel
        calc_crc32 = libscrc.crc32(ch_adc_payload)
        if calc_crc32 != crc32_int:
            failed_adc_crc_count += 1
        tmp_crc32_int_str = " {:#010x}".format(crc32_int)
        tmp_crc32_calc_str = " {:#010x}".format(calc_crc32)
        fw_adc_crc_str = "".join([fw_adc_crc_str, tmp_crc32_int_str])
        sw_adc_crc_str = "".join([sw_adc_crc_str, tmp_crc32_calc_str])
    if failed_adc_crc_count > 0:
        adc_crc_count_str = (f"Possible payload data corruption"
                             f" on {failed_adc_crc_count} channels, "
                             f"{pkt_name}")
        cprint_warn(adc_crc_count_str)
        log.warning(adc_crc_count_str)
    out_str = "".join([out_str, ch_id_str, "\n", fw_adc_crc_str,
                       "\n", sw_adc_crc_str, "\n"])
    return out_str, npdata

# BUG If PACKETS folder has no bin files, e.g. if pickle file used,
#       the script still looks in that folder, can't load any .bin
#       files and will crash. Edge case needs to be handled.


def main():
    # Zero pad flag, assume off by default
    z = False
    # Read pickle file, assume off by default
    unpickle_flag = False
    # Save numpy file
    save_numpy = False

    # Check for -z flag
    desc_str = "Convert individual packet files to human readable text files"
    parser = argparse.ArgumentParser(description=desc_str)
    parser.add_argument('folder', metavar='f', type=str, nargs='+',
                        help='folder(s) to process')
    parser.add_argument('-z', default=argparse.SUPPRESS,
                        action='store_const', const=True,
                        help='use flag -z if firmware version before e595a443')
    parser.add_argument('-u', '--unpickle', action='store_const', const=True,
                        default=False,
                        help='reads pickle file, not folder of packets',
                        dest="unpickle")
    parser.add_argument('-n', '--numpy', action='store_const', const=True,
                        default=False,
                        help='saves numpy binary file',
                        dest="save_numpy")

    args = parser.parse_args()

    # Overwrite default if user sets -z flag
    if hasattr(args, 'z'):
        z = args.z
    # Overwrite default if -u flag set
    if hasattr(args, "unpickle"):
        unpickle_flag = args.unpickle
    # Overwrite default if -n flag set
    if hasattr(args, "save_numpy"):
        save_numpy = args.save_numpy

    # Get packet directory/pickle list

    pkt_dir_list = args.folder

    # Overall packet stats
    bad_packet_counter = 0
    good_packet_counter = 0

    # Main loop
    start_t = time.time()
    for pkt_dir in pkt_dir_list:
        if unpickle_flag is True:
            with open(pkt_dir, "rb") as pf:
                dirlist = pickle.load(pf)
                # For pkl files a PACKETS folder may not exist, create one
                os.makedirs(pkt_dir.replace(".pkl", ""), exist_ok=True)
        else:
            dirlist = os.listdir(pkt_dir)
            dirlist = [x for x in dirlist if '.bin' in x and "bad" not in x]
            dirlist = natsort.natsorted(dirlist)
        num_pkts = len(dirlist)
        np_data_spec = [('metadata', 'U', 750), ('evt_id', np.int32), ('data', np.int16, (1024, 10))]
        out_nparr = np.zeros(num_pkts, dtype=np_data_spec)
        for i, f in enumerate(dirlist):
            if unpickle_flag is True:
                # For pkl files, dirlist is a list of packets
                # each element is a bytes obj
                data = f
                ofile_str = f"{pkt_dir}/packet_{i}_payload.txt"
                ofile_str = f"{ofile_str}".replace(".pkl", "")
            else:
                with open(pkt_dir + "/" + f, "rb") as datafile:
                    data = datafile.read()
                # Clear text file if it exists
                open(pkt_dir+"/"+f.replace('.bin', '_payload.txt'), "w").close()
                # Make clear text file with similar name
                ofile_str = pkt_dir+"/"+f.replace('.bin', '_payload.txt')
            # Get packet metadata and payload and handle bad packets
            metadata, adc_data = get_packet_data(data, z, ofile_str)
            if metadata == -1 and adc_data == -1:
                # Problem with packet, prepend file to differentiate from rest
                os.rename(pkt_dir + "/" + f, pkt_dir + "/" + "bad_" + f)
                # Do not create empty text file, continue to next packet file
                bad_packet_counter += 1
                continue
            # add record to numpy array
            # TODO turn the ch_mask and evt_id extraction into functions
            ch_mask_start = metadata.find("ch_mask")
            ch_mask_end = metadata.find("\n", ch_mask_start)
            ch_mask_str = metadata[ch_mask_start:ch_mask_end].split("=")[1]
            ch_mask_str = ch_mask_str.replace(" ", "")
            ch_mask_list = list(ch_mask_str)
            ch_mask_list.reverse()  # Bit mask ordering reversed compared to adc_data array
            # Insert True for cell_id col
            ch_mask_list.insert(0, "1")
            ch_mask_list = ch_mask_list[:-1]  # Do not use MSB, related to auto 9th channel toggle
            ch_mask_bool = np.array(ch_mask_list, dtype=int).astype(bool)
            out_nparr['metadata'][i] = metadata
            # Get event ID for easy searching
            evt_id_start = metadata.find("event_cnt")
            evt_id_end = metadata.find("\n", evt_id_start)
            evt_id_str = metadata[evt_id_start:evt_id_end].split("=")[1]
            evt_id_str = evt_id_str.replace(" ", "")
            evt_id = int(evt_id_str)
            out_nparr['evt_id'][i] = evt_id
            # Get roi len so assignment into data block works
            roi_start = metadata.find("roi")
            roi_end = metadata.find("\n", roi_start)
            roi_str = metadata[roi_start:roi_end].split("=")[1]
            roi_str = roi_str.replace(" ", "")
            roi = int(roi_str) + 1  # Size is + 1 of firmware value
            # Broadcast rest of payload
            out_nparr['data'][i][:roi, ch_mask_bool] = adc_data
            # Print some status updates occasionally
            if i % 500 == 0 and i > 0:
                # Replace any extension on pkt_dir (e.g. pickle used)
                tmp_msg = (f"Processed {i} packets "
                           f"to folder {pkt_dir}".replace(".pkl", ""))
                cprint_info(tmp_msg)
                log.info(tmp_msg)
            # Save to individual plain text files if save_numpy not on
            if save_numpy is not True:
                np.savetxt(ofile_str, adc_data, header=metadata, fmt="%i")
            good_packet_counter += 1
    if save_numpy is True:
        np_fstr = f"{pkt_dir}".replace("/", "").replace(".pkl", ".npy")
        np.save(np_fstr, out_nparr)
        np_save_info = f"Saving numpy binary file to {np_fstr}"
        cprint_info(np_save_info)
        log.info(np_save_info)
    stop_t = time.time()
    elapsed_t = stop_t - start_t
    pkts_sec = (good_packet_counter + bad_packet_counter) / elapsed_t
    MB_sec = (len(data) / 10**6) * pkts_sec
    finish_msg = f"Parsed {good_packet_counter} packets successfully."
    cprint_ok(finish_msg)
    log.info(finish_msg)
    if bad_packet_counter > 0:
        bad_msg = f"Could not parse {bad_packet_counter} packets."
        cprint_err(bad_msg)
        log.error(bad_msg)
    perf_str = ("Perf: {:.1f} packets/s"
                " {:.3f} MB/s".format(pkts_sec, MB_sec))
    cprint_info(perf_str)


if __name__ == '__main__':
    # Constants
    head = b'\xaa\xaa'
    word_size = 4
    zero_pad_len = 2
    tail = b'\x55\x55'

    # Logging
    # log file is simply name of script but with .log extension
    log_file_name_str = "get_packet_data.log"
    # Configure log message
    # Sort of a cludge, but this does not use stdout printing
    # we already have nice colorized printing
    # TODO use a custom class with logging to make
    #      nice color print
    logging.Formatter.formatTime = formatTime_RFC3339
    log_fmt = '%(asctime)s : %(levelname)s : %(module)s : %(message)s'
    log = logging.getLogger()
    log_fhandler = logging.FileHandler(filename=log_file_name_str, mode="a")
    log_fhandler.setLevel(logging.DEBUG)
    log_fhandler.setFormatter(logging.Formatter(log_fmt))
    log.addHandler(log_fhandler)

    # FIXME automate this step by importing the latest format from README
    # Metadata
    # dataformat dict, field name : field len (bytes)
    # must match dataformat used in firmware
    dat_fmt = {"head": 2,
               "status": 2,
               "len": 2,
               "roi": 2,
               "dna": 8,
               "fw_hash": 2,
               "id": 2,
               "ch_mask": 2,
               "event_cnt": 4,
               "dtap0": 2,
               "dtap1": 2,
               "timestamp": 6,
               "adc_dat": 2,
               "payload_crc32": 4,
               "stop_cell": 2,
               "crc32": 4,
               "tail": 2}

    main()
    cprint_ok("get_packet_data.py main() returns, exiting.")
    log.info("get_packet_data.py main() returns, exiting.")

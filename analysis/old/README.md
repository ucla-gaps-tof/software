# Introduction

[[_TOC_]]

This folder contains code for unpacking a RAM buffer binary blob (which is simply a large collection of serial packets) into individual packet binary files. Other scripts for then extracting the payload (e.g. DRS4 data) are also available.

## Prerequisites/dependencies

### Software

* Python 3.6+
  * numpy
  * [libscrc](https://pypi.org/project/libscrc/)
  * [natsort](https://pypi.org/project/natsort/)
  * [pyinstaller](https://pypi.org/project/pyinstaller/)

> ⚠️ Always use caution when installing things via `pip3`. The package name must be typed exactly, or you risk installing potentially malicious code. You may follow the link to the PyPI page where a copy to clipboard button is available.

Numpy should be available from your OS package manager.

# Installing (optional)

Create executables using

```bash
make install
```

# Usage

Scripts/binaries are command line utilities. When unpacking data, follow the sequence of scripts presented here. E.g. `blob_to_packets.py` comes first, followed by `get_packet_data.py`.

## `blob_to_packets.py`

```bash
# Using source file
python3 blob_to_packets.py input_file(s) [-z] [-s] [-p]
# Using exe
./blob_to_packets input_file(s) [-z] [-s] [-p]
```

The input file should be a raw RAM buffer dump from a UCLA DRS4 board. This is typically saved by a script called `server.py` which handles the network transfers. The size can be variable depending on configuration settings of the remote host, but should be 10s of MB. In recent testing (Aug 2021) the buffer size is set to 64 MB.

### `blob_to_packets.py` options

| Parameter | Arg type | Description                          | Notes                      |
| :---      | :---     | :---                                 | :---                       |
| z         | no arg   | Handles zero padded words     | Used with earlier firmware versions before implementation of asymmetric FIFO. Should not be used for current firmware. |
| s         | no arg   | Skips files that already have PACKETS folder created     | Useful if using wildcards for input files but there is no need to overwrite already parsed data |
| p         | no arg   | Writes all packets to single pickle file     | Pickle is a list of individual packets |

### Outputs

The script will create a new folder (or `.pkl` file) using the same name as the input file with the string `PACKETS_` prepended to that name. See example exploded directory below.

```text
  PACKETS_svr_51520_data_072521_164850/
    ├── packet_0.bin
    ├── packet_1.bin
    ├── packet_2.bin
    ├── packet_3.bin
    ├── packet_4.bin
    .
    .
    .
  PACKETS_svr_51520_data_072521_164850.pkl
```

The binary packet files can be easily inspected with your favorite hex editing tool. Any Debian based distro should come with the `hexdump` utility. Example output for a properly assembled packet is shown below. The format/structure of the packet is defined by firmware specification, see <https://gitlab.com/ucla-gaps-tof/firmware#dataformat>

```text
0000000 0000 0000 0000 0000 0000 0000 0000 0000
*
0000020 0000 0000 aaaa a680 202e 03ff 0058 9cc9
0000030 2f90 4854 2695 0000 00ff 0000 1f88 56ce
0000040 0000 0287 3a17 952c 0000 1d78 1e2b dd90
0000050 1d6a 1e28 9de6 1e21 ddc6 ddeb 9dec 9dc2
0000060 5d7a ddc6 ddd8 1d7e 9d9e 9dc4 5e31 5d19
0000070 dd9a 1d30 ddd1 5e2c 9dec 1e0a 5e19 1e14
0000080 9efb 1e09 1e2e dd95 9df8 ddd8 1e5a 5e13
...
...
0004020 1e41 5d01 9dab 5cc3 1e28 df70 9dce 5e02
0004030 9dd6 9dfd dd90 1e53 dda9 5e49 ddcc 1e1d
0004040 9dce 5e54 ddb7 1e2b 1d74 1e6f ddf6 1e5c
0004050 dd93 5e02 ddeb 1e4e ddfc 9d94 9db0 5e1c
0004060 5e10 9ddc ddee 9dda ddd8 5e1a 1e60 5e1a
0004070 ddbe 5d4c 0f0e f0d2 0225 38dd 61b0 5555

```

## `get_packet_data.py`

```bash
# Using source file
python3 get_packet_data.py packet_folder(s) [-z] [-u] [-n]
# Using exe
./get_packet_data packet_folder(s) [-z] [-u] [-n]
```

The input folder containing individual binary packet files.

### `get_packet_data.py` options

| Parameter | Arg type | Description                          | Notes                      |
| :---      | :---     | :---                                 | :---                       |
| z         | no arg   | Handles zero padded words     | Used with earlier firmware versions before implementation of asymmetric FIFO. Should not be used for current firmware. |
| u         | no arg   | Load a pickle file instead of folder     | Reads `.pkl` file instead of a folder of `.bin` files |
| n         | no arg   | Save all packets to single .npy array     | Use numpy binary format instead of plain text |

### Outputs

The script will write output files to the same folder that is passed to it as an input argument, e.g. `packet_folder` from the command above. For each binary packet file it will create a new text file with the same name but with the string `_payload` appended. The header of this file shows packet metadata and the ADC payload is written as a 2D array (space delimited).

Example folder after running this script. Original packet binary files are not modified.

```text
  PACKETS_svr_51520_data_072521_164850/
    ├── packet_0.bin
    ├── packet_0_payload.txt
    ├── packet_1.bin
    ├── packet_1_payload.txt
    ├── packet_2.bin
    ├── packet_2_payload.txt
    .
    .
    .
  PACKETS_svr_51520_data_072521_164850.npy
```

Contents of an example payload text file below. For more information about the metadata values, e.g. `status`, `dna`, `id`, see their definitions in the firmware repo <https://gitlab.com/ucla-gaps-tof/firmware#dataformat>.

The left most column (e.g. col 0) corresponds to the DRS4 cell ID. Each subsequent column corresponds to the channel specified by the `ch ID` string. So, for the example below, the 1024 vector for channel 0 would begin with these values: `7544, 7723, 7568, ...` Channel 1 would be: `7625 7665 7604, ...`

The ADC (AD9245) used on this board is 14-bit. The voltage reference is 1.0 V.

```text
# header_byte_index = 0
# status = 1010011011000000
# sync_err = 0
# lost_trig = 0
# zynq_temp_adc = 2668
# len = 8238
# roi = 1023
# dna = 0x589cc92f904854
# fw_hash = 0x2695
# id = 0x0000
# ch_mask = 0011111111
# event_cnt = 1910
# dtap0 = 22222
# dtap1 = 0
# timestamp = 5246036039
# stop_cell = 549
# fw_pkt_crc32 = 0x5c1ced07
# sw_pkt_crc32 = 0x5c1ced07
# ch ID 0 1 2 3 4 5 6 7
# fw_adc_crc32 0xcbfa0baa 0xf004549d 0x12443c47 0xc3e3f645 0x2c2ad65b 0x9e97d10c 0x53584c22 0x34ee6aea
# sw_adc_crc32 0x5b412bb0 0x0c54f153 0x08fae616 0xc169a3ce 0x99b44437 0xf5830d32 0xefa995c2 0x84ca8bc7
# 
549 7544 7625 7665 7826 7490 7663 7584 7714
550 7723 7665 7773 7736 7726 7651 7749 7782
551 7568 7604 7649 7689 7646 7715 7553 7729
552 7530 7614 7640 7686 7670 7644 7558 7563
553 7720 7524 7514 7644 7639 7711 7635 7540
554 7654 7559 7736 7605 7766 7659 7796 7882
...
...
...
544 7683 7462 7847 7613 7759 7651 7788 7706
545 7585 7633 7585 7595 7698 7587 7639 7776
546 7669 7604 7596 7706 7656 7496 7659 7706
547 7566 7632 7542 7641 7623 7728 7718 7614
548 7588 7615 7660 7700 7717 7709 7716 7500
```

### numpy data format

Record format/dtype: `np_data_spec = [('metadata', 'U', 750), ('evt_id', np.int32), ('data', np.int16, (1024, 10))]`

* each record corresponds to an event
* `metadata` field is a unicode string, up to 750 characters (current header is 591 characters)
* `evt_id` field is 32-bit int of event ID from firmware
* data is (1024,10) array of 16-bit ints. First column is cell ID, last remaining 9 columns are channel values
  * roi and ch_mask are used to fill this. If roi < 1024 or < 9 channels used, everything else will be 0s

```ipython
In [13]: np_test['metadata'][0]
Out[13]: 'header_byte_index = 0\nstatus = 1010001111000000\nsync_err = 0\nlost_trig = 0\nzynq_temp_adc = 2620\nlen = 8238\nroi = 1023\ndna = 0x2224292f90485c\nfw_hash = 60063\nid = 0x0000\nch_mask = 0011111111\nevent_cnt = 0\ndtap0 = 0\ndtap1 = 0\ntimestamp = 4045981450\nstop_cell = 282\nfw_pkt_crc32 = 0xa2f246f6\nsw_pkt_crc32 = 0xa2f246f6\nch ID 0 1 2 3 4 5 6 7\nfw_adc_crc32 0xae850001 0x591a0002 0x67880003 0x67680004 0x04270005 0x24160006 0xbf1e0007 0x4769011a\nsw_adc_crc32 0x94b3ae85 0xc7f0591a 0x40186788 0x36966768 0xcb7e0427 0xf9aa2416 0x50c9bf1e 0x9e5d4769\n'

In [14]: np_test['evt_id'][0]
Out[14]: 0

In [15]: np_test['data'][0]
Out[15]: 
array([[ 282, 7491, 7609, ..., 7621, 7541,    0],
       [ 283, 7786, 7768, ..., 7752, 7833,    0],
       [ 284, 7510, 7633, ..., 7549, 7589,    0],
       ...,
       [ 279, 7714, 7688, ..., 7719, 7761,    0],
       [ 280, 7485, 7557, ..., 7570, 7636,    0],
       [ 281, 7599, 7756, ..., 7779, 7779,    0]], dtype=int16)

In [16]: np_test.shape
Out[16]: (4033,)

In [17]: 
```
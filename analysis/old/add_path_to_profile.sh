BINDIR=$(pwd)"/dist"

case ":$PATH:" in
  *":$BINDIR:"*) :;; # already there
  *) PATHUPDATE="$PATH:$BINDIR";;
esac

echo "PATH=$PATHUPDATE" >> ~/.profile
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def cprint_info(s: str) -> None:
    """Prepend colorized [INFO]

    Args:
        s (str): input string to decorate
    """
    print(f"[{bcolors.OKCYAN}INFO{bcolors.ENDC}] {s}")


def cprint_warn(s: str) -> None:
    """Prepend colorized [WARN]

    Args:
        s (str): input string to decorate
    """
    print(f"[{bcolors.WARNING}WARN{bcolors.ENDC}] {s}")


def cprint_err(s: str) -> None:
    """Prepend colorized [ERROR]

    Args:
        s (str): input string to decorate
    """
    print(f"[{bcolors.FAIL}ERROR{bcolors.ENDC}] {s}")


def cprint_ok(s: str) -> None:
    """Prepend colorized [OK]

    Args:
        s (str): input string to decorate
    """
    print(f"[{bcolors.OKGREEN}OK{bcolors.ENDC}] {s}")

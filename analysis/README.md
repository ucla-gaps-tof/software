# Introduction

[[_TOC_]]

This folder contains code for the analysis of a RAM buffer binary (or "blob") sent over from a readout board's computer. Includes scripts for unpacking, plotting, calibration, etc.

## Prerequisites/dependencies

### Software

* ROOT (tested with 6.24)
* Python 3.6+
  * numpy
  * matplotlib
  * [libscrc](https://pypi.org/project/libscrc/) (for packet validation)

> ⚠️ Use caution when installing things via `pip3`. The package name must be typed exactly, or you risk installing potentially malicious code.
> Also, you can make sure the desired python installation is used with `$PYTHON -m pip install $PACKAGE_NAME`


# Usage

## blob2root
```bash
blob2root dat_file
```

Turns input dat_file (.dat) into a ROOT file (.root), after removing spikes and calibrating. Looks for a file in the working directory called `rb#_cal.txt`, and does voltage and timing calibrations, if found.

The input file should be a raw RAM buffer dump from a UCLA DRS4 board. This is typically saved by a script called `server.py` which handles the network transfers. The size can be variable depending on configuration settings of the remote host, but should be 10s of MB. In recent testing (Aug 2021) the buffer size is set to 64 MB.

## displayData
```bash
displayData root_file
```

Displays traces in root_file, event by event.

## cal.py
```
python3 cal.py [-d vdif] [-n rbnum] [-t] dat_file(s)
```
Creates voltage and timing calibration file in .npy or .txt form. Up to three files should be given in the order: first voltage calibration file, second voltage calibration file, timing calibration file. If one file is provided, only offset calibration is done. If two files are provided, full voltage calibration is done.

* -d vdif: Voltage difference between voltage calibration files (2nd - 1st), in mV.
* -v vcalfile: Use an existing voltage calibration file, and use the first .dat file for timing calibration
* -e edge: argument for timingCalibration (rising, falling, or average)
* -n name: Give a custom name to output file (otherwise, defaults to rbN_cal.txt)
* -npy: Also save calibrations as .npy file

Example instructions:
1. Take no-input data at 0V baseline by running the following on the readout board:
```
sudo devmem 0x80000104 32 0
rbsetup -i 0
sudo Readout_DRS4 -n 5000 -m 2
```
(The devmem command enables software triggering; sudo devmem 0x80000104 32 1 should be run before using the external trigger)
2. Take a second voltage calibration run at 185mV (measured in lab) with:
```
rbsetup -i 0 -p 46400
sudo Readout_DRS4 -n 5000 -m 2
```
3. Take timing calibration/sine wave data with:
```
rbsetup -i 1
sudo Readout_DRS4 -n 5000 -m 3
```
4. Make the calibration file with (replace file names):
```
python3 cal.py -d 185 file1.dat file2.dat file3.dat
```

## plot.py
```bash
python3 plot.py [-n event_num] [-v cal_file] [-s clean] [-r] dat_file(s)
```
Simple trace plotter that can apply the .npy calibration files

* -n event_num: event # to start at (default is 0)
* -v cal_file: voltage calibration file
* -s clean: enable spike cleaning (0 or 1; default is 1)
* -r: print RMS

## plot_blob.py
```bash
python3 plot_blob.py [-n #] dat_file(s)
```
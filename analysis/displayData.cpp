#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <iomanip>
#include <math.h>
#include <libgen.h>
#include <TFile.h>
#include <TH1D.h>
#include <TF1.h>
#include <TH2D.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TChain.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TRint.h>
#include <TLine.h>
#include <TTimeStamp.h>
#include <TText.h>
#include <TTree.h>
#include <TSystem.h>
#include <TMath.h>
#include <TPolyLine3D.h>
#include <TPolyMarker3D.h>
#include <TView.h>
#include <TMarker3DBox.h>
#include <TGraph2D.h>
#include <TMinuit.h>
#include <TApplication.h>

#include "include/Waveform.h"
#include "include/MainFrame.h"
#include "include/structures.h"

using namespace std;


/* Prototypes */
void displayData(char *root_file, char *parameter_file, int gui_flag);
void loadParameters(char *parameter_file, struct Limits *l);
int gui_wait();
void plot(int n_ch);
void read_events(void);
int  event_flag(int evno);
extern void InitGui();

/* Constants */
#define MAX_EVENTS 1000
enum GUISWITCH { WAITING, NEXT, QUIT, ALLCH, SELECT, PRINT, FINISH,
                 RESTRICT, FIT };

/* Global Variables */
int n_chan = 0;
int old_data;
int invert[MAX_CHAN];
double charge_min[MAX_CHAN];
Waveform *wave[MAX_CHAN];
Waveform *avg[MAX_CHAN];
Waveform *avg_noped[MAX_CHAN];
VoidFuncPtr_t initfuncs[] = { InitGui, 0};
int ALL_EVENTS = 1;
int plot_flag = ALLCH;  // Plot all channels by default
int plot_ch   = -1;
int restrict_range = 0;
float x_sc_lo, x_sc_hi, y_sc_lo, y_sc_hi;     // For full ranges
float x_scr_lo[MAX_CHAN], x_scr_hi[MAX_CHAN];
float y_scr_lo[MAX_CHAN], y_scr_hi[MAX_CHAN]; // For restricted ranges
int Events[1000];
TH2F    *h_dum2;
//TROOT root("GUI","test",initfuncs);
TApplication theApp("FADC", 0, 0, 0, 0);    
//TApplication *theApp;
MainFrame mainWin(gClient->GetRoot(), 600, 600);
//MainFrame *mainWin;
TCanvas *cm = mainWin.GetCanvas();
//TCanvas *cm;

int main(int argc, char * argv[]) {
  if (argc < 2) {
    cout << "Usage:  "<<argv[0]<<" DATAFILE [PARAMETERFILE] GUI_FLAG" << endl;
    exit (1);
  }
  int gui_flag = 1;
  char par_file[400] = "parameters.txt";
  if (argc == 3) {
    if (strlen(argv[2]) == 1)
      gui_flag = atoi(argv[2]);
    else
      sprintf(par_file, "%s", argv[2]);
  }
  
  if (gui_flag) {
    read_events();
    //theApp = new TApplication("FADC", 0, 0, 0, 0);    
    //mainWin = new MainFrame(gClient->GetRoot(), 600, 600);
    //cm = mainWin->GetCanvas();
    //cm = mainWin.GetCanvas();
  }
  
  displayData(argv[1], par_file, gui_flag);
  
  if (gui_flag) {
    //delete cm; 
    //delete mainWin;
    //delete theApp;
  }

  return 0;
}

void displayData(char *root_file, char *parameter_file, int gui_flag) {

  char text[400];
  char *basec, *bname, *fileBase;
  // This manipulation of the filename allows us to pass the root file
  // (from an arbitrary directory) and create the proper output
  // file (in the current directory).
  basec = strdup(root_file);
  bname = basename(basec);
  fileBase = strndup(bname, strlen(bname)-5); // Strip off ".root"
  fileBase[strlen(bname)-5] = '\0'; // Add terminating null byte
  //printf("FileBase = %s, length = %d\n", fileBase, strlen(bname));

  // Make things look good
  gSystem->Load("libTree");
  gROOT->SetStyle("Plain");
  if (gui_flag == 0) gROOT->SetBatch(kTRUE);
  gStyle->SetTitleBorderSize(0);
  gStyle->SetLabelSize(0.04,"xyz");
  gStyle->SetTitleSize(0.04,"xyz");
  gStyle->SetTitleOffset(1.3,"xyz");
  gStyle->SetHistLineColor(1);
  gStyle->SetPalette(1);
  gStyle->SetOptStat(111111);  // Seems to have no effect

  struct Limits      L;  // Various number for displaying/integrating/etc

  int    chan;           // To specify which channel to plot

  // Set the scale for the plots of the traces (this histogram is only
  // used if the program is called with 'gui_flag' set to 1).
  x_sc_lo =    0.0;    // in ns
  x_sc_hi =  500.0;    // in ns
  y_sc_lo =  -150.0;    // in mV
  y_sc_hi =  850.0;    // in mV

  float factor = 1.0;
  for (int i=0; i<MAX_CHAN; i++) {
    x_scr_lo[i] =   50.0;    // in ns
    x_scr_hi[i] =  250.0;    // in ns
    factor = (i<4) ? 3.0 : 1.0;
    y_scr_lo[i] = -270.0/factor;    // in mV
    y_scr_hi[i] =   35.0;    // in mV
  }
  
  // Delete any waveforms and initialize the average waveforms
  for(int c=0;c<MAX_CHAN;c++) {
    if ( wave[c] != NULL ) {
      delete wave[c];
      wave[c] = NULL;
    }
  }
 
  // Now read through the data.
  TFile f(root_file, "READ");
  if (f.IsZombie() ) {
    cout << "Unable to open '" << root_file << "'. Exiting" << endl;
    return; 
  }
  // Make a pointer to the ROOT tree with all the data. 
  TTree *rec = (TTree*)f.Get("rec");
  
  // Get number of channels
  char t_ch[10];
  char c_ch[10];
  for (int i=0; i<MAX_CHAN; i++) {
    sprintf(c_ch,"chn%d",i);
    if(rec->GetListOfBranches()->FindObject(c_ch))
      n_chan++;
  }

  // Read parameter file
  loadParameters(parameter_file, &L);

  // We switched to using four different time traces at run 353.
  if (rec->GetBranchStatus("t")) // new root files have t0,t1,... instead
    old_data = 1;
  else
    old_data = 0;

  Double_t chn[MAX_CHAN][1024];
  Double_t t[MAX_CHAN][1024];
  Int_t    eventNum;
  time_t EventDate; // Not filled yet. 

  // Identify the branches of the tree (defined in unpack_root.cpp)
  rec->SetBranchAddress("eventNum",  &eventNum);

  for (int c=0; c<n_chan; c++) { 
    sprintf(t_ch,"t%d",c);
    sprintf(c_ch,"chn%d",c);
    //r = tree->SetBranchAddress("value", &value);
    if(rec->GetListOfBranches()->FindObject(c_ch))
      rec->SetBranchAddress(c_ch, &chn[c]);
    if(rec->GetListOfBranches()->FindObject(t_ch))
      rec->SetBranchAddress(t_ch, &t[c]);
    else if (rec->GetListOfBranches()->FindObject("t")) // for old data
      rec->SetBranchAddress("t", &t[0]);
  }

  int nentries = rec->GetEntriesFast(); // Find the number of events
  
  // Now, loop through the data and calculate everything
  for (int pass = 1; pass < 2; pass++) {
    for (int i = 0; i < nentries; i++) {
      if (event_flag(i) == 0 && i!=0 )
        continue;
      rec->GetEntry(i);
      
      // Loop through channels
      for(int c = 0; c < n_chan; c++) {
        if ( wave[c] != NULL ) {
          delete wave[c];
          wave[c] = NULL;
        }
        // Optionally invert waveforms
        if (invert[c] == 1) {
          for (int j=0; j<1024; j++) { 
            chn[c][j] *= -1.0;
          }
        }
  
        // Initialize the waveform for this channel. 
        if (old_data == 0)
          wave[c] = new Waveform(chn[c], t[c], c, 0);
        else
          wave[c] = new Waveform(chn[c], t[0], c, 0); // Old data format

        if (i==0) {
          if (c==0) { // This is the same for all channels
            x_sc_lo = wave[c]->GetBinTime(0);
            x_sc_hi = wave[c]->GetBinTime(wave[c]->GetWaveSize()-1);
          }
          if (c<4) { // pulse_lo only filled for 4 channels
            //x_scr_lo[c] = L.pulse_lo[c];
            //x_scr_hi[c] = L.pulse_lo[c] + L.pulse_win[c];
          }
        }

        wave[c]->SetThreshold(L.thresh);

        // Calculate the pedestal
        wave[c]->SetPedBegin(L.ped_lo);
        wave[c]->SetPedRange(L.ped_win);
        wave[c]->CalcPedestalRange(); 
        //wave[c]->SubtractPedestal(); 

        if (pass == 1) {
          if (i == 0) { // Initialize the waveform on the first event
            if (old_data == 0) {
              avg[c] = new Waveform(chn[c], t[c], c, 0);
              avg_noped[c] = new Waveform(chn[c], t[c], c, 0);
            } else {
              avg[c] = new Waveform(chn[c], t[0], c, 0);
              avg_noped[c] = new Waveform(chn[c], t[0], c, 0);
            }
            avg[c]->SetPedBegin(L.ped_lo);
            avg[c]->SetPedRange(L.ped_win);
            avg[c]->CalcPedestalRange(); 
            avg[c]->SubtractPedestal(); 
          } else { // Add the waveform to average for channel
            avg_noped[c]->Add(wave[c]);
          }
          
          if (i > 0) {// Add the ped_subtracted waveform to average for channel
            avg[c]->Add(wave[c]);
          }
        }
      }
      
      // The next ~30 lines of code make some nice plots of each event
      // assuming the program was called with gui_flag set to 1.
      if (pass == 1) {
        int PLOT_EVENT = event_flag(eventNum);
        if (gui_flag && PLOT_EVENT) { 
          printf(".%d", eventNum);
          fflush(stdout);
          plot(n_chan);
  
          if (plot_flag != FINISH) { 
            while (gui_wait() != NEXT) {
              switch (mainWin.status) {
              case QUIT:
                return;
                break;
              case SELECT:
                chan=atoi(mainWin.GetText());
                if (chan < 0 || chan >= n_chan) {
                  cout << " Ch must be between 0 and " << n_chan-1 << endl;
                } else {
                  plot_flag = SELECT;
                  plot_ch = chan;
                  plot(n_chan);
                }
                break;
              case ALLCH:
                plot_flag = ALLCH;
                plot(n_chan);
                break;
              case PRINT: 
                //sprintf(title,"histo_%4.2f-%4.2f.jpg",data_charge[2][evt],
                //    data_charge[3][evt]);
                //cm->Print(title);
                cm->Print("histo.pdf");
                break;
              case FINISH: 
                plot_flag = FINISH;
                break;
              case RESTRICT: 
                restrict_range = (restrict_range == 1 ? 0 : 1);
                break;
              case FIT:
                for (int c = 0; c < n_chan; c++) {
                  if (plot_flag == ALLCH ||
                     (plot_flag == SELECT && plot_ch == c)) {
                    cm->cd(c+1);
                    wave[c]->FindPeaks(L.pulse_lo[c],L.pulse_win[c]);
                    if (wave[c]->GetNumPeaks() > 0)
                      wave[c]->PlotFit();
                  }
                }
                cm->Update();
                break;
              }
            }
          }
        }
      }
    }
  }
}

void loadParameters(char *parameter_file, struct Limits *l) {
  cout << "Parameter file: " << parameter_file << endl;
  map<string, string> params;
  char text[400];
  char key[40];
  char val[40];
  FILE *fp = fopen(parameter_file, "r");
  if (fp == NULL) {
    cout << "Parameter file not found" << endl;
  } else {
    while (fgets(text, sizeof text, fp)) {
      if (*text == '#')
        continue;
      sscanf(text, "%[^=]=%[^#\n]", key, val);
      *remove(key, key + strlen(key), ' ') = '\0'; // removes spaces
      string sval = val;
      size_t first = sval.find_first_not_of(' ');
      sval = sval.substr(first, sval.find_last_not_of(' ') - first + 1);
      params[key] = sval;
    }
  }
  // Window of time (in ns) used to calculate the pedestal for each event
  l->ped_lo = params.count("ped_lo") ? stof(params["ped_lo"]) : 10.0;
  l->ped_win = params.count("ped_win") ? stof(params["ped_win"]) : 50.0;
  // The threshold (in mV) for finding peaks. Unless the trace crosses 
  // this threshold, no timing info is calculated.
  l->thresh = params.count("thresh") ? stod(params["thresh"]) : -5.0;
  // The fraction for calculating the CFD timing using the SIMPLE method
  l->cfd_fraction = params.count("cfd_frac") ? stod(params["cfd_frac"]) : 0.10;
  // This is the threshold for doing the CFD_SIMPLE anaysis. 
  l->cfd_thresh = params.count("cfd_thresh") ? stod(params["cfd_thresh"]) : -10;
  // Now define some windows
  for (int i=0; i<n_chan; i++) { 
    // Window of time (in ns) used to find pulses. 
    l->pulse_lo[i]   = 350.0;
    l->pulse_win[i]  = 100.0;
    // Window of time (in ns) used to find pulse parameters for each
    // event. Start value of the window is initialized but set later
    l->charge_lo[i]  = 375.0;
    l->charge_win[i] =  50.0;
    // This is used as a threshold to determine if we do a timing analysis
    // This corresponds to a minimum charge (in pC)
    charge_min[i]   = 1.0;
  }
  stringstream ss;
  if (params.count("pulse_lo")) {
    ss << params["pulse_lo"];
    for (int i=0; i<n_chan; i++)
      ss >> l->pulse_lo[i];
    ss.str("");
    ss.clear();
  }
  if (params.count("pulse_win")) {
    ss << params["pulse_win"];
    for (int i=0; i<n_chan; i++)
      ss >> l->pulse_win[i];
    ss.str("");
    ss.clear();
  }
  if (params.count("charge_lo")) {
    ss << params["charge_lo"];
    for (int i=0; i<n_chan; i++)
      ss >> l->charge_lo[i];
    ss.str("");
    ss.clear();
  }
  if (params.count("charge_win")) {
    ss << params["charge_win"];
    for (int i=0; i<n_chan; i++)
      ss >> l->charge_win[i];
    ss.str("");
    ss.clear();
  }
  if (params.count("charge_min")) {
    ss << params["charge_min"];
    for (int i=0; i<n_chan; i++)
      ss >> charge_min[i];
    ss.str("");
    ss.clear();
  }
  if (params.count("invert")) {
    ss << params["invert"];
    for (int i=0; i<n_chan; i++) {
      ss >> invert[i];
      if (invert[i] == -1)
        invert[i] = invert[0];
    }
    ss.str("");
    ss.clear();
  }
}

int gui_wait() {
  
  mainWin.status = WAITING;
  while(mainWin.status == WAITING) {
    gSystem->Sleep(100);
    gSystem->ProcessEvents();
  }
  
  return(mainWin.status);
}

void read_events() {
  int i;
  for (i = 0; i < MAX_EVENTS; i++)
    Events[i] = -1; // Initialize the values

  // Now let's read the events to be displayed
  FILE *fp = fopen("event_list.txt","r");
  if (fp == NULL)
    return;

  ALL_EVENTS = 0;
  i=0;
  // Read events
  while ( (fscanf(fp,"%d",&Events[i++]) != EOF) && (i<MAX_EVENTS) ); 

  fclose (fp); 
  return ;
}

int event_flag( int evno ) {
  if (ALL_EVENTS==1) return 1;

  for (int i=0;i<MAX_EVENTS;i++) 
    if (evno == Events[i]) return 1;
  return 0;
}

void plot(int n_ch) {
  int ch; 
  
  if (plot_flag == SELECT) {
    cm->Clear();
    cm->cd(0);
    ch = plot_ch;
    //wave[ch]->SetPeakPlot(1); // Put some additional info on the plot
    if (restrict_range) 
      wave[ch]->PlotWaveform(0, x_scr_lo[ch], x_scr_hi[ch], 
                             y_scr_lo[ch], y_scr_hi[ch]);
    else 
      wave[ch]->PlotWaveform(0, x_sc_lo, x_sc_hi, y_sc_lo, y_sc_hi);
  }
  
  if (plot_flag == ALLCH) {
    cm->Clear();
    int y = (int) sqrt(n_ch);
    if (sqrt(n_ch) > (float) y)
      y++; 
    int x = ceil((float) n_ch / (float) y);
    // cm->Divide(x, y, 1.0e-5, 1.0e-5);  
    cm->Divide(9, 5, 1.0e-5, 1.0e-5);  
    for (ch = 0; ch < n_ch; ch++) {
      cm->cd(ch+1);
      //wave[ch]->SetPeakPlot(1); // Put some additional info on the plot
      if (restrict_range) 
        wave[ch]->PlotWaveform(0, x_scr_lo[ch], x_scr_hi[ch], 
                               y_scr_lo[ch], y_scr_hi[ch]);
      else 
        wave[ch]->PlotWaveform(0, x_sc_lo, x_sc_hi, y_sc_lo, y_sc_hi);
    }
  }
  cm->Update();
}  

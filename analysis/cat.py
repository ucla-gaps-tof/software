import configparser
import sys
from pathlib import Path
import sqlite3
from datetime import datetime, timezone, timedelta
import os

config0 = configparser.ConfigParser()
config0.read('/home/gaps/software/config.ini')
savedir = config0['net']['save_dir']
dbdir = config0['net']['db_dir']

filename = dbdir + '/run_config.ini'
dbfile = dbdir + '/GFPRuns.db'

if (sys.argv[1] == "runnum"):

    runnum = sys.argv[2]

    con = sqlite3.connect(dbfile)
    cur = con.cursor()

    command = "SELECT * FROM runs WHERE runnum=?"
    cur.execute(command, (runnum,))

    rows = cur.fetchone()

    directory = rows[14]

else:

    directory = sys.argv[1]

filenames = os.listdir(directory)
filenames.sort()

for rb in range(1,6,1):
    
    bash_command = "cat "
    #print(rb)

    for f in filenames:

        if f[-5] == str(rb):

            bash_command = bash_command + directory + "/" + f + " "

    bash_command = bash_command + "> " + directory + "/run" + runnum + "rb" + str(rb) + ".dat"
    print(bash_command)
    os.system(bash_command)

/* ROOT stuff for plotting traces */
#include <TH1F.h>
#include <TH2F.h>

#ifndef STRUCTURES
#define STRUCTURES

#define MAX_CHAN   54

struct Histograms {
  TH1D *traceAvg[MAX_CHAN];
  TH1D *traceAvg_noped[MAX_CHAN]; // no pedestal subtraction
  TH1D *pedHist[MAX_CHAN];
  TH1D *pedRMSHist[MAX_CHAN];
  TH1D *Peak[MAX_CHAN];
  TH1D *Charge[MAX_CHAN];
  TH1D *Charge_cut[MAX_CHAN];
  TH1D *Charge_cut_pe[MAX_CHAN];
  TH2D *QEnd2End[MAX_CHAN/2];
  TH2D *QPad2Pad[MAX_CHAN/4];
  TH2D *QPad2PadW[MAX_CHAN/4];
  TH2D *PPad2Pad[MAX_CHAN/4];
  TH2D *PPad2PadW[MAX_CHAN/4];
  TH1D *tdcCTHR[MAX_CHAN];
  TH1D *tdcCFDS[MAX_CHAN];
  TH1D *tdcCFDE[MAX_CHAN];
  // Pulse-related histograms
  TH1D *tdcPCTH[MAX_CHAN];
  TH1D *tdcPCFD[MAX_CHAN];
  TH1D *PulseAmplitude[MAX_CHAN];
  TH1D *PulsePower[MAX_CHAN];
  TH1D *PulseOffset[MAX_CHAN];
  TH1D *PulseScale[MAX_CHAN];
  TH1D *PulseChi2[MAX_CHAN];
  TH1D *PulseChi2NDF[MAX_CHAN];
  TH1D *NDF[MAX_CHAN];
};

struct TimingHists {
  int flag;
  TH2D *TimeVsQ[MAX_CHAN];
  TH2D *TimeDiffVsQ[MAX_CHAN];
  TH1D *TimingHist_AB;
  TH1D *TimingHist_12;
  TH2D *TimeEnd2End[MAX_CHAN/2];
  TH1D *TimingDist[MAX_CHAN/2];
  TH1D *PadTDiff[MAX_CHAN/2];
  TH2D *PadTDiffVsQ[MAX_CHAN/2];
  TH1D *TimeAvEnd[MAX_CHAN/2];
  TH1D *TimeAvPad[MAX_CHAN/2];
};

struct Limits {
  int    n_chan;
  float  ped_lo;
  float  ped_win;
  float  pulse_lo[MAX_CHAN];
  float  pulse_win[MAX_CHAN];
  // Integration Window
  float  charge_lo[MAX_CHAN];
  float  charge_win[MAX_CHAN];
  // Plotting window
  float  lo_ch;
  float  hi_ch;
  // Quantities for timing measurements
  double thresh;
  double cfd_thresh;
  double cfd_fraction;
  double cfde_fraction;
  int cfde_offset;
};

#endif

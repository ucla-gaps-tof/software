/* drsdaq -- record DRS4 evaluation board data
 Copyright (C) ????-2019 University of California

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Original authors: Jeffrey Zweerink, ???
 Current maintainers: Jeffrey Zweerink, James Ryan, Sean Quinn
 E-mail: zweerink@astro.ucla.edu, jryan@astro.ucla.edu, spq@ucla.edu

 Usage examples: see README */
//------------------------------------------------------------------------------
// System Headers
//------------------------------------------------------------------------------
// C++
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
// C
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
//------------------------------------------------------------------------------
// Local Headers
//------------------------------------------------------------------------------
#include "strlcpy.h"
#include "DRS.h"
#include "rapidxml.hpp"
#include "progressbar.h"
#define O_BINARY 0
#define DIR_SEPARATOR '/'

using namespace rapidxml;
using namespace std;

typedef struct
{
  uint16_t Year;
  uint16_t Month;
  uint16_t Day;
  uint16_t Hour;
  uint16_t Minute;
  uint16_t Second;
  uint16_t Milliseconds;
} TIMESTAMP;

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------

#define ABS(N) ((N<0)?(-N):(N))
void GetTimeStamp(TIMESTAMP &ts);
void ParseOptions(void);
int SaveWaveforms(int fd, int brd);
int init(int i);

//------------------------------------------------------------------------------
// Vars
//------------------------------------------------------------------------------

unsigned char buffer[100000];

#define BRDS 2
DRS *drs;
DRSBoard *board[BRDS];
TIMESTAMP evTimestamp;
DRSCallback *dcb;

int     evSerial = 0;         //event number
float   time_array[8][1024];  //timebinarray
float   waveform[8][1024];    //8x1024 array (8 channels x 1024 samples)
int     numEvents = 1000;     //number of events to record
int     waveDepth = 1024;     //waveform depth of the evaluation board
double  center = 0.0;         //zero point: (Vmax-Vmin)/2
double  triglevel = -0.02;    //trigger level (in VOLTS)
bool    chnOn[4] = {};        //channel-On bitmask
int     trigsource = 0;       //choose a trigger source
int     trig;                 //mask for trigger source
float   trigdelay = 0;        //trigger delay
double  freq = 5;             //sampling frequency

double  calib_range[BRDS];    // voltage range at last calibration
double  calib_freq[BRDS];     // Calibrated sample frequency 
int     calib_flag = 0;       // 0 => take data, 1=> calibrate

// Necessary board information
int master_brd = 0;
int master_sn  = 0;

/*------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
  //Validate calibration has been performed
  /*int calresponse;
  printf ( "This code does not calibrate the DRS4.\n" );
  printf ( "Use drsosc to perform amplitude and timing calibration.\n" );
  printf ( "Has calibration been completed?\n" );
  printf ( "Enter 1 if YES:  ");
  if ( scanf( "%d", &calresponse) != 1 )
  {
    printf ( "Board must be calibrated to ensure proper performance.\n\n" );
    return 1;
  }
  printf ( "Calibration check: [OK]\n\n\n" );*/

  //parse XML config file for run options
  ParseOptions();

  //Take in filename from argv
  if (argc != 2)
  {
    printf("Need to Specify Output Filename\n");
    return 0;
  }
  const char* filename;
  filename = argv[1];
  if (strncmp(filename,"CALIB",5) == 0 )
    calib_flag = 1;
  
  if ( calib_flag == 0) { 
    // Save run config to disk for future reference
    string evtidstr = filename;  // start with filename: xxx.dat
    evtidstr.erase(evtidstr.end() - 4, evtidstr.end());  //remove .dat
    ofstream centerfile; // open snapshot file for center value
    string cfilestr = evtidstr + ".center"; //create file name string
    centerfile.open(cfilestr.c_str()); // saved to xxx.center
    centerfile << center;
    centerfile.close();
  }
  
  int i, nBoards;

  // do initial scan for boards
  drs = new DRS();

  // exit if no board found
  nBoards = drs->GetNumberOfBoards();
  if (nBoards == 0)
  {
    printf("No DRS4 evaluation board found\n");
    return 0;
  }

  int current_sn = 0;
  // show any found board(s). Not sure how it detects boards, but we
  // need to keep track of the highest serial number since that is the
  // master trigger board.
  for (i = 0; i < nBoards; i++)
  {
    board[i] = drs->GetBoard(i);
    current_sn = board[i]->GetBoardSerialNumber();
    printf("Found DRS4 eval board,   serial #%d,   firmware rev %d\n",
           current_sn, board[i]->GetFirmwareVersion());
    if (current_sn > master_sn)
    {
      master_brd = i;
      master_sn  = current_sn;
    }
    //initialize boards
    init(i);
  }
  // All the calibration is done in the init routine as of 20191219. JAZ
  if (calib_flag) exit(1); 
  
  //print options for current run
  printf("Running with options: \n");
  printf("Trigger Level:        %4.3f\n", triglevel);
  printf("Trigger Source:       %d\n", trigsource);
  printf("Trigger Delay:        %4.3f\n", trigdelay);
  printf("Zero-point:           %4.3f\n", center);
  printf("Number of events:     %d\n", numEvents);
  printf("Sampling frequency:   %4.3f GHz\n", freq);
  printf("Channel 0 on/off:     %d\n", chnOn[0]);
  printf("Channel 1 on/off :    %d\n", chnOn[1]);
  printf("Channel 2 on/off :    %d\n", chnOn[2]);
  printf("Channel 3 on/off :    %d\n", chnOn[3]);

#define CH1OR  0x0001
#define CH2OR  0x0002
#define CH3OR  0x0004
#define CH4OR  0x0008
#define EXTOR  0x0010
#define CH1AND 0x0100
#define CH2AND 0x0200
#define CH3AND 0x0400
#define CH4AND 0x0800
#define EXTAND 0x1000

  /* NOTE:
   * SetTriggerSource accepts an INT, determined by 12-bit bitmask
   *
   * 1<<0 = CHN1 OR (in C++11 you can write 0b0001
   * 1<<1 = CHN2 OR                         0b0010
   * 1<<2 = CHN3 OR  oooh la la so fancy
   * 1<<3 = CHN4 OR
   * 1<<4 =  EXT OR
   *
   * 1<<(0+8) = CHN1 AND
   * 1<<(1+8) = CHN2 AND
   * 1<<(2+8) = CHN3 AND
   * 1<<(3+8) = CHN4 AND
   * 1<<(4+8) = EXT  AND
   *
   * OR and AND triggering is achieved by taking the bitwise OR of
   * whatever channels are desired...
   *
   * e.g.:
   * To trig on CHN1 or CHN2
   *      SetTriggerSource(1<<0 | 1<<1)
   *      SetTriggerSource(CHN1OR | CHN2OR)
   *
   * To Trig on CHN1 and CHN2
   *      SetTriggerSource( 1<<(0+8) | 1<<(1+8) )
   *      SetTriggerSource(CHN1AND | CHN2AND )
   *
   * 6 = 0110 = 0100 | 0010        (Trig on CHN2 AND 3)
   * 7 = 0111 = 0100 | 0010 | 0001 (Trig on CHN1 and 2 and 3)
   *
   */

  //trigger options
  for (int i = 0; i < nBoards; i++)
  {
    // Older board uses different system for trigger source.
    //trig = (board[i]->GetBoardSerialNumber() == 2314) ?
    //  trigsource : 1<<trigsource;

    // I am assuming that we are only working with newer boards (at
    // least for the master trigger).
    if (i == master_brd)
    {
      trig = 1 << trigsource; // Master trigger specified in config file
      printf("Master board is %d; sn = %d, trigsource = %d\n", i,
             master_sn, trigsource);
    }
    else
    {
      // All other triggers must be external.
      //trig = (board[i]->GetBoardSerialNumber() == 2314) ? 4 : 1<<4;
      trig = 1 << 4;
    }

    // Set the frequency before calibrating.
    board[i]->SetFrequency(freq, true);

    // After calibration, configure the trigger.
    board[i]->EnableTrigger(1, 0);             // enable hardware trigger
    board[i]->SetTriggerSource(trig);          // set trigger source
    board[i]->SetTriggerLevel(triglevel);      // trig level
    board[i]->SetTriggerPolarity(false);       // positive edge
    board[i]->SetTriggerDelayNs(trigdelay);    // trigger delay
  }

  // open output file
  int WFfd;
  WFfd = open(filename, O_RDWR | O_CREAT | O_TRUNC | O_BINARY, 0644);

  // Collect Events

  for (int j = 0; j < numEvents; j++)
  {
    // start board (activate domino wave)
    for (int i = 0; i < nBoards; i++)
      board[i]->StartDomino();

    // wait for trigger
    while (board[master_brd]->IsBusy());

    evSerial = j;
    // Save Waveforms
    for (int i = 0; i < nBoards; i++) SaveWaveforms(WFfd, i);

    // Print some status progress
    printf("Event #%d read successfully\n", j);
  }

  //Close Waveform File
  close(WFfd);

  // delete DRS object, close USB connection
  delete drs;
}

void GetTimeStamp(TIMESTAMP &ts)
{
  struct timeval t;
  struct tm *lt;
  time_t now;

  gettimeofday(&t, NULL);
  time(&now);
  lt = localtime(&now);

  ts.Year         = lt->tm_year + 1900;
  ts.Month        = lt->tm_mon + 1;
  ts.Day          = lt->tm_mday;
  ts.Hour         = lt->tm_hour;
  ts.Minute       = lt->tm_min;
  ts.Second       = lt->tm_sec;
  ts.Milliseconds = t.tv_usec / 1000;
}

int SaveWaveforms(int fd, int brd)
{
  //char str[80];
  unsigned char *p;
  uint16_t d;
  float t;

  if (fd == 0)
    return 0;

  // transfer wave
  board[brd]->TransferWaves(0, 8);

  // First, let's get the timestamp from the master board.
  p = buffer;
  int trigger_cell = board[brd]->GetTriggerCell(0);
  //printf("brd %d, trig %i", board[brd]->GetBoardSerialNumber(), trigger_cell);
  uint16_t trigger_cell_short = (uint16_t) trigger_cell;

  GetTimeStamp(evTimestamp);

  // NEED TO CHANGE THIS TO HANDLE FIRST EHDR DIFFERENTLY
  memcpy(p, "EHDR", 4);
  p += 4;
  *(uint32_t *)p = evSerial;
  p += sizeof(int);
  *(uint16_t *)p = evTimestamp.Year;
  p += sizeof(uint16_t);
  *(uint16_t *)p = evTimestamp.Month;
  p += sizeof(uint16_t);
  *(uint16_t *)p = evTimestamp.Day;
  p += sizeof(uint16_t);
  *(uint16_t *)p = evTimestamp.Hour;
  p += sizeof(uint16_t);
  *(uint16_t *)p = evTimestamp.Minute;
  p += sizeof(uint16_t);
  *(uint16_t *)p = evTimestamp.Second;
  p += sizeof(uint16_t);
  *(uint16_t *)p = evTimestamp.Milliseconds;
  p += sizeof(uint16_t);
  *(uint16_t *)p = trigger_cell_short; // reserved bit
  p += sizeof(uint16_t);

  // loop over active channels to get time arrays
  for (int i = 0; i < 4; i++)
  {
    if (chnOn[i])
    {
      board[brd]->GetTime(0, i * 2, board[brd]->GetTriggerCell(0), time_array[i]);

      for (int j = 0; j < waveDepth; j++)
      {
        // save binary time as 32-bit float value
        if (waveDepth == 2048)
        {
          t = (time_array[i][j] + time_array[i][j + 1]) / 2;
          j++;
        }
        else
        {
          t = time_array[i][j];
        }

        *(float *)p = t;
        p += sizeof(float);
      }
    }
  }

  // loop over active channels to get data arrays
  for (int i = 0; i < 4; i++)
  {
    if (chnOn[i])
    {
      board[brd]->GetWave(0, i * 2, waveform[i]);
      sprintf((char *)p, "C%03d", i + 1);
      p += 4;
      
      for (int j = 0; j < waveDepth; j++)
      {
        // save binary data as 16-bit value: 
        // 0 = center - 0.5 [V]
        // 65535 = center + 0.5 [V]
        if (waveDepth == 2048)
        {
          // in cascaded mode, save 1024 values
          // as averages of the 2048 values
          d = (uint16_t)(((waveform[i][j]+waveform[i][j+1]) \
                          / 2000.0 + 0.5 - center) * 65535);
          *(uint16_t *)p = d;
          p += sizeof(uint16_t);
          j++;
        }
        else
        {
          d = (uint16_t)((waveform[i][j] / 1000.0 + 0.5 - center) * 65535);
          *(uint16_t *)p = d;
          p += sizeof(uint16_t);
        }
      } // close for j<waveDepth
    } // close if chnOn[i]
  } // close for i<4

  int size = p - buffer;
  int n = write(fd, buffer, size);
  if (n != size)
    return -1;

  return 1;
}

//------------------------------------------------------------------------------
// Parses XML configuration file
//------------------------------------------------------------------------------
void ParseOptions(void)
{
  // Reads XML config file config.xml
  xml_document<> doc;
  xml_node<> * root_node;
  ifstream ConfigFile("config.xml");
  if (ConfigFile.is_open())
  {
    vector<char> buffer((istreambuf_iterator<char>(ConfigFile)),
                        istreambuf_iterator<char>());
    buffer.push_back('\0');

    // Parse the buffer using the xml file parsing library into doc
    doc.parse<0>(&buffer[0]);

    // Find our root node
    root_node = doc.first_node("DRS4");

    for (xml_node<> * RunConfig = root_node->first_node("RunConfig");
         RunConfig; RunConfig = RunConfig->next_sibling())
    {
      triglevel = atof(RunConfig->first_attribute("triglevel")->value());
      trigsource = atoi(RunConfig->first_attribute("trigsource")->value());
      trigdelay = atof(RunConfig->first_attribute("trigdelay")->value());
      center = atof(RunConfig->first_attribute("center")->value());
      numEvents = atoi(RunConfig->first_attribute("numEvents")->value());
      freq = atof(RunConfig->first_attribute("freq")->value());
      chnOn[0] = atoi(RunConfig->first_attribute("chnOn0")->value());
      chnOn[1] = atoi(RunConfig->first_attribute("chnOn1")->value());
      chnOn[2] = atoi(RunConfig->first_attribute("chnOn2")->value());
      chnOn[3] = atoi(RunConfig->first_attribute("chnOn3")->value());

      if (center < 0.0 || center > 0.5) {
	printf("'center'(%.2f) out of range (0.0:0.5). Exiting.\n\n", center);
	exit(1);
      }
    }
  }
  else
  {
    printf("Unable to open configuration file. Exiting\n");
    exit(1);
  }
}

//------------------------------------------------------------------------------
// Initializes DRS board
//------------------------------------------------------------------------------
int init(int i)
{
  // initialize board
  board[i]->Init();
  // Check if the calibrate range matches the input range.
  calib_range[i] = board[i]->GetCalibratedInputRange();
  calib_freq[i] = board[i]->GetCalibratedFrequency();
  double diff = calib_range[i] - center;
  if (calib_flag == 0 ) {
    printf("'center' value: DRS = %.2f; config.xml = %.2f -- freq = %.4f\n",
	   calib_range[i], center, calib_freq[i] );
    if ( ABS(diff) > 0.005) {
      printf("\n\tAttempting to take data with improper calibration!!\n\n");
      printf("To re-calibrate, disconnect all drs4 inputs and run drsdaq\n");
      printf("calibration mode:\n\n\t./drsdaq CALIBRATE\n\n");
      exit(2);
    }
    printf("\nFrequency calibration taken at %.3fGHz: frequency set to %.1f\n",
	   calib_freq[i], freq);
    printf("\n\tTo calibrate for a different frequency, use 'drsosc'.\n\n");
    fflush(stdout); sleep (0.5); 
  }
  
  // enable transparent mode needed for analog trigger
  board[i]->SetTranspMode(1);
  // set input range to: center-0.5 [V] to center+0.5 [V]
  board[i]->SetInputRange(center);
  // Just some code that injects the clock signal onto each channel. 
  //board[i]->EnableTcal(1);
  //board[i]->SelectClockSource(0); // select sync. clock

  if ( calib_flag == 1 && ABS(diff) ) { 
    /* remember current settings */
    double acalVolt   = board[i]->GetAcalVolt();
    int    acalMode   = board[i]->GetAcalMode();
    int    tcalFreq   = board[i]->GetTcalFreq();
    int    tcalLevel  = board[i]->GetTcalLevel();
    int    tcalSource = board[i]->GetTcalSource();
    int    flag1      = board[i]->GetTriggerEnable(0);
    int    flag2      = board[i]->GetTriggerEnable(1);
    int    trgSource  = board[i]->GetTriggerSource();
    int    trgDelay   = board[i]->GetTriggerDelay();
    double range      = board[i]->GetInputRange();
    int    config     = board[i]->GetReadoutChannelConfig();
    int    casc       = board[i]->GetChannelCascading();
    
    printf("Calibrating voltage: 'center' = %.2f ...", range); fflush(stdout);
    board[i]->CalibrateVolt(dcb);
    printf("Done.\n");
    
    /* restore old values */
    board[i]->EnableAcal(acalMode, acalVolt);
    board[i]->EnableTcal(tcalFreq, tcalLevel);
    board[i]->SelectClockSource(tcalSource);
    board[i]->EnableTrigger(flag1, flag2);
    board[i]->SetTriggerSource(trgSource);
    board[i]->SetTriggerDelayPercent(trgDelay);
    board[i]->SetInputRange(range);
    if (casc == 2)
      board[i]->SetChannelConfig(config, 8, 4);
    else
      board[i]->SetChannelConfig(config, 8, 8);
    
    if (board[i]->GetBoardType() == 5)
      board[i]->SetTranspMode(1); // Evaluation board with build-in trigger
    else
      board[i]->SetTranspMode(1); // VPC Mezzanine board

    //exit(0);
  } // end calibration

  // If you want to add the timing calibration, see the call to
  // "OnButtonCalTime()" in ConfigDialog.cpp.
  
  return 1;
}

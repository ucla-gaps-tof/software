#!/usr/local/opt/python@3.8/bin/python3.8
'''
Authors: Jamie Ryan (jryan@astro.ucla.edu)
plot_blob.py (c) UCLA 2021
Desc: Parse memory dump blob and plot traces
Created:  2021-02-02
Modified: 2021-09-17

Usage: python3 plot_blob.py [-n #] input_file(s)
	input_file: decompressed binary file(s) of the DRS4 readout board RAM buffer
	n: Trace number to start from

	Hitting `enter` proceeds to the next event
	Typing a number then `enter` displays that event number
'''
import sys
import argparse
import struct

import numpy as np
import matplotlib.pyplot as plt
import libscrc

# Parse arguments
parser = argparse.ArgumentParser(description='Plot traces from RAM buffer dump')
parser.add_argument('-n', default='0', help='event # to start at')
parser.add_argument('-c', default='', help='calibration file')
parser.add_argument('file', help='file(s) to parse')
args = parser.parse_args()

# Values fixed in firmware:
head = b'\xaa\xaa'
tail = b'\x55\x55'
# Header format (https://gitlab.com/ucla-gaps-tof/firmware#dataformat)
#   cf. firmware/drs/src/daq.vhd
blob_head_bytes = {
	'head': 2,
	'status': 2,
	'len': 2,
	'roi': 2,
	'dna': 8,
	'fw_hash': 2,
	'id': 2,
	'ch_mask': 2,
	'event_cnt': 4,
	'dtap0': 2,
	'dtap1': 2,
	'timestamp': 6
	#'stop_cell': 2,
	#'crc32': 4,
	#'tail': 2
}
len_index = 4 # offset of 'LEN' from start of packet (in # of bytes)
# Lengths of other, non-header data in blob
tracehead_bytes = 2
channelCRC32_bytes = 4
stopcell_bytes = 2
packetCRC32_bytes = 4
tail_bytes = 2

# unpack 32-bit word in header
def unpack_32(x):
	ba = bytearray(x)
	# Perform word swap
	ba[:]=ba[1],ba[0],ba[3],ba[2]
	return int.from_bytes(bytes(ba), 'big')

# unpack packet
def unpack(pkt):
	index = 0
	header = {}
	for field in blob_head_bytes.keys():
		next_index = index + blob_head_bytes[field]
		if field == 'status':
			status = struct.unpack('<H', pkt[index:next_index])[0]
			status_str = '{:016b}'.format(status) # binary
			sync = status_str[-1]
			drs_busy = status_str[-2]
			zynq_temp = int(status_str[:12], 2) # units TBD
			if sync != '0':
				print('WARNING: sync error')
			if drs_busy != '0':
				print('WARNING: DRS4 was busy (lost trigger)')
		elif field == 'roi': # gives trace length (cells) - 1
			header[field] = struct.unpack('<H', pkt[index:next_index])[0]
		elif field == 'ch_mask':
			mask = struct.unpack('<H', pkt[index:next_index])[0]
			mask_str = '{:016b}'.format(mask)[7:] # binary
			header[field] = mask_str
			# number of channels uses first 9 bits ONLY
			# 10th bit is related to AUTO_9TH_CHANNEL
			nchan = mask_str.count('1')
		elif field == 'event_cnt':
			header[field] = unpack_32(pkt[index:next_index])
		else:
			# not handled yet
			header[field] = pkt[index:next_index]
		index = next_index
	# get stop cell (int)
	stop_index = -tail_bytes - packetCRC32_bytes - stopcell_bytes
	stopcell = struct.unpack('<H', pkt[stop_index:stop_index+stopcell_bytes])[0]
	# cyclic redundancy check for packet
	# JLR TODO: check this is actually what SPQ firmware does
	crc_index = -tail_bytes - packetCRC32_bytes
	packetCRC32 = unpack_32(pkt[crc_index:crc_index+packetCRC32_bytes])
	packetCRC32_calc = libscrc.crc32(pkt[:crc_index])
	if packetCRC32_calc != packetCRC32: # print if mismatch
		print('WARNING: packet CRC32 mismatch')
	# set up trace + cell # arrays
	tracelen = header['roi'] + 1
	tracelen_bytes = 2 * tracelen # convert to bits
	cellnums = np.roll(np.arange(tracelen), -stopcell)
	traces = np.zeros((nchan,tracelen))
	# now read traces from "payload"
	# `adc` extracts ADC val from 2-byte word
	#   each value is 14 bits ADC data, followed by 2 bits "parity"
	adc = lambda x: int(bin(x)[2:].rjust(16,'0')[2:],2)
	for i in range(nchan):
		# get channel ID
		ch = struct.unpack('<H', pkt[index:index+tracehead_bytes])[0]
		index += tracehead_bytes
		# get ADC vals
		traceblob = pkt[index:index+tracelen_bytes]
		traceblob_ints = struct.unpack(f'<{tracelen}H', traceblob)
		trace = list(map(adc, traceblob_ints))
		traces[i] = np.array(trace)
		index += tracelen_bytes
		# cyclic redundancy check for channel
		channelCRC32 = unpack_32(pkt[index:index+channelCRC32_bytes])
		channelCRC32_calc = libscrc.crc32(traceblob)
		if channelCRC32_calc != channelCRC32: # print if mismatch
			print(f'WARNING: channel {i} CRC32 mismatch')
		index += channelCRC32_bytes
	return header, cellnums, traces


# Set up axes for plotting
fig = plt.figure()
axes = []
for i in range(8):
	axes.append(fig.add_subplot(3,3,i+1))

# Calibrate?
calibration = False
if len(args.c) > 0:
	try:
		vcal = np.load(args.c)
		calibration = True
	except:
		calibration = False

# Main loop over files to unpack
file_list = args.file.split(' ')
for fn, input_file in enumerate(file_list):
	# Check for compression based on extension
	if '.7z' in input_file or '.zip' in input_file:
		print('ERROR: Program cannot parse compressed files!')
		sys.exit()

	# set up run variables
	packets = [] # List to hold packets
	eventnum = int(args.n)
	n_unpacked = 0
	n_bad = 0 # packets w head but no tail

	# parse + plot
	with open(input_file, 'rb') as fp:
		print('Opening file {0}/{1}: {2}'.format(fn+1,len(file_list),input_file))
		blob = fp.read() # list of single bytes
		blobindex = 2 # start of potential packet + 2
		print('Entering packet loop')
		while True:
			# allow user to look at specific event
			if eventnum < n_unpacked-1:
				packet = packets[eventnum]
			else:
				if blobindex > len(blob)-2:
					break
				if blob[blobindex-2:blobindex] != head:
					blobindex += 1
					continue
				# found packet head! slice out packet
				s = blobindex - 2
				len_bytes = blob[s+len_index:s+len_index+2]
				pkt_len = struct.unpack('<H', len_bytes)[0]
				pkt_len *= 2 # convert words to bytes
				packet = blob[s:s+pkt_len]
				# check for tail
				if packet[-2:] != tail:
					blobindex += 1
					n_bad += 1
					continue
				# good packet (possibly)! append
				packets.append(packet)
				n_unpacked += 1
				blobindex += pkt_len # move index past good packet
				if eventnum != n_unpacked-1: # here, current packet ev.# = n-1 
					continue
			# correct event-numbered packet! unpack, plot, wait for input
			header,cellnums,traces = unpack(packet)
			cell0 = np.argmin(cellnums)
			trignum = header['event_cnt']
			# TODO: calibrate here
			# plot
			print(f'Event # {eventnum}   (Trigger # {trignum})')
			for ch in range(8):
				axes[ch].lines = [] # remove old trace
				if calibration:
					trace_cal = np.roll(vcal[ch],cell0)
					axes[ch].plot(traces[ch]-trace_cal,color='k')
				else:
					axes[ch].plot(traces[ch],color='k')
				axes[ch].axvline(cell0,color='k',alpha=.3,ls='--')
			axes[1].set_title(f'event # {eventnum}')   
			fig.canvas.draw()
			#fig.canvas.flush_events()

			next_eventnum = input()
			try:
				eventnum = int(next_eventnum)
			except:
				eventnum += 1
			blobindex += 1
		print('Exiting packet loop')
	# moving to next file



#!/usr/bin/env python3
# Licensed under a 3-clause BSD style license - see PYFITS.rst

'''
Authors: Jamie Ryan (jryan@astro.ucla.edu)
plot_blob.py (c) UCLA 2021
Desc: Parse memory dump blob and plot traces
Created:  2021-02-02
Modified: 2022-03-17

Usage: python3 plot_blob.py [-n #] input_file(s)
	input_file: decompressed binary file(s) of the DRS4 readout board RAM buffer
	n: Trace number to start from

	Hitting `enter` proceeds to the next event
	Typing a number then `enter` displays that event number
'''
import argparse
import sys

import tofpy
plt = tofpy.plotting.plt

# Parse arguments
parser = argparse.ArgumentParser(description='Plot traces from RAM buffer dump')
parser.add_argument('-n', default='0', help='event # to start at')
parser.add_argument('-c', default='', help='calibration file')
parser.add_argument('-r', action='store_true', help='print RMS')
parser.add_argument('-s', default='1', help='spike cleaning (0 or 1)')
parser.add_argument('-t', action='store_true', help='use calibrated times')
parser.add_argument('file', help='file(s) to parse')
args = parser.parse_args()

event = 0
calfile = None
try:
	event = int(args.n)
except:
	pass
if len(args.c) > 0:
	calfile = args.c
if args.s == '1':
	spikeCleaning = True
else:
	spikeCleaning = False

plt.ion()

fn = args.file
if '.dat' in fn: # blob
	gbf = tofpy.load(fn,False)
	try:
		tofpy.tracePlotter(gbf,event,calfile,clean=spikeCleaning,prms=args.r,tns=args.t)
	except Exception as inst: # exit nicely while ssh'd into tof-gfp-computer
		print(inst)
		print('Exiting...')
		sys.exit()
elif '.txt' in fn: # cal
	tofpy.plotCal(fn,flush=True)
	wait = input()
	sys.exit()
	
#include "memory.h" //Devmem header
#include "Readout_DRS4.h"
#include "Readout_struct.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <gsl/gsl_rng.h>
#include <zmq.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <time.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <pthread.h>

/*
DRS4 Software
*/

/*
Register
BASE + (Register Offset)
*/
#define drs4_chip             0x00000000 
#define drs4_readout          0x00000010<<2
#define drs4_readout_mask     0x00000011<<2
#define drs4_start            0x00000012<<2
#define drs4_reinit           0x00000013<<2
#define drs4_configure        0x00000014<<2
#define drs4_reset            0x00000015<<2

#define fpga_dna_lsb          0x00000020<<2
#define fpga_dna_msb          0x00000021<2

//DRS4 Mask
#define drs4_dmode            0x00000002 //rw
#define drs4_standby_mode     0x00000004 //rw
#define drs4_transparent_mode 0x00000008 //rw
#define drs4_pll_lock         0x00000010 //r
#define drs4_channel_config   0xFF000000 //rw

#define drs4_dtap_high_cnts   0x00000001 //r
#define drs4_dtap_low_cnts    0x00000002 //r

#define drs4_roi_mode         0x00000001
#define drs4_busy             0x00000002
#define drs4_adc_latency      0x000003f0
#define drs4_sample_count     0x003ff000

#define drs4_mask             0x000001ff

// RAM buffer info
// NOTE: must match definitions in firmware/dma/src/dma_controller.vhd
// TODO: autogenerate this from dma_controller.vhd. Might be tricky since it's another repo
#define ram_buff_start_a      "0x04100000"
#define ram_buff_start_b      "0x08100000"
// TODO: autogenerate ram_buff_size from plnx device tree file
#define ram_buff_size         "0x4000000"
// NOTE: ram_buff_a,b_occupancy has the range [0,65]. If register equals 65, the buffer is full and should be drained
#define ram_buff_a_occupancy  0x80000408 //r
#define ram_buff_b_occupancy  0x8000040C //r
#define dmac_pointer          0x80000410 //r
// NOTE: ram_buff_a,b_occ_rst must be reset by software after buffer drained
#define ram_buff_a_occ_rst    0x80000400 //w
#define ram_buff_b_occ_rst    0x80000404 //w
#define ram_buff_toggle       0x80000414 //w

// Trigger regs
#define ext_trig_reg          0x80000104 //rw
#define soft_trig_reg         0x80000100 //w

// Trigger counter reg
#define trig_count            0x80000154

// DAQ reset reg
#define daq_reset_reg         0x80000058 //w
// DMA reset reg
#define dma_reset_reg         0x8000005C //w


// Network env vars
#define svr_info_var          "TOF_DAQ_CPU_ADDR"

// Software trigger config
#define soft_trig_period      15000 // time in microseconds. Default = 400 Hz


drs4_reg drs4 = {
 .base           = 0x80000000,
 .chip           = 0x00000000,
 .dtap_high_cnts = 0x00000000,
 .dtap_low_cnts  = 0x00000000,
 .readout        = 0x400000,
 .readout_mask   = 0x00000000,
 .fpga_dna       = 0,
 .fpga_date      = 0x00000000,
 .fpga_version   = 0x00000000
};
 
// Defaults to external trigger
drs4_reg drs4_trigger = {
  .base          = ext_trig_reg
};

int RUNEND = 0;

// global sigint tracking var
// This should be monitored in long running blocks
static volatile sig_atomic_t got_sigint = 0;
void readoutTerm(int signo)
{
  got_sigint = 1;
}

// Arg parse exception handling
// https://stackoverflow.com/questions/27741301/convert-string-into-number
// paxdiablo answer
int robust_strtol(FILE *fp, char * str_in)
{
  char * tmpstr;
  int ret_val;

  errno = 0;

  ret_val = strtol(str_in, &tmpstr, 10);

  if (errno == EINVAL)
  {
    printf ("'%s' invalid\n", str_in);
    fprintf (fp, "'%s' invalid\n", str_in);
    exit(0);
  }
  else if (errno == ERANGE)
  {
    printf ("'%s' out of range\n", str_in);
    fprintf (fp, "'%s' out of range\n", str_in);
    exit(0);
  }
  else if (tmpstr == str_in)
  {
    printf ("'%s' is not valid at first character\n", str_in);
    fprintf (fp, "'%s' is not valid at first character\n", str_in);
    exit(0);
  }
  else if (*tmpstr != '\0')
  {
    printf ("'%s' is not valid at subsequent character\n", str_in);
    fprintf (fp, "'%s' is not valid at subsequent character\n", str_in);
    exit(0);
  }

  return ret_val;

}

// Multithread functions for draining buffer halves
uint32_t calc_buff_size()
{
  // mmap stuff
  // Individual variables for offset for A half
  off_t start_offset_a = strtoul(ram_buff_start_a, NULL, 0);
  size_t len = strtoul(ram_buff_size, NULL, 0);

  // Truncate offset to a multiple of the page size, or mmap will fail.
  size_t pagesize = sysconf(_SC_PAGE_SIZE);
  // Get offset for buff a
  off_t page_base_a = (start_offset_a / pagesize) * pagesize;
  off_t page_offset_a = start_offset_a - page_base_a;
  off_t mem_region_size = page_offset_a + len;
  return mem_region_size;
}

 char *hugebuff_a;
 char *hugebuff_b;

struct ptcreate_args
{
  void *zmq_context;
  // NOTE: log file might appear weird if threads and main write at same time
  FILE *log_file;
  char *server_addr;
};

struct ptcleanup_args
{
  void *zmq_skt;
  int memfdint;
};

void cleanup_handler(void *clnup_args)
{
  // Assign input args
  // TODO: Make sure this actually runs.. Try some print statements
  struct ptcleanup_args *cln_args = clnup_args;
  zmq_close(cln_args->zmq_skt);
  close(cln_args->memfdint);
}

void *ram_buffer_a_handler(void *ptargs)
{
  // Grab input args
  struct ptcreate_args *inargs = ptargs;
  struct ptcleanup_args ptcargs;

  // mmap stuff
  // Individual variables for offset for A half
  off_t start_offset_a = strtoul(ram_buff_start_a, NULL, 0);
  size_t len = strtoul(ram_buff_size, NULL, 0);

  // Truncate offset to a multiple of the page size, or mmap will fail.
  size_t pagesize = sysconf(_SC_PAGE_SIZE);
  // Get offset for buff a
  off_t page_base_a = (start_offset_a / pagesize) * pagesize;
  off_t page_offset_a = start_offset_a - page_base_a;
  //off_t mem_region_size = page_offset_a + len;
  uint32_t mem_region_size = strtoul(ram_buff_size, NULL, 0);

  // Enable cancels
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  int memfd = open("/dev/mem", O_RDONLY | O_SYNC);
  unsigned char *mem;
  mem = mmap(NULL, mem_region_size, PROT_READ, MAP_SHARED, memfd, page_base_a);
  //memset(mem, 0, sizeof mem); // Let's clear the buffer before using it. 
  //explicit_bzero(mem, sizeof mem); // Let's clear the buffer before using it. 
  void *requester = zmq_socket(inargs->zmq_context, ZMQ_REQ);
  zmq_connect(requester, inargs->server_addr);
  //This is some code to emulate sending data from two RBs
  /*  char svr_socket[200];
  sprintf(svr_socket, "tcp://10.0.1.10:38835");
  zmq_connect(requester, svr_socket); */

  // This allows me to change between using server.py and server.cpp
  // until I can get the later working for multiple readout boards
#define CSERVER 0
#define SOFTWARE 0
  
#define MSG_LEN 100
  // Array to hold server messages
  char recv_msg[MSG_LEN] = {[0 ... MSG_LEN-2] = ' ', [MSG_LEN-1] = '\0'};
  
  // Thread cleanup
  ptcargs.zmq_skt = requester;
  ptcargs.memfdint = memfd;
  pthread_cleanup_push(cleanup_handler, &ptcargs);

  uint32_t buff_a_occ = 0;

  // dmac ptr checking variables
  uint32_t buff_a_start = strtoul(ram_buff_start_a, NULL, 0);
#if SOFTWARE
  uint32_t buff_a_trip = strtoul("0x05070600", NULL, 0);
#else
  uint32_t buff_a_trip = strtoul("0x08070600", NULL, 0);
#endif
  
  // Main loop of thread: poll occupancy register, when full drain
  // over network then reset occupancy monitoring
  while(got_sigint == 0){
    // Poll occupancy register
    buff_a_occ = read_reg(ram_buff_a_occupancy); 
    if (buff_a_occ >= buff_a_trip || (buff_a_occ>buff_a_start && RUNEND!=0 ) )
    {
      printf("Runend(a) = %d\n",RUNEND);
#if SOFTWARE
      // First, switch ram buffers (only if not using firmware switch)
      if (RUNEND!=1) write_reg(ram_buff_toggle, 1);
#endif
      // Then calculate the blob size based on the current occupancy
      uint32_t blob_size = buff_a_occ - buff_a_start;
      // Now reset occupancy monitor before sending blob to server
      write_reg(ram_buff_a_occ_rst, 1);

#if CSERVER
      char blob_info[20];
      sprintf(blob_info, "%d", blob_size);
      // Before we send the blob, send the size so the server program
      // knows how large a blob to expect
      zmq_send_const(requester, blob_info, 20, 0);
      zmq_recv(requester, recv_msg, MSG_LEN, 0);
      //printf("Blob_recv: %s\n", recv_msg);
#endif
      // Finally, send the buffer (with proper size) to TOF-GFP computer
      memcpy(hugebuff_a, mem, blob_size);
      zmq_send_const(requester, hugebuff_a, blob_size, 0);
      // Next two lines are when sending the full buffer
      //memcpy(hugebuff_a, mem, mem_region_size);
      //zmq_send_const(requester, hugebuff_a, mem_region_size, 0);
      zmq_recv(requester, recv_msg, MSG_LEN, 0);
      fprintf(inargs->log_file, "Net message: %s\n", recv_msg);
      if (RUNEND!=0) RUNEND = 0;
    }
    // Avoid spinning CPU: poll every 500 ms
    usleep(500000);
    pthread_testcancel();
  }
  // Cleanup
  pthread_cleanup_pop(0);
  munmap(mem, page_offset_a + len);
  return NULL;
}

void *ram_buffer_b_handler(void *ptargs)
{
  // Grab input args
  struct ptcreate_args *inargs = ptargs;

  // Enable cancels
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  struct ptcleanup_args ptcargs;  

  // mmap stuff
  // Individual variables for offset for B half
  off_t start_offset_b = strtoul(ram_buff_start_b, NULL, 0);
  size_t len = strtoul(ram_buff_size, NULL, 0);

  // Truncate offset to a multiple of the page size, or mmap will fail.
  size_t pagesize = sysconf(_SC_PAGE_SIZE);
  // Get offset for buff b
  off_t page_base_b = (start_offset_b / pagesize) * pagesize;
  off_t page_offset_b = start_offset_b - page_base_b;
  off_t mem_region_size = page_offset_b + len;

  int memfd = open("/dev/mem", O_RDONLY | O_SYNC);
  unsigned char *mem = mmap(NULL, mem_region_size, PROT_READ, MAP_SHARED, memfd, page_base_b);
  void *requester = zmq_socket(inargs->zmq_context, ZMQ_REQ);
  zmq_connect(requester, inargs->server_addr);
  //This is some code to emulate sending data from two RBs
  /*  char svr_socket[200];
  sprintf(svr_socket, "tcp://10.0.1.10:38836");
  zmq_connect(requester, svr_socket); */

  // Array to hold server messages
  // MSG_LEN defined in a_buffer routine above
  char recv_msg[MSG_LEN] = {[0 ... MSG_LEN-2] = ' ', [MSG_LEN-1] = '\0'};
  //char recv_msg[100] = {[0 ... 98] = ' ', [99] = '\0'};

  // Thread cleanup
  ptcargs.zmq_skt = requester;
  ptcargs.memfdint = memfd;
  pthread_cleanup_push(cleanup_handler, &ptcargs);

  uint32_t buff_b_occ = 0;
  uint32_t buff_b_start = strtoul(ram_buff_start_b, NULL, 0);
#if SOFTWARE
  uint32_t buff_b_trip = strtoul("0x09070600", NULL, 0);
#else 
  uint32_t buff_b_trip = strtoul("0x0C070600", NULL, 0);
#endif
  
  // Main loop of thread: poll occupancy register, when full drain over network then reset occupancy monitoring
  while(got_sigint == 0){
    // Poll occupancy register
    buff_b_occ = read_reg(ram_buff_b_occupancy);
    if (buff_b_occ >= buff_b_trip || (buff_b_occ>buff_b_start && RUNEND!=0) )
    {
      printf("Runend(b) = %d\n",RUNEND);
#if SOFTWARE
      // First, switch ram buffers (only if not using firmware switch)
      if (RUNEND!=1) write_reg(ram_buff_toggle, 1);
#endif
      // Then calculate the blob size based on the current occupancy
      uint32_t blob_size = buff_b_occ - buff_b_start;
      // Now reset occupancy monitor before sending blob to server
      write_reg(ram_buff_b_occ_rst, 1);

#if CSERVER
      char blob_info[20];
      sprintf(blob_info, "%d", blob_size);
      // Before we send the blob, send the size so the server program
      // knows how large a blob to expect
      zmq_send_const(requester, blob_info, 20, 0);
      zmq_recv(requester, recv_msg, MSG_LEN, 0);
      //printf("Blob_recv: %s\n", recv_msg);
#endif
      // Finally, send the buffer (with proper size) to TOF-GFP computer
      memcpy(hugebuff_b, mem, blob_size);
      zmq_send_const(requester, hugebuff_b, blob_size, 0);
      // Next two lines are when sending the full buffer
      //memcpy(hugebuff_b, mem, mem_region_size);
      //zmq_send_const(requester, hugebuff_b, mem_region_size, 0);
      zmq_recv(requester, recv_msg, MSG_LEN, 0);
      fprintf(inargs->log_file, "Net message: %s\n", recv_msg);
      if (RUNEND!=0) RUNEND = 0;
    }
    // Avoid spinning CPU: poll every 500 ms
    usleep(500000);
    pthread_testcancel();
  }
  // Cleanup
  pthread_cleanup_pop(0);
  munmap(mem, page_offset_b + len);
  return NULL;
}

// Main
// TODO: add lock file
// TODO: add network timeout, write to disk as backup (must be robust)

int main(int argc, char **argv){

  // malloc the huge buff
  uint32_t huge_buff_size = calc_buff_size();
  hugebuff_a = malloc(huge_buff_size);
  hugebuff_b = malloc(huge_buff_size);

  int pll;

  // Get execution time
  time_t start_t = time(NULL);
  struct tm *tm_start = localtime(&start_t);
  char start_t_str[80];
  assert(strftime(start_t_str, sizeof(start_t_str), "%c", tm_start));

  // Log info: /home/gaps/log/Readout_DRS4_log.txt
  FILE *daq_log = fopen("/home/gaps/logs/GFP_daq_log.txt", "a");
  // Do not buffer writing to log file
  setbuf(daq_log, NULL);

  if (daq_log == NULL)
  {
    printf("[ERROR] (Readout_DRS4.c, main): Could not open file\n");
  }

  fprintf(daq_log, "\n===================================\n");
  fprintf(daq_log, "Begin new Readout_DRS4.c entry\n");
  fprintf(daq_log, "%s\n", start_t_str);

  // Setup sigint 
  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = &readoutTerm;
  if (sigaction(SIGINT, &sa, NULL) == -1) {
      perror("sigaction");
      return EXIT_FAILURE;
  }

  // DAQ defaults
  int num_samples = 3000;
  int duration = 0; // Default is 0 min (=> use events) 
  int roi_mode = 1;
  int transp_mode = 1;
  int run_mode = 0;
  int run_type = 0;        // 0 -> Events, 1 -> Time (default is Events)
  int config_drs_flag = 1; // By default, configure the DRS chip
  // run mode info
  // 0 = free run (must be manually halted), ext. trigger
  // 1 = finite sample run, ext. trigger
  // 2 = finite sample run, software trigger
  // 3 = finite sample run, software trigger, random delays/phase for timing calibration
  int spike_clean = 1;
  int read_ch9 = 1;
  
  // Sanity checking
  int max_samples  = 65000;
  int max_duration = 1440; // Minutes in 1 day

  // arg parse block (borrowed from i2c_ctrl)
  int c;
  opterr = 0;

  while ((c = getopt (argc, argv, "b:n:d:r:t:m:s:9:")) != -1)
  switch (c)
  {
    case 'b':
      config_drs_flag = 0;
      fprintf (daq_log, "DRS4 chip will NOT be configured!\n");
      break;
    case 'n':
      // Input sanitation
      num_samples = robust_strtol(daq_log, optarg);
      if(num_samples > max_samples || num_samples < 0){
        fprintf (stderr, "Invalid num_samples input\n");
        fprintf (daq_log, "Invalid num_samples input\n");
        return 1;
      }
      run_type = 0;
      break;
    case 'd':
      // Input sanitation
      duration = robust_strtol(daq_log, optarg);
      if(duration > max_duration || duration < 0){
        fprintf (stderr, "Invalid duration input\n");
        fprintf (daq_log, "Invalid duration input\n");
        return 1;
      }
      run_type = 1;
      break;
    case 'r':
      roi_mode = robust_strtol(daq_log, optarg);
      if(roi_mode != 0 && roi_mode != 1 ){
        fprintf (stderr, "Invalid roi_mode input\n");
        fprintf (daq_log, "Invalid roi_mode input\n");
        return 1;
      }
      break;
    case 't':
      transp_mode = robust_strtol(daq_log, optarg);
      if(transp_mode != 0 && transp_mode != 1 ){
        fprintf (stderr, "Invalid transp_mode input\n");
        fprintf (daq_log, "Invalid transp_mode input\n");
        return 1;
      }
      break;
    case 'm':
      run_mode = robust_strtol(daq_log, optarg);
      if(run_mode != 0 && run_mode !=1 && run_mode != 2 && run_mode != 3 ){
        fprintf (stderr, "Invalid run_mode input\n");
        fprintf (daq_log, "Invalid run_mode input\n");
        return 1;
      }
      break;
    case 's':
      spike_clean = robust_strtol(daq_log, optarg);
      if(spike_clean != 0 && spike_clean != 1 ){
        fprintf (stderr, "Invalid spike_clean input\n");
        fprintf (daq_log, "Invalid spike_clean input\n");
        return 1;
      }
      break;
    case '9':
      read_ch9 = robust_strtol(daq_log, optarg);
      fprintf(stderr, "Channel 9 enabled\n");
      fprintf(daq_log, "Channel 9 enabled\n");
      break;
    case '?':
      if (optopt == 'n' || optopt == 'd' || optopt == 'r' ||
	  optopt == 't' || optopt == 'm' || optopt == 's')
        fprintf (stderr, "Option -%c requires an argument.\n", optopt);
      else if (isprint (optopt))
        fprintf (stderr, "Unknown option `-%c'.\n", optopt);
      else
        fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
      return 1;
    default:
      abort ();
  }
    
  // Reset DAQ and DMA firmware modules
  write_reg(daq_reset_reg, 1);
  // Add artifical delays, no clue if this helps
  usleep(50);
  fprintf (daq_log, "[INFO] (Readout_DRS4, main): Reset DAQ firmware\n");
  printf("[INFO] (Readout_DRS4, main): Reset DAQ firmware\n");
  write_reg(dma_reset_reg, 1);
  usleep(50);
  write_reg(dma_reset_reg, 1);
  usleep(50);
  write_reg(ram_buff_a_occ_rst, 1);
  usleep(50);
  write_reg(ram_buff_b_occ_rst, 1);
  
  
  
  fprintf (daq_log, "[INFO] (Readout_DRS4, main): Reset DMA firmware\n");

  printf("[INFO] (Readout_DRS4, main): Reset DMA firmware\n");

  // Configure user input args
  usleep(50);
  set_drs4_spike(&drs4, spike_clean);
  fprintf (daq_log, "Spike clean mode: %d\n", spike_clean);
  printf("[INFO] (Readout_DRS4, main): Spike clean mode: %d\n", spike_clean);
  usleep(50);
  set_drs4_trigger(&drs4_trigger, run_mode);
  fprintf (daq_log, "Run mode: %d\n", run_mode);
  printf("[INFO] (Readout_DRS4, main): Run mode: %d\n", run_mode);

  // Configure DRS chip
  if (config_drs_flag == 1)
    configure_drs(roi_mode, transp_mode, read_ch9);

  /* DRS4 PLL Status */

  pll = get_drs4_pll_lock(&drs4);

  /*Check PLL Lock */
  printf("PLL Status 0x%x\n\r", pll);

  // TODO: update this to work with latest firmware
  /* Monitor DTAP for DRS Stability. */
  //get_drs4_dtap_low_cnts(&drs4);
  //get_drs4_dtap_high_cnts(&drs4);

  printf("[INFO] (Readout_DRS4, main): Starting DRS4\n");

  // Now that we are getting ready to start taking data, make sure
  // that the RUNEND flag is properly set.
  RUNEND = 0;
  
  set_drs4_start(&drs4, 0x01);
  usleep(500);
  printf("[INFO] (Readout_DRS4, main): DRS4 succesfully started.\n");

  usleep(500);
  printf("[INFO] (Readout_DRS4, main): Starting DAQ.\n");
  // Choose function for user selected mode
  if (run_mode == 0)
  {
    drs4_freerun_ext(&drs4, daq_log);
  }
  else if (run_mode == 1)
  {
    if (run_type == 1)
      drs4_timerun_ext(&drs4, daq_log, duration);
    else
      drs4_eventrun_ext(&drs4, daq_log, num_samples);
  }
  else if (run_mode == 2)
  {
    drs4_finiterun_soft(&drs4, daq_log, num_samples);
  }
  else if (run_mode == 3)
  {
    drs4_finiterun_soft_tcal(&drs4, daq_log, num_samples);
  }

  if(got_sigint == 1)
    printf("\n[INFO] (Readout_DRS4, main): SIGINT.\n");
  printf("[INFO] (Readout_DRS4, main): Returning DRS to IDLE.\n");
  // Set DRS4 idle
  set_drs4_reinit(&drs4, 0x01);
  printf("[INFO] (Readout_DRS4, main): DRS IDLE.\n");
  free(hugebuff_a);
  free(hugebuff_b);

  return 0;

}

void configure_drs(int roi_m, int transp_m, int ch9)
{
  printf("[Readout_DRS4 (configure_drs)]: Configuring DRS4\n");
  // TODO: add logging
  set_drs4_reset(&drs4,0x01);
  usleep(50);
  // set continuous mode, no channel cascading
  set_drs4_dmode(&drs4,1); 
  
  usleep(50);
  set_drs4_channel_config(&drs4,0xff); 

  usleep(50);
  // clock latency
  // TODO: make programmable
  set_drs4_adc_latency(&drs4,9);
  
  usleep(50);
  // Set sample count to 1024
  set_drs4_sample_count(&drs4,1023);
  
  usleep(50);
  // set transparent mode, ROI, ch9
  set_drs4_transparent_mode(&drs4, transp_m);
  set_drs4_roi(&drs4, roi_m);
  usleep(50);
  
  // Write mask used to set channels mux'd out
  // 8-bits where CH0 is LSB
  // CH8 CH7 CH6 CH5 CH4 CH3 CH2 CH1 CH0
  // Example: to enable channel 1 and 3 use the pattern below
  //  0   0   0   0   0   1   0   1   0
  // hex value: 0x0A
  if (ch9)
    set_readout_mask(&drs4,0x1ff);
  else
    set_readout_mask(&drs4,0xff);
  
  sleep(2);
  // Set DRS4 normal mode
  set_drs4_configure(&drs4, 1);
  printf("[Readout_DRS4 (configure_drs)]: DRS4 successfully configured.\n");
}

/*
Dtrigger register
*/

void set_start_dtrigger(drs4_reg *data, int enable){
	write_reg(data->base,enable);
}

uint32_t get_start_dtrigger(drs4_reg *data, int enable){
	return read_reg(data->base);
}

// Readout mode functions


// 0 = free run (must be manually halted), ext. trigger
void drs4_freerun_ext(drs4_reg *data, FILE *fp){

  // ZMQ resource setup
  // Make sure server info env var is set
  char* svr_addr = getenv(svr_info_var);
  if(svr_addr){
    printf("[INFO] (Readout_DRS4, drs4_freerun_ext): Env var found: %s\n", svr_addr );
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_freerun_ext): Env var found: %s\n", svr_addr );
  }
  else{
    printf("[INFO] (Readout_DRS4, drs4_freerun_ext): Server address env var not set\n");
    printf("[INFO] (Readout_DRS4, drs4_freerun_ext): Exiting..\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_freerun_ext): Server address env var not set\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_freerun_ext): Exiting..\n");       
    exit(0);
  }
  void *context = zmq_ctx_new();

  // Create struct with args to pass to threads
  struct ptcreate_args thread_args;
  thread_args.log_file = fp;
  thread_args.server_addr = svr_addr;
  thread_args.zmq_context = context;

  // Two threads
  pthread_t a_thread, b_thread;
  int a_thread_ret, b_thread_ret;

  // Main DAQ loop
  // While true loop, break with sigint
  a_thread_ret = pthread_create(&a_thread, NULL, &ram_buffer_a_handler, (void *) &thread_args);
  usleep(100);
  b_thread_ret = pthread_create(&b_thread, NULL, &ram_buffer_b_handler, (void *) &thread_args);

  while (got_sigint == 0)
  {
    usleep(500000);
  }

  // Reach here after DAQ terminated or SIGINT
  // Finished collected events, shutdown buffer handler threads
  a_thread_ret = pthread_cancel(a_thread);
  b_thread_ret = pthread_cancel(b_thread);

  // Cleanup
  pthread_join(a_thread, NULL);
  pthread_join(b_thread, NULL);

  printf("[INFO] (Readout_DRS4, drs4_freerun_ext): a_thread returns:%d\n", a_thread_ret);
  printf("[INFO] (Readout_DRS4, drs4_freerun_ext): b_thread returns:%d\n", b_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_freerun_ext): a_thread returns:%d\n", a_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_freerun_ext): b_thread returns:%d\n", b_thread_ret);

  printf("[INFO] (Readout_DRS4, drs4_freerun_ext): Terminating\n");
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_freerun_ext): Terminating\n");

  zmq_ctx_destroy(context);

}

// 1 = finite event run, ext. trigger
void drs4_eventrun_ext(drs4_reg *data, FILE *fp, int num_samp){

  // Setup ZMQ resources
  // Make sure server info env var is set
  char* svr_addr = getenv(svr_info_var);
  if(svr_addr){
    printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): Env var found: %s\n", svr_addr );
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_eventrun_ext): Env var found: %s\n", svr_addr );
  }
  else{
    printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): Server address env var not set\n");
    printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): Exiting..\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_eventrun_ext): Server address env var not set\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_eventrun_ext): Exiting..\n");       
    exit(0);
  }
  void *context = zmq_ctx_new();

  // Create struct with args to pass to threads
  struct ptcreate_args thread_args;
  thread_args.log_file = fp;
  thread_args.server_addr = svr_addr;
  thread_args.zmq_context = context;

  // Two threads
  pthread_t a_thread, b_thread;
  int a_thread_ret, b_thread_ret;

  // Start buffer handler threads
  a_thread_ret = pthread_create(&a_thread, NULL, &ram_buffer_a_handler, (void *) &thread_args);
  usleep(100);
  printf("DEBUG: returned from a_thread\n");
  b_thread_ret = pthread_create(&b_thread, NULL, &ram_buffer_b_handler, (void *) &thread_args);
  printf("DEBUG: returned from b_thread\n");

  // Get current event number at start of run
  long int curr_evt = read_reg(trig_count);
  long int start_evt = curr_evt;
  long int evt_cnt = 0;
  //long int prev_evt_cnt = 0;
  long int evt_print_state = 0;
  // Max value that can be stored by event count register
  // long int trig_count_len = 4294967295;
  printf("DEBUG: evt_cnt %ld\n", evt_cnt);
  printf("DEBUG: num_samp %d\n", num_samp);
  // Main DAQ loop

  //uint32_t buff_a_occ = 0;
  //uint32_t buff_b_occ = 0;
  //uint32_t buff_ptr1 = 0;
  
  while((evt_cnt < num_samp) && (got_sigint == 0)){
    curr_evt = read_reg(trig_count);
    //buff_a_occ = read_reg(ram_buff_a_occupancy);
    //buff_b_occ = read_reg(ram_buff_b_occupancy);
    //buff_ptr1 = read_reg(dmac_pointer);
    evt_cnt = curr_evt - start_evt;
    printf("%ld..", evt_cnt); fflush(stdout);
    //printf("Evt = %ld - Occ = %d : %d - Ptr = %d\n", evt_cnt, buff_a_occ,
    //	   buff_b_occ, buff_ptr1); 

    // if evt_cnt ever negative, trig_count has wrapped. Must reset start_evt
    if (evt_cnt < 0)
    {
       start_evt = curr_evt;
       evt_print_state = curr_evt;
    }
    if (evt_cnt >= num_samp) RUNEND = 1;

    if (evt_cnt > evt_print_state)
    {
      evt_print_state = evt_cnt;
      //printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): Current event number %ld\n", evt_cnt);
      //fprintf(fp,"[INFO] (Readout_DRS4, drs4_eventrun_ext): Current event number%ld\n", evt_cnt);
    }
    
    // Avoid spinning CPU while waiting for events
    //usleep(100000);
    sleep(3);
    //printf("Finish loop iteration\n");
  }
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_eventrun_ext): Current event number%ld\n", evt_cnt);

  // Finished collected events, shutdown buffer handler threads
  a_thread_ret = pthread_cancel(a_thread);
  b_thread_ret = pthread_cancel(b_thread);

  // Cleanup
  pthread_join(a_thread, NULL);
  pthread_join(b_thread, NULL);

  printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): a_thread returns:%d\n", a_thread_ret);
  printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): b_thread returns:%d\n", b_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_eventrun_ext): a_thread returns:%d\n", a_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_eventrun_ext): b_thread returns:%d\n", b_thread_ret);

  printf("[INFO] (Readout_DRS4, drs4_eventrun_ext): Terminating\n");
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_eventrun_ext): Terminating\n");

  zmq_ctx_destroy(context);

}

// 1 = finite time run, ext. trigger
void drs4_timerun_ext(drs4_reg *data, FILE *fp, int duration){

  // Setup ZMQ resources
  // Make sure server info env var is set
  char* svr_addr = getenv(svr_info_var);
  if(svr_addr){
    printf("[INFO] (Readout_DRS4, drs4_timerun_ext): Env var found: %s\n", svr_addr );
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_timerun_ext): Env var found: %s\n", svr_addr );
  }
  else{
    printf("[INFO] (Readout_DRS4, drs4_timerun_ext): Server address env var not set\n");
    printf("[INFO] (Readout_DRS4, drs4_timerun_ext): Exiting..\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_timerun_ext): Server address env var not set\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_timerun_ext): Exiting..\n");       
    exit(0);
  }
  void *context = zmq_ctx_new();

  // Create struct with args to pass to threads
  struct ptcreate_args thread_args;
  thread_args.log_file = fp;
  thread_args.server_addr = svr_addr;
  thread_args.zmq_context = context;

  time_t rawtime;
  struct tm * timeinfo;

  /* get current timeinfo and modify it to the user's choice */
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  printf("%d %d %d %d %d %d %ld\n", timeinfo->tm_year, timeinfo->tm_mon, 
	 timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min,
	 timeinfo->tm_sec, rawtime);
  printf("Waiting %d seconds to start at next minute\n", 60-timeinfo->tm_sec);
  sleep (60-timeinfo->tm_sec);
  
  // Two threads
  pthread_t a_thread, b_thread;
  int a_thread_ret, b_thread_ret;

  // Start buffer handler threads
  a_thread_ret = pthread_create(&a_thread, NULL, &ram_buffer_a_handler, (void *) &thread_args);
  usleep(100);
  printf("DEBUG: returned from a_thread\n");
  b_thread_ret = pthread_create(&b_thread, NULL, &ram_buffer_b_handler, (void *) &thread_args);
  printf("DEBUG: returned from b_thread\n");

  // Get current event number at start of run
  long int curr_evt = read_reg(trig_count);
  long int start_evt = curr_evt;
  long int prt_time = 0;
  long int prev_prt_time = 0;
  // Max value that can be stored by event count register
  // long int trig_count_len = 4294967295;
  //printf("DEBUG: evt_cnt %ld\n", evt_cnt);
  printf("DEBUG: duration %d\n", duration);
  // Main DAQ loop

  time ( &rawtime );
  time_t start_time = rawtime;
  time_t curr_time = rawtime - start_time;
  printf("Current run time: %ld\n", curr_time);

  int sleep_time = 3; // Amount of time (sec) between duration checks
  int print_time = 10; // Amount of time (sec) between print statements
  
  while((curr_time < duration*60) && (got_sigint == 0)){
    curr_evt = read_reg(trig_count) - start_evt;
    time (&rawtime);
    curr_time = rawtime - start_time;
    
    prt_time = curr_time - prev_prt_time;
    // if evt_cnt ever negative, trig_count has wrapped. Must reset start_evt
    //if (evt_print < 0) start_evt = curr_evt;

    if (curr_time >= duration*60) RUNEND = 1;
    // Progress print statements
    //printf("DEBUG: curr_evt %ld,  evt_cnt %ld, evt_print_state %ld\n", curr_evt,  evt_print, evt_print_state);

    if (prt_time > print_time)
    {
      prev_prt_time = curr_time;
      printf("[INFO] (Readout_DRS4, drs4_timerun_ext): Current event %ld and time(sec) %ld\n", curr_evt, curr_time);
    }
    
    // Avoid spinning CPU while waiting for events
    //usleep(300000);
    sleep(sleep_time);
  }
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_timerun_ext): Current event number%ld\n", curr_evt);

  // Finished collected events, shutdown buffer handler threads
  a_thread_ret = pthread_cancel(a_thread);
  b_thread_ret = pthread_cancel(b_thread);

  // Cleanup
  pthread_join(a_thread, NULL);
  pthread_join(b_thread, NULL);

  printf("[INFO] (Readout_DRS4, drs4_timerun_ext): a_thread returns:%d\n", a_thread_ret);
  printf("[INFO] (Readout_DRS4, drs4_timerun_ext): b_thread returns:%d\n", b_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_timerun_ext): a_thread returns:%d\n", a_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_timerun_ext): b_thread returns:%d\n", b_thread_ret);

  printf("[INFO] (Readout_DRS4, drs4_timerun_ext): Terminating\n");
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_timerun_ext): Terminating\n");

  zmq_ctx_destroy(context);

}


// 2 = finite run, software trigger
void drs4_finiterun_soft(drs4_reg *data, FILE *fp, int num_samp){

  // Setup ZMQ resources
  // Make sure server info env var is set
  char* svr_addr = getenv(svr_info_var);
  if(svr_addr){
    printf("[INFO] (Readout_DRS4, drs4_finiterun_soft): Env var found: %s\n", svr_addr );
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft): Env var found: %s\n", svr_addr );
  }
  else{
    printf("[INFO] (Readout_DRS4, drs4_finiterun_soft): Server address env var not set\n");
    printf("[INFO] (Readout_DRS4, drs4_finiterun_soft): Exiting..\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft): Server address env var not set\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft): Exiting..\n");       
    exit(0);
  }
  // ZMQ context *is* thread safe
  void *context = zmq_ctx_new();

  // Create struct with args to pass to threads
  struct ptcreate_args thread_args;
  thread_args.log_file = fp;
  thread_args.server_addr = svr_addr;
  thread_args.zmq_context = context;

  // Two threads
  pthread_t a_thread, b_thread;
  int a_thread_ret, b_thread_ret;

  // Start buffer handler threads
  a_thread_ret = pthread_create(&a_thread, NULL, &ram_buffer_a_handler, (void *) &thread_args);
  usleep(100);
  b_thread_ret = pthread_create(&b_thread, NULL, &ram_buffer_b_handler, (void *) &thread_args);

  // Soft trigger index
  int i = 0;

  // Main DAQ loop
  // Break after num_samp reached or sigint
  while((i < num_samp) && (got_sigint == 0))
  {
    // Assert trigger
    set_start_dtrigger(&drs4_trigger, 0x01);
    // Sleep for specified period to achieve software trigger rate
    usleep(soft_trig_period);
    i++;
  }

  if (i >= num_samp) RUNEND = 1;
  usleep(5000000);

  // Finished collected events, shutdown buffer handler threads
  a_thread_ret = pthread_cancel(a_thread);
  b_thread_ret = pthread_cancel(b_thread);

  // Cleanup
  pthread_join(a_thread, NULL);
  pthread_join(b_thread, NULL);

  printf("[INFO] (Readout_DRS4, drs4_finiterun_soft): a_thread returns:%d\n", a_thread_ret);
  printf("[INFO] (Readout_DRS4, drs4_finiterun_soft): b_thread returns:%d\n", b_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_finiterun_soft): a_thread returns:%d\n", a_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_finiterun_soft): b_thread returns:%d\n", b_thread_ret);

  printf("[INFO] (Readout_DRS4, drs4_finiterun_soft): Terminating\n");
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_finiterun_soft): Terminating\n");

  zmq_ctx_destroy(context);

}

// 3 = finite run, software trigger configured for timing calibration
void drs4_finiterun_soft_tcal(drs4_reg *data, FILE *fp, int num_samp){

  // Some RNG stuff for timing calibration runs
  // Ensures random phase of input

  const gsl_rng_type * T;
  gsl_rng * r;

  int lowlim = 10;
  // Actual upper limit = lowlim + uplim
  int uplim = 30;
  int rngseed = 432;

  gsl_rng_env_setup();

  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  gsl_rng_set(r, rngseed);

  struct timespec ts[num_samp];

  // Pre-allocate array with random nanosecond values for sleep
  // Each entry is a random nanosecond delay: Uniform(10,30)
  long u;

  for(int i = 0; i < num_samp; i++)
  {
    u = gsl_rng_uniform_int (r, uplim);
    u = u + lowlim;
    ts[i].tv_sec = 0;
    // Default rate of ~200 Hz
    ts[i].tv_nsec = 20e6 + u;
  }

  // setup network transfer
  // Make sure server info env var is set
  char* svr_addr = getenv(svr_info_var);
  if(svr_addr){
    printf("[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Env var found: %s\n", svr_addr );
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Env var found: %s\n", svr_addr );
  }
  else{
    printf("[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Server address env var not set\n");
    printf("[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Exiting..\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Server address env var not set\n");
    fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Exiting..\n");       
    exit(0);
  }
  // ZMQ context *is* thread safe
  void *context = zmq_ctx_new();

  // Create struct with args to pass to threads
  struct ptcreate_args thread_args;
  thread_args.log_file = fp;
  thread_args.server_addr = svr_addr;
  thread_args.zmq_context = context;

  // Two threads
  pthread_t a_thread, b_thread;
  int a_thread_ret, b_thread_ret;

  // Start buffer handler threads
  a_thread_ret = pthread_create(&a_thread, NULL, &ram_buffer_a_handler, (void *) &thread_args);
  usleep(100);
  b_thread_ret = pthread_create(&b_thread, NULL, &ram_buffer_b_handler, (void *) &thread_args);

  int i = 0;
  sleep(1);

  // Main DAQ loop
  // NOTE: do not add superfluous code in this block, must be fast
  for(i = 0; i < num_samp; i++){
    set_start_dtrigger(&drs4_trigger, 0x01);
    nanosleep(&ts[i], NULL);
  }
  RUNEND = 1;
  
  fprintf(fp, "[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Run complete, %d events.\n", i);
  usleep(5000000);


  //if (i >= num_samp) RUNEND = 1;
  //usleep(5000000);

  // Finished collected events, shutdown buffer handler threads
  a_thread_ret = pthread_cancel(a_thread);
  b_thread_ret = pthread_cancel(b_thread);

  // Cleanup
  pthread_join(a_thread, NULL);
  pthread_join(b_thread, NULL);

  printf("[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): a_thread returns:%d\n", a_thread_ret);
  printf("[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): b_thread returns:%d\n", b_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): a_thread returns:%d\n", a_thread_ret);
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_finiterun_soft): b_thread returns:%d\n", b_thread_ret);

  printf("[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Terminating\n");
  fprintf(fp,"[INFO] (Readout_DRS4, drs4_finiterun_soft_tcal): Terminating\n");

  zmq_ctx_destroy(context);

}

/*
DRS4 DMODE 
 enable 1  = continuous mode
 enable 0  = single shot
*/
void set_drs4_dmode(drs4_reg *data,int enable){
   data->chip = ((data->chip) & ~drs4_dmode) |  enable<<1;
   write_reg(data->base,data->chip);
}

/*
DRS4_STANDBY_MODE
 enable 1 = shutdown drs4
 enable 0 = run drs4 
*/
void set_standby_mode(drs4_reg *data, int enable){
  data->chip = ((data->chip) & ~drs4_standby_mode) | enable<<2;
  write_reg(data->base,data->chip);
}

/*
DRS4_TRANSPARENT
 enable 1 = set transparent
 enable 0 = disable transparent
*/
void set_drs4_transparent_mode(drs4_reg *data, int enable){
  data->chip = ((data->chip) & ~drs4_transparent_mode) |  enable<<3;
  write_reg(data->base,data->chip);
}

/*
DRS4_PLL_LOCK
This function returns if pll is locked for Drs4
*/
uint32_t get_drs4_pll_lock(drs4_reg *data){
 return read_reg(data->base)&drs4_pll_lock;
}

/*
DRS4_CHANNEL_CONFIG
\n # of chn | # of cells per ch | bit pattern
                        \n 8        | 1024              | 11111111b
                        \n 4        | 2048              | 01010101b
                        \n 2        | 4096              | 00010001b
                        \n 1        | 8192              | 00000001b

*/
void set_drs4_channel_config(drs4_reg *data, uint32_t bitPattern){
 data->chip = ((data->chip)&~drs4_channel_config) | (bitPattern<<24);
 write_reg(data->base, data->chip);
}

uint32_t get_drs4_channel_config(drs4_reg *data){
	return read_reg(data->base)&drs4_channel_config;
}


/*
DRS4_DTAP_HIGH_CNTS


*/
uint32_t get_drs4_dtap_high_cnts(drs4_reg *data){
 data->dtap_high_cnts = read_reg(data->base | drs4_dtap_high_cnts);
 return data->dtap_high_cnts;
}

/*
DRS4_DTAP_LOW_CNTS
*/
uint32_t get_drs4_dtap_low_cnts(drs4_reg *data){
 data->dtap_low_cnts = read_reg(data->base | drs4_dtap_low_cnts);
 return data->dtap_low_cnts;
}



/*
DRS4_ROI_MODE

*/
void set_drs4_roi(drs4_reg *data, uint32_t enable){
    data->readout = (data->readout & ~drs4_roi_mode) | enable<<0;
	write_reg((data->base | drs4_readout),data->readout);
}

/*
DRS4_BUSY
DRS is doing a readout.
*/
uint32_t get_drs4_busy(drs4_reg *data){
  return read_reg(data->base | drs4_readout) & drs4_busy;
 }

/*
DRS4_ADC_LATENCY

*/
void set_drs4_adc_latency(drs4_reg *data,uint32_t value){
	data->readout = (data->readout & ~drs4_adc_latency) | (value << 4);
	write_reg((data->base | drs4_readout), data->readout);
}

uint32_t get_drs4_adc_latency(drs4_reg *data){
	return read_reg((data->base) + (drs4_readout)) & drs4_adc_latency;
}

/*
SAMPLE_COUNT

*/
void set_drs4_sample_count(drs4_reg *data,uint32_t value){
	data->readout = (data->readout & ~drs4_sample_count) | (value<<12);
  printf( "0x%x\n", data->readout);
	write_reg((data->base | drs4_readout),data->readout);
}
 
uint32_t get_drs4_sample_count(drs4_reg *data){
	data->readout = read_reg((data->base) + (drs4_readout));
	return read_reg(data->base | drs4_readout) & drs4_sample_count;	
}

/*
READOUT MASK

*/

void set_readout_mask(drs4_reg *data,uint32_t value){
	data->readout_mask = value & drs4_mask;
	write_reg((data->base | drs4_readout_mask), data->readout_mask);
}


uint32_t get_readout_mask(drs4_reg *data){
	data->readout_mask = read_reg((data->base) + (drs4_readout_mask));
	return (data->readout_mask) & drs4_mask;
}

/*
DRS4_START

*/
void set_drs4_start(drs4_reg *data, int value){
	write_reg((data->base | drs4_start), value & 0x01);
}

/*
DRS4_REINIT 
restores state machine to idle

*/
void set_drs4_reinit(drs4_reg *data, int value){
	write_reg((data->base | drs4_reinit), value & 0x01);
}

/*
DRS4_CONFIGURE
 
*/
void set_drs4_configure(drs4_reg *data, int value){
printf("inside config method: base address is 0x%x\n\r", data->base);
	write_reg((data->base | drs4_configure), value & 0x01);
}

/*
DRS4_RESET
*/
void set_drs4_reset(drs4_reg *data,int value){
	write_reg((data->base | drs4_reset), value & 0x01);
}

////////////////////////////////////////////////////////////////////////////////////

/*
FPGA_DNA
*/

uint64_t get_fpga_dna(drs4_reg *data){
	read_reg((data->base) + (fpga_dna_lsb));
	read_reg((data->base) + (fpga_dna_msb));
  return 0;
}

/*
Set spike clean mode
*/
void set_drs4_spike( drs4_reg *data, int value){
  data->readout=value;
}

/*
Set trigger mode

2nd arg specifies type
ext. trigger: value = 0 or value = 1
software trigger: value = 2 or value = 3
*/
void set_drs4_trigger( drs4_reg *data, int value){

  if( value == 0 || value == 1){
    data->base=ext_trig_reg;
  }
  else if ( value == 2 || value == 3)
  {
    data->base=soft_trig_reg;
  }

}

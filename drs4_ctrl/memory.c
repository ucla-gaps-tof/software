/*
Block Ram executable file.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
__LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)

// FIXME: Neither of these functions find the offset as a multiple of page size. mmap could potentially fail...
// FIXME: Have these return an int or something to indicate success or failure
void write_reg(uint32_t address,uint32_t data)
{ 
	void *map_base, *virt_addr;
	int fd;

	if ((fd = open("/dev/mem",  O_RDWR |O_SYNC)) == -1)
		FATAL;

	map_base = mmap(NULL,MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, address & ~MAP_MASK);

	// ???: If there is a failure, will this actually print, or is it visible in stdout?
	if (map_base == MAP_FAILED)
		perror("Can't map memory");

	virt_addr = map_base + (address & MAP_MASK) ;

	//	printf("Virtual address: 0x%x.\n", virt_addr); 
	//        printf("Address: 0x%x.\n", address);

	*((uint32_t *) virt_addr) = data;

	if (munmap(map_base, MAP_SIZE) == -1) {
		close(fd);
		FATAL;
	}

	close(fd);
}

uint32_t read_reg(uint32_t address)
{ 
	void *map_base, *virt_addr;
	uint32_t data = 0;
	int fd;

	if ((fd = open("/dev/mem",  O_RDONLY |O_SYNC)) == -1)
		FATAL;
	map_base = mmap(NULL,MAP_SIZE, PROT_READ , MAP_SHARED, fd, address & ~MAP_MASK);

	// ???: If there is a failure, will this actually print, or is it visible in stdout?
	if (map_base == MAP_FAILED)
		perror("Can't map memory");

	virt_addr = map_base + (address & MAP_MASK) ;

	data = *((unsigned long *)virt_addr);

	if (munmap(map_base, MAP_SIZE) == -1) {
		close(fd);
		FATAL;
	}

	close(fd);

	return data;
}


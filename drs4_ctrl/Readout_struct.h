#ifndef READOUT_STRUCT_H
#define READOUT_STRUCT_H
#include <stdint.h>

typedef struct {
	uint32_t base;
	uint32_t chip;
	uint32_t dtap_high_cnts;
	uint32_t dtap_low_cnts;
	uint32_t readout;
	uint32_t readout_mask;
	//FPGA registers
	uint64_t fpga_dna;
	uint32_t fpga_date;
	uint64_t fpga_version;
} drs4_reg;

#endif

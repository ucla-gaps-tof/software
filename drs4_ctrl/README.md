[[_TOC_]]

# Introduction
This folder contains code for running the DRS4 and transferring data via network to central computer.

# Prerequisites/dependencies
## Hardware
* V2.3+ UCLA DRS4 board
* External 1 MHz LVDS clock connected to J1
* LAN connection to central computer

## Software
* Debian 10+
* libgsl-dev
* libzmq3-dev
* g++
* make

## Building
Compile executable using `make`. Install to `bin/` folder using `make install`. Remove/clean all binaries using `make clean`.

## Environment
This program relies on a network connection to transfer data. The central server address and port must be defined in the `TOF_DAQ_CPU_ADDR` environemnt variable.

To set a persisent system-wide variable, add line below to the `/etc/environment` file.

```
TOF_DAQ_CPU_ADDR="tcp://192.168.1.1:12345"
```

Replace the address and port with the correct values.

To set this in a current shell
```
TOF_DAQ_CPU_ADDR="tcp://192.168.1.1:12345"
export TOF_DAQ_CPU_ADDR
```

# Usage
The main executable `Readout_DRS4` has several argument flags that can be used to configure the DRS4 and the type of run.

Run the program as ***root***. A complete call would look like

```
./Readout_DRS4 -d -n <int> -r <0,1> -t <0,1> -m <0,1,2,3> -s <0,1>
```

Refer to the table below for definitions of the arguments

## Program options
| Parameter | Arg type | Description                          | Notes                      |
| :---      | :---     | :---                                 | :---                       |
| d         | no arg   | Resume with previous DRS4 config     | If flag present, code does not configure DRS4 chip |
| n         | `int`    | Number of events                     | Ignored for free run mode  |
| r         | `int`    | ROI mode: `1=on`, `0=off`            |                            |
| t         | `int`    | Transparent mode: `1=on`, `0=off`    |                            |
| m         | `int`    | `0=`free run, ext. trigger <br> `1=`finite run, ext. trigger <br> `2=`finite run, software trigger <br> `3=`finite run, software trigger random delays | Modes `2` and `3` useful for calibration  |
| s         | `int`    | Spike cleaner: `1=on`, `0=off`       |                            |


#include "Readout_struct.h" 
#include <stdint.h>
#include <stdio.h>

/*
DRS4 Software
*/

/*
Register
BASE + (Register Offset)
*/
  

void configure_drs(int roi_m, int transp_m, int ch9);

/*
Dtrigger register
*/

void set_start_dtrigger(drs4_reg *data, int enable);

uint32_t get_start_dtrigger(drs4_reg *data, int enable);

// run mode info
// 0 = free run (must be manually halted), ext. trigger
// 1 = finite sample run, ext. trigger
// 2 = finite sample run, software trigger
// 3 = finite sample run, software trigger, random delays/phase for timing calibration

void drs4_freerun_ext(drs4_reg *data, FILE *fp);
void drs4_finiterun_ext(drs4_reg *data, FILE *fp, int num_samp);
void drs4_eventrun_ext(drs4_reg *data, FILE *fp, int num_samp);
void drs4_timerun_ext(drs4_reg *data, FILE *fp, int duration);
void drs4_finiterun_soft(drs4_reg *data, FILE *fp, int num_samp);
void drs4_finiterun_soft_tcal(drs4_reg *data, FILE *fp, int num_samp);
 
/*
DRS4 DMODE 
 enable 1  = continuous mode
 enable 0  = single shot
*/
void set_drs4_dmode(drs4_reg *data,int enable);

/*
DRS4_STANDBY_MODE
 enable 1 = shutdown drs4
 enable 0 = run drs4 
*/
void set_standby_mode(drs4_reg *data, int enable);

/*
DRS4_TRANSPARENT
 enable 1 = set transparent
 enable 0 = disable transparent
*/
void set_drs4_transparent_mode(drs4_reg *data, int enable);

/*
DRS4_PLL_LOCK
This function returns if pll is locked for Drs4
*/
uint32_t get_drs4_pll_lock(drs4_reg *data);

/*
DRS4_CHANNEL_CONFIG
\n # of chn | # of cells per ch | bit pattern
                        \n 8        | 1024              | 11111111b
                        \n 4        | 2048              | 01010101b
                        \n 2        | 4096              | 00010001b
                        \n 1        | 8192              | 00000001b

*/
void set_drs4_channel_config(drs4_reg *data, uint32_t bitPattern);


uint32_t get_drs4_channel_config(drs4_reg *data);

/*
DRS4_DTAP_HIGH_CNTS
*/
uint32_t get_drs4_dtap_high_cnts(drs4_reg *data);

/*
DRS4_DTAP_LOW_CNTS
*/
uint32_t get_drs4_dtap_low_cnts(drs4_reg *data);

/*
DRS4_ROI_MODE
*/
void set_drs4_roi(drs4_reg *data, uint32_t enable);
/*
DRS4_BUSY
DRS is doing a readout.
*/
uint32_t get_drs4_busy(drs4_reg *data);
/*
DRS4_ADC_LATENCY
*/
void set_drs4_adc_latency(drs4_reg *data,uint32_t value);


uint32_t get_drs4_adc_latency(drs4_reg *data);
/*
SAMPLE_COUNT
*/
void set_drs4_sample_count(drs4_reg *data,uint32_t value);

uint32_t get_drs4_sample_count(drs4_reg *data);

/*
READOUT MASK
*/
void set_readout_mask(drs4_reg *data,uint32_t value);


uint32_t get_readout_mask(drs4_reg *data);

/*
DRS4_START
*/
void set_drs4_start(drs4_reg *data, int value);

/*
DRS4_REINIT 
restores state machine to idle
*/
void set_drs4_reinit(drs4_reg *data, int value);

/*
DRS4_CONFIGURE
*/
void set_drs4_configure(drs4_reg *data, int value);

/*
DRS4_RESET
*/
void set_drs4_reset(drs4_reg *data,int value);

////////////////////////////////////////////////////////////////////////////////////

/*
FPGA_DNA
*/
uint64_t get_fpga_dna(drs4_reg *data);

/*
Set spike clean mode
*/
void set_drs4_spike(drs4_reg *data, int value);

/*
Set trigger mode
*/
void set_drs4_trigger( drs4_reg *data, int value);


#ifndef MEMORY_H
#define MEMORY_H
#include <stdint.h>
 

void write_reg(uint32_t address,uint32_t data);
uint32_t read_reg(uint32_t address);

 
#endif
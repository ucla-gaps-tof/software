#!/bin/bash -x
  
# Setup the readout board components
now=`/bin/date +%Y%m%d.%H%M%S`
home_dir=/home/gaps
log_file=$home_dir/logs/setup_${now}
#/usr/bin/rbsetup > ${logfile} 2>&1 
/home/gaps/bin/rbsetup

# Now program the fpga
#/usr/bin/prog_fpga.sh > ${logfile} 2>&1
sudo /home/gaps/bin/prog_fpga.sh
/bin/sleep 10

# Now setup for external data running
sudo devmem 0x80000104 32 1

# Now use event counter board
sudo devmem 0x80000800 32 1
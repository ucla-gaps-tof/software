#!/bin/bash 
  
args=$*
nargs=$#

echo $nargs $args

# Set some useful variables
home_dir=/home/gaps
prog_dir=$home_dir/bin
log_dir=$home_dir/logs
date_str=`/bin/date +%Y%m%d.%H%M%S`
filename=$log_dir/$date_str".txt"


# First, we need to see if GFP_daq is currently running. If so, we
# exit gracefully without staring a new run
daq_status=`/bin/pidof GFP_daq`

if [ ! -z $daq_status ]; then
    echo Found GFP_daq process with PID of $daq_status
    exit
else
    /usr/bin/touch $filename
    cd $prog_dir
    ./GFP_daq $args &
fi

exit
# DRS4 Readout Board Software

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/ucla-gaps-tof/software/develop)
![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.0-4baaaa.svg)

## Organization

<!-- TODO: deprecate mem_read? -->
```text
  software/
    ├── analysis           : Unpacking, calibration, plotting routines (Python)
    ├── daq                : Data acquisition (Python)
    ├── drs4_ctrl          : DRS4 control/readout (C)
    ├── gfp                : 
    ├── i2c_ctrl           : I2C peripheral control (C)
    ├── net                : Server/client code for data transfer (Python)
    ├── si5395             : Project files and hex dumps for clock synth (C)
    └── util               : Helper programs for reconfiguring the board
```

## Documentation

Refer to the `README` in the various folders for program specific instructions on usage, and also required dependencies, etc.

* [Analysis](analysis/README.md)
* [DRS4 Control](drs4_ctrl/README.md)
* [I2C Control](i2c_ctrl/README.md)
* [Si5395 Clock Synth](si5395/README.md)
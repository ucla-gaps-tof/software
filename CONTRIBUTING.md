# TOC

[[_TOC_]]

# Introduction
Policies, guidelines and general info about how the project is run and steps to contribute.

# Code of conduct
See [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md)

# Merge policy
AKA "PR" if you are joining from GitHub land.

`master` and `develop` are protected: they can only be modified via merge request. The process would be

1. Branch off `develop`
2. Hack hack hack
3. Create merge request: `your_stuff` into `develop`

# Style guide and formatting

The codebase makes use of a wide variety of tools: Python, C, bash, etc. A formalized guide that handles all these languages would take too much time to put together.

## Indent

Please use two spaces

## Flow control

Braces/brackets on new line

```
if (/* condition */)
{
  /* code */
}
```

## Comments and documentation

Comment as much as possible, even if you assume something to be obvious.

Make use of language built in docstrings if available.

Eventually would be nice to get doxygen up and running, so if you want to help with that, would be a great contrib!

## Tags, issues and work

Please follow <https://www.python.org/dev/peps/pep-0350/#mnemonics> for all languages.


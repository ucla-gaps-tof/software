#!/bin/bash

sw_dir=/home/gaps/software

# Takes argument of the specific firmware version from ../config.ini

firmware=$(python3 << EOI
import configparser
cfg = configparser.ConfigParser()
cfg.read("/home/gaps/software/config.ini")
print(cfg['tof-gfp-rb']['firmware'])
EOI
)

sudo echo 0 > /sys/class/fpga_manager/fpga0/flags
sudo cp $sw_dir/fpga_fw/$firmware /lib/firmware
sudo echo $firmware > /sys/class/fpga_manager/fpga0/firmware